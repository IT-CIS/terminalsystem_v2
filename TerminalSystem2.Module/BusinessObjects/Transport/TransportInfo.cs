﻿using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;

namespace TerminalSystem2.Transport
{
    [ModelDefault("Caption", "Информация по транспорту")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class TransportInfo : BaseObjectXAF
    {
        public TransportInfo(Session session) : base(session) { }

        [Size(255), DisplayName("Транспорт")]
        public string Name
        {
            get {
                return String.Format("{0} № {1}", TransportType.Name, CarNumber); }
        }

        private string i_CarNumber;
        [Size(32), DisplayName("Номер ТС")]
        public string CarNumber
        {
            get { return i_CarNumber; }
            set { SetPropertyValue("CarNumber", ref i_CarNumber, value); }
        }

        private dTransportType i_TransportType;
        [DisplayName("Тип ТС")]
        public dTransportType TransportType
        {
            get { return i_TransportType; }
            set { SetPropertyValue("TransportType", ref i_TransportType, value); }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
        }
    }
}
