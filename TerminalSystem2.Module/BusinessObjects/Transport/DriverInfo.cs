﻿using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;

namespace TerminalSystem2.Transport
{
    [ModelDefault("Caption", "Информация по водителю")]
    [System.ComponentModel.DefaultProperty("BrifName")]
    public class DriverInfo : BaseObjectXAF
    {
        public DriverInfo(Session session) : base(session) { }

        private string i_BrifName;
        [Size(255), DisplayName("ФИО водителя")]
        public string BrifName
        {
            get { return i_BrifName; }
            set { SetPropertyValue("BrifName", ref i_BrifName, value); }
        }

        private string i_PhoneNumber;
        [Size(32), DisplayName("Номер телефона")]
        public string PhoneNumber
        {
            get { return i_PhoneNumber; }
            set { SetPropertyValue("PhoneNumber", ref i_PhoneNumber, value); }
        }

        private string i_CarNumber;
        [Size(32), DisplayName("Номер ТС")]
        public string CarNumber
        {
            get { return i_CarNumber; }
            set { SetPropertyValue("CarNumber", ref i_CarNumber, value); }
        }

        private dTransportType i_TransportType;
        [DisplayName("Тип ТС")]
        public dTransportType TransportType
        {
            get { return i_TransportType; }
            set { SetPropertyValue("TransportType", ref i_TransportType, value); }
        }

        //private TransportInfo i_TransportInfo;
        //[DisplayName("Транспортное средство")]
        //public TransportInfo TransportInfo
        //{
        //    get { return i_TransportInfo; }
        //    set { SetPropertyValue("TransportInfo", ref i_TransportInfo, value); }
        //}


    }
}
