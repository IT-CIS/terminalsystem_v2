﻿
using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSystem2.SystemDir
{
    public static class Messanger
    {
        public static void ShowMessage(XafApplication application, string caption, string message, InformationType informationType)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = message;
            options.Type = informationType;
            options.Web.Position = InformationPosition.Bottom;
            options.Win.Caption = caption;
            options.Win.Type = WinMessageType.Alert;

            application.ShowViewStrategy.ShowMessage(options);
        }
    }
}
