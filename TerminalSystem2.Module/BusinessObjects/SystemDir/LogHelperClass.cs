﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSystem2.SystemDir
{
    public class LogHelperClass
    {
        private static object sync = new object();
        public static string WriteErr(Exception ex, string message)
        {
            string fullText = "";
            try
            {
                // Путь .\\Log
                //var doc = XDocument.Load(Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, @"bin\DBconfig.xml"));
                string pathToLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");
                if (!Directory.Exists(pathToLog))
                    Directory.CreateDirectory(pathToLog); // Создаем директорию, если нужно
                string filename = Path.Combine(pathToLog, string.Format("{0}_{1:dd.MM.yyy}_TerminalServicesError.log",
                "Log", DateTime.Now));
                fullText = string.Format("[{0:dd.MM.yyy HH:mm:ss.fff}] [{1}.{2}()] {3}, объект: {4}\r\n",
                DateTime.Now, ex.TargetSite.DeclaringType, ex.TargetSite.Name, ex.Message, message);
                lock (sync)
                {
                    File.AppendAllText(filename, fullText, Encoding.GetEncoding("Windows-1251"));
                }
            }
            catch
            {
                // Перехватываем все и ничего не делаем
            }
            return fullText;
        }
        public static string WriteText(string message)
        {
            string fullText = "";
            try
            {
                // Путь .\\Log
                string pathToLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");
                if (!Directory.Exists(pathToLog))
                    Directory.CreateDirectory(pathToLog); // Создаем директорию, если нужно
                string filename = Path.Combine(pathToLog, string.Format("{0}_{1:dd.MM.yyy}_TerminalServices.log",
                "Log", DateTime.Now));
                fullText = string.Format("[{0:dd.MM.yyy HH:mm:ss.fff}] сообщение: {1}\r\n",
                DateTime.Now, message);
                lock (sync)
                {
                    File.AppendAllText(filename, fullText, Encoding.GetEncoding("Windows-1251"));
                }
            }
            catch
            {
                // Перехватываем все и ничего не делаем
            }
            return fullText;
        }
    }
}
