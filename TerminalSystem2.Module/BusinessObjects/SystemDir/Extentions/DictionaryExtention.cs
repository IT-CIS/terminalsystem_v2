﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem.DocFlow;

namespace TerminalSystem1.Module.BusinessObjects.SystemDir.Extentions
{
    public static class DictionaryExtention
    {
        public static void AddStage(this Dictionary<string, IRequestBaseStage> dictionary, IRequestBaseStage _IRequestBaseStage)
        {
            dictionary.Add(_IRequestBaseStage.StageName, _IRequestBaseStage);
        }
    }
}
