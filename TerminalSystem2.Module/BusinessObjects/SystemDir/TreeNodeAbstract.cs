﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;


namespace TerminalSystem2.SystemDir
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TreeNodeAbstract : BaseObjectXAF, ITreeNode
    {
        public TreeNodeAbstract(Session session) : base(session) { }

        private string name;
        protected abstract ITreeNode Parent {
            get;
        }
        protected abstract System.ComponentModel.IBindingList Children {
            get;
        }

        [Size(255), DisplayName("Наименование")]
        public string Name {
            get {
                return name;
            }
            set {
                SetPropertyValue("Name", ref name, value);
            }
        }
        private string i_Notes;
        [Size(255), DisplayName("Описание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }
        #region ITreeNode
        System.ComponentModel.IBindingList ITreeNode.Children {
            get {
                return Children;
            }
        }
        string ITreeNode.Name {
            get {
                return Name;
            }
        }

        ITreeNode ITreeNode.Parent {
            get {
                return Parent;
            }
        }
        #endregion
       
    }
}

