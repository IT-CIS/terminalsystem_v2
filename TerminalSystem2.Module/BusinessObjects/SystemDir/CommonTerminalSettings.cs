﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;

namespace TerminalSystem2.SystemDir
{
    /// <summary>
    /// Общие настройки
    /// </summary>
    [ModelDefault("Caption", "Общие настройки системы Терминал"), NavigationItem("Настройки")]
    [System.ComponentModel.DefaultProperty("Host")]
    public class CommonTerminalSettings : BaseObjectXAF
    {
        public CommonTerminalSettings(Session session) : base(session) { }

        private string i_Host;
        [Size(100), DisplayName("Хост")]
        public string Host
        {
            get { return i_Host; }
            set { SetPropertyValue("Host", ref i_Host, value); }
        }

        private int i_Port;
        [DisplayName("Порт")]
        public int Port
        {
            get { return i_Port; }
            set { SetPropertyValue("Port", ref i_Port, value); }
        }

        private string i_Login;
        [Size(255), DisplayName("Логин")]
        public string Login
        {
            get { return i_Login; }
            set { SetPropertyValue("Login", ref i_Login, value); }
        }
        private string i_Pass;
        [Size(255), DisplayName("Пароль")]
        public string Pass
        {
            get { return i_Pass; }
            set { SetPropertyValue("Pass", ref i_Pass, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        
    }
}

