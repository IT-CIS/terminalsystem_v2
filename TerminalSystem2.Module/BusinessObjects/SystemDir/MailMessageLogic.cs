﻿using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TerminalSystem2.OrgStructure;

namespace TerminalSystem2.SystemDir
{
    public class MailMessageLogic
    {
        //для добавления настройки просто добавьте свойство в класс (должно быть открытым статическим)
        //тип данных св-ва любой, приведение автоматическое (если возможно)
        //public static string host = "smtp.yandex.ru";
        //public static int port = 587;
        //public static string login = "donotreply@it-cis.ru";
        //public static string pass = "Cfv217Ceyu";
        public static string sender = "";
        public static string host = "";
        public static int port = 587;
        public static string login = "";
        public static string pass = "";
        public static string from = "";
        public static bool ssl = true;
        static readonly char[] mailSeparators = new char[] { ',', ';' };
        private static void GetMailSettings(Connect connect)
        {
            TerminalOrg terminalOrg = connect.FindFirstObject<TerminalOrg>(x=> true);
            if(terminalOrg != null)
            {
                sender = terminalOrg?.Name ?? "";
                host = terminalOrg?.Smtp ?? "";
                if (terminalOrg.SmtpPort != 0)
                    port = terminalOrg.SmtpPort;
                login = terminalOrg?.Account ?? "";
                pass = terminalOrg?.Password ?? "";
                from = terminalOrg?.Account ?? "";
                ssl = terminalOrg.IsSSL;
            }
        }
        static Regex EmailRegex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled);
        public static bool IsEmailValid(string email)
        {
            bool isValid = false;
            if (!String.IsNullOrEmpty(email))
            {
                if (email.Contains(";") || email.Contains(","))
                {
                    foreach (var item in email.Split(mailSeparators))
                    {
                        if (item.Trim().Contains(" "))
                            throw new Exception($"Ошибка в указании почты - {email}");
                    }
                }
                else if (email.Trim().Contains(" "))
                    throw new Exception($"Ошибка в указании почты - {email}");
                try
                {
                    isValid = EmailRegex.IsMatch(email);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Ошибка в указании почты - {email}");
                }
            }

            return isValid;
        }

        public static void SendMailMessage(Connect connect, string title, string bodyMessage, string fileAttachmentNames,
            string too, string bcc = "")
        {
            if (string.IsNullOrEmpty(too))
                return;
            GetMailSettings(connect);

            var message = new MimeMessage();
            //message.From.Add(new MailboxAddress(sender ?? login, login));
            message.From.Add(new MailboxAddress(sender ?? login, login)); 
            message.Subject = title ?? "";

            var builder = new BodyBuilder();
            builder.HtmlBody = bodyMessage;

            //message.Body = new TextPart("plain")
            //{
            //    Text = bodyMessage
            //};

            foreach (string to in too.Split(mailSeparators))
            {
                try
                {
                    if (IsEmailValid(to.Trim()))
                        message.To.Add(new MailboxAddress(to.Trim(), to.Trim()));
                }
                catch { }
            }
            if (string.IsNullOrEmpty(bcc))
            {
                foreach (string bc in bcc.Split(mailSeparators))
                {
                    try
                    {
                        if (IsEmailValid(bc.Trim()))
                            message.Bcc.Add(new MailboxAddress(bc.Trim(), bc.Trim()));
                    }
                    catch { }
                }
            }

            if (fileAttachmentNames != "")
            {
                foreach (string fileAttachmentName in fileAttachmentNames.Split(','))
                {
                    builder.Attachments.Add(fileAttachmentName);
                    //using (MemoryStream ms = new FileStream())
                    //{
                    //    attach.File.SaveToStream(ms);
                    //    ms.Seek(0, System.IO.SeekOrigin.Begin);
                    //    builder.Attachments.Add(attach.File.FileName, ms);
                    //}
                }
            }
            message.Body = builder.ToMessageBody();

            using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
            {
                //emailClient.ConnectAsync("smtp.office365.com", 587, SecureSocketOptions.StartTls);
                //emailClient.AuthenticateAsync(login, pass);
                //emailClient.SendAsync(message);
                //emailClient.DisconnectAsync(true);
                //ServicePointManager.Expect100Continue = true;
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //emailClient.ServerCertificateValidationCallback = (s, c, h, e) => true;
                //emailClient.Connect("smtp.office365.com", 587, SecureSocketOptions.Auto);

                //emailClient.SslProtocols = SslProtocols.Ssl3 | SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;
                //emailClient.Connect("smtp.office365.com", 465, true);

                //emailClient.CheckCertificateRevocation = false;
                //emailClient.Connect(host, 465, SecureSocketOptions.Auto);


                //emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                //var clientAuthenticationMechanisms = emailClient.AuthenticationMechanisms;
                //emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                //emailClient.Authenticate(login, "ATAfSATA1");
                //emailClient.Authenticate(login, "bfjldzhrrzgfhvbg");
                try
                {
                    emailClient.Connect(host, port, SecureSocketOptions.StartTls);
                    emailClient.Authenticate(login, pass);

                    emailClient.Send(message);
                    emailClient.Disconnect(true);
                }
                catch (Exception ex)
                {
                    LogHelperClass.WriteErr(ex, $"Ошибка отправки письма на почту: {too}. title = {title}");
                    throw new Exception($"Ошибка отправки письма. Message: {ex.Message}, StackTrace: {ex.StackTrace}, InnerException: {ex.InnerException}");
                }

                //emailClient.Connect(host, port, ssl);//MailKit.Security.SecureSocketOptions.StartTls);
                //emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                //emailClient.Authenticate(login, pass);
                //try
                //{
                //    emailClient.Send(message);
                //}
                //catch (Exception ex)
                //{
                //    LogHelperClass.WriteErr(ex, $"Ошибка отправки письма на почту: {too}. title = {title}");
                //    //throw new Exception($"Ошибка отправки письма. Message: {ex.Message}, StackTrace: {ex.StackTrace}, InnerException: {ex.InnerException}");
                //}
                //emailClient.Disconnect(true);
            }
            //ShowMessage("", "Письмо успешно отправлено", InformationType.Success);
        }
        public static void SendMailMessage1(Connect connect, string title, string bodyMessage, string fileAttachmentNames,
            string too, string bcc = "")
        {
            if (string.IsNullOrEmpty(too))
                return;
            GetMailSettings(connect);
            using (SmtpClient client = new SmtpClient()
            {
                Host = host,
                Port = port,
                UseDefaultCredentials = false, // This require to be before setting Credentials property
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(login, pass, "flyway-cargo.com"), // you must give a full email address for authentication 
                TargetName = "STARTTLS/smtp.office365.com", // Set to avoid MustIssueStartTlsFirst exception
                EnableSsl = ssl // Set to avoid secure connection exception
            })
            {

                MailMessage message = new MailMessage()
                {
                    From = new MailAddress(from), // sender must be a full email address
                    Subject = title,
                    IsBodyHtml = true,
                    Body = bodyMessage,
                    BodyEncoding = System.Text.Encoding.UTF8,
                    SubjectEncoding = System.Text.Encoding.UTF8,
                };
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                char[] seps = { ',', ';' };
                foreach (string to in too.Split(seps))
                {
                    try
                    {
                        message.To.Add(to.Trim());
                    }
                    catch { }
                }
                try { message.Bcc.Add(bcc); }
                catch { }
                if (fileAttachmentNames != "")
                {
                    foreach (string fileAttachmentName in fileAttachmentNames.Split(','))
                    {
                        System.Net.Mail.Attachment attachment;
                        attachment = new System.Net.Mail.Attachment(fileAttachmentName);
                        message.Attachments.Add(attachment);
                    }
                }

                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    LogHelperClass.WriteErr(ex, $"Ошибка отправки на почту: {too}. title = {title}");
                }
            }
        }

        //public static void SendMailMessage(string title, string bodyMessage, string fileAttachmentNames,  
        //    string too, string from, string bcc = "")
        //    public static void SendMailMessage(Connect connect, string title, string bodyMessage, string fileAttachmentNames,
        //    string too, string bcc = "")
        //{
        //    try
        //    {
        //        if (too != null && too != "")
        //        {
        //            GetMailSettings(connect);
        //            SmtpClient client = new SmtpClient();
        //            client.Port = port;
        //            client.Host = host;
        //            client.EnableSsl = ssl;
        //            client.Timeout = 10000;
        //            //client.
        //            client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //            client.UseDefaultCredentials = false;
        //            client.Credentials = new System.Net.NetworkCredential(login, pass);

        //            ServicePointManager.Expect100Continue = true;
        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //            MailMessage mm = new MailMessage();
        //            mm.From = new MailAddress(from);
        //            char[] seps = { ',',';'};
        //            foreach (string to in too.Split(seps))
        //            {
        //                try
        //                {
        //                    mm.To.Add(to.Trim());
        //                }
        //                catch { }
        //            }
        //            try { mm.Bcc.Add(bcc); }
        //            catch { }
        //            //try { mm.Bcc.Add("donotreply@it-cis.ru"); }
        //            //catch { }
        //            //try { mm.Bcc.Add(from); }
        //            //catch { }
        //            mm.Subject = title;
        //            mm.Body = bodyMessage;
        //            if (fileAttachmentNames != "")
        //            {
        //                foreach (string fileAttachmentName in fileAttachmentNames.Split(','))
        //                {
        //                    System.Net.Mail.Attachment attachment;
        //                    attachment = new System.Net.Mail.Attachment(fileAttachmentName);
        //                    mm.Attachments.Add(attachment);
        //                }
        //            }
        //            mm.BodyEncoding = UTF8Encoding.UTF8;
        //            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

        //            // УБРАНО ДЛЯ ДЕМО
        //            client.Send(mm);
        //            try { LogHelperClass.WriteText($"Письмо отправлено на email: {too}. title = {title}"); }
        //            catch { }
        //        }
        //    }
        //    catch(Exception ex) {
        //        LogHelperClass.WriteErr(ex, $"Ошибка отправки email: {too}. title = {title}");
        //    }
        //    //MailMessage mail = new MailMessage();
        //    //SmtpClient SmtpServer = new SmtpClient("smtp.yandex.ru");
        //    //mail.From = new MailAddress("dmitry@it-cis.ru");
        //    //mail.To.Add("info@it-cis.ru");
        //    //mail.Subject = "Test Mail - 1";
        //    //mail.Body = "mail with attachment";

        //    //System.Net.Mail.Attachment attachment;
        //    //attachment = new System.Net.Mail.Attachment(fileAttachmentName);
        //    //mail.Attachments.Add(attachment);

        //    //SmtpServer.Port = 587;
        //    //SmtpServer.Credentials = new System.Net.NetworkCredential("dmitry@it-cis.ru", "");
        //    //SmtpServer.EnableSsl = true;
        //    ////SmtpServer.Timeout = 100000;
        //    //SmtpServer.Send(mail);
        //}

        //public CommonTerminalSettings GetMailSettingsFromTerminal(Terminal terminal)
        //{
        //    CommonTerminalSettings mailSettings = null;
        //    try
        //    {
        //        TerminalOrg terminalOrg = terminal.TerminalOrg;
        //        //mailSettings = terminalOrg.EmailSettings;
        //    }
        //    catch(Exception ex) 
        //    {
        //        throw new Exception(String.Format("Возникла ошибка при определении настроек для отправки email: {0}", ex.Message));
        //    }
        //    return mailSettings;
        //}

        //public CommonTerminalSettings GetMailSettingsFromTerminalEmployee(Employee employee)
        //{
        //    CommonTerminalSettings mailSettings = null;
        //    try
        //    {
        //        TerminalOrg terminalOrg = employee.Department.TerminalOrg;
        //        //mailSettings = terminalOrg.EmailSettings;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(String.Format("Возникла ошибка при определении настроек для отправки email: {0}", ex.Message));
        //    }
        //    return mailSettings;
        //}

    }
}
