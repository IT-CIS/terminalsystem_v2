﻿using System;
using DevExpress.ExpressApp.Win;
using DevExpress.ExpressApp.Web;
using DevExpress.XtraEditors;

namespace DevExpress.ExpressApp
{
    /// <summary>
    /// A message box which works both in WinForms AND in Web.
    /// </summary>
    public static class XafMessageBox
    {
        public static void ShowMessageBox(this XafApplication xafApplication, string message)
        {
            if (xafApplication is WinApplication)
            {
                XtraMessageBox.Show(message);
            }

            if (xafApplication is WebApplication)
            {
                WebWindow.CurrentRequestWindow.RegisterClientScript("XafMessageBox", "alert('" + message + "');");
            }
        }
        //public static void ShowASPxMessageBox(this XafApplication xafApplication, string message)
        //{
        //    WebWindow.CurrentRequestWindow.RegisterStartupScript("showPopup", "ASPxMessageBox.Show();");
        //}
    }
}
