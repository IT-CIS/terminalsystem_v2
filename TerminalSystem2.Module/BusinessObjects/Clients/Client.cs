﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Containers;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Enums;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;
//using TerminalSystem2.DocFlow;
//using TerminalSystem2.Enums;
//using TerminalSystem2.OrgStructure;
//using TerminalSystem2.SystemDir;
//using TerminalSystem2.Tarification;
//using TerminalSystem2.Terminals;

namespace TerminalSystem2.Clients
{
    /// <summary>
    /// Клиент
    /// </summary>
    [ModelDefault("Caption", "Клиент"), NavigationItem("Клиенты")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class Client : TerminalSystem2.OrgStructure.Organization
    {
        public Client(Session session) : base(session) { }


        private ETrafficRepotKind i_TrafficRepotKind;
        [DisplayName("Вид отчета по движению контейнеров")]
        public ETrafficRepotKind TrafficRepotKind
        {
            get { return i_TrafficRepotKind; }
            set { SetPropertyValue("TrafficRepotKind", ref i_TrafficRepotKind, value); }
        }


        [Association, DisplayName("Контейнеры клиента")]
        public XPCollection<Container> Containers
        {
            get { return GetCollection<Container>("Containers"); }
        }
        [Association, DisplayName("Стоки клиента")]
        public XPCollection<Stock> Stocks
        {
            get { return GetCollection<Stock>("Stocks"); }
        }
        [Association, DisplayName("Сотрудники")]
        public XPCollection<ClientEmployee> Employees
        {
            get { return GetCollection<ClientEmployee>("Employees"); }
        }
        ////[Association, DisplayName("Тарифы на оказание услуг")]
        ////public XPCollection<Tariff> Tariffs
        ////{
        ////    get { return GetCollection<Tariff>("Tariffs"); }
        ////}

        [Association, DisplayName("Договоры")]
        public XPCollection<Contract> Contracts
        {
            get { return GetCollection<Contract>("Contracts"); }
        }

        [Association, DisplayName("Предоставленные услуги")]
        public XPCollection<ServiceDone> ServicesDone
        {
            get { return GetCollection<ServiceDone>("ServicesDone"); }
        }
        [Association, DisplayName("Заявки")]
        public XPCollection<RequestBase> Requests
        {
            get { return GetCollection<RequestBase>("Requests"); }
        }
        [Association, DisplayName("Отчеты")]
        public XPCollection<ServiceReport> ServiceReports
        {
            get { return GetCollection<ServiceReport>("ServiceReports"); }
        }
        //[Association, DisplayName("Клиент терминалов")]
        //public XPCollection<Terminal> Terminals
        //{
        //    get { return GetCollection<Terminal>("Terminals"); }
        //}
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //Connect connect = Connect.FromSession(Session);
            //if(connect.FindObjects<ClientEmployee>(x => true).Count<ClientEmployee>() + 1 > 1)
            //{
            //    throw new Exception("В тестовой версии возможно создание только одного клиента!");
            //}
            //try
            //{
            //    ServiceBase servBase = connect.FindFirstObject<ServiceBase>(mc => mc.Oid != null);
            //    foreach (ServiceRate servRate in servBase.ServiceRates)
            //    {
            //        ServiceRate serviceRate = connect.CreateObject<ServiceRate>();
            //        serviceRate.Value = servRate.Value;
            //        //serviceRate.ServiceType = servRate.ServiceType;
            //        serviceRate.Service = servRate.Service;
            //        serviceRate.Terminal = servRate.Terminal;
            //        serviceRate.Currency = servRate.Currency;
            //        serviceRate.Description = serviceRate.Description;
            //        this.ServiceRates.Add(serviceRate);
            //    }
            //}
            //catch { }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            Connect connect = Connect.FromSession(Session);
            //try
            //{
            //    foreach (Terminal terminal in Terminals)
            //    {
            //        TariffBase tariffBase = connect.FindFirstObject<TariffBase>(mc => mc.Terminal == terminal);
            //        foreach (Tariff tariff in tariffBase.Tariffs)
            //        {
            //            Tariff clientTariff = connect.CreateObject<Tariff>();
            //            clientTariff.Description = tariff.Description;
            //            clientTariff.TariffService = tariff.TariffService;
            //            clientTariff.UnitKind = tariff.UnitKind;
            //            clientTariff.Value20Empty = tariff.Value20Empty;
            //            clientTariff.Value20Laden = tariff.Value20Laden;
            //            clientTariff.Value40Empty = tariff.Value40Empty;
            //            clientTariff.Value40Laden = tariff.Value40Laden;
            //            clientTariff.Currency = tariff.Currency;

            //            clientTariff.Terminal = tariff.Terminal;

            //            this.Tariffs.Add(clientTariff);
            //            clientTariff.Save();
            //        }
            //    }
            //}
            //catch { }


        }


    }
}

