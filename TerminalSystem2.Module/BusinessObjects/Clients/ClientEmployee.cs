﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Clients
{
    /// <summary>
    /// Клиент
    /// </summary>
    [ModelDefault("Caption", "Сотрудник клиента"), NavigationItem("Клиенты")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class ClientEmployee : EmployeeBase
    {
        public ClientEmployee(Session session) : base(session) { }

        private bool i_isDirector;
        [DisplayName("Руководитель")]
        public bool isDirector
        {
            get { return i_isDirector; }
            set { SetPropertyValue("isDirector", ref i_isDirector, value); }
        }

        private Client i_Client;
        [Association, DisplayName("Клиент")]
        public Client Client
        {
            get { return i_Client; }
            set { SetPropertyValue("Client", ref i_Client, value); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Connect connect = Connect.FromSession(Session);
            if(this.SysUser == null)
            {
                //int nom = connect.FindObjects<ClientEmployee>(x => true).Count<ClientEmployee>()+1;
                //string userName = "ClientUser" + nom;
                //PermissionPolicyUser user = connect.FindFirstObject<PermissionPolicyUser>(x => x.UserName == userName);
                //if(user == null)
                //{
                //    user = connect.CreateObject<PermissionPolicyUser>();
                //    user.UserName = userName;
                //    user.SetPassword(userName);
                //    user.Save();
                //    PermissionPolicyRole clientRole = connect.FindFirstObject<PermissionPolicyRole>(x=> x.Name == "Сотрудник клиента");
                //    var clientRoles = connect.FindObjects<PermissionPolicyRole>(x=> true);
                //    user.Roles.Add(clientRole);
                //    user.Save();
                //}
                string userName = "ClientUser1";
                this.SysUser = connect.FindFirstObject<PermissionPolicyUser>(x => x.UserName == userName);
            }
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }
    }
}

