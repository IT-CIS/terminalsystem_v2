﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.Clients;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.BaseClasses
{
    public class ServiceReport : FileAttachmentBase
    {
        public ServiceReport(Session session) : base(session) { }

        private string i_Name;
        private DateTime i_RegDate;
        private Employee i_Registrator;
        private string i_Description;
        private string i_Sha256;

        private Stock i_Stock;
        [Association, DisplayName("Сток"), System.ComponentModel.Browsable(false)]
        public Stock Stock
        {
            get { return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value); }
        }
        private Client i_Client;
        [Association, DisplayName("Клиент"), System.ComponentModel.Browsable(false)]
        public Client Client
        {
            get { return i_Client; }
            set { SetPropertyValue("Client", ref i_Client, value); }
        }
        [Size(255), DevExpress.Xpo.DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        [DevExpress.Xpo.DisplayName("Дата отчета")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        //[Size(SizeAttribute.Unlimited), System.ComponentModel.Browsable(false)]
        //public string Sha256
        //{
        //    get { return i_Sha256; }
        //    set { SetPropertyValue("Sha256", ref i_Sha256, value); }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            i_RegDate = DateTime.Now.Date;
        }

    }
}
