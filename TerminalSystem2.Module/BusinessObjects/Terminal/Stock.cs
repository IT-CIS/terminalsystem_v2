﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
//using TerminalSystem2.DocFlow;
//using TerminalSystem2.Enums;

namespace TerminalSystem2.Terminals
{
    [ModelDefault("Caption", "Сток"), NavigationItem("Терминал")]
    [System.ComponentModel.DefaultProperty("Number")]
    public class Stock : BaseObjectXAF
    {
        public Stock(Session session) : base(session) { }

        private string i_Name;
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        private string i_Location;
        [Size(255), DisplayName("Местоположение")]
        public string Location
        {
            get { return i_Location; }
            set { SetPropertyValue("Location", ref i_Location, value); }
        }
        private Terminal i_Terminal;
        [Association, DisplayName("Терминал")]
        //[DataSourceProperty("AvailableTerminals")]
        public Terminal Terminal
        {
            get { return i_Terminal; }
            set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        }
        //----------------------------------------------------
        // Получаем возможные для выбора терминалы, которые указаны у клиента, при условии, что это не терминальный сток
        //private XPCollection<Terminal> availableTerminals;
        //[System.ComponentModel.Browsable(false)] //
        //public XPCollection<Terminal> AvailableTerminals
        //{
        //    get
        //    {
        //        if (availableTerminals == null)
        //        {
        //            // Retrieve all Terminals objects 
        //            availableTerminals = new XPCollection<Terminal>(Session);
        //            // Filter the retrieved collection according to current conditions 
        //            RefreshAvailableTerminals();
        //        }
        //        return availableTerminals;
        //    }
        //}
        //private void RefreshAvailableTerminals()
        //{
        //    if (availableTerminals == null)
        //        return;
        //    if (!TerminalStockFlag)
        //    {
        //        if (Client != null)
        //        {
        //            availableTerminals = Client.Terminals;
        //        }
        //        else
        //            availableTerminals = new XPCollection<Terminal>(Session);
        //    }
        //    else
        //    {
        //        availableTerminals = new XPCollection<Terminal>(Session);
        //    }
            
        //    Terminal = null;
        //}
        

        private bool i_TerminalStockFlag;
        [DisplayName("Терминальный сток")]
        [ImmediatePostData]
        public bool TerminalStockFlag
        {
            get { return i_TerminalStockFlag; }
            set
            {
                SetPropertyValue("TerminalStockFlag", ref i_TerminalStockFlag, value);
                RefreshTerminalStockFlag();
            }
        }
        private void RefreshTerminalStockFlag()
        {
            if (TerminalStockFlag)
                Client = null;
        }
        private Client i_Client;
        [Association, DisplayName("Владелец")]
        [ImmediatePostData]
        public Client Client
        {
            get { return i_Client; }
            set
            {
                SetPropertyValue("Client", ref i_Client, value);
                //RefreshAvailableTerminals();
            }
        }

        [Association, DisplayName("Контейнеры в стоке")]
        public XPCollection<Container> Containers
        {
            get { return GetCollection<Container>("Containers"); }
        }
        [Association, DisplayName("Движение контейнеров в стоке")]
        public XPCollection<ContainerHistory> ContainerHistorys
        {
            get { return GetCollection<ContainerHistory>("ContainerHistorys"); }
        }
        [Association, DisplayName("Отчеты")]
        public XPCollection<ServiceReport> ServiceReports
        {
            get { return GetCollection<ServiceReport>("ServiceReports"); }
        }
        protected override void OnSaving()
        {
            base.OnSaving();

        }
    }
}
