﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Terminals
{
    [ModelDefault("Caption", "Секция")]
    [System.ComponentModel.DefaultProperty("Number")]
    public class TerminalSection : TreeNodeAbstract
    {
        public TerminalSection(Session session) : base(session) { }
        public TerminalSection(Session session, string name) : base(session) { this.Name = name; }

        private TerminalRow i_TerminalRow;
        [Association, DisplayName("Ряд")]
        [ImmediatePostData]
        public TerminalRow TerminalRow
        {
            get { return i_TerminalRow; }
            set { SetPropertyValue("TerminalRow", ref i_TerminalRow, value); }
        }
        private Terminal i_Terminal;
        [DisplayName("Терминал")]
        public Terminal Terminal
        {
            get {
                    if (TerminalRow != null)
                        if (TerminalRow.Terminal != null)
                            i_Terminal = TerminalRow.Terminal;
                return i_Terminal; }
            set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        }
        //private string i_Name;
        //[Size(255),DisplayName("Наименование")]
        //public string Name
        //{
        //    get { return i_Name; }
        //    set { SetPropertyValue("Name", ref i_Name, value); }
        //}

        //private string i_Location;
        //[Size(255), DisplayName("Местоположение")]
        //public string Location
        //{
        //    get { return i_Location; }
        //    set { SetPropertyValue("Location", ref i_Location, value); }
        //}
        
        [Association, DisplayName("Ярусы")]
        public XPCollection<TerminalTier> TerminalTiers
        {
            get { return GetCollection<TerminalTier>("TerminalTiers"); }
        }
        [Association, DisplayName("Контейнеры в секции")]
        public XPCollection<Container> Containers
        {
            get { return GetCollection<Container>("Containers"); }
        }
        protected override ITreeNode Parent
        {
            get
            {
                return TerminalRow;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return TerminalTiers;
            }
        }
    }
}
