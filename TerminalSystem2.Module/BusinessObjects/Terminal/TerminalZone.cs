﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Terminals
{
    [ModelDefault("Caption", "Зоны терминала")]
    [System.ComponentModel.DefaultProperty("Number")]
    public class TerminalZone : TreeNodeAbstract
    {
        public TerminalZone(Session session) : base(session) { }
        public TerminalZone(Session session, string name) : base(session) { this.Name = name; }

        private Terminal i_Terminal;
        [Association, DisplayName("Терминал")]
        public Terminal Terminal
        {
            get { return i_Terminal; }
            set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        }
        //private string i_Name;
        //[Size(255),DisplayName("Наименование")]
        //public string Name
        //{
        //    get { return i_Name; }
        //    set { SetPropertyValue("Name", ref i_Name, value); }
        //}

        //private string i_Location;
        //[Size(255), DisplayName("Описание")]
        //public string Location
        //{
        //    get { return i_Location; }
        //    set { SetPropertyValue("Location", ref i_Location, value); }
        //}

        [Association, DisplayName("Контейнеры в зоне")]
        public XPCollection<Container> Containers
        {
            get { return GetCollection<Container>("Containers"); }
        }
        [Association, DisplayName("Ряды зоны")]
        public XPCollection<TerminalRow> TerminalRows
        {
            get { return GetCollection<TerminalRow>("TerminalRows"); }
        }
        protected override ITreeNode Parent
        {
            get
            {
                return Terminal;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return TerminalRows;
            }
        }
    }
}
