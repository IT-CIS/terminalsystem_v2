﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Terminals
{
    [ModelDefault("Caption", "Ряд")]
    [System.ComponentModel.DefaultProperty("Number")]
    public class TerminalRow : TreeNodeAbstract
    {
        public TerminalRow(Session session) : base(session) { }
        public TerminalRow(Session session, string name) : base(session) { this.Name = name; }

        private TerminalZone i_TerminalZone;
        [Association, DisplayName("Зона терминала")]
        [ImmediatePostData]
        public TerminalZone TerminalZone
        {
            get { return i_TerminalZone; }
            set { SetPropertyValue("TerminalZone", ref i_TerminalZone, value); }
        }
        private Terminal i_Terminal;
        [DisplayName("Терминал")]
        public Terminal Terminal
        {
            get {
                //if (Terminal == null)
                    if (TerminalZone != null)
                        if (TerminalZone.Terminal != null)
                            i_Terminal = TerminalZone.Terminal;
                return i_Terminal; }
            set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        }
        //private string i_Name;
        //[Size(255),DisplayName("Наименование")]
        //public string Name
        //{
        //    get { return i_Name; }
        //    set { SetPropertyValue("Name", ref i_Name, value); }
        //}

        //private string i_Location;
        //[Size(255), DisplayName("Местоположение")]
        //public string Location
        //{
        //    get { return i_Location; }
        //    set { SetPropertyValue("Location", ref i_Location, value); }
        //}
        
        [Association, DisplayName("Секции")]
        public XPCollection<TerminalSection> TerminalSections
        {
            get { return GetCollection<TerminalSection>("TerminalSections"); }
        }
        [Association, DisplayName("Контейнеры в ряде")]
        public XPCollection<Container> Containers
        {
            get { return GetCollection<Container>("Containers"); }
        }
        protected override ITreeNode Parent
        {
            get
            {
                return TerminalZone;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return TerminalSections;
            }
        }
    }
}
