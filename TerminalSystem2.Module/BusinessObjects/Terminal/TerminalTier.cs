﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Terminals
{
    [ModelDefault("Caption", "Ярус")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class TerminalTier : TreeNodeAbstract
    {
        public TerminalTier(Session session) : base(session) { }
        public TerminalTier(Session session, string name) : base(session) { this.Name = name; }

        
        private Terminal i_Terminal;
        [DisplayName("Терминал")]
        public Terminal Terminal
        {
            get {
                if (TerminalSection != null)
                    if (TerminalSection.Terminal != null)
                        i_Terminal = TerminalSection.Terminal;
                return i_Terminal; }
            set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        }
        private TerminalSection i_TerminalSection;
        [Association, DisplayName("Секция")]
        //[ImmediatePostData]
        public TerminalSection TerminalSection
        {
            get { return i_TerminalSection; }
            set { SetPropertyValue("TerminalSection", ref i_TerminalSection, value);
                OnChanged();
                try
                {
                    if (Number == 0)
                        Number = TerminalSection.TerminalTiers.Count + 1;
                }
                catch { }
            }
        }

        private int i_Number;
        [DisplayName("Номер яруса")]
        public int Number
        {
            get { return i_Number; }
            set { SetPropertyValue("Number", ref i_Number, value); }
        }

        //private string i_Location;
        //[Size(255), DisplayName("Местоположение")]
        //public string Location
        //{
        //    get { return i_Location; }
        //    set { SetPropertyValue("Location", ref i_Location, value); }
        //}
        //private Container i_Container;
        //[DisplayName("Контейнер")]
        //public Container Container
        //{
        //    get { return i_Container; }
        //    set { SetPropertyValue("Container", ref i_Container, value); }
        //}
        [Association, DisplayName("Контейнеры в ярусе")]
        public XPCollection<Container> Containers
        {
            get { return GetCollection<Container>("Containers"); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //try
            //{
            //    Number = TerminalSection.TerminalTiers.Count + 1;
            //}
            //catch { }
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            if (Number == 0)
                Number = i_TerminalSection.TerminalTiers.Count + 1;
            try
            {
                if (this.Name == null || this.Name == String.Empty)
                {
                    string cont = "";
                    if (this.Containers.Count > 0)
                        cont = "[занят]";
                    else
                        cont = "[свободен]";
                    this.Name = String.Format("Терминал:{0}, Зона: {1}, Ряд:{2}, Секция:{3}, Ярус:{4}, {5}", Terminal.Name, TerminalSection.TerminalRow.TerminalZone.Name,
                        TerminalSection.TerminalRow.Name, TerminalSection.Name, Number, cont);
                }
            }
            catch { }
        }
        protected override ITreeNode Parent
        {
            get
            {
                return TerminalSection;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return new System.ComponentModel.BindingList<object>();
            }
        }
    }
}
