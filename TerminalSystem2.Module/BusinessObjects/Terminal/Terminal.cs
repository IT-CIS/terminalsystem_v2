﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.DocFlow;
//using TerminalSystem.DocFlow;
//using TerminalSystem.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
//using TerminalSystem.Tarification;
//using TerminalSystem.Transports;

namespace TerminalSystem2.Terminals
{
    [ModelDefault("Caption", "Терминал"), NavigationItem("Терминал")]
    [System.ComponentModel.DefaultProperty("Number")]
    public class Terminal : TreeNodeAbstract
    {
        public Terminal(Session session) : base(session) { }
        public Terminal(Session session, string name) : base(session) { this.Name = name; }

        private bool i_SimpleStructureFlag;
        [DisplayName("Простая структура")]
        public bool SimpleStructureFlag
        {
            get { return i_SimpleStructureFlag; }
            set { SetPropertyValue("SimpleStructureFlag", ref i_SimpleStructureFlag, value); }
        }

        private string i_Location;
        [Size(255), DisplayName("Местоположение")]
        public string Location
        {
            get { return i_Location; }
            set { SetPropertyValue("Location", ref i_Location, value); }
        }

        //private string i_Location;
        //[Size(255), DisplayName("Местоположение")]
        //public string Location
        //{
        //    get { return i_Location; }
        //    set { SetPropertyValue("Location", ref i_Location, value); }
        //}
        private TerminalOrg i_TerminalOrg;
        [Association, DisplayName("Владелец терминала")]
        public TerminalOrg TerminalOrg
        {
            get { return i_TerminalOrg; }
            set { SetPropertyValue("TerminalOrg", ref i_TerminalOrg, value); }
        }

        //private Image image;
        //[ImageEditor(ListViewImageEditorMode = ImageEditorMode.DropDownPictureEdit), DisplayName("Логотип")]
        //[ValueConverter(typeof(DevExpress.Xpo.Metadata.ImageValueConverter))]
        //public Image Image
        //{
        //    get { return image; }
        //    set { SetPropertyValue("Image", ref image, value); }
        //}
        [Association, DisplayName("Контейнеры на терминале")]
        public XPCollection<Container> Containers
        {
            get { return GetCollection<Container>("Containers"); }
        }

        [Association, DisplayName("Зоны терминала")]
        public XPCollection<TerminalZone> TerminalZones
        {
            get { return GetCollection<TerminalZone>("TerminalZones"); }
        }
        //[Association, DisplayName("Транспорт на терминале")]
        //public XPCollection<TerminalTransport> TerminalTransports
        //{
        //    get { return GetCollection<TerminalTransport>("TerminalTransports"); }
        //}
        [Association, DisplayName("Сотрудники терминала")]
        public XPCollection<Employee> Employees
        {
            get { return GetCollection<Employee>("Employees"); }
        }
        [Association, DisplayName("Стоки на терминале")]
        public XPCollection<Stock> Stocks
        {
            get { return GetCollection<Stock>("Stocks"); }
        }
        //[Association, DisplayName("Клиенты терминала")]
        //public XPCollection<Client> Clients
        //{
        //    get { return GetCollection<Client>("Clients"); }
        //}
        //[Association, DisplayName("Договоры")]
        //public XPCollection<Contract> Contracts
        //{
        //    get { return GetCollection<Contract>("Contracts"); }
        //}
        protected override ITreeNode Parent
        {
            get
            {
                return null;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return TerminalZones;
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Connect connect = Connect.FromSession(Session);
            if (TerminalOrg == null)
                TerminalOrg = connect.FindFirstObject<TerminalOrg>(x=>true);
        }
    }
}
