﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.OrgStructure
{
    /// <summary>
    /// Владелец терминала
    /// </summary>
    [ModelDefault("Caption", "Данные о компании"), NavigationItem("Организационная структура")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class TerminalOrg : TerminalSystem2.OrgStructure.Organization
    {
        public TerminalOrg(Session session) : base(session) { }

        [DisplayName("Лого")]
        [Size(SizeAttribute.Unlimited), VisibleInListView(false)]
        [ImageEditor(DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
            DetailViewImageEditorFixedHeight = 100)]
        //DetailViewImageEditorFixedWidth = 300)]
        public byte[] Logo
        {
            get { return GetPropertyValue<byte[]>("Logo"); }
            set { SetPropertyValue<byte[]>("Logo", value); }
        }

        // Почтовые настройки

        /// <summary>
        /// Gets the own email address of the account
        /// </summary>
        private string i_Account;
        [DisplayName("Аккаунт (Email)")]
        [RuleRequiredField("OrgSettings_Account_RuleRequiredField", DefaultContexts.Save, ResultType = ValidationResultType.Error)]
        public string Account
        {
            get { return i_Account; }
            set { SetPropertyValue(nameof(Account), ref i_Account, value); }
        }

        /// <summary>
        /// Gets the own email address of the account
        /// </summary>
        //private string i_MailLogin;
        //[DisplayName("Логин в почту")]
        //[RuleRequiredField("OrgSettings_MailLogin_RuleRequiredField", DefaultContexts.Save, ResultType = ValidationResultType.Error)]
        //public string MailLogin
        //{
        //    get { return i_MailLogin; }
        //    set { SetPropertyValue(nameof(MailLogin), ref i_MailLogin, value); }
        //}

        ///// <summary>
        ///// Gets the IMAP server
        ///// </summary>
        //[DisplayName("Сервер IMAP")]
        //public string Imap { get; set; }

        ///// <summary>
        ///// Gets the port of the IMAP server
        ///// </summary>
        //[DisplayName("IMAP порт")]
        //public int ImapPort { get; set; }

        /// <summary>
        /// Gets the own email address of the account
        /// </summary>
        //private string i_SenderName;
        //[Size(255), DisplayName("Отправитель")]
        //public string SenderName
        //{
        //    get { return i_SenderName; }
        //    set { SetPropertyValue(nameof(SenderName), ref i_SenderName, value); }
        //}

        /// <summary>
        /// Gets the SMTP server
        /// </summary>
        private string i_Smtp;
        [DisplayName("Сервер SMTP")]
        [RuleRequiredField("OrgSettings_Smtp_RuleRequiredField", DefaultContexts.Save, ResultType = ValidationResultType.Error)]
        public string Smtp
        {
            get { return i_Smtp; }
            set { SetPropertyValue(nameof(Smtp), ref i_Smtp, value); }
        }

        /// <summary>
        /// Gets the port of the SMTP server
        /// </summary>
        private int i_SmtpPort;
        [DisplayName("SMTP порт")]
        [RuleRequiredField("OrgSettings_SmtpPort_RuleRequiredField", DefaultContexts.Save, ResultType = ValidationResultType.Error)]
        public int SmtpPort
        {
            get { return i_SmtpPort; }
            set { SetPropertyValue(nameof(SmtpPort), ref i_SmtpPort, value); }
        }
        private bool i_IsSSL;
        [DisplayName("Использовать SSL")]
        public bool IsSSL
        {
            get { return i_IsSSL; }
            set { SetPropertyValue(nameof(IsSSL), ref i_IsSSL, value); }
        }
        /// <summary>
        /// Gets the password of the email account
        /// </summary>
        private string i_Password;
        [DisplayName("Пароль")]
        [RuleRequiredField("OrgSettings_Password_RuleRequiredField", DefaultContexts.Save, ResultType = ValidationResultType.Error)]
        public string Password
        {
            get { return i_Password; }
            set { SetPropertyValue(nameof(Password), ref i_Password, value); }
        }


        /// <summary>
        /// Gets the Signature
        /// </summary>
        private string i_Signature;
        [Size(SizeAttribute.Unlimited), DisplayName("Подпись")]
        public string Signature
        {
            get { return i_Signature; }
            set { SetPropertyValue(nameof(Signature), ref i_Signature, value); }
        }

        [Association, DisplayName("Сотрудники")]
        public XPCollection<Employee> Employees
        {
            get { return GetCollection<Employee>("Employees"); }
        }

        [Association, DisplayName("Терминалы")]
        public XPCollection<Terminal> Terminals
        {
            get { return GetCollection<Terminal>("Terminals"); }
        }

        [Association, DisplayName("Договоры")]
        public XPCollection<Contract> Contracts
        {
            get { return GetCollection<Contract>("Contracts"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }


    }
}

