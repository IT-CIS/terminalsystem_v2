﻿using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.OrgStructure
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    [ModelDefault("Caption", "Сотрудник"), NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class Employee : EmployeeBase
    {
        public Employee(Session session) : base(session) { }

        private string i_UserName;
        private SecuritySystemUser i_user;

        private bool i_isDirector;
        [DisplayName("Владелец/директор компании")]
        public bool isDirector
        {
            get { return i_isDirector; }
            set { SetPropertyValue("isDirector", ref i_isDirector, value); }
        }

        private TerminalOrg i_TerminalOrg;
        [Association, DisplayName("Компания")]
        public TerminalOrg TerminalOrg
        {
            get { return i_TerminalOrg; }
            set { SetPropertyValue("TerminalOrg", ref i_TerminalOrg, value); }
        }
        private dDepartment i_Department;
        [DisplayName("Отдел")]
        public dDepartment Department
        {
            get { return i_Department; }
            set { SetPropertyValue("Department", ref i_Department, value); }
        }



        //[Association, DisplayName("Смены")]
        //public XPCollection<Shift> Shifts
        //{
        //    get { return GetCollection<Shift>("Shifts"); }
        //}

        [Association, DisplayName("Терминалы")]
        public XPCollection<Terminal> Terminals
        {
            get { return GetCollection<Terminal>("Terminals"); }
        }



        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
            //if (this.BriefName == null || this.BriefName == "")
            //{
            //    this.BriefName = GetBriefName();
            //}
        }
        //private string GetBriefName()
        //{
        //    string res = "";
        //    if (SurName != null && SurName != "")
        //        res = SurName;
        //    if (FirstName != null && FirstName != "")
        //        if (res == "")
        //            res = FirstName;
        //        else
        //            res = res + " " + FirstName;
        //    if (MiddleName != null && MiddleName != "")
        //        if (res == "")
        //            res = MiddleName;
        //        else
        //            res = res + " " + MiddleName;
        //    return res;
        //}
    }
}
