﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;

namespace TerminalSystem2.OrgStructure
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    [ModelDefault("Caption", "Информация по сотруднику")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class EmployeeBase : BaseObjectXAF
    {
        public EmployeeBase(Session session) : base(session) { }

        private string i_UserName;
        

        //private string i_SurName;
        //[Size(255), DisplayName("Фамилия")]
        ////[ImmediatePostData]
        //public string SurName
        //{
        //    get { return i_SurName; }
        //    set { SetPropertyValue("SurName", ref i_SurName, value); }
        //}
        //private string i_FirstName;
        //[Size(255), DisplayName("Имя")]
        ////[ImmediatePostData]
        //public string FirstName
        //{
        //    get { return i_FirstName; }
        //    set { SetPropertyValue("FirstName", ref i_FirstName, value); }
        //}
        //private string i_MiddleName;
        //[Size(255), DisplayName("Отчество")]
        ////[ImmediatePostData]
        //public string MiddleName
        //{
        //    get { return i_MiddleName; }
        //    set { SetPropertyValue("MiddleName", ref i_MiddleName, value); }
        //}
        private string i_BriefName;
        [Size(255), DisplayName("ФИО")]
        public string BriefName
        {
            get
            {
                return i_BriefName;
            }
            set { SetPropertyValue("BriefName", ref i_BriefName, value); }
        }
        //private string GetBriefName()
        //{
        //    string res = "";
        //    if (SurName != null && SurName != "")
        //        res = SurName;
        //    if (FirstName != null && FirstName != "")
        //        if (res == "")
        //            res = FirstName;
        //        else
        //            res = res + " " + FirstName;
        //    if (MiddleName != null && MiddleName != "")
        //        if (res == "")
        //            res = MiddleName;
        //        else
        //            res = res + " " + MiddleName;
        //    return res;
        //}

        private string i_PhonesNumbers;
        [Size(255), DisplayName("Телефоны")]
        public string PhonesNumbers
        {
            get { return i_PhonesNumbers; }
            set { SetPropertyValue("PhonesNumbers", ref i_PhonesNumbers, value); }
        }

        private string i_Email;
        [RuleRegularExpression("RuleRegularExpressionObject.Email_RuleRegularExpression", 
            DefaultContexts.Save, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")]
        [Size(255), DisplayName("E-mail")]
        public string Email
        {
            get { return i_Email; }
            set { SetPropertyValue("Email", ref i_Email, value); }
        }

        private dPosition i_Position;
        [DisplayName("Должность")]
        public dPosition Position
        {
            get { return i_Position; }
            set { SetPropertyValue("Position", ref i_Position, value); }
        }

        private string i_SysUserString;
        [Size(255), DisplayName("Пользователь системы")]
        public string SysUserString
        {
            get { return i_SysUserString; }
            set { SetPropertyValue("SysUserString", ref i_SysUserString, value); }
        }

        private PermissionPolicyUser i_user;
        [DisplayName("Пользователь системы (ссылка)")]
        public PermissionPolicyUser SysUser
        {
            get { return i_user; }
            set { SetPropertyValue("SysUser", ref i_user, value); }
        }



        

        public Employee GetCurrentEmployee()
        {
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    return currentEmpl;
                else
                    return null;
            }
            else return null;
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
            //if (i_BriefName == null || i_BriefName == "")
            //{
            //    i_BriefName = GetBriefName();
            //}
        }


    }
}
