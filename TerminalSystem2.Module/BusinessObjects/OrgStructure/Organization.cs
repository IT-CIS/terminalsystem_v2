﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;

namespace TerminalSystem2.OrgStructure
{
    /// <summary>
    /// Юридическое лицо
    /// </summary>
    [ModelDefault("Caption", "Юридическое лицо")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class Organization : BaseObjectXAF
    {
        public Organization(Session session) : base(session) { }

        
        private string i_Name;
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        private string i_UserName;
        [Size(255), DisplayName("ФИО  контактного лица")]
        public string UserName
        {
            get { return i_UserName; }
            set { SetPropertyValue("UserName", ref i_UserName, value); }
        }

        private string i_PhonesNumbers;
        [Size(255), DisplayName("Телефоны")]
        public string PhonesNumbers
        {
            get { return i_PhonesNumbers; }
            set { SetPropertyValue("PhonesNumbers", ref i_PhonesNumbers, value); }
        }

        private string i_Email;
        [Size(255), DisplayName("E-mail")]
        public string Email
        {
            get { return i_Email; }
            set { SetPropertyValue("Email", ref i_Email, value); }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }


    }
}

