﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Enums;
using TerminalSystem2.Tarification;

namespace TerminalSystem2.Classifiers
{
    [Custom("Caption", "Базовый класс справочников")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dClassifierBase : BaseObjectXAF
    {
        public dClassifierBase(Session session) : base(session) { }

        private string i_Code;
        [Size(32), DisplayName("Код")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }

        private string i_Name;
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }

    #region Справочники оргструктуры
    [ModelDefault("Caption", "Должность"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dPosition : dClassifierBase
    {
        public dPosition(Session session) : base(session) { }

        
    }

    [ModelDefault("Caption", "Подразделение(отдел)"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dDepartment : dClassifierBase
    {
        public dDepartment(Session session) : base(session) { }

    }
    #endregion

    #region Справочники контейнера
    [ModelDefault("Caption", "Тип контейнера"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dContainerType : BaseObjectXAF
    {
        public dContainerType(Session session) : base(session) { }

        private string i_Name;
        [DisplayName("Наименование")]
        public string Name
        {
            get {
                string contSize = "";
                string contType = "";
                try { contSize = ContainerTypeSize.ContainerTypeSize.ToString(); }
                catch { }
                //try { contType = ContainerSubType.Code; }
                //catch { }
                //if (contType == "")
                //{
                    try { contType = ContainerSubType.Name; }
                    catch { }
                //}
                i_Name = String.Format("{0} {1}", contSize, contType);
                return i_Name;
            }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        private dContainerTypeSize i_ContainerTypeSize;
        [DisplayName("Типоразмер контейнера")]
        public dContainerTypeSize ContainerTypeSize
        {
            get { return i_ContainerTypeSize; }
            set { SetPropertyValue("ContainerTypeSize", ref i_ContainerTypeSize, value); }
        }
        private dContainerSubType i_ContainerSubType;
        [DisplayName("Подтип контейнера")]
        public dContainerSubType ContainerSubType
        {
            get { return i_ContainerSubType; }
            set { SetPropertyValue("ContainerSubType", ref i_ContainerSubType, value); }
        }
    }
    [ModelDefault("Caption", "Типоразмер контейнера"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("ContainerTypeSize")]
    public class dContainerTypeSize : BaseObjectXAF
    {
        public dContainerTypeSize(Session session) : base(session) { }

        private EContainerCategorySize i_ContainerCategorySize;
        [DisplayName("Категория размера для таррификации")]
        public EContainerCategorySize ContainerCategorySize
        {
            get { return i_ContainerCategorySize; }
            set { SetPropertyValue("ContainerCategorySize", ref i_ContainerCategorySize, value); }
        }

        private int i_ContainerTypeSize;
        [DisplayName("Типоразмер")]
        public int ContainerTypeSize
        {
            get { return i_ContainerTypeSize; }
            set { SetPropertyValue("ContainerTypeSize", ref i_ContainerTypeSize, value); }
        }
    }
    [ModelDefault("Caption", "Подтип контейнера"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dContainerSubType : dClassifierBase
    {
        public dContainerSubType(Session session) : base(session) { }

        [Association, DisplayName("Предоставляемые услуги по подтипу")]
        public XPCollection<TariffService> TariffServices
        {
            get { return GetCollection<TariffService>("TariffServices"); }
        }

    }
    [ModelDefault("Caption", "Международная классификация"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dContainerISOClass : dClassifierBase
    {
        public dContainerISOClass(Session session) : base(session) { }
    }

    [ModelDefault("Caption", "Габариты контейнера"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dContainerSize : BaseObjectXAF
    {
        public dContainerSize(Session session) : base(session) { }

        [DisplayName("Габариты")]
        public string Name
        {
            get {
                string res1 = "";
                string res2 = "";
                string res = "";
                try { res1 = "д*";
                    res2 = Length.ToString() + "*"; }
                catch { }
                try
                {
                    res1 += "ш*";
                    res2 += Width.ToString() + "*";
                }
                catch { }
                try
                {
                    res1 += "в";
                    res2 += Height.ToString();
                }
                catch { }
                if(res1!="")
                    res = res1.TrimEnd('*') + ":" + res2.TrimEnd('*') + ", мм";
                return res; }
        }

        private double i_Length;
        [DisplayName("Длина, мм")]
        public double Length
        {
            get { return i_Length; }
            set { SetPropertyValue("Length", ref i_Length, value); }
        }
        private double i_Width;
        [DisplayName("Ширина, мм")]
        public double Width
        {
            get { return i_Width; }
            set { SetPropertyValue("Width", ref i_Width, value); }
        }
        private double i_Height;
        [DisplayName("Высота, мм")]
        public double Height
        {
            get { return i_Height; }
            set { SetPropertyValue("Height", ref i_Height, value); }
        }
    }

    [ModelDefault("Caption", "Грузоподъемность"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dCarrying : dClassifierBase
    {
        public dCarrying(Session session) : base(session) { }
    }

    [ModelDefault("Caption", "Состояние"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dState : dClassifierBase
    {
        public dState(Session session) : base(session) { }
    }
    #endregion

    #region Справочники ТС
    [ModelDefault("Caption", "Тип ТС"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dTransportType : dClassifierBase
    {
        public dTransportType(Session session) : base(session) { }
    }
    [ModelDefault("Caption", "Марка ТС"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dTransportMark : dClassifierBase
    {
        public dTransportMark(Session session) : base(session) { }
    }
    [ModelDefault("Caption", "Перевозочная компания")]//, NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dTransportCompany : dClassifierBase
    {
        public dTransportCompany(Session session) : base(session) { }
    }
    #endregion

    #region Справочники заявок
    [ModelDefault("Caption", "Тип заявки")]//, NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dRequestType : dClassifierBase
    {
        public dRequestType(Session session) : base(session) { }
    }
    #endregion

    #region Справочники Таррификации
    [ModelDefault("Caption", "Валюта"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dCurrency : dClassifierBase
    {
        public dCurrency(Session session) : base(session) { }
    }
    [ModelDefault("Caption", "Единица измерения"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dUnitKind : dClassifierBase
    {
        public dUnitKind(Session session) : base(session) { }
    }
    [ModelDefault("Caption", "Вид услуги"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dServiceType : dClassifierBase
    {
        public dServiceType(Session session) : base(session) { }


    }
    #endregion
}
