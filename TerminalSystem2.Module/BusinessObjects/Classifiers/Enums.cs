﻿using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;

namespace TerminalSystem2.Enums
{
    /// <summary>
    /// Статус заявки
    /// </summary>
    public enum ERequestStatus
    {
        [XafDisplayName("Черновик")]
        Черновик = 0,
        [XafDisplayName("Подана")]
        Подана = 1,
        [XafDisplayName("На редактировании")]
        Редактирование = 2,
        [XafDisplayName("Отредактирована")]
        Отредактирована = 3,
        [XafDisplayName("В обработке")]
        Обработка = 4,
        [XafDisplayName("Отработана")]
        Отработана = 5,
        [XafDisplayName("Отказ")]
        Отказ = 6,
        [XafDisplayName("Обработана частично")]
        ОбработанаЧастично = 7,
        [XafDisplayName("Закрыта по сроку действия")]
        ЗакрытаПоВремени = 8,
        [XafDisplayName("Не определено")]
        НеОпределено = 9
    }

    /// <summary>
    /// Вид движения контейнера
    /// </summary>
    public enum EContainerHistoryKind
    {
        [XafDisplayName("Размещение на терминале")]
        Размещение = 0,
        [XafDisplayName("Выдача с терминала")]
        Выдача = 1,
        [XafDisplayName("Изменение стока")]
        ИзменениеСтока = 2,
        [XafDisplayName("Перестановка в другое место")]
        Перестановка = 3,
        [XafDisplayName("Ремонт")]
        Ремонт = 4,
        [XafDisplayName("Осмотр")]
        Осмотр = 5,
        [XafDisplayName("Продажа")]
        Продажа = 6,
        [XafDisplayName("Не определено")]
        НеОпределено = 7
    }

    /// <summary>
    /// Тип отчета по движению контейнеров
    /// </summary>
    public enum ETrafficRepotKind
    {
        [XafDisplayName("ПоСтоку")]
        ПоСтоку = 0,
        [XafDisplayName("ПоДвижению")]
        ПоДвижению = 1
    }

    /// <summary>
    /// Категория типоразмера
    /// </summary>
    public enum EContainerCategorySize
    {
        [XafDisplayName("20")]
        twenty = 0,
        [XafDisplayName("40")]
        forty = 1
    }

    /// <summary>
    /// Вид скидки
    /// </summary>
    public enum EDiscountType
    {
        [XafDisplayName("По суткам хранения")]
        byDailyStorage = 0,
        [XafDisplayName("По сроку действия")]
        byTime = 1
    }

    /// <summary>
    /// Вид расчета
    /// </summary>
    public enum ECalcType
    {
        [XafDisplayName("Учитывать тарификацию")]
        byDaily = 0,
        [XafDisplayName("Не вести расчет")]
        byMonth = 1
    }

}
