﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;
//using TerminalSystem2.Clients;
//using TerminalSystem2.DocFlow;
//using TerminalSystem2.Enums;
//using TerminalSystem2.Tarification;
//using TerminalSystem2.Terminals;

namespace TerminalSystem2.Containers
{
    [ModelDefault("Caption", "Контейнер"), NavigationItem("Терминал")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class Container : AttachBase
    {
        public Container(Session session) : base(session) { }

        [DisplayName("Фото контейнера")]
        [Size(SizeAttribute.Unlimited), VisibleInListView(false)]
        [ImageEditor(DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
            //ImageSizeMode = ImageSizeMode.StretchImage,
            DetailViewImageEditorFixedHeight = 300, DetailViewImageEditorFixedWidth = 300)]
        public byte[] ContainerPhoto
        {
            get { return GetPropertyValue<byte[]>("ContainerPhoto"); }
            set { SetPropertyValue<byte[]>("ContainerPhoto", value); }
        }

        private string i_Name;
        [Size(255), DisplayName("Контейнер")]
        public string Name
        {
            get {
                try { i_Name = String.Format("№ {1}, тип {0}", ContainerType.Name, ContainerNumber); }
                catch { }
                return i_Name; }
        }

        private string i_ContainerNumber;
        [Size(11), DisplayName("Номер контейнера")]
        public string ContainerNumber
        {
            get { return i_ContainerNumber; }
            set { SetPropertyValue("ContainerNumber", ref i_ContainerNumber, value); }
        }

        private dContainerType i_ContainerType;
        [DisplayName("Тип контейнера")]
        public dContainerType ContainerType
        {
            get { return i_ContainerType; }
            set { SetPropertyValue("ContainerType", ref i_ContainerType, value); }
        }

        private dContainerISOClass i_ContainerISOClass;
        [DisplayName("Международная классификация")]
        public dContainerISOClass ContainerISOClass
        {
            get { return i_ContainerISOClass; }
            set { SetPropertyValue("ContainerISOClass", ref i_ContainerISOClass, value); }
        }

        private dContainerSize i_Size;
        [DisplayName("Габариты контейнера")]
        public dContainerSize Size
        {
            get { return i_Size; }
            set { SetPropertyValue("Size", ref i_Size, value); }
        }

        [Persistent("Color")]
        [DisplayName("Цвет контейнера")]
        private Int32 color;
        [NonPersistent]
        public System.Drawing.Color Color
        {
            get { return System.Drawing.Color.FromArgb(color); }
            set
            {
                color = value.ToArgb();
                OnChanged("Color");
            }
        }
        private dCarrying i_Carrying;
        [DisplayName("Грузоподъемность")]
        public dCarrying Carrying
        {
            get { return i_Carrying; }
            set { SetPropertyValue("Carrying", ref i_Carrying, value); }
        }
        private dState i_State;
        [DisplayName("Состояние")]
        public dState State
        {
            get { return i_State; }
            set { SetPropertyValue("State", ref i_State, value); }
        }

        private bool i_ToRepair;
        [DisplayName("Требуется ремонт")]
        public bool ToRepair
        {
            get { return i_ToRepair; }
            set { SetPropertyValue("ToRepair", ref i_ToRepair, value); }
        }

        private string i_SpecialNotes;
        [Size(750), DisplayName("Особые отметки")]
        public string SpecialNotes
        {
            get { return i_SpecialNotes; }
            set { SetPropertyValue("SpecialNotes", ref i_SpecialNotes, value); }
        }

        private bool i_onTerminal;
        [DisplayName("На терминале")]
        public bool onTerminal
        {
            get { return i_onTerminal; }
            set { SetPropertyValue("onTerminal", ref i_onTerminal, value); }
        }

        private Client i_Owner;
        [Association, DisplayName("Владелец")]
        public Client Owner
        {
            get { return i_Owner; }
            set { SetPropertyValue("Owner", ref i_Owner, value); }
        }

        private Stock i_Stock;
        [Association, DisplayName("Сток")]
        [DataSourceCriteria("Client = '@This.Owner'")]
        public Stock Stock
        {
            get { return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value); }
        }

        private Terminal i_Terminal;
        [Association, DisplayName("Терминал")]
        [ImmediatePostData]
        public Terminal Terminal
        {
            get { return i_Terminal; }
            set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        }
        private TerminalZone i_TerminalZone;
        [Association, DisplayName("Зона")]
        [ImmediatePostData]
        [DataSourceCriteria("Terminal = '@This.Terminal'")]
        public TerminalZone TerminalZone
        {
            get { return i_TerminalZone; }
            set { SetPropertyValue("TerminalZone", ref i_TerminalZone, value); }
        }
        private TerminalRow i_TerminalRow;
        [Association, DisplayName("Ряд")]
        [ImmediatePostData]
        [DataSourceCriteria("TerminalZone = '@This.TerminalZone'")]
        public TerminalRow TerminalRow
        {
            get { return i_TerminalRow; }
            set { SetPropertyValue("TerminalRow", ref i_TerminalRow, value); }
        }
        private TerminalSection i_TerminalSection;
        [Association, DisplayName("Секция")]
        [ImmediatePostData]
        [DataSourceCriteria("TerminalRow = '@This.TerminalRow'")]
        public TerminalSection TerminalSection
        {
            get { return i_TerminalSection; }
            set { SetPropertyValue("TerminalSection", ref i_TerminalSection, value); }
        }
        private TerminalTier i_Tier;
        [Association, DisplayName("Ярус")]
        [DataSourceCriteria("TerminalSection = '@This.TerminalSection'")]
        public TerminalTier Tier
        {
            get { return i_Tier; }
            set { SetPropertyValue("Tier", ref i_Tier, value); }
        }

        private bool i_isSingleOccupancy;
        [DisplayName("Отдельное размещение")]
        public bool isSingleOccupancy
        {
            get { return i_isSingleOccupancy; }
            set { SetPropertyValue("TransportationTime", ref i_isSingleOccupancy, value); }
        }
        private string i_Dislocation;
        [Size(255), DisplayName("Размещение")]
        public string Dislocation
        {
            get { return i_Dislocation; }
            set { SetPropertyValue("Dislocation", ref i_Dislocation, value); }
        }

        private bool i_isLaden;
        [DisplayName("Груженый")]
        [ImmediatePostData]
        public bool isLaden
        {
            get { return i_isLaden; }
            set { SetPropertyValue("isLaden", ref i_isLaden, value); }
        }
        private double i_LadenWeight;
        [DisplayName("Вес груженого контейнера, кг")]
        public double LadenWeight
        {
            get { return i_LadenWeight; }
            set { SetPropertyValue("LadenWeight", ref i_LadenWeight, value); }
        }
        private string i_CargoType;
        [Size(255), DisplayName("Характер груза")]
        public string CargoType
        {
            get { return i_CargoType; }
            set { SetPropertyValue("CargoType", ref i_CargoType, value); }
        }
        private string i_SealNumber;
        [Size(255), DisplayName("Номер пломбы")]
        public string SealNumber
        {
            get { return i_SealNumber; }
            set { SetPropertyValue("SealNumber", ref i_SealNumber, value); }
        }

        private string GetDislocation()
        {
            string res = "";

            return res;
        }
        private bool i_EditFlag;
        [DisplayName("Открыт на редактирование")]
        public bool EditFlag
        {
            get { return i_EditFlag; }
            set { SetPropertyValue("EditFlag", ref i_EditFlag, value); }
        }

        [Association, DisplayName("Фотографии контейнера")]
        public XPCollection<ContainerPhoto> ContainerPhotos
        {
            get { return GetCollection<ContainerPhoto>("ContainerPhotos"); }
        }
        [Association, DisplayName("История контейнера")]
        public XPCollection<ContainerHistory> ContainerHistorys
        {
            get { return GetCollection<ContainerHistory>("ContainerHistorys"); }
        }
        [Association, DisplayName("Предоставленные услуги")]
        public XPCollection<ServiceDone> ServicesDone
        {
            get { return GetCollection<ServiceDone>("ServicesDone"); }
        }
        //[Association, DisplayName("Фотографии контейнера")]
        //public XPCollection<ContainerPlacementAct> ContainerPlacementActs
        //{
        //    get { return GetCollection<ContainerPlacementAct>("ContainerPlacementActs"); }
        //}
        //[Association, DisplayName("Осмотр контейнера")]
        //public XPCollection<ContainerInspection> ContainerInspections
        //{
        //    get { return GetCollection<ContainerInspection>("ContainerInspections"); }
        //}
        //[Association, DisplayName("Заявки по контейнеру")]
        //public XPCollection<RequestBase> Requests
        //{
        //    get { return GetCollection<RequestBase>("Requests"); }
        //}
        //[Association, DisplayName("Операции по контейнеру")]
        //public XPCollection<BusinessProcess> Operations
        //{
        //    get { return GetCollection<BusinessProcess>("Operations"); }
        //}

        //[Association, DisplayName("Движение контейнера")]
        //public XPCollection<ContainerTraffic> ContainerTraffic
        //{
        //    get { return GetCollection<ContainerTraffic>("ContainerTraffic"); }
        //}
        //[Association, DisplayName("Модификаторы типа контейнера")]
        //public XPCollection<ContainerCommonProperty> ContainerCommonPropertys
        //{
        //    get { return GetCollection<ContainerCommonProperty>("ContainerCommonPropertys"); }
        //}

        public bool isCurrentUserClient()
        {
            bool res = false;
            if (SecuritySystem.CurrentUser != null)
            {
                //ClientEmployee currentEmpl = connect.FindFirstObject<ClientEmployee>(x => x.SysUser.Oid.ToString() == SecuritySystem.CurrentUserId.ToString());
                ClientEmployee currentEmpl = Session.FindObject<ClientEmployee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    res = true;
            }
            return res;
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            //EditFlag = false;
            //string res = "";
            //try
            //{
            //    res = String.Format("{0}' {1}", Size.Name, ContainerType.Name);
            //    foreach (ContainerCommonProperty ContainerCommonProperty in ContainerCommonPropertys)
            //    {
            //        res += " " + ContainerCommonProperty.Name;
            //    }
            //    this.ContainerTypeString = res;
            //}
            //catch { }
        }
    }
}
