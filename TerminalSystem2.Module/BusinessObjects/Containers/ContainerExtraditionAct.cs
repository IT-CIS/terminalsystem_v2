﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;
using TerminalSystem2.Transport;
//using TerminalSystem2.Clients;
//using TerminalSystem2.DocFlow;
//using TerminalSystem2.Enums;
//using TerminalSystem2.Tarification;
//using TerminalSystem2.Terminals;

namespace TerminalSystem2.Containers
{
    //[NonPersistent]
    [ModelDefault("Caption", "Акт выдачи контейнера")]
    [System.ComponentModel.DefaultProperty("Name")]
    //[NavigationItem("Терминал")]
    public class ContainerExtraditionAct : BaseObjectXAF
    {
        public ContainerExtraditionAct(Session session) : base(session) { }

        private string i_Name;
        [Size(255), DisplayName("Описание")]
        public string Name
        {
            get {
                string contNum = "";
                string contType = "";
                try { contNum = Container?.ContainerNumber??""; }
                catch { }
                try { contType = Container?.ContainerType?.Name?? ""; }
                catch { }
                return String.Format("№ {0}, № контейнера {1}, тип {2}", ActNumber, contNum, contType); }
        }

        private string i_ActNumber;
        [Size(255), DisplayName("Номер акта")]
        public string ActNumber
        {
            get { return i_ActNumber; }
            set { SetPropertyValue("ActNumber", ref i_ActNumber, value); }
        }

        private DateTime i_ActDate;
        [DisplayName("Дата")]
        public DateTime ActDate
        {
            get { return i_ActDate; }
            set { SetPropertyValue("ActDate", ref i_ActDate, value); }
        }

        private Employee i_Employee;
        [DisplayName("Регистратор")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }
        private Stock i_Stock;
        [DisplayName("Сток")]
        [ImmediatePostData]
        [DataSourceProperty("AvailableStocks")]
        public Stock Stock
        {
            get {
                //try {
                //    if(RequestContainerExtraditionInfo != null)
                //        i_Stock = RequestContainerExtraditionInfo.Stock; }
                //catch { }
                return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value);
                RefreshAvailableStocks();
                RefreshAvailableContainers();
                OnChanged();
            }
        }
        #region Получаем возможные для выбора стоки
        private XPCollection<Stock> availableStocks;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Stock> AvailableStocks
        {
            get
            {
                if (availableStocks == null)
                {
                    // Retrieve all Terminals objects 
                    availableStocks = new XPCollection<Stock>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableStocks();
                }
                return availableStocks;
            }
        }
        private void RefreshAvailableStocks()
        {
            if (availableStocks == null)
                return;
            availableStocks.Criteria = new InOperator("Oid", GetAvailebleStocks());
            if (Stock == null && availableStocks != null && availableStocks.Count == 1)
                Stock = availableStocks[0];
        }
        private List<string> GetAvailebleStocks()
        {
            List<string> stocksOids = new List<string>();
            if (Stock != null)
                stocksOids.Add(Stock.Oid.ToString());
            else
            {
                if (Container != null && Container.Stock != null)
                {
                    stocksOids.Add(Container.Stock.Oid.ToString());
                }
                else if (Request != null && Request.Client != null && Request.Client.Stocks != null && Request.Client.Stocks.Count > 0 &&
                    Request.Terminal != null)
                {
                    foreach (var stock in Request.Client.Stocks.Where(x => x.Terminal.Oid == Request.Terminal.Oid))
                    {
                        stocksOids.Add(stock.Oid.ToString());
                    }
                }
            }
            return stocksOids;
        }
        #endregion
        private dContainerType i_ContainerType;
        [DisplayName("Тип контейнера")]
        [ImmediatePostData]
        public dContainerType ContainerType
        {
            get { return i_ContainerType; }
            set { SetPropertyValue("ContainerType", ref i_ContainerType, value);
                RefreshAvailableContainers();
            }
        }

        //[EditorAlias(EditorAliases.DetailPropertyEditor)]
        private Container i_Container;
        [DisplayName("Контейнер")]
        [DataSourceProperty("AvailableContainers")]
        [ImmediatePostData]
        public Container Container
        {
            get { return i_Container; }
            set { SetPropertyValue("Container", ref i_Container, value);
                RefreshAvailableLiftTarrifs();
                OnChanged();
            }
        }
        //----------------------------------------------------
        #region Получаем возможные для выбора контейнеры
        private XPCollection<Container> availableContainers;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Container> AvailableContainers
        {
            get
            {
                if (availableContainers == null)
                {
                    // Retrieve all Terminals objects 
                    availableContainers = new XPCollection<Container>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableContainers();
                }
                return availableContainers;
            }
        }
        private void RefreshAvailableContainers()
        {
            if (availableContainers == null)
                return;
            //if (Container == null)
            //{
                availableContainers.Criteria = new InOperator("Oid", GetAvailebleContainers());
                if (availableContainers.Count == 1)
                    Container = availableContainers[0];
                #region NOT USED
                    //availableContainers.Criteria = new GroupOperator(GroupOperatorType.And,
                    //    new BinaryOperator("Stock", Stock), new BinaryOperator("ContainerType.Name", ContainerType?.Name));
                    //if (Stock != null)
                    //{
                    //availableContainers = Stock.Containers.Criteria;
                    //    if (ContainerType != null)
                    //    {

                    //        //CriteriaOperator crit = new BinaryOperator("ContainerType.Oid", ContainerType.Oid);
                    //        availableContainers.Criteria = new BinaryOperator("ContainerType.Name", ContainerType.Name);
                    //        //availableContainers = Stock.Containers.Criteria;
                    //    }
                    //}
                    //else
                    //{
                    //    availableContainers = null;
                    //    //availableContainers = new XPCollection<Container>(Session);
                    //    //if (ContainerType != null)
                    //    //    availableContainers.Criteria = new BinaryOperator("ContainerType", ContainerType);
                    //}
                    #endregion
            //}
            //Container = null;
        }
        private List<string> GetAvailebleContainers()
        {
            List<string> containersOids = new List<string>();
            if (Container != null)
                containersOids.Add(Container.Oid.ToString());
            else
            {
                if(Stock != null && Stock.Containers != null && Stock.Containers.Where(x=> x.onTerminal).ToList().Count > 0)
                {
                    foreach (var cont in Stock.Containers.Where(x => x.onTerminal))
                    {
                        if (ContainerType != null)
                        {
                            if(cont.ContainerType != null && cont.ContainerType.Oid == ContainerType.Oid)
                                containersOids.Add(cont.Oid.ToString());
                        }
                        else
                            containersOids.Add(cont.Oid.ToString());
                    }
                }
                else if (Request != null && Request.Client != null && Request.Terminal != null &&
                    Request.Client.Containers != null && Request.Client.Containers.Where(x => x.onTerminal).ToList().Count > 0)
                {
                    foreach (var cont in Request.Client.Containers.Where(x => x.onTerminal && x.Terminal.Oid == Request.Terminal.Oid))
                    {
                        if (ContainerType != null)
                        {
                            if (cont.ContainerType != null && cont.ContainerType.Oid == ContainerType.Oid)
                                containersOids.Add(cont.Oid.ToString());
                        }
                        else
                            containersOids.Add(cont.Oid.ToString());
                    }
                }
            }
            return containersOids;
        }
        #endregion

        private int i_LiftCount;
        [DisplayName("Количество крановых операций")]
        [ImmediatePostData]
        public int LiftCount
        {
            get { return i_LiftCount; }
            set { SetPropertyValue("LiftCount", ref i_LiftCount, value); }
        }

        private double i_LiftPrice;
        [DisplayName("Тариф на лифт")]
        [ImmediatePostData]
        public double LiftPrice
        {
            get
            {
                return i_LiftPrice;
            }
            set { SetPropertyValue("LiftPrice", ref i_LiftPrice, value); }
        }

        private double i_LiftPriceTotal;
        [DisplayName("Общая стоимость лифта")]
        public double LiftPriceTotal
        {
            get
            {
                try
                {
                    i_LiftPriceTotal = i_LiftCount* i_LiftPrice;
                }
                catch { }
                return i_LiftPriceTotal;
            }
            set { SetPropertyValue("LiftPrice", ref i_LiftPrice, value); }
        }

        private dCurrency i_Currency;
        [DisplayName("Валюта")]
        public dCurrency Currency
        {
            get { return i_Currency; }
            set { SetPropertyValue("Currency", ref i_Currency, value); }
        }

        private Tariff i_TariffLift;
        [DisplayName("Тариф на лифт(ссылка)")]
        [DataSourceProperty("AvailableLiftTariffs")]
        //[DataSourceCriteria("TariffScale.Client = '@This.Client'")]
        [ImmediatePostData]
        public Tariff TariffLift
        {
            get { return i_TariffLift; }
            set
            {
                SetPropertyValue("TariffLift", ref i_TariffLift, value);
                try
                {
                    SetTariffPrice();
                }
                catch { }
                //OnChanged();
                //if(i_TariffLift!= null)
                //{
                //    if(LiftPrice == 0)
                //    {
                //        LiftPrice = TariffLift.
                //    }
                //}
            }
        }

        

        private XPCollection<Tariff> availableLiftTariffs;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Tariff> AvailableLiftTariffs
        {
            get
            {
                if (availableLiftTariffs == null)
                {
                    availableLiftTariffs = new XPCollection<Tariff>(Session);
                    //try
                    //{
                    RefreshAvailableLiftTarrifs();
                    //}
                    //catch { throw new Exception("Ошибка при поиске тарифов для клиента"); }
                }
                return availableLiftTariffs;
            }
        }
        private void RefreshAvailableLiftTarrifs()
        {
            if (availableLiftTariffs == null)
                return;
            availableLiftTariffs.Criteria = new InOperator("Oid", GetTariffs("Крановая операция"));
            //if (Stock != null)
            //{
            //    availableLiftTariffs.Criteria = new InOperator("Oid", GetTariffs("Крановая операция"));
            //    //try { TariffLift = availableLiftTariffs[0]; }
            //    //catch { }

            //}
            //else
            //{
            //    availableLiftTariffs = new XPCollection<Tariff>(Session);
            //}
            //TariffLift = null;
        }

        private List<string> GetTariffs(string TariffType)
        {
            List<string> tariffList = new List<string>();
            if(Request != null && Request.Client != null)
            {
                Connect connect = Connect.FromSession(Session);
                Contract contract = connect.FindFirstObject<Contract>(x => x.Client.Oid == Request.Client.Oid && x.ActiveFlag == true);
                if(contract != null)
                {
                    TariffScale tariffScale = connect.FindFirstObject<TariffScale>(x => x.Contract.Oid == contract.Oid && x.ActiveFlag == true);
                    if (tariffScale != null)
                    {
                        dContainerType containerType = null;
                        if (ContainerType != null)
                            containerType = ContainerType;
                        if (containerType == null && Container != null && Container.ContainerType != null)
                            containerType = Container.ContainerType;
                        if (containerType != null)
                        {
                            this.CalcType = tariffScale.CalcType;
                            try
                            {
                                this.Currency = tariffScale.Currency;
                            }
                            catch { }
                            if (tariffScale.CalcType == ECalcType.byDaily)
                            {
                                foreach (Tariff Tariff in tariffScale.Tariffs)
                                {
                                    try
                                    {
                                        foreach (dContainerSubType ContainerSubType in Tariff.TariffService.ContainerSubTypes)
                                        {
                                            try
                                            {
                                                if (containerType.ContainerSubType == ContainerSubType)
                                                {
                                                    if (Tariff.TariffService.ServiceType.Name == TariffType)
                                                    {
                                                        tariffList.Add(Tariff.Oid.ToString());
                                                    }
                                                    if (Tariff.TariffService.ServiceType.Name == "Крановая операция")
                                                    {
                                                        TariffLift = Tariff;
                                                    }
                                                }
                                            }
                                            catch { }
                                        }
                                    }
                                    catch { }
                                }
                            }
                        }
                    }
                }
            }
            //if (Stock != null && Container != null && Container.ContainerType != null)
            //{
            //    Connect connect = Connect.FromSession(Session);
            //    dContainerType containerType = Container.ContainerType;
            //    Contract Contract = connect.FindFirstObject<Contract>(x => x.Client == Stock.Client && x.ActiveFlag == true);
            //    if (Contract == null)
            //    {
            //        throw new Exception("Не найден действующий договор для клиента: " + Stock.Client?.Name ?? "[не указано имя клиента]");
            //    }
            //    TariffScale TariffScale = connect.FindFirstObject<TariffScale>(x => x.Contract == Contract && x.ActiveFlag == true);
            //    //if (TariffScale == null)
            //    //{
            //    //    throw new Exception("Не найдена действующая тарифная сетка по договору клиента: " + Stock.Client?.Name ?? "[не указано имя клиента]");
            //    //}
            //    if (TariffScale != null)
            //    {
            //        this.CalcType = TariffScale.CalcType;
            //        try
            //        {
            //            this.Currency = TariffScale.Currency;
            //        }
            //        catch { }
            //        if (TariffScale.CalcType == ECalcType.byDaily)
            //        {
            //            foreach (Tariff Tariff in TariffScale.Tariffs)
            //            {
            //                try
            //                {
            //                    foreach (dContainerSubType ContainerSubType in Tariff.TariffService.ContainerSubTypes)
            //                    {
            //                        try
            //                        {
            //                            if (containerType.ContainerSubType == ContainerSubType)
            //                            {
            //                                if (Tariff.TariffService.ServiceType.Name == TariffType)
            //                                {
            //                                    tariffList.Add(Tariff.Oid.ToString());
            //                                }
            //                                if (Tariff.TariffService.ServiceType.Name == "Крановая операция")
            //                                {
            //                                    TariffLift = Tariff;
            //                                }
            //                            }
            //                        }
            //                        catch { }
            //                    }
            //                }
            //                catch { }
            //            }
            //        }

            //    }
            //}
            //SetTariffPrice();
            return tariffList;
        }

        private void SetTariffPrice()
        {
            try
            {
                if (Container != null && Container.ContainerType != null)
                {
                    EContainerCategorySize ContainerCategorySize = Container.ContainerType.ContainerTypeSize.ContainerCategorySize;
                    if (TariffLift != null)
                    {
                        if (ContainerCategorySize == EContainerCategorySize.twenty)
                        {
                            if (!Container.isLaden)
                                LiftPrice = TariffLift.Value20Empty;
                            else
                                LiftPrice = TariffLift.Value20Laden;
                        }
                        else
                        {
                            if (!Container.isLaden)
                                LiftPrice = TariffLift.Value40Empty;
                            else
                                LiftPrice = TariffLift.Value40Laden;
                        }
                    }
                }
            }
            catch { }
        }
        

        private ECalcType i_CalcType;
        [DisplayName("Вид расчета")]
        public ECalcType CalcType
        {
            get { return i_CalcType; }
            set { SetPropertyValue("CalcType", ref i_CalcType, value); }
        }


        private string i_Notes;
        [Size(1000), DisplayName("Примечание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }

        private string i_RtfText;
        [Size(SizeAttribute.Unlimited), DisplayName("Текст")]
        [EditorAlias("RTF")]
        public string RtfText
        {
            get { return i_RtfText; }
            set { SetPropertyValue("RtfText", ref i_RtfText, value); }
        }

        [Aggregated]
        [ExpandObjectMembers(ExpandObjectMembers.Never)]
        [FileTypeFilter("Скан", 1, "*.pdf", "*.bmp", "*.png", "*.gif", "*.jpg", "*.jpeg")]
        [DisplayName("Pdf")]
        public FileData File { get; set; }

        //[EditorAlias(EditorAliases.DetailPropertyEditor)]
        //private DriverInfo i_DriverInfo;
        //[DisplayName("Водитель")]
        //public DriverInfo DriverInfo
        //{
        //    get { return i_DriverInfo; }
        //    set { SetPropertyValue("DriverInfo", ref i_DriverInfo, value); }
        //}

        #region Информация о водителе
        private string i_DriverName;
        [Size(255), DisplayName("ФИО водителя")]
        public string DriverName
        {
            get { return i_DriverName; }
            set { SetPropertyValue("DriverName", ref i_DriverName, value); }
        }

        private string i_DriverPhoneNumber;
        [Size(32), DisplayName("Номер телефона")]
        public string DriverPhoneNumber
        {
            get { return i_DriverPhoneNumber; }
            set { SetPropertyValue("DriverPhoneNumber", ref i_DriverPhoneNumber, value); }
        }
        private string i_TransportNumber;
        [Size(32), DisplayName("Номер ТС")]
        public string TransportNumber
        {
            get { return i_TransportNumber; }
            set { SetPropertyValue("TransportNumber", ref i_TransportNumber, value); }
        }

        private dTransportType i_TransportType;
        [DisplayName("Тип ТС")]
        public dTransportType TransportType
        {
            get { return i_TransportType; }
            set { SetPropertyValue("TransportType", ref i_TransportType, value); }
        }
        private dTransportMark i_TransportMark;
        [DisplayName("Марка ТС")]
        public dTransportMark TransportMark
        {
            get { return i_TransportMark; }
            set { SetPropertyValue("TransportMark", ref i_TransportMark, value); }
        }
        private string i_TransportCompany;
        [DisplayName("Перевозочная компания")]
        public string TransportCompany
        {
            get { return i_TransportCompany; }
            set { SetPropertyValue("TransportCompany", ref i_TransportCompany, value); }
        }
        #endregion

        private RequestContainersExtradition i_Request;
        [DisplayName("Заявка")]
        public RequestContainersExtradition Request
        {
            get { return i_Request; }
            set { SetPropertyValue("Request", ref i_Request, value); }
        }

        // ссылки на контейнер и водителя заявки
        private RequestDriverInfo i_RequestDriverInfo;
        [DisplayName("Водитель из заявки")]
        [System.ComponentModel.Browsable(false)]
        public RequestDriverInfo RequestDriverInfo
        {
            get { return i_RequestDriverInfo; }
            set { SetPropertyValue("RequestDriverInfo", ref i_RequestDriverInfo, value); }
        }
        private RequestContainerExtraditionInfo i_RequestContainerExtraditionInfo;
        [DisplayName("Информация по контейнеру заявки")]
        [System.ComponentModel.Browsable(false)]
        public RequestContainerExtraditionInfo RequestContainerExtraditionInfo
        {
            get { return i_RequestContainerExtraditionInfo; }
            set { SetPropertyValue("RequestContainerExtraditionInfo", ref i_RequestContainerExtraditionInfo, value); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            ActDate = DateTime.Now.Date;
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    Employee = currentEmpl;
            }
            //ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, 
            //    "Акт приемки контейнеров" + Request.Client.Name + DateTime.Now.Year, string.Empty));
            //+ "/" + String.Format("{0:yyyy}", DateTime.Now.Date);
        }
        protected override void OnSaving()
        {
            base.OnSaving();
        }
    }
}
