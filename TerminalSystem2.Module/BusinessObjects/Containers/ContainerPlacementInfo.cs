﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;
//using TerminalSystem2.Clients;
//using TerminalSystem2.DocFlow;
//using TerminalSystem2.Enums;
//using TerminalSystem2.Tarification;
//using TerminalSystem2.Terminals;

namespace TerminalSystem2.Containers
{
    [ModelDefault("Caption", "Информация по размещению контейнера")]
    [System.ComponentModel.DefaultProperty("ContainerNumber")]
    public class ContainerPlacementInfo : BaseObjectXAF
    {
        public ContainerPlacementInfo(Session session) : base(session) { }


        private string i_ContainerNumber;
        [Size(11), DisplayName("Номер контейнера")]
        public string ContainerNumber
        {
            get { return i_ContainerNumber; }
            set { SetPropertyValue("ContainerNumber", ref i_ContainerNumber, value); }
        }
        //private string i_ContainerNumberString;
        //[Size(4), DisplayName("Номер контейнера буквы")]
        //public string ContainerNumberString
        //{
        //    get { return i_ContainerNumberString; }
        //    set { SetPropertyValue("ContainerNumberString", ref i_ContainerNumberString, value); }
        //}
        //private string i_ContainerNumberInt;
        ////[Custom("EditMask", "([1-9]{1}[0-9]{6}")]
        ////[Custom("EditMaskType", "Simple")]
        //[Size(7), DisplayName("Номер контейнера цифры")]
        //public string ContainerNumberInt
        //{
        //    get { return i_ContainerNumberInt; }
        //    set { SetPropertyValue("ContainerNumberInt", ref i_ContainerNumberInt, value); }
        //}

        private dContainerType i_ContainerType;
        [DisplayName("Тип контейнера")]
        public dContainerType ContainerType
        {
            get { return i_ContainerType; }
            set { SetPropertyValue("ContainerType", ref i_ContainerType, value); }
        }

        private Client i_Owner;
        [DisplayName("Владелец")]
        public Client Owner
        {
            get {
                try { i_Owner = Stock.Client; }
                catch { }
                return i_Owner; }
            set { SetPropertyValue("Owner", ref i_Owner, value); }
        }

        private Stock i_Stock;
        [DisplayName("Сток")]
        [DataSourceProperty("AvailableStocks")]
        [ImmediatePostData]
        public Stock Stock
        {
            get { return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value); }
        }
        private XPCollection<Stock> availableStocks;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Stock> AvailableStocks
        {
            get
            {
                if (availableStocks == null)
                {
                    availableStocks = new XPCollection<Stock>(Session);
                    RefreshAvailableStocks();
                }
                return availableStocks;
            }
        }
        private void RefreshAvailableStocks()
        {
            if (availableStocks == null)
                return;

            availableStocks.Criteria = new InOperator("Oid", GetStocks());
            try
            {
                if (Stock == null)
                    if (availableStocks.Count == 1)
                        Stock = availableStocks[0];
            }
            catch { }

        }
        private List<string> GetStocks()
        {
            List<string> stockList = new List<string>();

            if(this.Terminal != null)
            {
                foreach(Stock st in this.Terminal.Stocks)
                {
                    stockList.Add(st.Oid.ToString());
                }
            }
            return stockList;
        }

        private Terminal i_Terminal;
        [DisplayName("Терминал")]
        [ImmediatePostData]
        [DataSourceProperty("AvailableTerminals")]
        public Terminal Terminal
        {
            get { return i_Terminal; }
            set { SetPropertyValue("Terminal", ref i_Terminal, value);
                OnChanged();
                //Stock = null;
                RefreshAvailableStocks();
            }
        }

        private XPCollection<Terminal> availableTerminals;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Terminal> AvailableTerminals
        {
            get
            {
                if (availableTerminals == null)
                {
                    availableTerminals = new XPCollection<Terminal>(Session);
                    //try
                    //{
                    RefreshAvailableTerminals();
                    //}
                    //catch { throw new Exception("Ошибка при поиске тарифов для клиента"); }
                }
                return availableTerminals;
            }
        }
        private void RefreshAvailableTerminals()
        {
            if (availableTerminals == null)
                return;
           
                availableTerminals.Criteria = new InOperator("Oid", GetTerminals());
            try { if(Terminal == null)
                    Terminal = availableTerminals[0]; }
            catch { }

        }
        private List<string> GetTerminals()
        {
            List<string> terminalList = new List<string>();
            Employee empl = null;
            if (SecuritySystem.CurrentUser != null)
            {
                PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;

                Connect connect = Connect.FromSession(Session);
                empl = connect.FindFirstObject<Employee>(x => x.SysUser.Oid == currentuser.Oid);
                if(empl != null)
                {
                    if(empl.isDirector)
                    {
                        foreach(Terminal ter in connect.FindObjects<Terminal>(x=> true))
                        {
                            terminalList.Add(ter.Oid.ToString());
                        }
                    }
                    else
                    {
                        foreach (Terminal ter in empl.Terminals)
                        {
                            terminalList.Add(ter.Oid.ToString());
                        }
                    }
                }
            }
            return terminalList;
        }

        private string i_Info;
        [Size(1000), DisplayName("Информация")]
        public string Info
        {
            get { return i_Info; }
            set { SetPropertyValue("Info", ref i_Info, value); }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
        }
    }
}
