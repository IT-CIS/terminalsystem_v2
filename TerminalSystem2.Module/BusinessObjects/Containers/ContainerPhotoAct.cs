﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;
using TerminalSystem2.Transport;
//using TerminalSystem2.Clients;
//using TerminalSystem2.DocFlow;
//using TerminalSystem2.Enums;
//using TerminalSystem2.Tarification;
//using TerminalSystem2.Terminals;

namespace TerminalSystem2.Containers
{
    //[NonPersistent]
    [ModelDefault("Caption", "Акт фотографирования контейнера")]
    [System.ComponentModel.DefaultProperty("Name")]
    //[NavigationItem("Терминал")]
    public class ContainerPhotoAct : BaseObjectXAF
    {
        public ContainerPhotoAct(Session session) : base(session) { }

        private string i_Name;
        [Size(255), DisplayName("Описание")]
        public string Name
        {
            get {
                string contNum = "";
                string contType = "";
                try { contNum = Container.ContainerNumber; }
                catch { }
                try { contType = Container.ContainerType.Name; }
                catch { }
                return String.Format("№ контейнера {0}, тип {1}", contNum, contType); }
        }

        //private string i_ActNumber;
        //[Size(255), DisplayName("Номер акта")]
        //public string ActNumber
        //{
        //    get { return i_ActNumber; }
        //    set { SetPropertyValue("ActNumber", ref i_ActNumber, value); }
        //}

        private DateTime i_ActDate;
        [DisplayName("Дата")]
        public DateTime ActDate
        {
            get { return i_ActDate; }
            set { SetPropertyValue("ActDate", ref i_ActDate, value); }
        }
        private int i_ServiceCount;
        [DisplayName("Количество услуги")]
        public int ServiceCount
        {
            get { return i_ServiceCount; }
            set { SetPropertyValue("ServiceCount", ref i_ServiceCount, value); }
        }
        private Employee i_Employee;
        [DisplayName("Регистратор")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }

        //[EditorAlias(EditorAliases.DetailPropertyEditor)]
        private Container i_Container;
        [DisplayName("Контейнер")]
        public Container Container
        {
            get { return i_Container; }
            set { SetPropertyValue("Container", ref i_Container, value); }
        }
        private Tariff i_TariffPhoto;
        [DisplayName("Тариф на фотографирование (ссылка)")]
        [DataSourceProperty("AvailablePhotoTariffs")]
        //[DataSourceCriteria("TariffScale.Client = '@This.Client'")]
        //[ImmediatePostData]
        public Tariff TariffPhoto
        {
            get { return i_TariffPhoto; }
            set { SetPropertyValue("TariffPhoto", ref i_TariffPhoto, value); }
        }
        private double i_PhotoPrice;
        [DisplayName("Тариф на фотографирование")]
        public double PhotoPrice
        {
            get
            {
                //try
                //{
                //    SetTariffPrice();
                //}
                //catch { }
                return i_PhotoPrice;
            }
            set { SetPropertyValue("PhotoPrice", ref i_PhotoPrice, value); }
        }
        private dCurrency i_Currency;
        [DisplayName("Валюта")]
        public dCurrency Currency
        {
            get { return i_Currency; }
            set { SetPropertyValue("Currency", ref i_Currency, value); }
        }
        private XPCollection<Tariff> availablePhotoTariffs;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Tariff> AvailablePhotoTariffs
        {
            get
            {
                if (availablePhotoTariffs == null)
                {
                    availablePhotoTariffs = new XPCollection<Tariff>(Session);
                    //try
                    //{
                        RefreshAvailablePhotoTarrifs();
                    //}
                    //catch { throw new Exception("Ошибка при поиске тарифов для клиента"); }
                }
                return availablePhotoTariffs;
            }
        }
        private void RefreshAvailablePhotoTarrifs()
        {
            if (availablePhotoTariffs == null)
                return;
            if (Container != null)
            {
                availablePhotoTariffs.Criteria = new InOperator("Oid", GetTariffs("Фотографирование контейнера"));
                try { TariffPhoto = availablePhotoTariffs[0]; }
                catch { }
            }
            else
            {
                availablePhotoTariffs = new XPCollection<Tariff>(Session);
            }
            //TariffLift = null;
        }

        private List<string> GetTariffs(string TariffType)
        {
            List<string> tariffList = new List<string>();
            if (Container != null && Container.ContainerType != null)
            {
                Connect connect = Connect.FromSession(Session);
                dContainerType containerType = Container.ContainerType;
                Contract Contract = connect.FindFirstObject<Contract>(x => x.Client == Container.Owner && x.ActiveFlag == true);
                if (Contract == null)
                {
                    throw new Exception("Не найден действующий договор для клиента: " + Stock.Client?.Name ?? "[не указано имя клиента]");
                }
                TariffScale TariffScale = connect.FindFirstObject<TariffScale>(x => x.Contract == Contract && x.ActiveFlag == true);
                //if (TariffScale == null)
                //{
                //    throw new Exception("Не найдена действующая тарифная сетка по договору клиента: " + Stock.Client?.Name ?? "[не указано имя клиента]");
                //}
                if (TariffScale != null)
                {
                    try
                    {
                        this.Currency = TariffScale.Currency;
                    }
                    catch { }
                    foreach (Tariff Tariff in TariffScale.Tariffs)
                    {
                        foreach (dContainerSubType ContainerSubType in Tariff.TariffService.ContainerSubTypes)
                        {
                            if (containerType.ContainerSubType == ContainerSubType)
                            {
                                try
                                {
                                    if (Tariff.TariffService.ServiceType.Name == TariffType)
                                    {
                                        tariffList.Add(Tariff.Oid.ToString());
                                        TariffPhoto = Tariff;
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
            }
            SetTariffPrice();
            //else
            //    throw new Exception("Контейнер = null");
            return tariffList;
        }

        private void SetTariffPrice()
        {
            try
            {
                if (Container != null && Container.ContainerType != null)
                {
                    EContainerCategorySize ContainerCategorySize = Container.ContainerType.ContainerTypeSize.ContainerCategorySize;
                    if (TariffPhoto != null)
                    {
                        if (ContainerCategorySize == EContainerCategorySize.twenty)
                        {
                            if (!Container.isLaden)
                                PhotoPrice = TariffPhoto.Value20Empty;
                            else
                                PhotoPrice = TariffPhoto.Value20Laden;
                        }
                        else
                        {
                            if (!Container.isLaden)
                                PhotoPrice = TariffPhoto.Value40Empty;
                            else
                                PhotoPrice = TariffPhoto.Value40Laden;
                        }
                    }
                    
                }
            }
            catch { }
        }
        //private RequestContainersPlacement i_Request;
        //[DisplayName("Заявка")]
        //public RequestContainersPlacement Request
        //{
        //    get { return i_Request; }
        //    set { SetPropertyValue("Request", ref i_Request, value); }
        //}

        //private RequestContainerInfo i_RequestContainerInfo;
        //[DisplayName("Информация по контейнеру заявки")]
        //[System.ComponentModel.Browsable(false)]
        //public RequestContainerInfo RequestContainerInfo
        //{
        //    get { return i_RequestContainerInfo; }
        //    set { SetPropertyValue("RequestContainerInfo", ref i_RequestContainerInfo, value); }
        //}
        private Stock i_Stock;
        [DisplayName("Сток")]
        [System.ComponentModel.Browsable(false)]
        public Stock Stock
        {
            get { return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            ActDate = DateTime.Now.Date;

            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    Employee = currentEmpl;
            }
            //ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, 
            //    "Акт приемки контейнеров" + Request.Client.Name + DateTime.Now.Year, string.Empty));
            //+ "/" + String.Format("{0:yyyy}", DateTime.Now.Date);
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            //XtraMessageBox.Show("OnSaving");
        }
        protected override void OnSaved()
        {
            base.OnSaved();
            //XtraMessageBox.Show("OnSaved");
        }
        // добавляем подсчет услуги фотографирования контейнеров
        public void AddPhotoServiceDone()
        {

            try
            {
                UnitOfWork unitOfWork = (UnitOfWork)Session;
                Connect connect = Connect.FromUnitOfWork(unitOfWork);

                if (Container != null && TariffPhoto != null)
                {
                    if (ServiceCount != 0)
                    {
                        TariffScale tariffScale = (TariffScale)TariffPhoto?.TariffBase;
                        if (tariffScale != null)
                        {
                            ServiceDone serviceDone = connect.CreateObject<ServiceDone>();
                            serviceDone.Container = Container;
                            serviceDone.Client = Container.Owner;
                            serviceDone.StartDate = ActDate;//DateTime.Now.Date;
                                                            //serviceLiftDone.FinishDate = DateTime.Now.Date;
                            serviceDone.Tariff = TariffPhoto;
                            serviceDone.UnitKind = TariffPhoto?.UnitKind;
                            serviceDone.ServiceCount = ServiceCount;
                            serviceDone.Currency = tariffScale.Currency;

                            // Определям, есть ли скидка и ее тип. Если есть - проставляем ее значение и действие для обеих услуг
                            //либо на все операции какие то проценты до определенной даты, 
                            //либо на на хранение - первые несколько дней  
                            if (tariffScale.AddDiscount)
                            {
                                if (tariffScale.DiscountType == Enums.EDiscountType.byTime)
                                {
                                    // смотрим, не прошла ли еще скидка
                                    if (tariffScale.DiscountActiveThroughDate >= ActDate)
                                    {
                                        serviceDone.Discount = tariffScale.Discount;
                                        serviceDone.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;
                                    }
                                }
                            }

                            // назначаем стоимости единицы услуги
                            if (Container.ContainerType.ContainerTypeSize.ContainerCategorySize == Enums.EContainerCategorySize.twenty)
                            {
                                if (Container.isLaden)
                                {
                                    serviceDone.Value = TariffPhoto.Value20Laden;
                                }
                                else
                                {
                                    serviceDone.Value = TariffPhoto.Value20Empty;
                                }
                            }
                            else
                            {
                                if (Container.isLaden)
                                {
                                    serviceDone.Value = TariffPhoto.Value40Laden;
                                }
                                else
                                {
                                    serviceDone.Value = TariffPhoto.Value40Empty;
                                }
                            }

                            if (serviceDone.DiscountActiveThroughDate != DateTime.MinValue)
                            {
                                if (serviceDone.DiscountActiveThroughDate >= ActDate)
                                {
                                    serviceDone.ValueTotal += serviceDone.ServiceCount * serviceDone.Value * (100 - serviceDone.Discount) / 100;
                                    serviceDone.ValueTotalDate = ActDate;
                                }
                                else
                                {
                                    serviceDone.ValueTotal += serviceDone.Value * serviceDone.ServiceCount;
                                    serviceDone.ValueTotalDate = ActDate;
                                }
                            }
                            else
                            {
                                serviceDone.ValueTotal += serviceDone.Value * serviceDone.ServiceCount;
                                serviceDone.ValueTotalDate = ActDate;
                            }
                            serviceDone.isDone = true;
                            serviceDone.FinishDate = ActDate;

                            serviceDone.Save();
                            unitOfWork.CommitChanges();
                        }
                    }
                }
            }
            catch { }
        }
    }
}
