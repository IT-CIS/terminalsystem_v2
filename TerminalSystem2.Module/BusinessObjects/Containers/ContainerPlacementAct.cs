﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;
using TerminalSystem2.Transport;
//using TerminalSystem2.Clients;
//using TerminalSystem2.DocFlow;
//using TerminalSystem2.Enums;
//using TerminalSystem2.Tarification;
//using TerminalSystem2.Terminals;

namespace TerminalSystem2.Containers
{
    //[NonPersistent]
    [ModelDefault("Caption", "Акт приемки контейнера")]
    [System.ComponentModel.DefaultProperty("Name")]
    //[NavigationItem("Терминал")]
    public class ContainerPlacementAct : BaseObjectXAF
    {
        public ContainerPlacementAct(Session session) : base(session) { }

        private string i_Name;
        [Size(255), DisplayName("Описание")]
        public string Name
        {
            get {
                string contNum = "";
                string contType = "";
                try { contNum = Container.ContainerNumber; }
                catch { }
                try { contType = Container.ContainerType.Name; }
                catch { }
                return String.Format("№ {0}, № контейнера {1}, тип {2}", ActNumber, contNum, contType); }
        }

        private string i_ActNumber;
        [Size(255), DisplayName("Номер акта")]
        public string ActNumber
        {
            get { return i_ActNumber; }
            set { SetPropertyValue("ActNumber", ref i_ActNumber, value); }
        }

        private DateTime i_ActDate;
        [DisplayName("Дата")]
        public DateTime ActDate
        {
            get { return i_ActDate; }
            set { SetPropertyValue("ActDate", ref i_ActDate, value); }
        }

        private Employee i_Employee;
        [DisplayName("Регистратор")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }

        [EditorAlias(EditorAliases.DetailPropertyEditor)]
        private Container i_Container;
        [DisplayName("Контейнер")]
        public Container Container
        {
            get { return i_Container; }
            set { SetPropertyValue("Container", ref i_Container, value); }
        }
        private double i_LiftPrice;
        [DisplayName("Тариф на лифт")]
        public double LiftPrice
        {
            get {
                try
                {
                    SetTariffPrice();
                }
                catch { }
                return i_LiftPrice; }
            set { SetPropertyValue("LiftPrice", ref i_LiftPrice, value); }
        }
        private double i_StoragePrice;
        [DisplayName("Тариф на хранение (в сутки)")]
        public double StoragePrice
        {
            get { return i_StoragePrice; }
            set { SetPropertyValue("StoragePrice", ref i_StoragePrice, value); }
        }
        private dCurrency i_Currency;
        [DisplayName("Валюта")]
        public dCurrency Currency
        {
            get { return i_Currency; }
            set { SetPropertyValue("Currency", ref i_Currency, value); }
        }

        private bool i_AddDiscount;
        [DisplayName("Действует скидка")]
        public bool AddDiscount
        {
            get { return i_AddDiscount; }
            set { SetPropertyValue("AddDiscount", ref i_AddDiscount, value); }
        }

        private EDiscountType i_DiscountType;
        [DisplayName("Тип скидки")]
        public EDiscountType DiscountType
        {
            get { return i_DiscountType; }
            set { SetPropertyValue("DiscountType", ref i_DiscountType, value); }
        }

        private double i_Discount;
        [DisplayName("Скидка, %")]
        [Custom("MinValue", "0"), Custom("MaxValue", "100")]
        public double Discount
        {
            get { return i_Discount; }
            set { SetPropertyValue("Discount", ref i_Discount, value); }
        }
        private int i_DiscountActiveDays;
        [DisplayName("Срок действия скидки, дней")]
        public int DiscountActiveDays
        {
            get { return i_DiscountActiveDays; }
            set { SetPropertyValue("DiscountActiveDays", ref i_DiscountActiveDays, value); }
        }



        private DateTime i_DiscountActiveThroughDate;
        [DisplayName("Скидка действительна до")]
        public DateTime DiscountActiveThroughDate
        {
            get { return i_DiscountActiveThroughDate; }
            set { SetPropertyValue("DiscountActiveThroughDate", ref i_DiscountActiveThroughDate, value); }
        }

        private Tariff i_TariffLift;
        [DisplayName("Тариф на лифт(ссылка)")]
        [DataSourceProperty("AvailableLiftTariffs")]
        //[DataSourceCriteria("TariffScale.Client = '@This.Client'")]
        [ImmediatePostData]
        public Tariff TariffLift
        {
            get { return i_TariffLift; }
            set { SetPropertyValue("TariffLift", ref i_TariffLift, value);
                //OnChanged();
                //if(i_TariffLift!= null)
                //{
                //    if(LiftPrice == 0)
                //    {
                //        LiftPrice = TariffLift.
                //    }
                //}
            }
        }
        
        private XPCollection<Tariff> availableLiftTariffs;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Tariff> AvailableLiftTariffs
        {
            get
            {
                if (availableLiftTariffs == null)
                {
                    availableLiftTariffs = new XPCollection<Tariff>(Session);
                    try
                    {
                        RefreshAvailableLiftTarrifs();
                    }
                    catch {
                        //throw new Exception("Ошибка при поиске тарифов для клиента");
                    }
                }
                return availableLiftTariffs;
            }
        }
        private void RefreshAvailableLiftTarrifs()
        {
            if (availableLiftTariffs == null)
                return;
            if (Stock != null)
            {
                availableLiftTariffs.Criteria = new InOperator("Oid", GetTariffs("Крановая операция"));
                //try { TariffLift = availableLiftTariffs[0]; }
                //catch { }
                
            }
            else
            {
                availableLiftTariffs = new XPCollection<Tariff>(Session);
            }
            //TariffLift = null;
        }

        private List<string> GetTariffs(string TariffType)
        {
            List<string> tariffList = new List<string>();
            if (Stock != null && Container != null && Container.ContainerType != null)
            {
                Connect connect = Connect.FromSession(Session);
                dContainerType containerType = Container.ContainerType;
                Contract Contract = connect.FindFirstObject<Contract>(x => x.Client == Stock.Client && x.ActiveFlag == true);
                if (Contract == null)
                {
                    return tariffList;
                    //throw new Exception("Не найден действующий договор для клиента: " + Stock.Client?.Name ?? "[не указано имя клиента]");
                }
                TariffScale TariffScale = connect.FindFirstObject<TariffScale>(x => x.Contract == Contract && x.ActiveFlag == true);
                //if (TariffScale == null)
                //{
                //    throw new Exception("Не найдена действующая тарифная сетка по договору клиента: " + Stock.Client?.Name ?? "[не указано имя клиента]");
                //}
                if (TariffScale != null)
                {
                    this.CalcType = TariffScale.CalcType;
                    try
                    {
                        this.Currency = TariffScale.Currency;
                    }
                    catch { }
                    try
                    {
                        this.AddDiscount = TariffScale.AddDiscount;
                    }
                    catch { }
                    this.DiscountType = TariffScale.DiscountType;
                    
                    this.Discount = TariffScale.Discount;
                    try
                    {
                        this.DiscountActiveDays = TariffScale.DiscountActiveDays;
                    }
                    catch { }
                    try
                    {
                        this.DiscountActiveThroughDate = TariffScale.DiscountActiveThroughDate;
                    }
                    catch { }
                    if (TariffScale.CalcType == ECalcType.byDaily)
                    {
                        foreach (Tariff Tariff in TariffScale.Tariffs)
                        {
                            try
                            {
                                foreach (dContainerSubType ContainerSubType in Tariff.TariffService.ContainerSubTypes)
                                {
                                    try
                                    {
                                        if (containerType.ContainerSubType == ContainerSubType)
                                        {
                                            if (Tariff.TariffService.ServiceType.Name == TariffType)
                                            {
                                                tariffList.Add(Tariff.Oid.ToString());
                                            }
                                            if (Tariff.TariffService.ServiceType.Name == "Крановая операция")
                                            {
                                                TariffLift = Tariff;
                                            }
                                            if (Tariff.TariffService.ServiceType.Name == "Хранение контейнера")
                                            {
                                                TariffStorage = Tariff;
                                            }
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                        }
                    }
                    
                }
            }
            //SetTariffPrice();
            return tariffList;
        }

        private Tariff i_TariffStorage;
        [DisplayName("Тариф по хранению (ссылка)")]
        [ImmediatePostData]
        [DataSourceProperty("AvailableStorageTariffs")]
        public Tariff TariffStorage
        {
            get { return i_TariffStorage; }
            set { SetPropertyValue("TariffStorage", ref i_TariffStorage, value); }
        }

        public void SetTariffs()
        {
            if (Stock != null && Container != null && Container.ContainerType != null)
            {
                dContainerType containerType = Container.ContainerType;
                Connect connect = Connect.FromSession(Session);
                Contract Contract = connect.FindFirstObject<Contract>(x => x.Client == Stock.Client && x.ActiveFlag == true);
                if (Contract != null)
                {
                    TariffScale TariffScale = connect.FindFirstObject<TariffScale>(x => x.Contract == Contract && x.ActiveFlag == true);
                    if (TariffScale != null)
                    {
                        this.CalcType = TariffScale.CalcType;
                        this.Currency = TariffScale.Currency;
                        foreach (Tariff Tariff in TariffScale.Tariffs)
                        {
                            foreach(dContainerSubType ContainerSubType in Tariff.TariffService.ContainerSubTypes)
                            {
                                if(containerType.ContainerSubType == ContainerSubType)
                                {
                                    if (Tariff.TariffService.ServiceType.Name == "Крановая операция")
                                    {
                                        TariffLift = Tariff;
                                    }
                                    if (Tariff.TariffService.ServiceType.Name == "Хранение контейнера")
                                    {
                                        TariffStorage = Tariff;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void SetTariffPrice()
        {
            try
            {
                if (Container != null && Container.ContainerType != null)
                {
                    EContainerCategorySize ContainerCategorySize = Container.ContainerType.ContainerTypeSize.ContainerCategorySize;
                    if (TariffLift != null)
                    {
                        if (ContainerCategorySize == EContainerCategorySize.twenty)
                        {
                            if (!Container.isLaden)
                                LiftPrice = TariffLift.Value20Empty;
                            else
                                LiftPrice = TariffLift.Value20Laden;
                        }
                        else
                        {
                            if (!Container.isLaden)
                                LiftPrice = TariffLift.Value40Empty;
                            else
                                LiftPrice = TariffLift.Value40Laden;
                        }
                    }
                    if (TariffStorage != null)
                    {
                        if (ContainerCategorySize == EContainerCategorySize.twenty)
                        {
                            if (!Container.isLaden)
                                StoragePrice = TariffStorage.Value20Empty;
                            else
                                StoragePrice = TariffStorage.Value20Laden;
                        }
                        else
                        {
                            if (!Container.isLaden)
                                StoragePrice = TariffStorage.Value40Empty;
                            else
                                StoragePrice = TariffStorage.Value40Laden;
                        }
                    }
                }
            }
            catch { }
        }
        private XPCollection<Tariff> availableStorageTariffs;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Tariff> AvailableStorageTariffs
        {
            get
            {
                if (availableStorageTariffs == null)
                {
                    availableStorageTariffs = new XPCollection<Tariff>(Session);
                    try
                    {
                        RefreshAvailableStorageTariffs();
                    }
                    catch { throw new Exception("Ошибка при поиске тарифов для клиента"); }
                }
                return availableStorageTariffs;
            }
        }
        private void RefreshAvailableStorageTariffs()
        {
            if (availableStorageTariffs == null)
                return;
            if (Stock != null)
            {
                availableStorageTariffs.Criteria = new InOperator("Oid", GetTariffs("Хранение контейнера"));
                //TariffStorage = availableStorageTariffs.FirstOrDefault();
            }
            else
            {
                availableStorageTariffs = new XPCollection<Tariff>(Session);
            }
            //TariffStorage = null;
        }

        private ECalcType i_CalcType;
        [DisplayName("Вид расчета")]
        public ECalcType CalcType
        {
            get { return i_CalcType; }
            set { SetPropertyValue("CalcType", ref i_CalcType, value); }
        }
        //private string i_Notes;
        //[Size(1000), DisplayName("Примечание")]
        //public string Notes
        //{
        //    get { return i_Notes; }
        //    set { SetPropertyValue("Notes", ref i_Notes, value); }
        //}
        //[EditorAlias(EditorAliases.DetailPropertyEditor)]
        //private DriverInfo i_DriverInfo;
        //[DisplayName("Водитель")]
        //public DriverInfo DriverInfo
        //{
        //    get { return i_DriverInfo; }
        //    set { SetPropertyValue("DriverInfo", ref i_DriverInfo, value); }
        //}

        #region Информация о водителе
        private string i_DriverName;
        [Size(255), DisplayName("ФИО водителя")]
        public string DriverName
        {
            get { return i_DriverName; }
            set { SetPropertyValue("DriverName", ref i_DriverName, value); }
        }

        private string i_DriverPhoneNumber;
        [Size(32), DisplayName("Номер телефона")]
        public string DriverPhoneNumber
        {
            get { return i_DriverPhoneNumber; }
            set { SetPropertyValue("DriverPhoneNumber", ref i_DriverPhoneNumber, value); }
        }
        private string i_TransportNumber;
        [Size(32), DisplayName("Номер ТС")]
        public string TransportNumber
        {
            get { return i_TransportNumber; }
            set { SetPropertyValue("TransportNumber", ref i_TransportNumber, value); }
        }

        private dTransportType i_TransportType;
        [DisplayName("Тип ТС")]
        public dTransportType TransportType
        {
            get { return i_TransportType; }
            set { SetPropertyValue("TransportType", ref i_TransportType, value); }
        }
        private dTransportMark i_TransportMark;
        [DisplayName("Марка ТС")]
        public dTransportMark TransportMark
        {
            get { return i_TransportMark; }
            set { SetPropertyValue("TransportMark", ref i_TransportMark, value); }
        }
        private string i_TransportCompany;
        [DisplayName("Перевозочная компания")]
        public string TransportCompany
        {
            get { return i_TransportCompany; }
            set { SetPropertyValue("TransportCompany", ref i_TransportCompany, value); }
        }
        #endregion

        private RequestContainersPlacement i_Request;
        [DisplayName("Заявка")]
        public RequestContainersPlacement Request
        {
            get { return i_Request; }
            set { SetPropertyValue("Request", ref i_Request, value); }
        }

        // ссылки на контейнер и водителя заявки
        private RequestDriverInfo i_RequestDriverInfo;
        [DisplayName("Водитель из заявки")]
        [System.ComponentModel.Browsable(false)]
        public RequestDriverInfo RequestDriverInfo
        {
            get { return i_RequestDriverInfo; }
            set { SetPropertyValue("RequestDriverInfo", ref i_RequestDriverInfo, value); }
        }
        private RequestContainerInfo i_RequestContainerInfo;
        [DisplayName("Информация по контейнеру заявки")]
        [System.ComponentModel.Browsable(false)]
        public RequestContainerInfo RequestContainerInfo
        {
            get { return i_RequestContainerInfo; }
            set { SetPropertyValue("RequestContainerInfo", ref i_RequestContainerInfo, value); }
        }
        private Stock i_Stock;
        [DisplayName("Сток")]
        [System.ComponentModel.Browsable(false)]
        public Stock Stock
        {
            get { return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value); }
        }

        private string i_RtfText;
        [Size(SizeAttribute.Unlimited), DisplayName("Текст")]
        [EditorAlias("RTF")]
        public string RtfText
        {
            get { return i_RtfText; }
            set { SetPropertyValue("RtfText", ref i_RtfText, value); }
        }

        //[DisplayName("Pdf")]
        //[Size(SizeAttribute.Unlimited)]
        //public byte[] DocPDF
        //{
        //    get { return GetPropertyValue<byte[]>("DocPDF"); }
        //    set { SetPropertyValue<byte[]>("DocPDF", value); }
        //}

        [Aggregated]
        [ExpandObjectMembers(ExpandObjectMembers.Never)]
        [FileTypeFilter("Скан", 1, "*.pdf", "*.bmp", "*.png", "*.gif", "*.jpg", "*.jpeg")]
        [DisplayName("Pdf")]
        public FileData File { get; set; }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            ActDate = DateTime.Now.Date;
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    Employee = currentEmpl;
            }
            //ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, 
            //    "Акт приемки контейнеров" + Request.Client.Name + DateTime.Now.Year, string.Empty));
            //+ "/" + String.Format("{0:yyyy}", DateTime.Now.Date);
        }
        protected override void OnSaving()
        {
            base.OnSaving();
        }
    }
}
