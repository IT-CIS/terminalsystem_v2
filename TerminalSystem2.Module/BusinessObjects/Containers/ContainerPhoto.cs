﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;

namespace TerminalSystem2.Containers
{
    [ModelDefault("Caption", "Фото контейнера")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class ContainerPhoto : BaseObjectXAF
    {
        public ContainerPhoto(Session session) : base(session) { }

        [DisplayName("Фото контейнера")]
        [Size(SizeAttribute.Unlimited)]//, VisibleInListView(false)]
        [ImageEditor(DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
            ListViewImageEditorMode = ImageEditorMode.DropDownPictureEdit,
            DetailViewImageEditorFixedHeight = 400, DetailViewImageEditorFixedWidth = 400,
            ListViewImageEditorCustomHeight =200, ImageSizeMode = ImageSizeMode.StretchImage)]
        public byte[] PhotoContainer
        {
            get { return GetPropertyValue<byte[]>("PhotoContainer"); }
            set { SetPropertyValue<byte[]>("PhotoContainer", value); }
        }

        private string i_Name;
        [Size(255), DisplayName("Наименование фото")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        private DateTime i_PhotoDate;
        [Size(255), DisplayName("Дата фото")]
        public DateTime PhotoDate
        {
            get { return i_PhotoDate; }
            set { SetPropertyValue("PhotoDate", ref i_PhotoDate, value); }
        }
        private Container i_Container;
        [Association, DisplayName("Контейнер")]
        public Container Container
        {
            get { return i_Container; }
            set { SetPropertyValue("Container", ref i_Container, value); }
        }
    }
}
