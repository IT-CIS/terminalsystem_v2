﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Enums;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.Containers
{
    [ModelDefault("Caption", "История движения контейнера")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class ContainerHistory : BaseObjectXAF
    {
        public ContainerHistory(Session session) : base(session) { }

        private EContainerHistoryKind i_EContainerHistoryKind;
        [DisplayName("Вид движения")]
        public EContainerHistoryKind ContainerHistoryKind
        {
            get { return i_EContainerHistoryKind; }
            set { SetPropertyValue("ContainerHistoryKind", ref i_EContainerHistoryKind, value); }
        }

        private string i_Name;
        [Size(255), DisplayName("Описание собятия")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        private DateTime i_Date;
        [DisplayName("Дата")]
        public DateTime Date
        {
            get { return i_Date; }
            set { SetPropertyValue("Date", ref i_Date, value); }
        }
        private string i_Time;
        [DisplayName("Время")]
        public string Time
        {
            get { return i_Time; }
            set { SetPropertyValue("Time", ref i_Time, value); }
        }

        private Container i_Container;
        [Association, DisplayName("Контейнер")]
        public Container Container
        {
            get { return i_Container; }
            set { SetPropertyValue("Container", ref i_Container, value); }
        }
        // события размещения и выдачи
        private Stock i_Stock;
        [Association, DisplayName("Сток")]
        public Stock Stock
        {
            get { return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value); }
        }
        private ContainerPlacementAct i_ContainerPlacementAct;
        [DisplayName("Акт приемки контейнера")]
        public ContainerPlacementAct ContainerPlacementAct
        {
            get { return i_ContainerPlacementAct; }
            set { SetPropertyValue("ContainerPlacementAct", ref i_ContainerPlacementAct, value); }
        }
        private ContainerExtraditionAct i_ContainerExtraditionAct;
        [DisplayName("Акт выдачи контейнера")]
        public ContainerExtraditionAct ContainerExtraditionAct
        {
            get { return i_ContainerExtraditionAct; }
            set { SetPropertyValue("ContainerExtraditionAct", ref i_ContainerExtraditionAct, value); }
        }
        private string i_Notes;
        [Size(1000), DisplayName("Примечание")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }
        //private Stock i_StockOut;
        //[Association, DisplayName("Удаление из стока")]
        //public Stock StockOut
        //{
        //    get { return i_StockOut; }
        //    set { SetPropertyValue("StockOut", ref i_StockOut, value); }
        //}
        #region Информация о водителе
        private string i_DriverName;
        [Size(255), DisplayName("ФИО водителя")]
        public string DriverName
        {
            get { return i_DriverName; }
            set { SetPropertyValue("DriverName", ref i_DriverName, value); }
        }

        private string i_DriverPhoneNumber;
        [Size(32), DisplayName("Номер телефона")]
        public string DriverPhoneNumber
        {
            get { return i_DriverPhoneNumber; }
            set { SetPropertyValue("DriverPhoneNumber", ref i_DriverPhoneNumber, value); }
        }
        private string i_TransportNumber;
        [Size(32), DisplayName("Номер ТС")]
        public string TransportNumber
        {
            get { return i_TransportNumber; }
            set { SetPropertyValue("TransportNumber", ref i_TransportNumber, value); }
        }

        private dTransportType i_TransportType;
        [DisplayName("Тип ТС")]
        public dTransportType TransportType
        {
            get { return i_TransportType; }
            set { SetPropertyValue("TransportType", ref i_TransportType, value); }
        }
        #endregion
        #region Информация по грузу
        private bool i_isLaden;
        [DisplayName("Груженый")]
        public bool isLaden
        {
            get { return i_isLaden; }
            set { SetPropertyValue("isLaden", ref i_isLaden, value); }
        }
        private double i_LadenWeight;
        [DisplayName("Вес груженого контейнера, кг")]
        public double LadenWeight
        {
            get { return i_LadenWeight; }
            set { SetPropertyValue("LadenWeight", ref i_LadenWeight, value); }
        }
        private string i_CargoType;
        [Size(255), DisplayName("Характер груза")]
        public string CargoType
        {
            get { return i_CargoType; }
            set { SetPropertyValue("CargoType", ref i_CargoType, value); }
        }
        private string i_SealNumber;
        [Size(255), DisplayName("Номер пломбы")]
        public string SealNumber
        {
            get { return i_SealNumber; }
            set { SetPropertyValue("SealNumber", ref i_SealNumber, value); }
        }
        #endregion
    }
}
