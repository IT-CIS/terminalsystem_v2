﻿using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Transport;

namespace TerminalSystem2.DocFlow
{
    [ModelDefault("Caption", "Информация по водителю заявки")]
    [System.ComponentModel.DefaultProperty("BrifName")]
    public class RequestDriverInfo : BaseObjectXAF
    {
        public RequestDriverInfo(Session session) : base(session) { }

        private RequestBaseDriversInfo i_Request;
        [Association, DisplayName("Заявка")]
        public RequestBaseDriversInfo Request
        {
            get { return i_Request; }
            set { SetPropertyValue("Request", ref i_Request, value); }
        }

        //[EditorAlias(EditorAliases.DetailPropertyEditor)]
        //private DriverInfo i_DriverInfo;
        //[DisplayName("Водитель")]
        //public DriverInfo DriverInfo
        //{
        //    get { return i_DriverInfo; }
        //    set { SetPropertyValue("DriverInfo", ref i_DriverInfo, value); }
        //}
        #region Информация о водителе
        private string i_DriverName;
        [Size(255), DisplayName("ФИО водителя")]
        public string DriverName
        {
            get { return i_DriverName; }
            set { SetPropertyValue("DriverName", ref i_DriverName, value); }
        }

        private string i_DriverPhoneNumber;
        [Size(32), DisplayName("Номер телефона")]
        public string DriverPhoneNumber
        {
            get { return i_DriverPhoneNumber; }
            set { SetPropertyValue("DriverPhoneNumber", ref i_DriverPhoneNumber, value); }
        }
        private string i_TransportNumber;
        [Size(32), DisplayName("Номер ТС")]
        public string TransportNumber
        {
            get { return i_TransportNumber; }
            set { SetPropertyValue("TransportNumber", ref i_TransportNumber, value); }
        }

        private dTransportType i_TransportType;
        [DisplayName("Тип ТС")]
        public dTransportType TransportType
        {
            get { return i_TransportType; }
            set { SetPropertyValue("TransportType", ref i_TransportType, value); }
        }
        private dTransportMark i_TransportMark;
        [DisplayName("Марка ТС")]
        public dTransportMark TransportMark
        {
            get { return i_TransportMark; }
            set { SetPropertyValue("TransportMark", ref i_TransportMark, value); }
        }
        private string i_TransportCompany;
        [DisplayName("Перевозочная компания")]
        public string TransportCompany
        {
            get { return i_TransportCompany; }
            set { SetPropertyValue("TransportCompany", ref i_TransportCompany, value); }
        }
        #endregion

        
        //private DateTime i_ArrivalPlanTime;
        //[ModelDefault("DisplayFormat", "{0: hh:mm}")]
        //[ModelDefault("EditMask", "hh:mm")]
        //[DisplayName("Предполагаемое время приезда")]
        //public DateTime ArrivalPlanTime
        //{
        //    get { return i_ArrivalPlanTime; }
        //    set { SetPropertyValue("ArrivalPlanTime", ref i_ArrivalPlanTime, value); }
        //}
        //[Association, DisplayName("Информация по контейнерам")]
        //public XPCollection<RequestContainerInfo> RequestContainerInfos
        //{
        //    get { return GetCollection<RequestContainerInfo>("RequestContainerInfos"); }
        //}

    }
}
