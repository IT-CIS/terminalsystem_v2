﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Clients;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.DocFlow
{
    [ModelDefault("Caption", "Договор"), NavigationItem("Договоры")]
    [System.ComponentModel.DefaultProperty("RegNumber")]
    public class Contract : AttachBase
    {
        public Contract(Session session) : base(session) { }

        private string i_RegNumber;
        [Size(255), DisplayName("Номер документа")]
        public string RegNumber
        {
            get { return i_RegNumber; }
            set { SetPropertyValue("RegNumber", ref i_RegNumber, value); }
        }

        private DateTime i_StartDate;
        [DisplayName("Дата начала действия документа")]
        public DateTime StartDate
        {
            get { return i_StartDate; }
            set { SetPropertyValue("StartDate", ref i_StartDate, value); }
        }

        private DateTime i_RequestStopDate;
        [DisplayName("Дата окончания действия документа")]
        public DateTime RequestStopDate
        {
            get { return i_RequestStopDate; }
            set { SetPropertyValue("RequestStopDate", ref i_RequestStopDate, value); }
        }

        private string i_Descript;
        [Size(SizeAttribute.Unlimited), DisplayName("Краткое содержание")]
        public string Descript
        {
            get { return i_Descript; }
            set { SetPropertyValue("Descript", ref i_Descript, value); }
        }

        private Employee i_RegEmployee;
        [DisplayName("Регистратор документа")]
        public Employee RegEmployee
        {
            get { return i_RegEmployee; }
            set { SetPropertyValue("RegEmployee", ref i_RegEmployee, value); }
        }
        private bool i_ActiveFlag;
        [DisplayName("Действующий договор")]
        [ImmediatePostData]
        public bool ActiveFlag
        {
            get { return i_ActiveFlag; }
            set { SetPropertyValue("ActiveFlag", ref i_ActiveFlag, value); }
        }
        private string i_Note;
        [Size(SizeAttribute.Unlimited), DisplayName("Дополнительная информация")]
        public string Note
        {
            get { return i_Note; }
            set { SetPropertyValue("Note", ref i_Note, value); }
        }

        [Association, DisplayName("Тарифные сетки")]
        public XPCollection<TariffScale> TariffScales
        {
            get { return GetCollection<TariffScale>("TariffScales"); }
        }

        private TerminalOrg i_TerminalOrg;
        [Association, DisplayName("Владелец терминала")]
        public TerminalOrg TerminalOrg
        {
            get { return i_TerminalOrg; }
            set { SetPropertyValue("TerminalOrg", ref i_TerminalOrg, value); }
        }

        private Client i_Client;
        [Association, DisplayName("Клиент")]
        public Client Client
        {
            get { return i_Client; }
            set { SetPropertyValue("Client", ref i_Client, value); }
        }

        //[Association, DisplayName("Терминалы")]
        //public XPCollection<Terminal> Terminals
        //{
        //    get { return GetCollection<Terminal>("Terminals"); }
        //}

        //private Terminal i_Terminal;
        //[DisplayName("Терминал")]
        //public Terminal Terminal
        //{
        //    get { return i_Terminal; }
        //    set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_RegEmployee = currentEmpl;
            }
            i_StartDate = DateTime.Now.Date;
            ActiveFlag = true;
            Connect connect = Connect.FromSession(Session);
            if(TerminalOrg == null)
            {
                TerminalOrg = connect.FindFirstObject<TerminalOrg>(x => true);
            }
        }

        protected override void OnSaving()
        {
            bool newContractFlag = true;

            //if (i_Terminal != null && i_Client != null)
            //{
            //    /*
            //    // Поиск договора обслуживания клиента в заданном терминале
            //    Guid termID = i_Terminal.Oid;
            //    Guid clientID = i_Client.Oid;

            //    CriteriaOperator linkCriteria = CriteriaOperator.Parse("[Terminal] = ? AND [Client] = ?", termID, clientID);



            //    XPCollection<Contract> contract = new XPCollection<Contract>(Session, linkCriteria);
            //    if (contract.Count > 0)
            //    {
            //        newContractFlag = false;
            //    }
            //    */

            //    Connect connect = Connect.FromSession(Session);
            //    List<Contract> contracts = connect.FindObjects<Contract>(obj => obj.Terminal == i_Terminal && obj.Client == i_Client).ToList();

            //    if (contracts.Count != 0)
            //    {
            //        newContractFlag = false;
            //    }

            //    // Если создается новый договор (не было найдено договора обслуживания), клонировать базовый тариф для заданного терминала и привязать его к клиенту
            //    if (newContractFlag)
            //    {
            //        try
            //        {
            //            TariffBase tariffBase = connect.FindFirstObject<TariffBase>(mc => mc.Terminal == i_Terminal);
            //            TariffScale newTariffScale = connect.CreateObject<TariffScale>();
            //            foreach (Tariff tariff in tariffBase.Tariffs)
            //            {
            //                Tariff clientTariff = connect.CreateObject<Tariff>();
            //                clientTariff.Description = tariff.Description;
            //                clientTariff.TariffService = tariff.TariffService;
            //                clientTariff.UnitKind = tariff.UnitKind;
            //                clientTariff.Value20Empty = tariff.Value20Empty;
            //                clientTariff.Value20Laden = tariff.Value20Laden;
            //                clientTariff.Value40Empty = tariff.Value40Empty;
            //                clientTariff.Value40Laden = tariff.Value40Laden;
            //                clientTariff.Currency = tariff.Currency;

            //                newTariffScale.Tariffs.Add(clientTariff);
            //                clientTariff.Save();
            //            }
            //            this.TariffScales.Add(newTariffScale);
            //            newTariffScale.Save();
            //        }
            //        catch { }
            //    }
            //}

            //    UnitOfWork unitOfWork = (UnitOfWork)Session;
            //    if(IsFinished)
            //    {
            //        RequestStopDate = DateTime.Now.Date;
            //        if(RequestType == BusinessObjects.RequestType.placement)
            //        {
            //            foreach(Container container in Containers)
            //            {
            //                container.onTerminal = true;
            //                container.Save();
            //            }
            //        }
            //        else
            //        {
            //            foreach (Container container in Containers)
            //            {
            //                container.onTerminal = false;
            //                container.Save();
            //            }
            //        }
            //        if (SecuritySystem.CurrentUser != null)
            //        {
            //            try
            //            {
            //                Employee currentEmpl = Session.FindObject<Employee>(
            //                    new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
            //                if (currentEmpl != null)
            //                    i_Employee = currentEmpl;
            //            }
            //            catch { }
            //        }
            //    }
            //    base.OnSaving();
        }

    }
}
