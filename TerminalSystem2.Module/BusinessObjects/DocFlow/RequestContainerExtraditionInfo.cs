﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.Enums;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.DocFlow
{
    [ModelDefault("Caption", "Информация по контейнеру в заявке")]
    [System.ComponentModel.DefaultProperty("BrifName")]
    public class RequestContainerExtraditionInfo : BaseObjectXAF
    {
        public RequestContainerExtraditionInfo(Session session) : base(session) { }

        private RequestContainersExtradition i_Request;
        [Association, DisplayName("Заявка")]
        public RequestContainersExtradition Request
        {
            get { return i_Request; }
            set { SetPropertyValue("Request", ref i_Request, value); }
        }
        // to do - выбор из указанных в заявке водил
        private RequestDriverInfo i_RequestDriverInfo;
        [DisplayName("Водитель")]
        [DataSourceProperty("AvailableDrivers")]
        public RequestDriverInfo RequestDriverInfo
        {
            get { return i_RequestDriverInfo; }
            set { SetPropertyValue("RequestDriverInfo", ref i_RequestDriverInfo, value); }
        }

        // Получаем возможные для выбора терминалы, которые указаны у клиента, при условии, что это не терминальный сток
        private XPCollection<RequestDriverInfo> availableDrivers;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<RequestDriverInfo> AvailableDrivers
        {
            get
            {
                if (availableDrivers == null)
                {
                    // Retrieve all Terminals objects 
                    availableDrivers = new XPCollection<RequestDriverInfo>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableDrivers();
                }
                return availableDrivers;
            }
        }
        private void RefreshAvailableDrivers()
        {
            if (availableDrivers == null)
                return;
            if (Request != null)

            {

                availableDrivers = Request.RequestDriverInfos;
            }
            else
                availableDrivers = new XPCollection<RequestDriverInfo>(Session);
            RequestDriverInfo = null;
        }

        //private bool i_isContainerOnTerminal;
        //[DisplayName("Контейнер на терминале")]
        //[ImmediatePostData]
        //public bool isContainerOnTerminal
        //{
        //    get { return i_isContainerOnTerminal; }
        //    set { SetPropertyValue("isContainerOnTerminal", ref i_isContainerOnTerminal, value); }
        //}

        private string i_Number;
        [Size(11), DisplayName("Номер контейнера")]
        public string Number
        {
            get { return i_Number; }
            set { SetPropertyValue("Number", ref i_Number, value); }
        }
        private Container i_Container;
        [DisplayName("Контейнер")]
        [DataSourceProperty("AvailableContainers")]
        [ImmediatePostData]
        public Container Container
        {
            get { return i_Container; }
            set { 
                bool isChanged = SetPropertyValue("Container", ref i_Container, value);
                if (isChanged)
                    FillContainerData();
            }
        }
        private void FillContainerData()
        {
            if(i_Container != null)
            {
                Number = i_Container.ContainerNumber;
                ContainerType = i_Container.ContainerType;
                Stock = i_Container.Stock;
            }
            else
            {
                Number = "";
                ContainerType = null;
                Stock = null;
            }
        }
        #region AvailableContainers
        private XPCollection<Container> availableContainers;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Container> AvailableContainers
        {
            get
            {
                if (availableContainers == null)
                {
                    availableContainers = new XPCollection<Container>(Session);
                    RefreshAvailableContainers();
                }
                return availableContainers;
            }
        }
        private void RefreshAvailableContainers()
        {
            if (availableContainers == null)
                return;
            availableContainers.Criteria = new InOperator("Oid", GetAvailableContainers());

            //Container = null;
        }
        private List<string> GetAvailableContainers()
        {
            List<string> containers = new List<string>();
            if (Stock != null)
            {
                foreach (var cont in Stock.Containers.Where(x => x.onTerminal == true))
                {
                    containers.Add(cont.Oid.ToString());
                }
            }
            else
            {
                if (Request != null && Request.Client != null && Request.Terminal != null && Request.Client.Containers != null
                    && Request.Client.Containers.Count != 0)
                {
                    foreach (var cont in Request.Client.Containers.Where(x => x.onTerminal == true &&
                    x.Terminal.Oid == Request.Terminal.Oid))
                    {
                        containers.Add(cont.Oid.ToString());
                    }
                    //containers.AddRange(Request.Client.Containers.Where(x => x.onTerminal == true).Select(x => Oid.ToString()));
                }
            }

            return containers;
        }
        #endregion


        private dContainerType i_ContainerType;
        [DisplayName("Тип контейнера")]
        public dContainerType ContainerType
        {
            get { return i_ContainerType; }
            set { SetPropertyValue("ContainerType", ref i_ContainerType, value); }
        }

        // to do - выбор из стоков клиента (из заявки) указанного терминала (из заявки)
        private Stock i_Stock;
        [DisplayName("Сток")]
        [ImmediatePostData]
        [DataSourceCriteria("Client = '@This.Request.Client' AND Terminal = '@This.Request.Terminal'")]
        public Stock Stock
        {
            get {
                if (Request != null)
                    if (Request.Client != null)
                        if (Request.Client.Stocks.Count == 1)
                            i_Stock = Request.Client.Stocks[0];
                 return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value);
                RefreshAvailableContainers();
            }
        }


        private DateTime i_ArrivalPlanDateFrom;
        [DisplayName("с")]
        public DateTime ArrivalPlanDateFrom
        {
            get { return i_ArrivalPlanDateFrom; }
            set { SetPropertyValue("ArrivalPlanDateFrom", ref i_ArrivalPlanDateFrom, value); }
        }
        private DateTime i_ArrivalPlanDateTo;
        [DisplayName("по")]
        public DateTime ArrivalPlanDateTo
        {
            get { return i_ArrivalPlanDateTo; }
            set { SetPropertyValue("ArrivalPlanDateTo", ref i_ArrivalPlanDateTo, value); }
        }

        private ERequestStatus i_RequestStatus;
        [DisplayName("Статус")]
        public ERequestStatus RequestStatus
        {
            get { return i_RequestStatus; }
            set { SetPropertyValue("RequestStatus", ref i_RequestStatus, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            RefreshAvailableContainers();
        }
    }
}
