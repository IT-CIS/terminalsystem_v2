﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Enums;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.DocFlow
{
    [ModelDefault("Caption", "Информация по контейнеру в заявке")]
    [System.ComponentModel.DefaultProperty("BrifName")]
    public class RequestContainerInfo : BaseObjectXAF
    {
        public RequestContainerInfo(Session session) : base(session) { }

        private RequestContainersPlacement i_Request;
        [Association, DisplayName("Заявка")]
        public RequestContainersPlacement Request
        {
            get { return i_Request; }
            set { SetPropertyValue("Request", ref i_Request, value); }
        }
        // to do - выбор из указанных в заявке водил
        private RequestDriverInfo i_RequestDriverInfo;
        [DisplayName("Водитель")]
        [DataSourceProperty("AvailableDrivers")]
        public RequestDriverInfo RequestDriverInfo
        {
            get { return i_RequestDriverInfo; }
            set { SetPropertyValue("RequestDriverInfo", ref i_RequestDriverInfo, value); }
        }

        // Получаем возможные для выбора терминалы, которые указаны у клиента, при условии, что это не терминальный сток
        private XPCollection<RequestDriverInfo> availableDrivers;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<RequestDriverInfo> AvailableDrivers
        {
            get
            {
                if (availableDrivers == null)
                {
                    // Retrieve all Terminals objects 
                    availableDrivers = new XPCollection<RequestDriverInfo>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableDrivers();
                }
                return availableDrivers;
            }
        }
        private void RefreshAvailableDrivers()
        {
            if (availableDrivers == null)
                return;
            if (Request != null)

            {

                availableDrivers = Request.RequestDriverInfos;
                if (availableDrivers.Count == 1)
                    RequestDriverInfo = availableDrivers[0];
            }
            else
                availableDrivers = new XPCollection<RequestDriverInfo>(Session);
            RequestDriverInfo = null;
        }

        private string i_Number;
        [Size(11), DisplayName("Номер контейнера")]
        public string Number
        {
            get { return i_Number; }
            set { SetPropertyValue("Number", ref i_Number, value); }
        }

        private dContainerType i_ContainerType;
        [DisplayName("Тип контейнера")]
        public dContainerType ContainerType
        {
            get { return i_ContainerType; }
            set { SetPropertyValue("ContainerType", ref i_ContainerType, value); }
        }

        // to do - выбор из стоков клиента (из заявки) указанного терминала (из заявки)
        private Stock i_Stock;
        [DisplayName("Сток")]
        [DataSourceProperty("AvailableStocks")]
        //[DataSourceCriteria("Client = '@This.Request.Client'")]
        public Stock Stock
        {
            get {
                //if (Request != null)
                //    if (Request.Client != null)
                //        if (Request.Client.Stocks.Count == 1)
                //            i_Stock = Request.Client.Stocks[0];
                 return i_Stock; }
            set { SetPropertyValue("Stock", ref i_Stock, value); }
        }
        // Получаем возможные для выбора терминалы, которые указаны у клиента, при условии, что это не терминальный сток
        private XPCollection<Stock> availableStocks;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Stock> AvailableStocks
        {
            get
            {
                if (availableStocks == null)
                {
                    // Retrieve all Terminals objects 
                    availableStocks = new XPCollection<Stock>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableStocks();
                }
                return availableStocks;
            }
        }
        private void RefreshAvailableStocks()
        {
            if (availableStocks == null)
                return;
            if (Request != null)

            {
                availableStocks.Criteria = new InOperator("Oid", GetStocks());
                if (availableStocks.Count == 1)
                    Stock = availableStocks[0];
            }
            else
                availableStocks = new XPCollection<Stock>(Session);
            RequestDriverInfo = null;
        }
        private List<string> GetStocks()
        {
            List<string> stockList = new List<string>();
            if(Request!= null && Request.Client != null)
            {
                foreach (Stock stock in Request.Client.Stocks)
                {
                    if (Request.Terminal != null)
                    {
                        if (stock.Terminal == Request.Terminal)
                            stockList.Add(stock.Oid.ToString());
                    }
                    else
                    {
                        stockList.Add(stock.Oid.ToString());
                    }
                }
            }
            return stockList;
        }

        private bool i_isLaden;
        [DisplayName("Груженый")]
        public bool isLaden
        {
            get { return i_isLaden; }
            set { SetPropertyValue("isLaden", ref i_isLaden, value); }
        }
        private double i_LadenWeight;
        [DisplayName("Вес груженого контейнера, кг")]
        public double LadenWeight
        {
            get { return i_LadenWeight; }
            set { SetPropertyValue("LadenWeight", ref i_LadenWeight, value); }
        }
        private string i_CargoType;
        [Size(255), DisplayName("Характер груза")]
        public string CargoType
        {
            get { return i_CargoType; }
            set { SetPropertyValue("CargoType", ref i_CargoType, value); }
        }
        private string i_SealNumber;
        [Size(255), DisplayName("Номер пломбы")]
        public string SealNumber
        {
            get { return i_SealNumber; }
            set { SetPropertyValue("SealNumber", ref i_SealNumber, value); }
        }

        private DateTime i_ArrivalPlanDateFrom;
        [DisplayName("с")]
        public DateTime ArrivalPlanDateFrom
        {
            get { return i_ArrivalPlanDateFrom; }
            set { SetPropertyValue("ArrivalPlanDateFrom", ref i_ArrivalPlanDateFrom, value); }
        }
        private DateTime i_ArrivalPlanDateTo;
        [DisplayName("по")]
        public DateTime ArrivalPlanDateTo
        {
            get { return i_ArrivalPlanDateTo; }
            set { SetPropertyValue("ArrivalPlanDateTo", ref i_ArrivalPlanDateTo, value); }
        }

        private ERequestStatus i_RequestStatus;
        [DisplayName("Статус")]
        public ERequestStatus RequestStatus
        {
            get { return i_RequestStatus; }
            set { SetPropertyValue("RequestStatus", ref i_RequestStatus, value); }
        }
    }
}
