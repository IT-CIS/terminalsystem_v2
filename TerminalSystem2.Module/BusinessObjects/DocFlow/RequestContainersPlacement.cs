﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.DocFlow
{
    [ModelDefault("Caption", "Заявка на размещение контейнеров"), NavigationItem("Заявки")]
    [System.ComponentModel.DefaultProperty("RequestNumber")]
    public class RequestContainersPlacement : RequestBaseDriversInfo
    {
        public RequestContainersPlacement(Session session) : base(session) { }

        private bool i_isRelease;
        [DisplayName("Релиз")]
        public bool isRelease
        {
            get { return i_isRelease; }
            set { SetPropertyValue("isRelease", ref i_isRelease, value); }
        }
        private string i_ReleaseNumber;
        [Size(255), DisplayName("Номер релиза")]
        public string ReleaseNumber
        {
            get { return i_ReleaseNumber; }
            set { SetPropertyValue("ReleaseNumber", ref i_ReleaseNumber, value); }
        }

        [Association, DisplayName("Информация по контейнерам заявки")]
        public XPCollection<RequestContainerInfo> RequestContainerInfos
        {
            get { return GetCollection<RequestContainerInfo>("RequestContainerInfos"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            Connect connect = Connect.FromSession(Session);
            RequestType = connect.FindFirstObject<dRequestType>(mc => mc.Name == "На размещение контейнеров");
            try
            {
                if(Client != null)
                    RequestNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, "На размещение контейнеров" + Client.Name + DateTime.Now.Year, string.Empty)) + "/" +
                  String.Format("{0:yyyy}", DateTime.Now.Date);
            }
            catch { }
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            if (RequestNumber == null || RequestNumber == "")
                try
                {
                    if (Client != null)
                        RequestNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, "На размещение контейнеров" + Client.Name + DateTime.Now.Year, string.Empty)) + "/" +
                        String.Format("{0:yyyy}", DateTime.Now.Date);
                }
                catch { }

        }
        
        }
}
