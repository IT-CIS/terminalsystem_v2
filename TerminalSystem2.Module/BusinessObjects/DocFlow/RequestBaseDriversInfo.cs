﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
//using TerminalSystem2.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Terminals;
//using TerminalSystem2.Transports;

namespace TerminalSystem2.DocFlow
{
    [ModelDefault("Caption", "Заявка")]
    public partial class RequestBaseDriversInfo : RequestBase
    {
        public RequestBaseDriversInfo(Session session) : base(session) { }



        [Association, DisplayName("Информация по водителям")]
        public XPCollection<RequestDriverInfo> RequestDriverInfos
        {
            get { return GetCollection<RequestDriverInfo>("RequestDriverInfos"); }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        protected override void OnSaving()
        {
            base.OnSaving();
        }
        
    }
}
