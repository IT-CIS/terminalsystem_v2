﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.Enums;
//using TerminalSystem2.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;
//using TerminalSystem2.Transports;

namespace TerminalSystem2.DocFlow
{
    [ModelDefault("Caption", "Заявка"), NavigationItem("Заявки")]
    [System.ComponentModel.DefaultProperty("RequestNumber")]
    [Appearance("Request_Client_Disabled", Context = "DetailView", Method = "IsCurrUserClient",
        Enabled = false, TargetItems = "Client")]

    [Appearance("Request_IsDraft", Context = "DetailView", Criteria = "RequestStatus =='Черновик'",
        Visibility = ViewItemVisibility.Hide, AppearanceItemType = "Action",
        TargetItems = "RequestOnEditAction,RequestFinishEditAction,RequestProcessingAction")]
    [Appearance("Request_IsFiled", Context = "DetailView", Criteria = "RequestStatus =='Подана' OR RequestStatus =='Отредактирована'",
        Visibility = ViewItemVisibility.Hide, AppearanceItemType = "Action",
        TargetItems = "RequestSubmissionAction,RequestFinishEditAction")]
    [Appearance("Request_IsEditing", Context = "DetailView", Criteria = "RequestStatus =='Редактирование'",
        Visibility = ViewItemVisibility.Hide, AppearanceItemType = "Action",
        TargetItems = "RequestSubmissionAction,RequestOnEditAction,RequestProcessingAction,ContainerPlacementAction,ContainerExtraditionAction")]
    [Appearance("Request_IsEditing2", Context = "DetailView", Criteria = "RequestStatus =='Редактирование'",
        Visibility = ViewItemVisibility.Show, AppearanceItemType = "Action",
        TargetItems = "RequestFinishEditAction")]
    [Appearance("Request_IsProcessing", Context = "DetailView", Criteria = "RequestStatus =='Обработка'",
        Visibility = ViewItemVisibility.Hide, AppearanceItemType = "Action",
        TargetItems = "RequestSubmissionAction,RequestFinishEditAction,RequestProcessingAction")]
    [Appearance("Request_IsClosed", Context = "DetailView", Criteria = "RequestStatus =='Отработана' OR RequestStatus =='ОбработанаЧастично' OR RequestStatus =='ЗакрытаПоВремени'",
        Visibility = ViewItemVisibility.Hide, AppearanceItemType = "Action",
        TargetItems = "RequestSubmissionAction,RequestOnEditAction,RequestFinishEditAction,RequestProcessingAction")]
    public partial class RequestBase : AttachBase
    {
        //public bool IsDraft
        //{

        //}
        public RequestBase(Session session) : base(session) { }

        private dRequestType i_RequestType;
        [DisplayName("Тип заявки")]
        public dRequestType RequestType
        {
            get { return i_RequestType; }
            set { SetPropertyValue("RequestType", ref i_RequestType, value); }
        }


        private string i_RequestNumber;
        [Size(255), DisplayName("Номер заявки")]
        public string RequestNumber
        {
            get { return i_RequestNumber; }
            set { SetPropertyValue("RequestNumber", ref i_RequestNumber, value); }
        }

        private DateTime i_RequestStartDate;
        [DisplayName("Дата заявки")]
        public DateTime RequestStartDate
        {
            get { return i_RequestStartDate; }
            set { SetPropertyValue("RequestStartDate", ref i_RequestStartDate, value); }
        }
        private DateTime i_RequestFinishDate;
        [DisplayName("Действие заявки")]
        public DateTime RequestFinishDate
        {
            get { return i_RequestFinishDate; }
            set { SetPropertyValue("RequestFinishDate", ref i_RequestFinishDate, value); }
        }
        private DateTime i_RequestStopDate;
        [DisplayName("Дата окончания обработки заявки")]
        public DateTime RequestStopDate
        {
            get { return i_RequestStopDate; }
            set { SetPropertyValue("RequestStopDate", ref i_RequestStopDate, value); }
        }

        private ERequestStatus i_RequestStatus;
        [DisplayName("Статус заявки")]
        [ImmediatePostData]
        public ERequestStatus RequestStatus
        {
            get { return i_RequestStatus; }
            set { SetPropertyValue("RequestStatus", ref i_RequestStatus, value); }
        }
        
        private Terminal i_Terminal;
        [DisplayName("Терминал")]
        [DataSourceProperty("AvailableTerminals")]
        [ImmediatePostData]
        public Terminal Terminal
        {
            get {
                //if (Client != null)
                //{
                //    if(Client.Stocks.Count == 1)
                //                i_Terminal = Client.Stocks[0].Terminal;

                //}
                return i_Terminal;
            }
            set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        }
        // -------------------------------------------------------------
        // Получаем возможные для выбора терминалы, которые указаны у клиента, при условии, что это не терминальный сток
        private XPCollection<Terminal> availableTerminals;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Terminal> AvailableTerminals
        {
            get
            {
                if (availableTerminals == null)
                {
                    // Retrieve all Terminals objects 
                    availableTerminals = new XPCollection<Terminal>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableTerminals();
                }
                return availableTerminals;
            }
        }
        private void RefreshAvailableTerminals()
        {
            if (availableTerminals == null)
                return;

            if (Client != null)
            {
                availableTerminals.Criteria = new InOperator("Oid", GetAvailableTerminals());
            }
            else
                availableTerminals = new XPCollection<Terminal>(Session);

            if (availableTerminals.Count == 1)
                Terminal = availableTerminals[0];
            //Terminal = null;
        }
        private List<string> GetAvailableTerminals()
        {
            List<string> AvailableTerminals = new List<string>();
            foreach (Stock stock in Client.Stocks)
            {
                try
                {
                    if (!stock.TerminalStockFlag)
                    AvailableTerminals.Add(stock.Terminal.Oid.ToString());
                }
                catch { }
            }
            return AvailableTerminals;
        }
        // -------------------------------------------------------------
        private ClientEmployee i_Requester;
        [DisplayName("Заявитель")]
        [ImmediatePostData]
        public ClientEmployee Requester
        {
            get { return i_Requester; }
            set { SetPropertyValue("Requester", ref i_Requester, value); }
        }

        private Client i_Client;
        [Association, DisplayName("Клиент")]
        [ImmediatePostData]
        public Client Client
        {
            get {
                if (Requester != null)
                {
                    if (Requester.Client != null)
                        i_Client = Requester.Client;
                }
                return i_Client; }
            set { SetPropertyValue("Client", ref i_Client, value);
                RefreshAvailableTerminals();
            }
        }
        public bool IsCurrUserClient()
        {
            bool res = false;
            if (SecuritySystem.CurrentUser != null)
            {
                //ClientEmployee currentEmpl = connect.FindFirstObject<ClientEmployee>(x => x.SysUser.Oid.ToString() == SecuritySystem.CurrentUserId.ToString());
                ClientEmployee currentEmpl = Session.FindObject<ClientEmployee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    res = true;
            }
            return res;
        }
        private string i_RequesterInfo;
        [Size(4000), DisplayName("Контактная информация")]
        public string RequesterInfo
        {
            get {
                if (i_RequesterInfo == null || i_RequesterInfo == String.Empty)
                    i_RequesterInfo = GetRequesterInfo();
                if (i_RequesterInfo == null || i_RequesterInfo == String.Empty)
                    i_RequesterInfo = GetClientInfo();
                return i_RequesterInfo; }
            set { SetPropertyValue("RequesterInfo", ref i_RequesterInfo, value); }
        }
       

        private string GetRequesterInfo()
        {
            string res = "";
            if (Requester != null)
            {
                if (Requester.PhonesNumbers != null && Requester.PhonesNumbers != String.Empty)
                    res = "Тел.: " + Requester.PhonesNumbers;
                if (Requester.Email != null && Requester.Email != String.Empty)
                { if(res == "")
                    res = "Email: " + Requester.Email; 
                else
                    res = res + ", Email: " + Requester.Email;
                }
            }
            return res;
        }
        private string GetClientInfo()
        {
            string res = "";
            if (Client != null)
            {
                if (Client.PhonesNumbers != null && Client.PhonesNumbers != String.Empty)
                    res = "Тел.: " + Client.PhonesNumbers;
                if (Client.Email != null && Client.Email != String.Empty)
                {
                    if (res == "")
                        res = "Email: " + Client.Email;
                    else
                        res = res + ", Email: " + Client.Email;
                }
            }
            return res;
        }
        private bool i_IsFinished;
        [DisplayName("Заявка обработана")]
        public bool IsFinished
        {
            get { return i_IsFinished; }
            set { SetPropertyValue("IsFinished", ref i_IsFinished, value); }
        }
        private Employee i_Employee;
        [DisplayName("Сотрудник, обработавший заявку")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }
        private string i_Note;
        [Size(SizeAttribute.Unlimited), DisplayName("Дополнительная информация")]
        public string Note
        {
            get { return i_Note; }
            set { SetPropertyValue("Note", ref i_Note, value); }
        }

        

        //[Association, DisplayName("Контейнеры по заявке")]
        //public XPCollection<Container> Containers
        //{
        //    get { return GetCollection<Container>("Containers"); }
        //}
        //[Association, DisplayName("Сообщения")]
        //public XPCollection<Message> Messages
        //{
        //    get { return GetCollection<Message>("Messages"); }
        //}
        //[Association, DisplayName("Список машин, обрабатывающих заявку")]
        //public XPCollection<Car> Cars
        //{
        //    get { return GetCollection<Car>("Cars"); }
        //}
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            Connect connect = Connect.FromSession(Session);
            if (SecuritySystem.CurrentUser != null)
            {
                //ClientEmployee currentEmpl = connect.FindFirstObject<ClientEmployee>(x => x.SysUser.Oid.ToString() == SecuritySystem.CurrentUserId.ToString());
                ClientEmployee currentEmpl = Session.FindObject<ClientEmployee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Requester = currentEmpl;
            }
            i_RequestStartDate = DateTime.Now.Date;
            if(i_RequestFinishDate == DateTime.MinValue)
                i_RequestFinishDate = DateTime.Now.AddDays(2).Date;
        }
        //protected override void OnSaving()
        //{
            
        //    UnitOfWork unitOfWork = (UnitOfWork)Session;
        //    //if(IsFinished)
        //    //{
        //    //    RequestStopDate = DateTime.Now.Date;
        //    //    //if(RequestType == RequestType.placement)
        //    //    //{
        //    //    //    foreach(Container container in Containers)
        //    //    //    {
        //    //    //        container.onTerminal = true;
        //    //    //        container.Save();
        //    //    //    }
        //    //    //}
        //    //    //else
        //    //    //{
        //    //    //    foreach (Container container in Containers)
        //    //    //    {
        //    //    //        container.onTerminal = false;
        //    //    //        container.Save();
        //    //    //    }
        //    //    //}
        //    //    if (SecuritySystem.CurrentUser != null)
        //    //    {
        //    //        try
        //    //        {
        //    //            Employee currentEmpl = Session.FindObject<Employee>(
        //    //                new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
        //    //            if (currentEmpl != null)
        //    //                i_Employee = currentEmpl;
        //    //        }
        //    //        catch { }
        //    //    }
        //    //}
        //    base.OnSaving();


        //}
        //protected override void OnSaved()
        //{
        //    base.OnSaved();
        //    if(this.RequestStatus == ERequestStatus.Редактирование)
        //    {

        //    }
        //}
    }
}
