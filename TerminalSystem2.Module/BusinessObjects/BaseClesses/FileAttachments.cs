﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;

namespace TerminalSystem2.BaseClasses
{
    public class AttachmentFiles : FileAttachmentBase
    {
        public AttachmentFiles(Session session) : base(session) { }
        //private DocumentType documentType;
        protected AttachBase _attachBase;

        private string i_Name;
        private DateTime i_RegDate;
        private Employee i_Registrator;
        private string i_Description;
        private string i_Sha256;

        [Size(255), DevExpress.Xpo.DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        [DevExpress.Xpo.DisplayName("Дата размещения")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        [DevExpress.Xpo.DisplayName("Сотрудник, разместивший файл")]
        public Employee Registrator
        {
            get { return i_Registrator; }
            set { SetPropertyValue("Registrator", ref i_Registrator, value); }
        }
        [Size(4000), DevExpress.Xpo.DisplayName("Примечание")]
        public string Description
        {
            get { return i_Description; }
            set { SetPropertyValue("Description", ref i_Description, value); }
        }
        [Size(SizeAttribute.Unlimited), System.ComponentModel.Browsable(false)]
        public string Sha256
        {
            get { return i_Sha256; }
            set { SetPropertyValue("Sha256", ref i_Sha256, value); }
        }
        //[Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never), NoForeignKey, ImmediatePostData]
        //[DevExpress.Xpo.DisplayName("Файл")]
        //public FileSystemStoreObject File
        //{
        //    get { return GetPropertyValue<FileSystemStoreObject>("File"); }
        //    set { SetPropertyValue<FileSystemStoreObject>("File", value); }
        //}

        [Persistent, Association("AttachBase-AttachmentFiles"), DevExpress.Xpo.DisplayName("Электронные файлы")]
        //[FileTypeFilter("Файлы", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        //[FileTypeFilter("Изображения", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        public AttachBase AttachBase
        {
            get { return _attachBase; }
            set
            {
                SetPropertyValue("AttachBase", ref _attachBase, value);
            }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Registrator = currentEmpl;
            }
            i_RegDate = DateTime.Now.Date;
        }

    }
}
