﻿

using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Скидка
    /// </summary>
    [ModelDefault("Caption", "Скидка")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class TariffDiscount : TariffScaleBase
    {
        public TariffDiscount(Session session) : base(session) { }

        private string i_Name;
        [Size(255), DisplayName("Описание")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        private EDiscountType i_DiscountType;
        [DisplayName("Тип скидки")]
        public EDiscountType DiscountType
        {
            get { return i_DiscountType; }
            set { SetPropertyValue("DiscountType", ref i_DiscountType, value); }
        }

        private double i_Discount;
        [DisplayName("Скидка в %")]
        public double Discount
        {
            get { return i_Discount; }
            set { SetPropertyValue("Discount", ref i_Discount, value); }
        }
        private int i_DiscountActiveDays;
        [DisplayName("Срок действия скидки")]
        public int DiscountActiveDays
        {
            get { return i_DiscountActiveDays; }
            set { SetPropertyValue("DiscountActiveDays", ref i_DiscountActiveDays, value); }
        }

        //private DateTime i_DiscountActiveThroughDate;
        //[DisplayName("Срок действия скидки")]
        //public DateTime DiscountActiveThroughDate
        //{
        //    get { return i_DiscountActiveThroughDate; }
        //    set { SetPropertyValue("DiscountActiveThroughDate", ref i_DiscountActiveThroughDate, value); }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        
        protected override void OnSaving()
        {
            base.OnSaving();
        }
    }
}

