﻿
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Базовый тариф по услугам
    /// </summary>
    [ModelDefault("Caption", "Базовая тарифная сетка"), NavigationItem("Тарификация")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class TariffBaseTerminal : TariffScaleBase
    {
        public TariffBaseTerminal(Session session) : base(session) { }

        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            //try {
            //    this.Name = String.Format("Базовая тарификация");
            //}
            //catch { }
        }
    }
}

