﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Тариф по услуге
    /// </summary>
    [ModelDefault("Caption", "Тариф по услуге")]
    [System.ComponentModel.DefaultProperty("Description")]
    public class Tariff : BaseObjectXAF
    {
        public Tariff(Session session) : base(session) { }
        
        private string i_UserName;
        private SecuritySystemUser i_user;

        private string i_description;
        [Size(500), DisplayName("Описание")]
        public string Description
        {
            get {
                return i_description; }
            set { SetPropertyValue("Description", ref i_description, value); }
        }

        private TariffService i_TariffService;
        [DisplayName("Наименование услуги")]
        public TariffService TariffService
        {
            get { return i_TariffService; }
            set { SetPropertyValue("TariffService", ref i_TariffService, value); }
        }

        private dUnitKind i_UnitKind;
        [DisplayName("Единица измерения")]
        public dUnitKind UnitKind
        {
            get { return i_UnitKind; }
            set { SetPropertyValue("UnitKind", ref i_UnitKind, value); }
        } 

        private double i_Value20Empty;
        [DisplayName("20' порожний")]
        public double Value20Empty
        {
            get { return i_Value20Empty; }
            set { SetPropertyValue("Value20Empty", ref i_Value20Empty, value); }
        }
        private double i_Value20Laden;
        [DisplayName("20' груженый")]
        public double Value20Laden
        {
            get { return i_Value20Laden; }
            set { SetPropertyValue("Value20Laden", ref i_Value20Laden, value); }
        }
        private double i_Value40Empty;
        [DisplayName("40' порожний")]
        public double Value40Empty
        {
            get { return i_Value40Empty; }
            set { SetPropertyValue("Value40Empty", ref i_Value40Empty, value); }
        }
        private double i_Value40Laden;
        [DisplayName("40' груженый")]
        public double Value40Laden
        {
            get { return i_Value40Laden; }
            set { SetPropertyValue("Value40Laden", ref i_Value40Laden, value); }
        }

        


        //private Terminal i_Terminal;
        //[DisplayName("Терминал")]
        ////[System.ComponentModel.Browsable(false)]
        //public Terminal Terminal
        //{
        //    get {
        //        try
        //        {
        //            i_Terminal = TariffBase.Terminal;
        //        }
        //        catch { }
        //        return i_Terminal; }
        //    set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        //}

        //private Client i_Client;
        //[Association, DisplayName("Клиент")]
        //[System.ComponentModel.Browsable(false)]
        //public Client Client
        //{
        //    get { return i_Client; }
        //    set { SetPropertyValue("Client", ref i_Client, value); }
        //}

        private TariffScaleBase i_TariffBase;
        [Association, DisplayName("Базовый тариф")]
        [ImmediatePostData]
        [System.ComponentModel.Browsable(false)]
        public TariffScaleBase TariffBase
        {
            get { return i_TariffBase; }
            set { SetPropertyValue("TariffBase", ref i_TariffBase, value); }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            try { this.Description = GetDescription(); }
            catch { }
        }
        private string GetDescription()
        {
            string res = "";
            try
            {
                res = String.Format("Услуга {0}, стоимость [20' порожний: {1}] [20' груженый: {2}] [40' порожний: {3}] [40' груженый: {4}]",
                    TariffService.Name, Value20Empty, Value20Laden, Value40Empty, Value40Laden);
                //res = String.Format("Услуга: {0} {1}, стоимость: {2} {3}", i_ServiceType.Name, i_ServiceSubType.Name, Value, Currency.Code);
            }
            catch { }
            return res;
        }
       
    }
}

