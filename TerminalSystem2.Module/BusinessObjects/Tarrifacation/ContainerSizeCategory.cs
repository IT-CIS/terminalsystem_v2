﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Услуг
    /// </summary>
    [ModelDefault("Caption", "Категория размера"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class ContainerSizeCategory : BaseObjectXAF
    {
        public ContainerSizeCategory(Session session) : base(session) { }

        
        private string i_Name;
        [DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        [Association, DisplayName("Типоразмеры контейнеров")]
        public XPCollection<ContainerTypeSize> ContainerSizes
        {
            get { return GetCollection<ContainerTypeSize>("ContainerSizes"); }
        }
    }
}

