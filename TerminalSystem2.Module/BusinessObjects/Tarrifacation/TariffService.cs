﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Услуг
    /// </summary>
    [ModelDefault("Caption", "Услуга")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class TariffService : BaseObjectXAF
    {
        public TariffService(Session session) : base(session) { }

        private dServiceType i_ServiceType;
        [DisplayName("Вид услуги")]
        public dServiceType ServiceType
        {
            get { return i_ServiceType; }
            set { SetPropertyValue("ServiceType", ref i_ServiceType, value); }
        }
        private string name;
        [Size(4000),DisplayName("Наименование")]
        public string Name
        {
            get
            {
                try { name = GetName(); }
                catch { }
                return name;
            }
            set
            {
                SetPropertyValue("Name", ref name, value);
            }
        }
        private string GetName()
        {
            string res = "";
            string contType = "";
            //if (ContainerSubTypes.Count == 1)
            //    contType = ContainerSubTypes[0].Name + "-";
            //else
            //    contType = "";
            //res = String.Format("{0} {1}контейнера", ServiceType.Name, contType);
            foreach (dContainerSubType containerSubType in ContainerSubTypes)
            { contType += containerSubType.Name + ", "; }
            res = String.Format("{0} контейнеры: {1}", ServiceType?.Name?? "", contType.TrimEnd(','));
            return res;
        }


        //private bool i_isLaden;
        //[DisplayName("Груженый")]
        //public bool isLaden
        //{
        //    get { return i_isLaden; }
        //    set { SetPropertyValue("isLaden", ref i_isLaden, value); }
        //}

        //private bool i_isWeightRange;
        //[DisplayName("С учетом веса")]
        //public bool isWeightRange
        //{
        //    get { return i_isWeightRange; }
        //    set { SetPropertyValue("isWeightRange", ref i_isWeightRange, value); }
        //}
        //private double i_WeightRangeFrom;
        //[DisplayName("Вес груженого контейнера, с")]
        //public double WeightRangeFrom
        //{
        //    get { return i_WeightRangeFrom; }
        //    set { SetPropertyValue("WeightRangeFrom", ref i_WeightRangeFrom, value); }
        //}
        //private double i_WeightRangeTo;
        //[DisplayName("Вес груженого контейнера, по")]
        //public double WeightRangeTo
        //{
        //    get { return i_WeightRangeTo; }
        //    set { SetPropertyValue("WeightRangeTo", ref i_WeightRangeTo, value); }
        //}

        //private bool i_isSingleOccupancy;
        //[DisplayName("Отдельное размещение")]
        //public bool isSingleOccupancy
        //{
        //    get { return i_isSingleOccupancy; }
        //    set { SetPropertyValue("TransportationTime", ref i_isSingleOccupancy, value); }
        //}
        [Association, DisplayName("Подтипы контейнеров")]
        public XPCollection<dContainerSubType> ContainerSubTypes
        {
            get { return GetCollection<dContainerSubType>("ContainerSubTypes"); }
        }
        protected override void OnSaving()
        {
            base.OnSaving();
            try { this.Name = GetName(); }
            catch { }
        }
       
    }
}

