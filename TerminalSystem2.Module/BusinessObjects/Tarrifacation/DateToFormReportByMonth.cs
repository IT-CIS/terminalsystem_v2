﻿
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Дата для формирования отчета за месяц  
    /// </summary>
    [ModelDefault("Caption", "Дата для формирования отчета за месяц")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class DateToFormReportByMonth : BaseObjectXAF
    {
        public DateToFormReportByMonth(Session session) : base(session) { }

        
        private DateTime i_DateForReport;
        [DisplayName("Дата")]
        public DateTime DateForReport
        {
            get { return i_DateForReport; }
            set { SetPropertyValue("DateForReport", ref i_DateForReport, value); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaving()
        {
            base.OnSaving();
        }
    }
}

