﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Тариф по услуге
    /// </summary>
    [ModelDefault("Caption", "Предоставленная услуга")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class ServiceDone : AttachBase
    {
        public ServiceDone(Session session) : base(session) { }

        private dServiceType i_ServiceType;
        [DisplayName("Вид услуги")]
        public dServiceType ServiceType
        {
            get
            {
                if (Tariff != null)
                    if (Tariff.TariffService != null)
                        if (Tariff.TariffService.ServiceType != null)
                            i_ServiceType = Tariff.TariffService.ServiceType;
                return i_ServiceType;
            }
            set { SetPropertyValue("ServiceType", ref i_ServiceType, value); }
        }

        //private ServiceSubType i_ServiceSubType;
        //[DisplayName("Вид услуги уточненный")]
        //[DataSourceCriteria("ServiceType = '@This.ServiceType'")]
        //public ServiceSubType ServiceSubType
        //{
        //    get {
        //        if (ServiceRate != null)
        //            if (ServiceRate.ServiceSubType != null)
        //                i_ServiceSubType = ServiceRate.ServiceSubType;
        //        return i_ServiceSubType; }
        //    set { SetPropertyValue("ServiceSubType", ref i_ServiceSubType, value); }
        //}

        private Tariff i_Tariff;
        [DisplayName("Тариф по услуге")]
        //[DataSourceCriteria("TariffScale.Client = '@This.Client'")]
        [ImmediatePostData]
        public Tariff Tariff
        {
            get { return i_Tariff; }
            set { SetPropertyValue("Tariff", ref i_Tariff, value); }
        }

        private DateTime i_StartDate;
        [DisplayName("Дата начала предоставления услуги")]
        public DateTime StartDate
        {
            get { return i_StartDate; }
            set { SetPropertyValue("StartDate", ref i_StartDate, value); }
        }
        private DateTime i_FinishDate;
        [DisplayName("Дата окончания предоставления услуги")]
        public DateTime FinishDate
        {
            get { return i_FinishDate; }
            set { SetPropertyValue("FinishDate", ref i_FinishDate, value); }
        }

        private double i_ServiceCount;
        [DisplayName("Количество предоставленной услуги")]
        [ImmediatePostData]
        public double ServiceCount
        {
            get { return i_ServiceCount; }
            set { SetPropertyValue("ServiceCount", ref i_ServiceCount, value); }
        }
        private dUnitKind i_UnitKind;
        [DisplayName("Единица измерения услуги")]
        public dUnitKind UnitKind
        {
            get { return i_UnitKind; }
            set { SetPropertyValue("UnitKind", ref i_UnitKind, value); }
        }
        private double i_Value;
        [DisplayName("Стоимость единицы услуги")]
        [ImmediatePostData]
        public double Value
        {
            get {
                //try { i_Value = GetValueByTariff(); }
                //catch { }
                return i_Value; }
            set { SetPropertyValue("Value", ref i_Value, value); }
        }
        private double GetValueByTariff()
        {
            double res = 0;
            try {
                EContainerCategorySize containerSizeCategory = Container.ContainerType.ContainerTypeSize.ContainerCategorySize;
                if (containerSizeCategory == EContainerCategorySize.twenty)
                {
                    if (Container.isLaden)
                        res = Tariff.Value20Laden;
                    else
                        res = Tariff.Value20Empty;
                }
                else
                {
                    if (Container.isLaden)
                        res = Tariff.Value40Laden;
                    else
                        res = Tariff.Value40Empty;
                }
            }
            catch { }
            return res;
        }
        private double i_Discount;
        [DisplayName("Скидка, %")]
        public double Discount
        {
            get { return i_Discount; }
            set { SetPropertyValue("Discount", ref i_Discount, value); }
        }
        private DateTime i_DiscountActiveThroughDate;
        [DisplayName("Действие скидки")]
        public DateTime DiscountActiveThroughDate
        {
            get { return i_DiscountActiveThroughDate; }
            set { SetPropertyValue("DiscountActiveThroughDate", ref i_DiscountActiveThroughDate, value); }
        }
        private double i_ValueTotal;
        [DisplayName("Общая стоимость услуги")]
        public double ValueTotal
        {
            get {
                //try {
                //    if (FinishDate != DateTime.MinValue)
                //    {
                //        //if()
                //    }
                //    else
                //    {
                //        // скидка действует на все время
                //        if(DiscountActiveThroughDate >= FinishDate)
                //        {
                //            i_ValueTotal = Value * ServiceCount * (100 - Discount) / 100;
                //        }
                //        // скидка не действует на окончание услуги - считаем, сколько дней она действовала
                //        else { }
                //    }
                //    i_ValueTotal = Value * ServiceCount * (100 - Discount)/100; }
                //catch { }
                return i_ValueTotal; }
            set { SetPropertyValue("ValueTotal", ref i_ValueTotal, value); }
        }
        private DateTime i_ValueTotalDate;
        [DisplayName("На дату")]
        public DateTime ValueTotalDate
        {
            get { return i_ValueTotalDate; }
            set { SetPropertyValue("ValueTotalDate", ref i_ValueTotalDate, value); }
        }
        private double GetValueTotal()
        {
            double res = 0;
            // ищем активную тарифную сетку у клиента
            if (Client != null)
            {
                Connect connect = Connect.FromSession(Session);
                Contract contract = connect.FindFirstObject<Contract>(mc => mc.Client == Client && mc.ActiveFlag == true);
                if (contract != null)
                {
                    TariffScale tariffScale = connect.FindFirstObject<TariffScale>(mc => mc.Contract == contract && mc.ActiveFlag == true);
                    if (tariffScale != null)
                    {
                        if (tariffScale.AddDiscount)
                        {
                            if (tariffScale.DiscountType == EDiscountType.byTime)
                            {

                            }
                            else
                            {
                                if (ServiceType != null)
                                {
                                    if (ServiceType.Name == "Хранение контейнера")
                                    {
                                        if (FinishDate != DateTime.MinValue)
                                        { }
                                        else
                                        {

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return res;
        }

        private dCurrency i_Currency;
        [DisplayName("Валюта")]
        public dCurrency Currency
        {
            get { return i_Currency; }
            set { SetPropertyValue("Currency", ref i_Currency, value); }
        }

        private bool i_isDone;
        [DisplayName("Услуга закрыта")]
        [ImmediatePostData]
        public bool isDone
        {
            get { return i_isDone; }
            set { SetPropertyValue("isDone", ref i_isDone, value);
                //SetValueTotalInCloseService();
            }
        }
        /// <summary>
        /// Функция расчета стоимости услуги, при ее закрытии
        /// </summary>
        public void SetValueTotalInCloseService()
        {
            if (isDone)
            {
                // высчитываем, если дата последнего расчета стоимости меньше текущей
                if (ValueTotalDate != DateTime.MinValue)
                    if (ValueTotalDate < DateTime.Now.Date)
                    {
                        // 
                        if (DiscountActiveThroughDate >= DateTime.Now.Date)
                        {
                            ValueTotal += (DateTime.Now.Date - ValueTotalDate).TotalDays * Value * (100 - Discount) / 100;
                        }
                        else
                        {
                            ValueTotal += (DateTime.Now.Date - ValueTotalDate).TotalDays * Value;
                        }
                        ValueTotalDate = DateTime.Now.Date;
                    }
            }
        }

        private Client i_Client;
        [Association, DisplayName("Клиент")]
        [System.ComponentModel.Browsable(false)]
        public Client Client
        {
            get {
                if (i_Client == null)
                    if (Container != null)
                        if (Container.Owner != null)
                            i_Client = Container.Owner;
                return i_Client; }
            set { SetPropertyValue("Client", ref i_Client, value); }
        }
        private Container i_Container;
        [Association, DisplayName("Контейнер")]
        public Container Container
        {
            get { return i_Container; }
            set { SetPropertyValue("Container", ref i_Container, value); }
        }

        //private BusinessProcessActivity i_BusinessProcessActivity;
        //[DisplayName("Процесс")]
        //public BusinessProcessActivity BusinessProcessActivity
        //{
        //    get { return i_BusinessProcessActivity; }
        //    set { SetPropertyValue("BusinessProcessActivity", ref i_BusinessProcessActivity, value); }
        //}
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
            //try
            //{
            //    if (ServiceType.Name == "Хранение контейнера")
            //        ServiceCount = (DateTime.Now.Date - StartDate).Days;
            //    if (ServiceCount == 0)
            //        ServiceCount = 1;
            //}
            //catch { }
        }

       
    }
}

