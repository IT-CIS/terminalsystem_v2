﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Услуг
    /// </summary>
    [ModelDefault("Caption", "Услуга")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class Service : BaseObjectXAF
    {
        public Service(Session session) : base(session) { }

        private ServiceType i_ServiceType;
        [DisplayName("Вид услуги")]
        public ServiceType ServiceType
        {
            get { return i_ServiceType; }
            set { SetPropertyValue("ServiceType", ref i_ServiceType, value); }
        }
        private string name;
        [DisplayName("Наименование")]
        public string Name
        {
            get
            {
                try { name = GetName(); }
                catch { }
                return name;
            }
            set
            {
                SetPropertyValue("Name", ref name, value);
            }
        }
        private string GetName()
        {
            string res = "";
            string contType = "";
            //if (ContainerTypes.Count == 1)
            //    contType = ContainerTypes[0].Name;
            //else
                contType = "все";
            res = String.Format("Услуга: [{0}], тип контейнера: [{1}'{2}]", ServiceType.Name,ContainerSizeCategory.Name, contType);
            if (ServiceType.Name == "Крановая операция")
            {
                if (isLaden)
                {
                    if (!isWeightRange)
                        res = res + ", груженый";
                    else
                        res = res + String.Format(", груженый, цена по диапазону веса [с {0} по {1}]", WeightRangeFrom, WeightRangeTo);
                }
                else
                    res = res + ", порожний";
            }
            if (ServiceType.Name == "Хранение контейнера")
            {
                if (isSingleOccupancy)
                {
                    res = res + ", отдельное размещение";
                }
            }

            return res;
        }

        private ContainerSizeCategory i_ContainerSizeCategory;
        [DisplayName("Категория размерности")]
        public ContainerSizeCategory ContainerSizeCategory
        {
            get { return i_ContainerSizeCategory; }
            set { SetPropertyValue("ContainerSizeCategory", ref i_ContainerSizeCategory, value); }
        }

        private UnitKind i_UnitKind;
        [DisplayName("Единица измерения")]
        public UnitKind UnitKind
        {
            get { return i_UnitKind; }
            set { SetPropertyValue("UnitKind", ref i_UnitKind, value); }
        }

        private bool i_isLaden;
        [DisplayName("Груженый")]
        public bool isLaden
        {
            get { return i_isLaden; }
            set { SetPropertyValue("isLaden", ref i_isLaden, value); }
        }

        private bool i_isWeightRange;
        [DisplayName("С учетом веса")]
        public bool isWeightRange
        {
            get { return i_isWeightRange; }
            set { SetPropertyValue("isWeightRange", ref i_isWeightRange, value); }
        }
        private double i_WeightRangeFrom;
        [DisplayName("Вес груженого контейнера, с")]
        public double WeightRangeFrom
        {
            get { return i_WeightRangeFrom; }
            set { SetPropertyValue("WeightRangeFrom", ref i_WeightRangeFrom, value); }
        }
        private double i_WeightRangeTo;
        [DisplayName("Вес груженого контейнера, по")]
        public double WeightRangeTo
        {
            get { return i_WeightRangeTo; }
            set { SetPropertyValue("WeightRangeTo", ref i_WeightRangeTo, value); }
        }

        private bool i_isSingleOccupancy;
        [DisplayName("Отдельное размещение")]
        public bool isSingleOccupancy
        {
            get { return i_isSingleOccupancy; }
            set { SetPropertyValue("TransportationTime", ref i_isSingleOccupancy, value); }
        }
        //[Association, DisplayName("Типы контейнеров")]
        //public XPCollection<ContainerType> ContainerTypes
        //{
        //    get { return GetCollection<ContainerType>("ContainerTypes"); }
        //}
        protected override void OnSaving()
        {
            base.OnSaving();
            try { this.Name = GetName(); }
            catch { }
        }
       
    }
}

