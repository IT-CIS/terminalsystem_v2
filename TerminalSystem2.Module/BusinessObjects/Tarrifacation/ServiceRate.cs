﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem.BaseClasses;
using TerminalSystem.Classifiers;
using TerminalSystem.Clients;
using TerminalSystem.Containers;
using TerminalSystem.OrgStructure;
using TerminalSystem.SystemDir;
using TerminalSystem.Terminals;

namespace TerminalSystem.Tarification
{
    /// <summary>
    /// Тариф по услуге
    /// </summary>
    [ModelDefault("Caption", "Тариф по услуге")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class ServiceRate : BaseObjectXAF
    {
        public ServiceRate(Session session) : base(session) { }
        
        private string i_UserName;
        private SecuritySystemUser i_user;

        private string i_description;
        [Size(500), DisplayName("Описание")]
        public string Description
        {
            get {
                return i_description; }
            set { SetPropertyValue("Description", ref i_description, value); }
        }
        //private ServiceType i_ServiceType;
        //[DisplayName("Вид услуги")]
        //[ImmediatePostData]
        //public ServiceType ServiceType
        //{
        //    get { return i_ServiceType; }
        //    set { SetPropertyValue("ServiceType", ref i_ServiceType, value); }
        //}
        private ServiceSubType i_ServiceSubType;
        [DisplayName("Вид услуги уточненный")]
        [System.ComponentModel.Browsable(false)]
        //[ImmediatePostData]
        //[DataSourceCriteria("ServiceType = '@This.ServiceType'")]
        public ServiceSubType ServiceSubType
        {
            get { return i_ServiceSubType; }
            set { SetPropertyValue("ServiceSubType", ref i_ServiceSubType, value); }
        }

        private Service i_Service;
        [DisplayName("Услуга")]
        public Service Service
        {
            get { return i_Service; }
            set { SetPropertyValue("Service", ref i_Service, value); }
        }
        private double i_Value;
        [DisplayName("Стоимость услуги")]
        public double Value
        {
            get { return i_Value; }
            set { SetPropertyValue("Value", ref i_Value, value); }
        }

        private Currency i_Currency;
        [DisplayName("Валюта")]
        public Currency Currency
        {
            get { return i_Currency; }
            set { SetPropertyValue("Currency", ref i_Currency, value); }
        }


        private Terminal i_Terminal;
        [DisplayName("Терминал")]
        //[System.ComponentModel.Browsable(false)]
        public Terminal Terminal
        {
            get { return i_Terminal; }
            set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        }

        //private Client i_Client;
        //[Association, DisplayName("Клиент")]
        //[System.ComponentModel.Browsable(false)]
        //public Client Client
        //{
        //    get { return i_Client; }
        //    set { SetPropertyValue("Client", ref i_Client, value); }
        //}

        //private ServiceBase i_ServiceBase;
        //[Association, DisplayName("Базовый тариф")]
        //[ImmediatePostData]
        //[System.ComponentModel.Browsable(false)]
        //public ServiceBase ServiceBase
        //{
        //    get { return i_ServiceBase; }
        //    set { SetPropertyValue("ServiceBase", ref i_ServiceBase, value); }
        //}


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //try {
            //    this.Terminal = ServiceBase.Terminal;
            //}
            //catch { }
            Connect connect = Connect.FromSession(Session);
            Currency curr = connect.FindFirstObject<Currency>(mc => mc.Code == "RU");
            if (curr != null)
                this.Currency = curr;
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            try { this.Description = GetDescription(); }
            catch { }
        }
        private string GetDescription()
        {
            string res = "";
            try
            {
                res = String.Format("Терминал:{0}, {1}, стоимость: {2} {3}",Terminal.Name, i_Service.Name, Value, Currency.Code);
                //res = String.Format("Услуга: {0} {1}, стоимость: {2} {3}", i_ServiceType.Name, i_ServiceSubType.Name, Value, Currency.Code);
            }
            catch { }
            return res;
        }
       
    }
}

