﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;

namespace TerminalSystem2.Clients
{
    /// <summary>
    /// Тариф по услуге
    /// </summary>
    [ModelDefault("Caption", "Тариф по услуге")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class ServiceRates : BaseObjectXAF
    {
        public ServiceRates(Session session) : base(session) { }
        

        private double i_Value;
        [DisplayName("Стоимость услуги")]
        public double Value
        {
            get { return i_Value; }
            set { SetPropertyValue("Value", ref i_Value, value); }
        }

        private ServiceRate i_ServiceRate;
        [Association, DisplayName("Клиент")]
        [System.ComponentModel.Browsable(false)]
        public ServiceRate ServiceRate
        {
            get { return i_ServiceRate; }
            set { SetPropertyValue("ServiceRate", ref i_ServiceRate, value); }
        }

        private ServiceBase i_ServiceBase;
        [Association, DisplayName("Базовый тариф")]
        [System.ComponentModel.Browsable(false)]
        public ServiceBase ServiceBase
        {
            get { return i_ServiceBase; }
            set { SetPropertyValue("ServiceBase", ref i_ServiceBase, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

       
    }
}

