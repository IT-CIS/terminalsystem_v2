﻿
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Базовый тариф по услугам
    /// </summary>
    [ModelDefault("Caption", "Базовый тарифная сетка по услугам")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class TariffScaleBase : BaseObjectXAF
    {
        public TariffScaleBase(Session session) : base(session) { }

        
        private dCurrency i_Currency;
        [DisplayName("Валюта")]
        public dCurrency Currency
        {
            get { return i_Currency; }
            set { SetPropertyValue("Currency", ref i_Currency, value); }
        }
        //private Terminal i_Terminal;
        //[DisplayName("Терминал")]
        //[ImmediatePostData]
        //public Terminal Terminal
        //{
        //    get { return i_Terminal; }
        //    set { SetPropertyValue("Terminal", ref i_Terminal, value); }
        //}
        [Association, DisplayName("Тарифы на оказание услуг")]
        public XPCollection<Tariff> Tariffs
        {
            get { return GetCollection<Tariff>("Tariffs"); }
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaving()
        {
            base.OnSaving();
        }
    }
}

