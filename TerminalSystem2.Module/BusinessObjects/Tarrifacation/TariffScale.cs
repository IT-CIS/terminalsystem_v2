﻿
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Enums;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.Tarification
{
    /// <summary>
    /// Базовый тариф по услугам
    /// </summary>
    [ModelDefault("Caption", "Тарифная сетка")]
    [System.ComponentModel.DefaultProperty("Name")]
    [Appearance("isCalcType", AppearanceItemType = "ViewItem", Priority = 1,
    Criteria = "CalcType = ##Enum#TerminalSystem2.Enums.ECalcType,byMonth#", Context = "Any", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide,
        TargetItems = @"AddDiscount,DiscountType,Discount,DiscountActiveDays,DiscountActiveThroughDate,Tariffs,Currency")]
    public class TariffScale : TariffScaleBase
    {
        public TariffScale(Session session) : base(session) { }

        private string i_Name;
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        private Contract i_Contract;
        [Association, DisplayName("Договор")]
        public Contract Contract
        {
            get { return i_Contract; }
            set { SetPropertyValue("Contract", ref i_Contract, value); }
        }

        private bool i_ActiveFlag;
        [DisplayName ("Активная тарифная сетка")]
        [ImmediatePostData]
        public bool ActiveFlag
        {
            get { return i_ActiveFlag; }
            set { SetPropertyValue("ActiveFlag", ref i_ActiveFlag, value); }
        }

        private ECalcType i_CalcType;
        [DisplayName("Вид расчета")]
        [ImmediatePostData]
        public ECalcType CalcType
        {
            get { return i_CalcType; }
            set { SetPropertyValue("CalcType", ref i_CalcType, value); }
        }

        private bool i_AddDiscount;
        [DisplayName("Действует скидка")]
        public bool AddDiscount
        {
            get { return i_AddDiscount; }
            set { SetPropertyValue("AddDiscount", ref i_AddDiscount, value); }
        }

        private EDiscountType i_DiscountType;
        [DisplayName("Тип скидки")]
        public EDiscountType DiscountType
        {
            get { return i_DiscountType; }
            set { SetPropertyValue("DiscountType", ref i_DiscountType, value); }
        }

        private double i_Discount;
        [DisplayName("Скидка, %")]
        [Custom("MinValue", "0"), Custom("MaxValue", "100")]
        public double Discount
        {
            get { return i_Discount; }
            set { SetPropertyValue("Discount", ref i_Discount, value); }
        }
        private int i_DiscountActiveDays;
        [DisplayName("Срок действия скидки, дней")]
        public int DiscountActiveDays
        {
            get { return i_DiscountActiveDays; }
            set { SetPropertyValue("DiscountActiveDays", ref i_DiscountActiveDays, value); }
        }

        

        private DateTime i_DiscountActiveThroughDate;
        [DisplayName("Скидка действительна до")]
        public DateTime DiscountActiveThroughDate
        {
            get { return i_DiscountActiveThroughDate; }
            set { SetPropertyValue("DiscountActiveThroughDate", ref i_DiscountActiveThroughDate, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            
            
            // TO-DO повесить на отдельный action
            // ищем базовый тариф для терминала и копируем все его тарифы в эту сетку
            //Connect connect = Connect.FromSession(Session);
            //TariffBaseTerminal tariffBaseTerminal = connect.FindFirstObject<TariffBaseTerminal>(mc => mc.Oid != null);
            //if(tariffBaseTerminal!=null)
            //{
            //    this.Currency = tariffBaseTerminal.Currency;
            //    foreach(Tariff tarifBase in tariffBaseTerminal.Tariffs)
            //    {
            //        Tariff tarifNew = connect.CreateObject<Tariff>();
            //        try { tarifNew.TariffService = tarifBase.TariffService; }
            //        catch { }
            //        try { tarifNew.UnitKind = tarifBase.UnitKind; }
            //        catch { }
            //        try { tarifNew.Value20Empty = tarifBase.Value20Empty; }
            //        catch { }
            //        try { tarifNew.Value20Laden = tarifBase.Value20Laden; }
            //        catch { }
            //        try { tarifNew.Value40Empty = tarifBase.Value40Empty; }
            //        catch { }
            //        try { tarifNew.Value40Laden = tarifBase.Value40Laden; }
            //        catch { }
            //        tarifNew.Save();
            //        this.Tariffs.Add(tarifNew);
            //    }
            //}
        }

        
        protected override void OnSaving()
        {
            base.OnSaving();
            try
            {
                if (this.Name == null || this.Name == string.Empty)
                    this.Name = String.Format("Тарификация клиента '{0}'", i_Contract.Client.Name);
            }
            catch { }

            if (i_ActiveFlag == true)
            {
                //try
                //{
                Connect connect = Connect.FromSession(Session);
                IQueryable TtariffScales = connect.FindObjects<TariffScale>(ts => ts.Contract == Contract);
                foreach (TariffScale ts in TtariffScales)
                {
                    if (ts != this)
                    {
                        ts.ActiveFlag = false;
                        ts.Save();
                    }
                }
                //}
                //catch { }
            }
        }
    }
}

