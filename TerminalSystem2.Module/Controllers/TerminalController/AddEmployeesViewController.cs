﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.OrgStructure;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.SystemDir;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using TerminalSystem2.Clients;

namespace TerminalSystem2.Module.Controllers.TerminalController
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AddEmployeesViewController : ViewController
    {
        ModificationsController controller;
        public AddEmployeesViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            //controller = Frame.GetController<ModificationsController>();
            //if (controller != null)
            //    controller.SaveAction.Executed += SaveObjectAction_Executed;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Создать пользователя системы для сотрудника Терминала
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateEmplSysUserAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();
            Employee employee;
            // получаем наш попап объект, его сессию и сохраняем его, после этого, получаем его из ObjectSpace
            var x_oid = ((BaseObject)View.CurrentObject).Oid;
            Connect connect = Connect.FromObjectSpace(os);
            employee = connect.FindFirstObject<Employee>(x => x.Oid == x_oid);
            //employee.Save();

            PermissionPolicyUser user = null;
            if (!String.IsNullOrEmpty(employee.SysUserString))
            {
                //Regex rgx = new Regex(@"^[a-zA-Zа-яА-Я0-9]");
                //if (!rgx.IsMatch(clientEmployee.SysUserString))
                //{
                //    throw new UserFriendlyException(String.Format($"Имя пользователя системы должно содержать только русские, латинские буквы и цифры."));
                //}
                user = connect.FindFirstObject<PermissionPolicyUser>(x => x.UserName == employee.SysUserString);
                if (user != null)
                {
                    Employee otherEmpl = connect.FindFirstObject<Employee>(x => x.SysUser.Oid == user.Oid);
                    if (otherEmpl != null)
                    {
                        if (employee != otherEmpl)
                        {
                            throw new UserFriendlyException(String.Format($"Другой пользователь (сотрудник {otherEmpl.BriefName ?? ""}) " +
                                $"с таким аккаунтом уже заведен в системе. Пожалуйста, задайте другое имя пользователя системы."));
                        }
                    }
                    else
                    {
                        ClientEmployee otherEmplClient = connect.FindFirstObject<ClientEmployee>(x => x.SysUser.Oid == user.Oid);
                        if(otherEmplClient != null)
                        {
                            throw new UserFriendlyException($"Другой пользователь (сотрудник клиента {otherEmplClient.BriefName??""}) " +
                                $"с таким аккаунтом уже заведен в системе. Пожалуйста, задайте другое имя пользователя системы.");
                        }
                        else
                        {
                            employee.SysUser = user;
                            employee.Save();
                        }
                        
                    }
                }
                else
                {
                    user = connect.CreateObject<PermissionPolicyUser>();
                    user.ChangePasswordOnFirstLogon = true;
                    user.IsActive = true;
                    user.UserName = employee.SysUserString;
                    PermissionPolicyRole employeeRole = connect.FindFirstObject<PermissionPolicyRole>(x => x.Name == "Сотрудник терминала");
                    if (employeeRole != null)
                        user.Roles.Add(employeeRole);
                    user.Save();
                    employee.SysUser = user;
                    employee.Save();
                }
            }
            //if (user != null)
            //{
            //employee.SysUser = user;
            //employee.Save();
            //}
            //else
            //    employee.Delete();
            os.CommitChanges();
            RefreshFrame();
        }

        /// <summary>
        /// Обновить фрейм (страницу)
        /// </summary>
        protected void RefreshFrame()
        {
            RefreshController controller = Frame.GetController<RefreshController>();
            if (controller != null)
                controller.RefreshAction.DoExecute();
        }
    }
}
