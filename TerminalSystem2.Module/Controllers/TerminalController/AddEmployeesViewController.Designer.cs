﻿namespace TerminalSystem2.Module.Controllers.TerminalController
{
    partial class AddEmployeesViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreateEmplSysUserAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CreateEmplSysUserAction
            // 
            this.CreateEmplSysUserAction.Caption = "Создать пользователя";
            this.CreateEmplSysUserAction.Category = "CreateEmplSysUserContainer";
            this.CreateEmplSysUserAction.ConfirmationMessage = null;
            this.CreateEmplSysUserAction.Id = "CreateEmplSysUserAction";
            this.CreateEmplSysUserAction.ToolTip = null;
            this.CreateEmplSysUserAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateEmplSysUserAction_Execute);
            // 
            // AddEmployeesViewController
            // 
            this.Actions.Add(this.CreateEmplSysUserAction);
            this.TargetObjectType = typeof(TerminalSystem2.OrgStructure.Employee);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CreateEmplSysUserAction;
    }
}
