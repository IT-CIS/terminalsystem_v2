﻿namespace TerminalSystem2.Module.Controllers.Helpers
{
    partial class CreateDataViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreateDataChoiceAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // CreateDataChoiceAction
            // 
            this.CreateDataChoiceAction.Caption = "Заполнить данные";
            this.CreateDataChoiceAction.ConfirmationMessage = null;
            this.CreateDataChoiceAction.Id = "CreateDataChoiceAction";
            this.CreateDataChoiceAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CreateDataChoiceAction.ToolTip = "Только для Администратора! Не нажимать!";
            this.CreateDataChoiceAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CreateDataChoiceAction_Execute);
            // 
            // CreateDataViewController
            // 
            this.Actions.Add(this.CreateDataChoiceAction);
            this.TargetObjectType = typeof(TerminalSystem2.OrgStructure.TerminalOrg);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction CreateDataChoiceAction;
    }
}
