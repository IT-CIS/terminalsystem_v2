﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;
using DevExpress.Xpo;

namespace TerminalSystem2.Module.Controllers.Helpers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class NewContainerCreateViewController : ViewController
    {
        private NewObjectViewController createNewObjectController;
        private const string Key = "Deactivation in code";

        public NewContainerCreateViewController()
        {
            InitializeComponent();
            //TargetObjectType = typeof(Containers.Container);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

            createNewObjectController = Frame.GetController<NewObjectViewController>();
            if (createNewObjectController != null)
            {
                createNewObjectController.Active[Key] =
                !(View.ObjectTypeInfo.Type == typeof(Containers.Container));
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.

        }
        protected override void OnDeactivated()
        {
            if (createNewObjectController != null)
            {
                createNewObjectController.Active.RemoveItem(Key);
                createNewObjectController = null;
            }
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        //private void SingleChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        //{
        //    IObjectSpace os = Application.CreateObjectSpace();
        //    Connect connect = Connect.FromObjectSpace(os);
        //    ContainerPlacementInfo placementInfo = connect.CreateObject<ContainerPlacementInfo>();
        //    placementInfo.ContainerNumber = "";

        //    placementInfo.Save();
        //    UnitOfWork unitOfWorkNew = (UnitOfWork)placementInfo.Session;
        //    unitOfWorkNew.CommitChanges();


        //    var svp = new ShowViewParameters
        //    {
        //        Context = TemplateContext.PopupWindow,
        //        TargetWindow = TargetWindow.NewModalWindow,
        //        CreatedView = this.Application.CreateDetailView(
        //os,
        //placementInfo,
        //true)
        //    };

        //    this.Application.ShowViewStrategy.ShowView(svp, new ShowViewSource(null, null));
        //    // открываем окно информации 
        //    //DetailView dv = Application.CreateDetailView(os, placementInfo);//Specify the IsRoot parameter if necessary.
        //    //dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
        //    //e.ShowViewParameters.CreatedView = dv;
        //    //e.View = dv;

        //}
    }
}
