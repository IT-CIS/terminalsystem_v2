﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Persistent.Validation;
using TerminalSystem2.Classifiers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;

namespace TerminalSystem2.Module.Controllers.Helpers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreateDataViewController : ViewController
    {
        private ChoiceActionItem setContainerTypesDataAction;
        private ChoiceActionItem createTariffBaseAction;
        public CreateDataViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            CreateDataChoiceAction.Items.Clear();

            setContainerTypesDataAction =
               new ChoiceActionItem("Типы контейнеров", null);
            CreateDataChoiceAction.Items.Add(setContainerTypesDataAction);
            createTariffBaseAction =
               new ChoiceActionItem("Базовый тариф", null);
            CreateDataChoiceAction.Items.Add(createTariffBaseAction);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            if (SecuritySystem.CurrentUser != null)
            {
                if (SecuritySystem.CurrentUser != null)
                {
                    PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;
                    if (currentuser.UserName != "DevAdmin")
                    {
                        Frame.GetController<CreateDataViewController>().CreateDataChoiceAction.Active.SetItemValue("myReason", false);
                    }
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CreateDataChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);


            // Заполнить данные по типам контейнеров
            if (e.SelectedChoiceActionItem == setContainerTypesDataAction)
            {
                SetContainerTypesData(os, connect);
            }
            if (e.SelectedChoiceActionItem == createTariffBaseAction)
            {
                CreateTariffBase(os, connect);
            }
        }

        private void SetContainerTypesData(IObjectSpace os, Connect connect)
        {
            // подвиды контейнеров
            string[] containerSubTypesString = { "Standart",
                                   "High Cube (HC)", "Double Door (DD)", "Side Door (SD)", "Ref", "Деревянный", "Duocon",
                                    "Hard Top", "Open Side", "Open Top", "Flat Rack"
                                    };
            foreach (string cst in containerSubTypesString)
                SetContainerSubTypes(cst, os, connect);

            setContainerTypes(os, connect);
        }

        private void SetContainerSubTypes(string containerSubTypeString, IObjectSpace os, Connect connect)
        {
            dContainerSubType containerSubType = ObjectSpace.FindObject<dContainerSubType>(
                new BinaryOperator("Name", containerSubTypeString));
            if (containerSubType == null)
            {
                containerSubType = ObjectSpace.CreateObject<dContainerSubType>();
                containerSubType.Name = containerSubTypeString;
            }
            containerSubType.Save();
            ObjectSpace.CommitChanges();
            containerSubType = null;
        }
        private void setContainerTypes(IObjectSpace os, Connect connect)
        {
            IList<dContainerTypeSize> containerSizes = ObjectSpace.GetObjects<dContainerTypeSize>();
            IList<dContainerSubType> containerSubTypes = ObjectSpace.GetObjects<dContainerSubType>();
            foreach (dContainerTypeSize containerTypeSize in containerSizes)
            {
                foreach (dContainerSubType containerSubType in containerSubTypes)
                {
                    dContainerType containerType = connect.FindFirstObject<dContainerType>(mc => mc.ContainerTypeSize == containerTypeSize &&
                mc.ContainerSubType == containerSubType);
                    if (containerType == null)
                    {
                        containerType = ObjectSpace.CreateObject<dContainerType>();
                        containerType.ContainerTypeSize = containerTypeSize;
                        containerType.ContainerSubType = containerSubType;
                        containerType.Name = $"{containerType?.ContainerTypeSize?.ContainerTypeSize.ToString()} {containerType?.ContainerSubType?.Name ?? ""}".Trim();
                        containerType.Save();
                        ObjectSpace.CommitChanges();
                    }
                }
            }
        }
        private void CreateTariffBase(IObjectSpace os, Connect connect)
        {
            TariffBaseTerminal tariffBase = connect.FindFirstObject<TariffBaseTerminal>(x => true);
            if (tariffBase == null)
            {
                tariffBase = connect.CreateObject<TariffBaseTerminal>();
                tariffBase.Save();
                os.CommitChanges();
            }
        }

        private void CreateInitialData()
        {
            
        }
    }
}
