﻿namespace TerminalSystem2.Module.Controllers.Services
{
    partial class StocksReportsByDayController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.StockReportByDayAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // StockReportByDayAction
            // 
            this.StockReportByDayAction.Caption = "Отчеты за день";
            this.StockReportByDayAction.ConfirmationMessage = null;
            this.StockReportByDayAction.Id = "StockReportByDayAction";
            this.StockReportByDayAction.TargetObjectType = typeof(TerminalSystem2.OrgStructure.TerminalOrg);
            this.StockReportByDayAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.StockReportByDayAction.ToolTip = null;
            this.StockReportByDayAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.StockReportByDayAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.StockReportByDayAction_Execute);
            // 
            // StocksReportsByDayController
            // 
            this.Actions.Add(this.StockReportByDayAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction StockReportByDayAction;
    }
}
