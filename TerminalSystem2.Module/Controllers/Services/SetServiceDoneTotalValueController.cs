﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.Tarification;
using DevExpress.Xpo;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Module.Controllers.Services
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SetServiceDoneTotalValueController : ViewController
    {
        public SetServiceDoneTotalValueController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// добавить стоимость незакрытой услуги за день 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AddServiceValueByDayAction(UnitOfWork unitOfWork)
        {
            // перебираем все незакрытые услуги и добавляем стоимость за новый день
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            try
            {
                foreach (ServiceDone serviceDone in connect.FindObjects<ServiceDone>(mc => mc.isDone == false))
                {
                    try
                    {

                        // смотрим, если новый месяц не начался
                        if (DateTime.Now.Month == DateTime.Now.AddDays(-1).Month)
                        {
                            ServiceCalculationValue(serviceDone, unitOfWork, DateTime.Now.Date, false);
                        }
                        // если начался новый месяц - закрываем услугу вчерашним днем и создаем новую такую же
                        else
                        {
                            // закрываем услугу вчерашним днем
                            serviceDone.isDone = true;
                            serviceDone.FinishDate = DateTime.Now.AddDays(-1).Date;
                            // если не все в услуге посчитано, то считаем
                            if (serviceDone.ValueTotalDate < DateTime.Now.AddDays(-1).Date)
                            {
                                ServiceCalculationValue(serviceDone, unitOfWork, DateTime.Now.AddDays(-1).Date, false);
                            }
                            serviceDone.Save();

                            // создаем новую услугу
                            ServiceDone serviceNew = connect.CreateObject<ServiceDone>();
                            serviceNew.StartDate = DateTime.Now.Date;
                            serviceNew.Client = serviceDone.Client;
                            serviceNew.Container = serviceDone.Container;
                            serviceNew.Currency = serviceDone.Currency;
                            if (serviceDone.DiscountActiveThroughDate != null)
                            {
                                if (serviceDone.DiscountActiveThroughDate >= DateTime.Now.Date)
                                {
                                    serviceNew.DiscountActiveThroughDate = serviceDone.DiscountActiveThroughDate;
                                    serviceNew.Discount = serviceDone.Discount;
                                }
                            }
                            serviceNew.isDone = false;
                            serviceNew.Tariff = serviceDone.Tariff;
                            serviceNew.UnitKind = serviceDone.UnitKind;
                            serviceNew.Value = serviceDone.Value;
                            serviceNew.ServiceType = serviceDone.ServiceType;

                            serviceNew.Save();
                            unitOfWork.CommitChanges();
                            ServiceCalculationValue(serviceNew, unitOfWork, DateTime.Now.Date, true);
                        }
                        //string text = String.Format("Контейнер {0}, serviceDone: StartDate {1}, ValueTotalDate {2}, servcount {3}", 
                        //    serviceDone.Container.Name, serviceDone.StartDate.ToShortDateString(),
                        //    serviceDone.ValueTotalDate.ToShortDateString(), servcount.ToString());
                        //LogHelperClass.WriteText(text);
                    }
                    catch (Exception ex) {  }
                }
            }
            catch (Exception ex) {  }
        }

        /// <summary>
        /// Функция подсчета стоимости услуги на дату
        /// </summary>
        /// <param name="serviceDone">Услуга, по которой производится расчет</param>
        /// <param name="unitOfWork"></param>
        /// <param name="dateCalc">расчитываем на дату</param>
        /// <param name="isNewService">новая услуга или нет (для определения количества услуги)</param>
        private void ServiceCalculationValue(ServiceDone serviceDone, UnitOfWork unitOfWork, DateTime dateCalc, bool isNewService)
        {
            // если сервис не работал несколько дней, то надо расчитать - сколько (сравниваем дату последнего расчета с датой расчета)
            int servcount = 1;
            if (!isNewService)
            {
                TimeSpan diffDays = dateCalc - serviceDone.ValueTotalDate;
                servcount = diffDays.Days;
            }

            if (serviceDone.DiscountActiveThroughDate != dateCalc)
            {
                if (serviceDone.DiscountActiveThroughDate >= dateCalc)
                {
                    serviceDone.ValueTotal += servcount * serviceDone.Value * (100 - serviceDone.Discount) / 100;
                    serviceDone.ValueTotalDate = dateCalc;
                    serviceDone.ServiceCount += servcount;
                }
                else
                {
                    serviceDone.ValueTotal += serviceDone.Value * servcount;
                    serviceDone.ValueTotalDate = dateCalc;
                    serviceDone.ServiceCount += servcount;
                }
            }
            else
            {
                serviceDone.ValueTotal += serviceDone.Value * servcount;
                serviceDone.ValueTotalDate = dateCalc;
                serviceDone.ServiceCount += servcount;
            }
            serviceDone.Save();
            unitOfWork.CommitChanges();
        }
    }
}
