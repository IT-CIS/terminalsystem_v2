﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using TerminalSystem2.SystemDir;
using TerminalSystem2.DocFlow;

namespace TerminalSystem2.Module.Controllers.Services
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CheckRequestFinishDateController : ViewController
    {
        public CheckRequestFinishDateController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Функция закрытия заявок с истекшим сроком исполнения
        /// </summary>
        /// <param name="unitOfWork"></param>
        public void CheckRequestFinishDate(UnitOfWork unitOfWork)
        {
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            try
            {
                foreach (RequestBase request in connect.FindObjects<RequestBase>(x => x.IsFinished == false))
                {
                    if (request.RequestFinishDate < DateTime.Now.Date)
                    {
                        request.IsFinished = true;
                        request.RequestStopDate = DateTime.Now.Date;
                        request.RequestStatus = TerminalSystem2.Enums.ERequestStatus.ЗакрытаПоВремени;
                        request.Save();
                        unitOfWork.CommitChanges();
                    }
                }
            }
            catch (Exception ex) {  }
        }
    }
}
