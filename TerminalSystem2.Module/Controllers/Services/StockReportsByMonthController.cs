﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;
using TerminalSystem2.BaseClasses;
using System.IO;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.Tarification;

namespace TerminalSystem2.Module.Controllers.Services
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class StockReportsByMonthController : ViewController
    {
        public StockReportsByMonthController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        // формирование отчетов за месяц по каждому стоку
        // перебираем все стоки - смотрим 

        public void CreateMailReportFromStockByMonthAction(UnitOfWork unitOfWork, DateTime rDate)
        {
            // перебираем все стоки, смотрим у них за прошлый день движения контейнеров, если есть - отправляем отчет клиенту по движению
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            foreach (Stock stock in connect.FindObjects<Stock>(mc => true))
            {

                if (isServicesInStockinMonth(stock, rDate))
                {
                    try
                    {
                        // отсылаем клиенту
                        string titleClient = "Отчет по стоку";
                        string messbody = "";
                        string email = "";
                        string month = "";
                        switch (rDate.Month)
                        {
                            case 1:
                                month = "январь";
                                break;
                            case 2:
                                month = "февраль";
                                break;
                            case 3:
                                month = "март";
                                break;
                            case 4:
                                month = "апрель";
                                break;
                            case 5:
                                month = "май";
                                break;
                            case 6:
                                month = "июнь";
                                break;
                            case 7:
                                month = "июль";
                                break;
                            case 8:
                                month = "август";
                                break;
                            case 9:
                                month = "сентябрь";
                                break;
                            case 10:
                                month = "октябрь";
                                break;
                            case 11:
                                month = "ноябрь";
                                break;
                            case 12:
                                month = "декабрь";
                                break;
                            default:
                                month = rDate.Month.ToString();
                                break;
                        }
                        try
                        {
                            titleClient = String.Format("Отчет по стоку {0} за {1}", stock.Name, month);
                        }
                        catch { }
                        try { messbody = String.Format(@"Здравствуйте!
Высылаем Вам отчет по стоку {0} за {1}.", stock.Name, month); }
                        catch { }
                        try { email = stock.Client.Email; }
                        catch { }
                        // формируем Excel файл и получаем ссылку на него
                        string path = "";

                        try
                        {
                            CreateReportsFromStockController CreateReportsFromStockController = new CreateReportsFromStockController();

                            path = CreateReportsFromStockController.CreateXLSReportFromStockByMonth(connect, stock, rDate.Month, rDate.Year);
                        }
                        catch (Exception ex)
                        {
                            string err = String.Format($"Ошибка в формировании отчета за месяц. Сток {stock.Name}.  {ex.Message}, {ex.Source}");
                            try { MailMessageLogic.SendMailMessage(connect, "Ошибка в формировании отчета за месяц", err, "", "info@it-cis.ru"); }
                            catch { }

                        }

                        // отправляем отчет на почту
                        try { MailMessageLogic.SendMailMessage(connect, titleClient, messbody, path, email); }
                        catch (Exception ex) {  }

                        // прикрепляем его к стоку и клиенту
                        try
                        {
                            ServiceReport serviceReport = connect.CreateObject<ServiceReport>();
                            try
                            {
                                serviceReport.Name = String.Format("Отчет по стоку {0} за {1}", stock.Name, month);
                            }
                            catch { }
                            serviceReport.RegDate = DateTime.Now.Date;


                            System.IO.MemoryStream ms = new System.IO.MemoryStream();
                            using (FileStream fs = File.OpenRead(path))
                            {
                                fs.CopyTo(ms);
                                ms.Seek(0, System.IO.SeekOrigin.Begin);
                                FileData filedata = connect.CreateObject<FileData>();
                                filedata.LoadFromStream(path.Split('\\').Last(), ms);
                                serviceReport.File = filedata;
                                ms.Dispose();
                            }
                            stock.ServiceReports.Add(serviceReport);
                            stock.Client.ServiceReports.Add(serviceReport);
                            connect.GetUnitOfWork().CommitChanges();
                        }
                        catch (Exception ex) { }
                        //LogHelperClass.WriteText("Месячный отчет_CreateMailReportFromStockByMonthAction_finish");
                    }
                    catch (Exception ex) {
                        //LogHelperClass.WriteErr(ex, "Ошибка в CreateMailReportFromStockByMonthAction");
                    }
                }
            }
        }

        /// <summary>
        /// Есть ли услуги по контейнерам в стоке в месяце 
        /// </summary>
        /// <param name="stock">Сток</param>
        /// <param name="date">Дата в которой смотрим месяц и год</param>
        /// <returns></returns>
        private bool isServicesInStockinMonth(Stock stock, DateTime date)
        {
            bool res = false;
            foreach (ServiceDone service in stock.Client.ServicesDone)
            {
                // переделать, так как бывают косяки с началом предоставления услуг (в новом месяце не идет пересчет - услуга продолжается)
                if (service.StartDate.Year == date.Year && service.StartDate.Month == date.Month)
                {
                    res = true;
                    break;
                }
            }
            return res;
        }
    }
}
