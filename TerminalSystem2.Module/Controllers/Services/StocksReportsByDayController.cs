﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;
using TerminalSystem2.Containers;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

namespace TerminalSystem2.Module.Controllers.Services
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class StocksReportsByDayController : ViewController
    {
        public StocksReportsByDayController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            bool isAdmin = false;
            // Perform various tasks depending on the target View.
            if (SecuritySystem.CurrentUser != null)
            {
                PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;
                foreach (PermissionPolicyRole role in currentuser.Roles)
                {
                    if (role.IsAdministrative)
                        isAdmin = true;
                }
                if(isAdmin)
                    Frame.GetController<StocksReportsByDayController>().StockReportByDayAction.Active.SetItemValue("myReason", true);
                else
                    Frame.GetController<StocksReportsByDayController>().StockReportByDayAction.Active.SetItemValue("myReason", false);
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void StockReportByDayAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            CreateXLSReportFromStockByDayAction(DateTime.Now.Date.AddDays(-1), connect);
        }

        /// <summary>
        /// сформировать и отпправить отчеты по стокам за день на дату 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CreateXLSReportFromStockByDayAction(DateTime pDate, Connect connect)
        {
            //IObjectSpace os = Application.CreateObjectSpace();
            //Connect connect = Connect.FromObjectSpace(os);
            // перебираем все стоки, смотрим у них за прошлый день движения контейнеров, если есть - отправляем отчет клиенту по движению
            LogHelperClass.WriteText($"-------[CreateXLSReportFromStockByDayAction] Начало формирования отчетов за день {pDate} по стокам!");
            try
            {
                foreach (Stock stock in connect.FindObjects<Stock>(mc => true))
                {
                    bool isCreateRepot = false;
                    // если в стоке есть конейнеры - формируем отчет
                    if (stock.Containers.Count > 0)
                        isCreateRepot = true;
                    // если есть операции по стоку за дату - формируем отчет (возможен случай, когда все контейнеры ушли, в стоке их нет, но операция выдачи в этот день есть)
                    if(connect.IsExist<ContainerHistory>(mc => mc.Stock == stock && mc.Date == pDate))
                        isCreateRepot = true;

                    //LogHelperClass.WriteText("2");
                    if (isCreateRepot)
                    {
                        try
                        {
                            //if (connect.IsExist<ContainerHistory>(mc => mc.Stock == stock && mc.Date == DateTime.Now.AddDays(-1).Date))
                            //{
                            // формируем Excel файл и получаем ссылку на него
                            string path = "";
                            try
                            {
                                CreateReportsFromStockController CreateReportsFromStockController = new CreateReportsFromStockController();

                                path = CreateReportsFromStockController.CreateXlsReportByStock(connect, stock, pDate);
                            }
                            catch (Exception ex) { LogHelperClass.WriteErr(ex, $"-------[CreateXLSReportFromStockByDayAction] Ошибка формирования Excel файла"); }
                            // отсылаем клиенту
                            //stock.Client
                            string titleClient = "Отчет по стоку / Stock report";
                            string messbody = "";
                            string email = "";
                            try { titleClient =  $"Отчет по стоку {stock.Name} за {pDate.ToShortDateString()} / {stock.Name} stock report {pDate.ToShortDateString()}"; }
                            catch { }
                            try { messbody = $"Здравствуйте! " + Environment.NewLine +
                                    $"Высылаем Вам отчет по стоку {stock.Name} за {pDate.ToShortDateString()}." + Environment.NewLine + Environment.NewLine +
                                    $"Hello!" + Environment.NewLine +
                                    $"We send you a report on the stock {stock.Name} for {pDate.ToShortDateString()}"; }
                            catch { }
                            try { email = stock.Client.Email; }
                            catch { }
                            
                            try
                            {
                                MailMessageLogic.SendMailMessage(connect, titleClient, messbody, path, email);
                            }
                            catch (Exception ex) { LogHelperClass.WriteErr(ex, $"-------[CreateXLSReportFromStockByDayAction] Ошибка SendMailMessage. email = {email}"); }
                            //}
                        }
                        catch (Exception ex) { LogHelperClass.WriteErr(ex, $"------- [CreateXLSReportFromStockByDayAction]Ошибка перебора стоков. " +
                            $"stock = {stock?.Name?? "[stock не указан]"}"); }
                    }
                }
            }
            catch (Exception ex) { LogHelperClass.WriteErr(ex, $"-------[CreateXLSReportFromStockByDayAction] Ошибка формирования отчета"); }
        }

        
    }
}
