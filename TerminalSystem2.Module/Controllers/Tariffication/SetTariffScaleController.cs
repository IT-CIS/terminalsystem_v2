﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.DocFlow;
using DevExpress.Xpo;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using DevExpress.ExpressApp.Web;

namespace TerminalSystem2.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SetTariffScaleController : ViewController
    {
        public SetTariffScaleController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Создать к договору активную тарифную сетку, скопировав с базовой
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetTariffScaleFromBaseAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            Contract contract = null;
            contract = os.FindObject<Contract>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)contract.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            // создаем новую активную тарифную сетку, делая все остальные не активными
            // TODO - спрашивать у пользователя, создать просто тарифную сетку, или сразу сделать ее активной.
            TariffScale tariffScale = connect.CreateObject<TariffScale>();
            //foreach(TariffScale ts in contract.TariffScales)
            //{ ts.ActiveFlag = false;
            //    ts.Save();
            //}
            // если тарифных сеток еще нет, то сделать новую сразу активной
            if(contract.TariffScales.Count == 0)
                tariffScale.ActiveFlag = true;
            tariffScale.Save();
            contract.TariffScales.Add(tariffScale);
            // ищем базовый тариф для терминала и копируем все его тарифы в эту сетку
            TariffBaseTerminal tariffBaseTerminal = connect.FindFirstObject<TariffBaseTerminal>(mc => mc.Oid != null);
            if (tariffBaseTerminal != null)
            {
                tariffScale.Currency = tariffBaseTerminal.Currency;
                foreach (Tariff tarifBase in tariffBaseTerminal.Tariffs)
                {
                    Tariff tarifNew = connect.CreateObject<Tariff>();
                    try { tarifNew.TariffService = tarifBase.TariffService; }
                    catch { }
                    try { tarifNew.UnitKind = tarifBase.UnitKind; }
                    catch { }
                    try { tarifNew.Value20Empty = tarifBase.Value20Empty; }
                    catch { }
                    try { tarifNew.Value20Laden = tarifBase.Value20Laden; }
                    catch { }
                    try { tarifNew.Value40Empty = tarifBase.Value40Empty; }
                    catch { }
                    try { tarifNew.Value40Laden = tarifBase.Value40Laden; }
                    catch { }
                    tarifNew.Save();
                    tariffScale.Tariffs.Add(tarifNew);
                }
            }
            tariffScale.Save();
            os.CommitChanges();
            RefreshFrame();
            //DevExpress.ExpressApp.Web.ShowViewStrategy
            //foreach (WebWindow existingWindow in ((ShowViewStrategy)Application.ShowViewStrategy).
            //Window existingWindow = connect.FindFirstObject<Window>(x => x.View.CreateShortcut() == View.CreateShortcut());
            //if(existingWindow != null && existingWindow.GetController<RefreshController>() != null)
            //    existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
        }

        protected void RefreshFrame()
        {
            RefreshController controller = Frame.GetController<RefreshController>();
            if (controller != null)
                controller.RefreshAction.DoExecute();
        }

        //protected void RefreshAllWindows()
        //{
        //    foreach (Window existingWindow in ((ShowViewStrategyBase)Application.ShowViewStrategy)..Windows)
        //    {
        //        if (existingWindow.View != null && existingWindow.GetController<RefreshController>() != null)
        //        {
        //            existingWindow.GetController<DevExpress.ExpressApp.SystemModule.RefreshController>().RefreshAction.DoExecute();
        //        }
        //    }
        //}
    }
}
