﻿namespace TerminalSystem2.Module.Controllers
{
    partial class ContainerPlacementViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ContainerPlacementAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ContainerFinishPlacementAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ContainerPlacementPopupAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.ContainerPlacementFromStockPopupAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // ContainerPlacementAction
            // 
            this.ContainerPlacementAction.Caption = "Приемка";
            this.ContainerPlacementAction.Category = "ContainerPlacement";
            this.ContainerPlacementAction.ConfirmationMessage = null;
            this.ContainerPlacementAction.Id = "ContainerPlacementAction";
            this.ContainerPlacementAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ContainerPlacementAction.TargetObjectType = typeof(TerminalSystem2.DocFlow.RequestContainersPlacement);
            this.ContainerPlacementAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerPlacementAction.ToolTip = null;
            this.ContainerPlacementAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerPlacementAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ContainerPlacementAction_Execute);
            // 
            // ContainerFinishPlacementAction
            // 
            this.ContainerFinishPlacementAction.Caption = "Контейнер размещен";
            this.ContainerFinishPlacementAction.ConfirmationMessage = null;
            this.ContainerFinishPlacementAction.Id = "ContainerFinishPlacementAction";
            this.ContainerFinishPlacementAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ContainerFinishPlacementAction.TargetObjectType = typeof(TerminalSystem2.Containers.ContainerPlacementAct);
            this.ContainerFinishPlacementAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerFinishPlacementAction.ToolTip = null;
            this.ContainerFinishPlacementAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerFinishPlacementAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ContainerFinishPlacementAction_Execute);
            // 
            // ContainerPlacementPopupAction
            // 
            this.ContainerPlacementPopupAction.AcceptButtonCaption = null;
            this.ContainerPlacementPopupAction.CancelButtonCaption = null;
            this.ContainerPlacementPopupAction.Caption = "Разместить контейнер";
            this.ContainerPlacementPopupAction.Category = "ObjectsCreation";
            this.ContainerPlacementPopupAction.ConfirmationMessage = null;
            this.ContainerPlacementPopupAction.Id = "ContainerPlacementPopupAction";
            this.ContainerPlacementPopupAction.TargetObjectType = typeof(TerminalSystem2.Containers.Container);
            this.ContainerPlacementPopupAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.ContainerPlacementPopupAction.ToolTip = "Размещение контейнера на терминале";
            this.ContainerPlacementPopupAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.ContainerPlacementPopupAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ContainerPlacementPopupAction_CustomizePopupWindowParams);
            this.ContainerPlacementPopupAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.ContainerPlacementPopupAction_Execute);
            // 
            // ContainerPlacementFromStockPopupAction
            // 
            this.ContainerPlacementFromStockPopupAction.AcceptButtonCaption = null;
            this.ContainerPlacementFromStockPopupAction.CancelButtonCaption = null;
            this.ContainerPlacementFromStockPopupAction.Caption = "Разместить контейнер";
            this.ContainerPlacementFromStockPopupAction.ConfirmationMessage = null;
            this.ContainerPlacementFromStockPopupAction.Id = "ContainerPlacementFromStockPopupAction";
            this.ContainerPlacementFromStockPopupAction.TargetObjectType = typeof(TerminalSystem2.Terminals.Stock);
            this.ContainerPlacementFromStockPopupAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerPlacementFromStockPopupAction.ToolTip = "Размещение контейнера в стоке";
            this.ContainerPlacementFromStockPopupAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerPlacementFromStockPopupAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ContainerPlacementFromStockPopupAction_CustomizePopupWindowParams);
            this.ContainerPlacementFromStockPopupAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.ContainerPlacementPopupAction_Execute);
            // 
            // ContainerPlacementViewController
            // 
            this.Actions.Add(this.ContainerPlacementAction);
            this.Actions.Add(this.ContainerFinishPlacementAction);
            this.Actions.Add(this.ContainerPlacementPopupAction);
            this.Actions.Add(this.ContainerPlacementFromStockPopupAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ContainerPlacementAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ContainerFinishPlacementAction;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ContainerPlacementPopupAction;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ContainerPlacementFromStockPopupAction;
    }
}
