﻿namespace TerminalSystem2.Module.Controllers
{
    partial class ReportContainerPlacementActController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ContainerInspectionActLandScapeAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ContainerInspectionActLandScapeAction
            // 
            this.ContainerInspectionActLandScapeAction.Caption = "Сформировать акт осмотра";
            this.ContainerInspectionActLandScapeAction.ConfirmationMessage = null;
            this.ContainerInspectionActLandScapeAction.Id = "ContainerInspectionActLandScapeAction";
            this.ContainerInspectionActLandScapeAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ContainerInspectionActLandScapeAction.TargetObjectType = typeof(TerminalSystem2.Containers.ContainerPlacementAct);
            this.ContainerInspectionActLandScapeAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerInspectionActLandScapeAction.ToolTip = null;
            this.ContainerInspectionActLandScapeAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerInspectionActLandScapeAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ContainerInspectionActLandScapeAction_Execute);
            // 
            // ReportContainerPlacementActController
            // 
            this.Actions.Add(this.ContainerInspectionActLandScapeAction);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ContainerInspectionActLandScapeAction;
    }
}
