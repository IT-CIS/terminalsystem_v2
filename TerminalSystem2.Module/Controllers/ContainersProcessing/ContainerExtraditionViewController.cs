﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.DocFlow;
using TerminalSystem2.SystemDir;
using DevExpress.Xpo;
using TerminalSystem2.Containers;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Terminals;
using TerminalSystem2.Tarification;
using TerminalSystem2.OrgStructure;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ContainerExtraditionViewController : ViewController
    {
        public ContainerExtraditionViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            Employee empl = null;
            ClientEmployee clientEmpl = null;
            if (SecuritySystem.CurrentUser != null)
            {
                PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;

                IObjectSpace os = Application.CreateObjectSpace();
                empl = os.FindObject<Employee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                clientEmpl = os.FindObject<ClientEmployee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                if (clientEmpl != null)
                {
                    Frame.GetController<ContainerExtraditionViewController>().ContainerExtraditionAction.Active.SetItemValue("myReason", false);
                    Frame.GetController<ContainerExtraditionViewController>().ContainerExtraditionFinishAction.Active.SetItemValue("myReason", false); 
                    Frame.GetController<ContainerExtraditionViewController>().ExtraditionFromContainerAction.Active.SetItemValue("myReason", false);
                }
            }
            if (View is DetailView)
            {
                if (empl != null)
                {
                    if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersExtradition")
                    {
                        RequestContainersExtradition request = View.CurrentObject as RequestContainersExtradition;
                        // to do - изменение видимости в зависисмости от типа сотрудника (сотрудник клиента или терминала)
                        if (request.RequestStatus == Enums.ERequestStatus.Подана || request.RequestStatus == Enums.ERequestStatus.Обработка ||
                            request.RequestStatus == Enums.ERequestStatus.Отредактирована || request.RequestStatus == Enums.ERequestStatus.ОбработанаЧастично)
                        {
                            Frame.GetController<ContainerExtraditionViewController>().ContainerExtraditionAction.Active.SetItemValue("myReason", true);
                        }
                        else
                            Frame.GetController<ContainerExtraditionViewController>().ContainerExtraditionAction.Active.SetItemValue("myReason", false);
                    }
                    if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.Containers.ContainerExtraditionAct")
                    {
                        ContainerExtraditionAct containerExtraditionAct = View.CurrentObject as ContainerExtraditionAct;
                        if (containerExtraditionAct.Container == null)
                            Frame.GetController<ContainerExtraditionViewController>().ContainerExtraditionFinishAction.Active.SetItemValue("myReason", true);
                        else
                        {
                            if (containerExtraditionAct.Container?.onTerminal == true)
                            {
                                Frame.GetController<ContainerExtraditionViewController>().ContainerExtraditionFinishAction.Active.SetItemValue("myReason", true);
                            }
                            else
                                Frame.GetController<ContainerExtraditionViewController>().ContainerExtraditionFinishAction.Active.SetItemValue("myReason", false);
                        }

                    }
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Выдача контейнера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerExtraditionAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;
            //RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            RequestContainersExtradition request;
            request = os.FindObject<RequestContainersExtradition>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)request.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            RequestContainerExtraditionInfo requestContainerInfo = null;
            RequestDriverInfo requestDriverInfo = null;
            //request.RequestContainerInfos
            // получаем выбранные данные по контейнеру
            DetailView masterDV = (DetailView)View;
            foreach (ListPropertyEditor editor in masterDV.GetItems<ListPropertyEditor>())
            {
                if (editor.ListView.Id == "RequestContainersExtradition_RequestContainerInfos_ListView")
                {
                    if (request.RequestContainerInfos.Count == 1)
                        requestContainerInfo = request.RequestContainerInfos[0];
                    else
                    {
                        if (editor.ListView.SelectedObjects.Count == 0)
                            throw new Exception("Необходимо выбрать один контейнер!");
                        if (editor.ListView.SelectedObjects.Count > 1)
                            throw new Exception("Необходимо выбрать только один контейнер!");
                        requestContainerInfo = editor.ListView.SelectedObjects[0] as RequestContainerExtraditionInfo;
                    }
                }
                if (editor.ListView.Id == "RequestBaseDriversInfo_RequestDriverInfos_ListView")
                {
                    //if (editor.ListView.SelectedObjects.Count == 0)
                    //    throw new Exception("Необходимо выбрать водителя!");
                    //if (editor.ListView.SelectedObjects.Count > 1)
                    //    throw new Exception("Необходимо выбрать только одного водителя!");
                    if (request.RequestDriverInfos.Count == 1)
                        requestDriverInfo = request.RequestDriverInfos[0];
                    else
                    {
                        try
                        {
                            requestDriverInfo = editor.ListView.SelectedObjects[0] as RequestDriverInfo;
                        }
                        catch { }
                    }
                }
            }
            if (requestContainerInfo != null)
            {

                ContainerExtraditionAct containerExtraditionAct = null;
                // Проверяем, что такой акт еще не создавали
                //if (requestContainerInfo.RequestStatus == Enums.ERequestStatus.Отработана ||
                //    requestContainerInfo.RequestStatus == Enums.ERequestStatus.Обработка)
                //{
                //    containerExtraditionAct = connect.FindFirstObject<ContainerExtraditionAct>(mc => mc.RequestContainerExtraditionInfo.Oid == requestContainerInfo.Oid);
                //}
                containerExtraditionAct = connect.FindFirstObject<ContainerExtraditionAct>(mc => mc.RequestContainerExtraditionInfo.Oid == requestContainerInfo.Oid);

                if(containerExtraditionAct == null)
                {
                    // создаем акт о приемке
                    containerExtraditionAct = connect.CreateObject<ContainerExtraditionAct>();
                    containerExtraditionAct.LiftCount = 1;

                    try
                    {
                        containerExtraditionAct.ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(request.Session.DataLayer,
                  "Акт выдачи контейнеров" + request.Client.Name + DateTime.Now.Year, string.Empty)); ;
                    }
                    catch { }
                    // сначала проверям, указан ли водитель у контейнера напрямую, если нет, пробуем посмотреть, может его выбрали галкой
                    if (requestContainerInfo.RequestDriverInfo != null)
                    {
                        try { containerExtraditionAct.DriverName = requestContainerInfo.RequestDriverInfo.DriverName?? ""; }
                        catch { }
                        try { containerExtraditionAct.DriverPhoneNumber = requestContainerInfo.RequestDriverInfo.DriverPhoneNumber ?? ""; }
                        catch { }
                        try { containerExtraditionAct.TransportNumber = requestContainerInfo.RequestDriverInfo.TransportNumber ?? ""; }
                        catch { }
                        try
                        {
                            if(requestContainerInfo.RequestDriverInfo.TransportType != null)
                                containerExtraditionAct.TransportType = connect.FindFirstObject<dTransportType>(mc =>
                          mc.Oid == requestContainerInfo.RequestDriverInfo.TransportType.Oid);
                        }
                        catch { }
                        try
                        {
                            if (requestContainerInfo.RequestDriverInfo.TransportMark != null)
                                containerExtraditionAct.TransportMark = connect.FindFirstObject<dTransportMark>(mc =>
                      mc.Oid == requestContainerInfo.RequestDriverInfo.TransportMark.Oid);
                        }
                        catch { }
                        try { containerExtraditionAct.TransportCompany = requestContainerInfo.RequestDriverInfo.TransportCompany ?? ""; }
                        catch { }
                        try
                        {
                            containerExtraditionAct.RequestDriverInfo = connect.FindFirstObject<RequestDriverInfo>(mc =>
                            mc.Oid == requestContainerInfo.RequestDriverInfo.Oid);
                        }
                        catch { }
                    }
                    else
                    {
                        if (requestDriverInfo != null)
                        {
                            try { containerExtraditionAct.DriverName = requestDriverInfo?.DriverName?? ""; }
                            catch { }
                            try { containerExtraditionAct.DriverPhoneNumber = requestDriverInfo?.DriverPhoneNumber?? ""; }
                            catch { }
                            try { containerExtraditionAct.TransportNumber = requestDriverInfo?.TransportNumber?? ""; }
                            catch { }
                            try {
                                if(requestDriverInfo.TransportType != null)
                                    containerExtraditionAct.TransportType = connect.FindFirstObject<dTransportType>(mc => mc.Oid == requestDriverInfo.TransportType.Oid); }
                            catch { }
                            try {
                                if (requestDriverInfo.TransportMark != null)
                                    containerExtraditionAct.TransportMark = connect.FindFirstObject<dTransportMark>(mc => mc.Oid == requestDriverInfo.TransportMark.Oid); }
                            catch { }
                            try { containerExtraditionAct.TransportCompany = requestDriverInfo?.TransportCompany?? ""; }
                            catch { }
                            try
                            {
                                containerExtraditionAct.RequestDriverInfo = connect.FindFirstObject<RequestDriverInfo>(mc => mc.Oid == requestDriverInfo.Oid);
                            }
                            catch { }
                        }
                    }
                    
                    // если контейнер указан - подставляем его
                    string contNumber = String.Empty;
                    if (requestContainerInfo.Container != null)
                    {
                        try
                        {
                            contNumber = requestContainerInfo.Container.ContainerNumber;
                            //containerExtraditionAct.Container = connect.FindFirstObject<Container>(mc => mc.Oid.ToString() == requestContainerInfo.Container.Oid.ToString());
                        }
                        catch { }
                    }
                    else
                        if(!String.IsNullOrEmpty(requestContainerInfo.Number))
                        {
                            contNumber = requestContainerInfo.Number;
                            //try
                            //{
                            //    containerExtraditionAct.Container = connect.FindFirstObject<Container>(mc => mc.ContainerNumber == requestContainerInfo.Number);
                            //}
                            //catch { }
                        }
                    if (contNumber != String.Empty)
                    {
                        //try
                        //{
                            containerExtraditionAct.Container = connect.FindFirstObject<Container>(mc => mc.ContainerNumber == contNumber && mc.onTerminal);
                        //}
                        //catch { }
                        if (containerExtraditionAct.Container == null)
                            throw new UserFriendlyException($"Указанный Вами контейнер [{contNumber}]  не найден на терминале. Пожалуйста, проверте правильность указанных данных контейнера!");
                    }


                    try
                    {
                        if(requestContainerInfo.ContainerType != null)
                            containerExtraditionAct.ContainerType = connect.FindFirstObject<dContainerType>(mc => mc.Oid.ToString() == requestContainerInfo.ContainerType.Oid.ToString());
                    }
                    catch { }


                    containerExtraditionAct.Request = request;
                    requestContainerInfo.RequestStatus = Enums.ERequestStatus.Обработка;
                    containerExtraditionAct.RequestContainerExtraditionInfo = connect.FindFirstObject<RequestContainerExtraditionInfo>(mc => mc.Oid == requestContainerInfo.Oid);


                    containerExtraditionAct.Save();
                    os.CommitChanges();
                }
                // открываем окно акта 
                DetailView dv = Application.CreateDetailView(os, containerExtraditionAct);//Specify the IsRoot parameter if necessary.
                dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                e.ShowViewParameters.CreatedView = dv;
                View.Close();
            }
        }

        /// <summary>
        /// Окончательно выдаем контейнер:
        /// - Создаем историю по выдаче
        /// - Создаем оказанные услуги на терминале (крановая операция)
        /// - Закрываем хранение
        /// - меняем статусы (инфо по контейнеру, и если нужно - заявки)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerExtraditionFinishAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            ContainerExtraditionAct containerExtraditionAct = null;
            containerExtraditionAct = os.FindObject<ContainerExtraditionAct>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)containerExtraditionAct.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            // История движения контейнера (выдачи с терминала)
            ContainerHistory containerHistory = null;
            Container container;

            if (containerExtraditionAct.Container == null)
                throw new Exception("Необходимо указать выданный контейнер");

            container = containerExtraditionAct.Container;


            // проверяем, создали ли уже по этому размещению историю
            containerHistory = connect.FindFirstObject<ContainerHistory>(mc => mc.ContainerExtraditionAct.Oid == containerExtraditionAct.Oid);
            if (containerHistory == null)
            {
                containerHistory = connect.CreateObject<ContainerHistory>();
                containerHistory.ContainerHistoryKind = Enums.EContainerHistoryKind.Выдача;
                containerHistory.ContainerExtraditionAct = containerExtraditionAct;
                containerHistory.Container = container;
                containerHistory.Stock = container.Stock;
                containerHistory.Date = containerExtraditionAct.ActDate;
                containerHistory.Time = DateTime.Now.ToShortTimeString();
                try
                {
                    containerHistory.DriverName = containerExtraditionAct.DriverName;
                }
                catch { }
                try
                {
                    containerHistory.DriverPhoneNumber = containerExtraditionAct.DriverPhoneNumber;
                }
                catch { }
                try
                {
                    containerHistory.TransportNumber = containerExtraditionAct.TransportNumber;
                }
                catch { }
                try
                {
                    containerHistory.Notes = container.SpecialNotes;
                }
                catch { }
                try
                {
                    containerHistory.isLaden = container.isLaden;
                }
                catch { }
                try
                {
                    containerHistory.LadenWeight = container.LadenWeight;
                }
                catch { }
                try
                {
                    containerHistory.CargoType = container.CargoType;
                }
                catch { }
                try
                {
                    containerHistory.SealNumber = container.SealNumber;
                }
                catch { }

                containerHistory.Save();

                try {
                    CreateEmailNotificationsController CreateEmailNotificationsController = new CreateEmailNotificationsController();
                    CreateEmailNotificationsController.CreateNotifacations(containerHistory, connect);
                }
                catch { }
            }

            // очищаем данные по контейнеру (по размещению, по грузу и т.д.), кроме основных данных (тип, цвет, размер)
            container.onTerminal = false;
            container.EditFlag = false;
            container.Dislocation = "";
            container.isLaden = false;
            container.isSingleOccupancy = false;
            container.LadenWeight = 0;
            container.SealNumber = "";
            container.Stock = null;
            container.Terminal = null;
            container.TerminalRow = null;
            container.TerminalSection = null;
            container.TerminalZone = null;
            container.Tier = null;
            container.Save();

            os.CommitChanges();

            // создаем\завершаем оказанные услуги по контейнеру:
            if (containerExtraditionAct.CalcType == Enums.ECalcType.byDaily)
            {
                // a. По крановой операции
                /// a ///
                ServiceDone serviceLiftDone = null;
                bool tariffLiftFound = false;
                if (containerExtraditionAct.LiftPriceTotal != 0)
                {
                    serviceLiftDone = connect.CreateObject<ServiceDone>();
                    serviceLiftDone.Container = container;
                    serviceLiftDone.Client = container.Owner;
                    serviceLiftDone.StartDate = containerExtraditionAct.ActDate;//DateTime.Now.Date;
                                                                              //serviceLiftDone.FinishDate = DateTime.Now.Date;
                    try
                    {
                        serviceLiftDone.Tariff = containerExtraditionAct?.TariffLift;
                    }
                    catch { }
                    try
                    {
                        serviceLiftDone.UnitKind = containerExtraditionAct?.TariffLift?.UnitKind;
                    }
                    catch { }
                    try
                    {
                        serviceLiftDone.Currency = containerExtraditionAct.Currency;
                    }
                    catch { }
                    serviceLiftDone.ServiceCount = containerExtraditionAct.LiftCount;
                    serviceLiftDone.ServiceType = connect.FindFirstObject<dServiceType>(x=> x.Name == "Крановая операция");

                    serviceLiftDone.ValueTotal = containerExtraditionAct.LiftPriceTotal;
                    serviceLiftDone.ValueTotalDate = containerExtraditionAct.ActDate;
                    serviceLiftDone.FinishDate = containerExtraditionAct.ActDate;
                    tariffLiftFound = true;
                }

                #region Переделали по другому
                /////// a ///
                ////ServiceDone serviceLiftDone = connect.CreateObject<ServiceDone>();
                ////serviceLiftDone.Container = container;
                ////serviceLiftDone.Client = container.Owner;
                ////serviceLiftDone.StartDate = containerExtraditionAct.ActDate;
                //////serviceLiftDone.FinishDate = DateTime.Now.Date;
                //////serviceLiftDone.isDone = true;
                ////serviceLiftDone.ServiceCount = containerExtraditionAct.LiftCount;
                ////bool tariffLiftFound = false;


                /// 1. Ищем активный договор у клиента
                /// 2. Ищем в договоре актиную тарифную сетку
                /// 3. Ищем тарифы по виду операции 
                /// 4. Ищем нужный тариф по типу контейнера
                /// 4.1 Проверям на действие скидки
                /// 5. Создаем у контейнера 

                // 1 //
                //Contract contract = connect.FindFirstObject<Contract>(mc => mc.Client == container.Owner && mc.ActiveFlag == true);
                //if (contract != null)
                //{
                //    // 2 //
                //    TariffScale tariffScale = connect.FindFirstObject<TariffScale>(mc => mc.Contract == contract && mc.ActiveFlag == true);
                //    if (tariffScale != null)
                //    {
                //        serviceLiftDone.Currency = tariffScale.Currency;

                //        // Определям, есть ли скидка и ее тип. Если есть - проставляем ее значение и действие для услуги лифта
                //        //либо на все операции какие то проценты до определенной даты, 
                //        if (tariffScale.AddDiscount)
                //        {
                //            if (tariffScale.DiscountType == Enums.EDiscountType.byTime)
                //            {
                //                // смотрим, не прошла ли еще скидка
                //                if (tariffScale.DiscountActiveThroughDate >= containerExtraditionAct.ActDate)
                //                {
                //                    serviceLiftDone.Discount = tariffScale.Discount;
                //                    serviceLiftDone.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;
                //                }
                //            }
                //        }
                //        // a //
                //        //3-4//
                //        IQueryable tariffs = connect.FindObjects<Tariff>(mc => mc.TariffBase == tariffScale && mc.TariffService.ServiceType.Name == "Крановая операция");
                //        foreach (Tariff tariff in tariffs)
                //        {
                //            foreach (dContainerSubType containerSubType in tariff.TariffService.ContainerSubTypes)
                //            {
                //                //5//
                //                if (container.ContainerType.ContainerSubType == containerSubType)
                //                {
                //                    serviceLiftDone.Tariff = tariff;
                //                    serviceLiftDone.UnitKind = tariff.UnitKind;
                //                    if (container.ContainerType.ContainerTypeSize.ContainerCategorySize == Enums.EContainerCategorySize.twenty)
                //                    {
                //                        if (container.isLaden)
                //                            serviceLiftDone.Value = tariff.Value20Laden;
                //                        else
                //                            serviceLiftDone.Value = tariff.Value20Empty;
                //                    }
                //                    else
                //                    {
                //                        if (container.isLaden)
                //                            serviceLiftDone.Value = tariff.Value40Laden;
                //                        else
                //                            serviceLiftDone.Value = tariff.Value40Empty;
                //                    }
                //                    tariffLiftFound = true;
                //                    break;
                //                }
                //            }
                //        }
                //    }
                //}
                //// считаем расходы по крановым операциям
                //if (tariffLiftFound)
                //{
                //    if (serviceLiftDone.DiscountActiveThroughDate != DateTime.MinValue)
                //    {
                //        if (serviceLiftDone.DiscountActiveThroughDate >= containerExtraditionAct.ActDate)
                //        {
                //            serviceLiftDone.ValueTotal += serviceLiftDone.ServiceCount * serviceLiftDone.Value * (100 - serviceLiftDone.Discount) / 100;
                //            serviceLiftDone.ValueTotalDate = containerExtraditionAct.ActDate;
                //        }
                //        else
                //        {
                //            serviceLiftDone.ValueTotal += serviceLiftDone.Value * serviceLiftDone.ServiceCount;
                //            serviceLiftDone.ValueTotalDate = containerExtraditionAct.ActDate;
                //        }
                //    }
                //    else
                //    {
                //        serviceLiftDone.ValueTotal += serviceLiftDone.Value * serviceLiftDone.ServiceCount;
                //        serviceLiftDone.ValueTotalDate = containerExtraditionAct.ActDate;
                //    }
                //    serviceLiftDone.isDone = true;
                //    serviceLiftDone.FinishDate = containerExtraditionAct.ActDate;
                //    serviceLiftDone.Save();
                //}
                #endregion

                // b. Завершение хранения
                /// b - ищем у контейнера не закрытую услугу хранения ///
                ServiceDone serviceStorageDone = connect.FindFirstObject<ServiceDone>(mc => mc.Container == container && mc.ServiceType.Name == "Хранение контейнера" && mc.isDone == false);
                if (serviceStorageDone != null)
                {
                    // если на текущий день нет подсчета
                    if (serviceStorageDone.ValueTotalDate < containerExtraditionAct.ActDate)
                    {
                        // количество не подсчитанных дней
                        //int noCalcDays = (containerExtraditionAct.ActDate - serviceStorageDone.ValueTotalDate).Days;

                        serviceStorageDone.ServiceCount = (containerExtraditionAct.ActDate.AddDays(1) - serviceStorageDone.StartDate).Days;

                        if (serviceStorageDone.DiscountActiveThroughDate != DateTime.MinValue)
                        {
                            if (serviceStorageDone.DiscountActiveThroughDate >= containerExtraditionAct.ActDate)
                            {
                                serviceStorageDone.ValueTotal = serviceStorageDone.ServiceCount * serviceStorageDone.Value * (100 - serviceStorageDone.Discount) / 100;
                                serviceStorageDone.ValueTotalDate = containerExtraditionAct.ActDate;
                            }
                            else
                            {
                                serviceStorageDone.ValueTotal = serviceStorageDone.Value *
                                    ((serviceStorageDone.DiscountActiveThroughDate.AddDays(1) - serviceStorageDone.StartDate).Days * (100 - serviceStorageDone.Discount) / 100 +
                                    (containerExtraditionAct.ActDate - serviceStorageDone.DiscountActiveThroughDate).Days);
                                serviceStorageDone.ValueTotalDate = containerExtraditionAct.ActDate;
                            }
                        }
                        else
                        {
                            serviceStorageDone.ValueTotal = serviceStorageDone.ServiceCount * serviceStorageDone.Value;
                            serviceStorageDone.ValueTotalDate = containerExtraditionAct.ActDate;
                        }


                    }
                    serviceStorageDone.isDone = true;
                    serviceStorageDone.FinishDate = containerExtraditionAct.ActDate;
                    serviceStorageDone.Save();
                }
            }
            // меняем статус инфо по контейнеру
            try
            {
                RequestContainerExtraditionInfo requestContainerInfo = containerExtraditionAct.RequestContainerExtraditionInfo;
                RequestContainersExtradition request = containerExtraditionAct.Request;
                requestContainerInfo.RequestStatus = Enums.ERequestStatus.Отработана;
                requestContainerInfo.Save();
                os.CommitChanges();
                bool isClosed = true;
                foreach (RequestContainerExtraditionInfo rContainerInfo in containerExtraditionAct.Request.RequestContainerInfos)
                {
                    if (rContainerInfo.RequestStatus != Enums.ERequestStatus.Отработана)
                    {
                        isClosed = false;
                        break;
                    }
                }
                if (SecuritySystem.CurrentUser != null)
                {
                    PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;
                    request.Employee = os.FindObject<Employee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                }
                request.RequestStatus = Enums.ERequestStatus.ОбработанаЧастично;
                if (isClosed)
                {
                    request.RequestStatus = Enums.ERequestStatus.Отработана;
                    request.RequestStopDate = DateTime.Now.Date;
                    request.IsFinished = true;
                }
                request.Save();
            }
            catch { }
            os.CommitChanges();
            View.Close();
        }


        /// <summary>
        /// Выдача контейнера с терминала (action с самого контейнера) - открываем окно акта выдачи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExtraditionFromContainerAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;
            //RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            Container container;
            container = os.FindObject<Container>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)container.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            ContainerExtraditionAct containerExtraditionAct = null;

            // создаем акт о приемке
            containerExtraditionAct = connect.CreateObject<ContainerExtraditionAct>();
            containerExtraditionAct.LiftCount = 1;

            try
            {
                containerExtraditionAct.ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(container.Session.DataLayer,
          "Акт выдачи контейнеров" + container.Owner.Name + DateTime.Now.Year, string.Empty)); ;
            }
            catch { }
            
            try
            {
                containerExtraditionAct.Container = connect.FindFirstObject<Container>(mc => mc.Oid.ToString() == container.Oid.ToString());
            }
            catch { }
            try
            {
                containerExtraditionAct.ContainerType = connect.FindFirstObject<dContainerType>(mc => mc.Oid.ToString() == container.ContainerType.Oid.ToString());
            }
            catch { }
            try
            {
                containerExtraditionAct.Stock = connect.FindFirstObject<Stock>(mc => mc.Oid.ToString() == container.Stock.Oid.ToString());
            }
            catch { }

            containerExtraditionAct.Save();
            os.CommitChanges();

        // открываем окно акта 
        DetailView dv = Application.CreateDetailView(os, containerExtraditionAct);//Specify the IsRoot parameter if necessary.
        dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                e.ShowViewParameters.CreatedView = dv;
                View.Close();
        }
    }
}
