﻿namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    partial class ReportContainerExtraditionActController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ContainerInspectionActExtraditionAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ContainerInspectionActExtraditionAction
            // 
            this.ContainerInspectionActExtraditionAction.Caption = "Сформировать акт осмотра";
            this.ContainerInspectionActExtraditionAction.ConfirmationMessage = null;
            this.ContainerInspectionActExtraditionAction.Id = "ContainerInspectionActExtraditionAction";
            this.ContainerInspectionActExtraditionAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ContainerInspectionActExtraditionAction.TargetObjectType = typeof(TerminalSystem2.Containers.ContainerExtraditionAct);
            this.ContainerInspectionActExtraditionAction.ToolTip = null;
            this.ContainerInspectionActExtraditionAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ContainerInspectionActExtraditionAction_Execute);
            // 
            // ReportContainerExtraditionActController
            // 
            this.Actions.Add(this.ContainerInspectionActExtraditionAction);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ContainerInspectionActExtraditionAction;
    }
}
