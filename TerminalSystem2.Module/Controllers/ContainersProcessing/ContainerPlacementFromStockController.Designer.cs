﻿namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    partial class ContainerPlacementFromStockController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ContainerPlacementFromStockAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ContainerPlacementPopupAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // ContainerPlacementFromStockAction
            // 
            this.ContainerPlacementFromStockAction.Caption = "Разместить контейнер";
            this.ContainerPlacementFromStockAction.Category = "ContainerPlacementFromStock";
            this.ContainerPlacementFromStockAction.ConfirmationMessage = null;
            this.ContainerPlacementFromStockAction.Id = "ContainerPlacementFromStockAction";
            this.ContainerPlacementFromStockAction.TargetObjectType = typeof(TerminalSystem2.Containers.ContainerPlacementInfo);
            this.ContainerPlacementFromStockAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerPlacementFromStockAction.ToolTip = null;
            this.ContainerPlacementFromStockAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerPlacementFromStockAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ContainerPlacementFromStockAction_Execute);
            // 
            // ContainerPlacementPopupAction
            // 
            this.ContainerPlacementPopupAction.AcceptButtonCaption = null;
            this.ContainerPlacementPopupAction.CancelButtonCaption = null;
            this.ContainerPlacementPopupAction.Caption = "Разместить контейнер в стоке";
            this.ContainerPlacementPopupAction.ConfirmationMessage = null;
            this.ContainerPlacementPopupAction.Id = "ContainerPlacementPopupAction";
            this.ContainerPlacementPopupAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ContainerPlacementPopupAction.TargetObjectType = typeof(TerminalSystem2.Terminals.Stock);
            this.ContainerPlacementPopupAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerPlacementPopupAction.ToolTip = null;
            this.ContainerPlacementPopupAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerPlacementPopupAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ContainerPlacementPopupAction_CustomizePopupWindowParams);
            this.ContainerPlacementPopupAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.ContainerPlacementPopupAction_Execute);
            // 
            // ContainerPlacementFromStockController
            // 
            this.Actions.Add(this.ContainerPlacementFromStockAction);
            this.Actions.Add(this.ContainerPlacementPopupAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ContainerPlacementFromStockAction;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ContainerPlacementPopupAction;
    }
}
