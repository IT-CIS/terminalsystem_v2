﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.SystemDir;
using DevExpress.Xpo;
using TerminalSystem2.Terminals;
using TerminalSystem2.Containers;
using TerminalSystem2.Clients;
using TerminalSystem2.Classifiers;
using DevExpress.ExpressApp.Web;

namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ContainerPlacementFromStockController : ViewController
    {
        public ContainerPlacementFromStockController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// TODO - переделать но ASPXPopUpControl

        /// <summary>
        /// Размещение контейнера в стоке сотрудником терминала
        /// 1. Сначала открываем окно заполнения данных по контейнеру (номер и тип), если такой контейнер есть в системе, то в акт подставлем его.
        /// 2. потом открываем окно акта о приемке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerPlacementFromStockAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            ContainerPlacementInfo placementInfo;
            // получаем наш попап объект, его сессию и сохраняем его, после этого, получаем его из ObjectSpace
            //placementInfo = (ContainerPlacementInfo)e.CurrentObject;
            //UnitOfWork unitOfWork = (UnitOfWork)placementInfo.Session;
            //placementInfo.Save();
            //unitOfWork.CommitChanges();
            //Connect connect = Connect.FromUnitOfWork(unitOfWork);

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            placementInfo = os.FindObject<ContainerPlacementInfo>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)placementInfo.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            if (placementInfo.ContainerNumber == "")
                throw new Exception("Не указан номер контейнера!");
            // ищем контейнер
            Container container = connect.FindFirstObject<Container>(x => x.ContainerNumber == placementInfo.ContainerNumber);
            if (container != null)
            {
                try
                {
                    if (container.Owner.Oid != placementInfo.Owner.Oid)
                    {
                        if (container.SpecialNotes != null && container.SpecialNotes != "")
                            container.SpecialNotes += Environment.NewLine;
                        container.SpecialNotes += String.Format("Похоже, что у контейнера сменился владелец: раньше был {0}, стал {1} !", container.Owner.Name, placementInfo.Owner.Name);
                    }
                    //throw new Exception(String.Format("В системе найден контейнер с номером [{0}], но его владелец !"));
                }
                catch { }

            }
            else
            {
                container = connect.CreateObject<Container>();
                container.ContainerNumber = placementInfo.ContainerNumber;
                try { container.ContainerType = connect.FindFirstObject<dContainerType>(mc => mc.Oid == placementInfo.ContainerType.Oid); }
                catch { }
            }
            Stock stock = placementInfo.Stock;
            ContainerPlacementAct containerPlacementAct = connect.CreateObject<ContainerPlacementAct>();

            try
            {
                containerPlacementAct.ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(stock.Session.DataLayer,
          "Акт приемки контейнеров" + stock.Client.Name + DateTime.Now.Year, string.Empty)); ;
            }
            catch { }
            containerPlacementAct.Stock = stock;
            container.EditFlag = true;
            try { container.Owner = connect.FindFirstObject<Client>(mc => mc.Oid == stock.Client.Oid); }
            catch { }
            try { container.Stock = connect.FindFirstObject<Stock>(mc => mc.Oid == stock.Oid); }
            catch { }
            try { container.Terminal = connect.FindFirstObject<Terminal>(mc => mc.Oid == stock.Terminal.Oid); }
            catch { }
            container.Save();
            containerPlacementAct.Container = container;
            containerPlacementAct.Save();
            os.CommitChanges();

            // открываем окно акта 
            DetailView dv = Application.CreateDetailView(os, containerPlacementAct);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;

            //e.ShowViewParameters.CreatedView = dv;
            Application.MainWindow.SetView(dv);
            Window window = Frame as PopupWindow;
            if (window != null)
            {
                window.Close();
            }
        }

        

        /// <summary>
        /// Размещение контейнера в стоке сотрудником терминала
        /// 1. Сначала открываем окно заполнения данных по контейнеру (номер и тип), если такой контейнер есть в системе, то в акт подставлем его.
        /// 2. потом открываем окно акта о приемке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerPlacementPopupAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace(typeof(Stock));//View.ObjectSpace;
            //RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;

            var x_id = ((BaseObject)View.CurrentObject).Oid;

            Stock stock;
            stock = os.FindObject<Stock>(new BinaryOperator("Oid", x_id));
            //Stock stock = (Stock)View.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)stock.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            ContainerPlacementInfo placementInfo = connect.CreateObject<ContainerPlacementInfo>();
            placementInfo.ContainerNumber = "";
            try { placementInfo.Owner = connect.FindFirstObject<Client>(mc => mc.Oid == stock.Client.Oid); }
            catch { }
            try { placementInfo.Stock = connect.FindFirstObject<Stock>(mc => mc.Oid == stock.Oid); }
            catch { }
            try { placementInfo.Terminal = connect.FindFirstObject<Terminal>(mc => mc.Oid == stock.Terminal.Oid); }
            catch { }
            placementInfo.Save();
            UnitOfWork unitOfWorkNew = (UnitOfWork)placementInfo.Session;
            unitOfWorkNew.CommitChanges();
            // открываем окно информации 
            DetailView dv = Application.CreateDetailView(os, placementInfo);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.View = dv;
            //e.ShowViewParameters.CreatedView = dv;
        }

        /// <summary>
        /// При нажатии на кнопку ОК в Popup окне при размещении контейнера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerPlacementPopupAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            // сохраняем данные
            //View.ObjectSpace.CommitChanges();
            DevExpress.XtraEditors.XtraMessageBox.Show("2");
            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;
            
            ContainerPlacementInfo placementInfo;
            // получаем наш попап объект, его сессию и сохраняем его, после этого, получаем его из ObjectSpace
            placementInfo = (ContainerPlacementInfo)e.PopupWindowViewCurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)placementInfo.Session;
            placementInfo.Save();
            unitOfWork.CommitChanges();
            //Connect connect = Connect.FromUnitOfWork(unitOfWork);

            var x_id = ((BaseObject)e.PopupWindowViewCurrentObject).Oid;
            placementInfo = os.FindObject<ContainerPlacementInfo>(new BinaryOperator("Oid", x_id));
            unitOfWork = (UnitOfWork)placementInfo.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            if (placementInfo.ContainerNumber == "")
                throw new Exception("Не указан номер контейнера!");
            if (placementInfo.ContainerType == null)
                throw new Exception("Не указан тип контейнера!");
            // ищем контейнер
            Container container = connect.FindFirstObject<Container>(x => x.ContainerNumber == placementInfo.ContainerNumber);
            if (container != null)
            {
                try {
                    if (container.Owner.Oid != placementInfo.Owner.Oid)
                    { if (container.SpecialNotes != null && container.SpecialNotes != "")
                            container.SpecialNotes += Environment.NewLine;
                      container.SpecialNotes += String.Format("Похоже, что у контейнера сменился владелец: раньше был {0}, стал {1} !", container.Owner.Name, placementInfo.Owner.Name);
                    }
                        //throw new Exception(String.Format("В системе найден контейнер с номером [{0}], но его владелец !"));
                }
                catch { }
                
            }
            else
            {
                container = connect.CreateObject<Container>();
                container.ContainerNumber = placementInfo.ContainerNumber;
                try { container.ContainerType = connect.FindFirstObject<dContainerType>(mc => mc.Oid == placementInfo.ContainerType.Oid); }
                catch { }
            }
            Stock stock = placementInfo.Stock;
            ContainerPlacementAct containerPlacementAct = connect.CreateObject<ContainerPlacementAct>();

            try
            {
                containerPlacementAct.ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(stock.Session.DataLayer,
          "Акт приемки контейнеров" + stock.Client.Name + DateTime.Now.Year, string.Empty)); ;
            }
            catch { }
            containerPlacementAct.Stock = stock;
            container.EditFlag = true;
            try { container.Owner = connect.FindFirstObject<Client>(mc => mc.Oid == stock.Client.Oid); }
            catch { }
            try { container.Stock = connect.FindFirstObject<Stock>(mc => mc.Oid == stock.Oid); }
            catch { }
            try { container.Terminal = connect.FindFirstObject<Terminal>(mc => mc.Oid == stock.Terminal.Oid); }
            catch { }
            container.Save();
            containerPlacementAct.Container = container;
            containerPlacementAct.Save();
            os.CommitChanges();
            //containerPlacementAct.SetTariffs();
            // открываем окно акта 
            DetailView dv = Application.CreateDetailView(os, containerPlacementAct);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            
            //e.ShowViewParameters.CreatedView = dv;
            Application.MainWindow.SetView(dv);
            Window window = Frame as PopupWindow;
            if (window != null)
            {
                window.Close();
            }
            //View.Close();
        }

        public void CreateContainerPlacementAct(Connect connect, Stock stock)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;
            ContainerPlacementAct containerPlacementAct = connect.CreateObject<ContainerPlacementAct>();

            try
            {
                containerPlacementAct.ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(stock.Session.DataLayer,
          "Акт приемки контейнеров" + stock.Client.Name + DateTime.Now.Year, string.Empty)); ;
            }
            catch { }
            containerPlacementAct.Stock = stock;
            Container container = connect.CreateObject<Container>();
            container.EditFlag = true;
            try { container.Owner = connect.FindFirstObject<Client>(mc => mc.Oid == stock.Client.Oid); }
            catch { }
            try { container.Stock = connect.FindFirstObject<Stock>(mc => mc.Oid == stock.Oid); }
            catch { }
            try { container.Terminal = connect.FindFirstObject<Terminal>(mc => mc.Oid == stock.Terminal.Oid); }
            catch { }
            container.Save();
            containerPlacementAct.Container = container;
            containerPlacementAct.Save();
            os.CommitChanges();

            // открываем окно акта 
            //DetailView dv = Application.CreateDetailView(os, containerPlacementAct);//Specify the IsRoot parameter if necessary.
            //dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            //e.ShowViewParameters.CreatedView = dv;
        }
    }
}
