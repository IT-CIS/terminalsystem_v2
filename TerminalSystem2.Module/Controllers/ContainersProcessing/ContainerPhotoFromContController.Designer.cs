﻿namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    partial class ContainerPhotoFromContController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ContainerPhotoFromContAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SaveContPhotoActAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ContainerPhotoFromContAction
            // 
            this.ContainerPhotoFromContAction.Caption = "Добавить фото";
            this.ContainerPhotoFromContAction.Category = "PhotoFromContainer";
            this.ContainerPhotoFromContAction.ConfirmationMessage = null;
            this.ContainerPhotoFromContAction.Id = "ContainerPhotoFromContAction";
            this.ContainerPhotoFromContAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ContainerPhotoFromContAction.TargetObjectType = typeof(TerminalSystem2.Containers.Container);
            this.ContainerPhotoFromContAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerPhotoFromContAction.ToolTip = null;
            this.ContainerPhotoFromContAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerPhotoFromContAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ContainerPhotoFromContAction_Execute);
            // 
            // SaveContPhotoActAction
            // 
            this.SaveContPhotoActAction.Caption = "Сохранить и закрыть";
            this.SaveContPhotoActAction.Category = "SaveContPhotoAct";
            this.SaveContPhotoActAction.ConfirmationMessage = null;
            this.SaveContPhotoActAction.Id = "SaveContPhotoActAction";
            this.SaveContPhotoActAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.SaveContPhotoActAction.TargetObjectType = typeof(TerminalSystem2.Containers.ContainerPhotoAct);
            this.SaveContPhotoActAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.SaveContPhotoActAction.ToolTip = null;
            this.SaveContPhotoActAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.SaveContPhotoActAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SaveContPhotoActAction_Execute);
            // 
            // ContainerPhotoFromContController
            // 
            this.Actions.Add(this.ContainerPhotoFromContAction);
            this.Actions.Add(this.SaveContPhotoActAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ContainerPhotoFromContAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SaveContPhotoActAction;
    }
}
