﻿namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    partial class ContainerExtraditionViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ContainerExtraditionFinishAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ContainerExtraditionAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ExtraditionFromContainerAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ContainerExtraditionFinishAction
            // 
            this.ContainerExtraditionFinishAction.Caption = "Контейнер выдан";
            this.ContainerExtraditionFinishAction.ConfirmationMessage = null;
            this.ContainerExtraditionFinishAction.Id = "ContainerExtraditionFinishAction";
            this.ContainerExtraditionFinishAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ContainerExtraditionFinishAction.TargetObjectType = typeof(TerminalSystem2.Containers.ContainerExtraditionAct);
            this.ContainerExtraditionFinishAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerExtraditionFinishAction.ToolTip = null;
            this.ContainerExtraditionFinishAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerExtraditionFinishAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ContainerExtraditionFinishAction_Execute);
            // 
            // ContainerExtraditionAction
            // 
            this.ContainerExtraditionAction.Caption = "Выдача";
            this.ContainerExtraditionAction.Category = "ContainerExtradition";
            this.ContainerExtraditionAction.ConfirmationMessage = null;
            this.ContainerExtraditionAction.Id = "ContainerExtraditionAction";
            this.ContainerExtraditionAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ContainerExtraditionAction.TargetObjectType = typeof(TerminalSystem2.DocFlow.RequestContainersExtradition);
            this.ContainerExtraditionAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ContainerExtraditionAction.ToolTip = null;
            this.ContainerExtraditionAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ContainerExtraditionAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ContainerExtraditionAction_Execute);
            // 
            // ExtraditionFromContainerAction
            // 
            this.ExtraditionFromContainerAction.Caption = "Выдать контейнер";
            this.ExtraditionFromContainerAction.ConfirmationMessage = null;
            this.ExtraditionFromContainerAction.Id = "ExtraditionFromContainerAction";
            this.ExtraditionFromContainerAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ExtraditionFromContainerAction.TargetObjectType = typeof(TerminalSystem2.Containers.Container);
            this.ExtraditionFromContainerAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ExtraditionFromContainerAction.ToolTip = null;
            this.ExtraditionFromContainerAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ExtraditionFromContainerAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ExtraditionFromContainerAction_Execute);
            // 
            // ContainerExtraditionViewController
            // 
            this.Actions.Add(this.ContainerExtraditionFinishAction);
            this.Actions.Add(this.ContainerExtraditionAction);
            this.Actions.Add(this.ExtraditionFromContainerAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ContainerExtraditionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ContainerExtraditionFinishAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ExtraditionFromContainerAction;
    }
}
