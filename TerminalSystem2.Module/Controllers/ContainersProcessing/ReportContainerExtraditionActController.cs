﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Layout;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Web;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReportContainerExtraditionActController : ViewController
    {
        public ReportContainerExtraditionActController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        DetailView currentView;
        ASPxPageControl pageControl = null;

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            currentView = (DetailView)View;
            ((WebLayoutManager)currentView.LayoutManager).PageControlCreated += OnPageControlCreated;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            currentView = (DetailView)View;
            ((WebLayoutManager)currentView.LayoutManager).PageControlCreated -= OnPageControlCreated;
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void OnPageControlCreated(object sender, PageControlCreatedEventArgs e)
        {
            if (e.Model.Id == "Item1")
            {
                pageControl = e.PageControl;
                pageControl.Disposed += (object sender2, EventArgs e2) =>
                {
                    pageControl = null;
                };
            }
        }

        private void ContainerInspectionActExtraditionAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();

            ContainerExtraditionAct containerExtraditionAct;
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            containerExtraditionAct = os.FindObject<ContainerExtraditionAct>(new BinaryOperator("Oid", x_id));
            Connect connect = Connect.FromObjectSpace(os);
            containerExtraditionAct.RtfText = String.Empty;
            os.CommitChanges();
            //CreateContainerInspectionPlacementActReport(containerPlacementAct);
            OnDeactivated();
            CreateContainerInspectionExtraditionActRich(os, connect, containerExtraditionAct);
        }

        /// <summary>
        /// Сформировать акт осмотра контейнера при размещении на терминале с помощью RichEditDocumentServer
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        /// <param name="containerPlacementAct"></param>
        private void CreateContainerInspectionExtraditionActRich(IObjectSpace os, Connect connect, ContainerExtraditionAct containerExtraditionAct)
        {
            OnActivated();
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            //richServer.Dispose();

            //richServer.Options.DocumentSaveOptions.

            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                LocalPath.Replace("TerminalSystem2.Module.DLL", @"\DocTemplates\ActOfTechStatusContainer.rtf");

            richServer.LoadDocumentTemplate(templatePath);

            
            // ищем терминальную организацию и вставляем, если есть - логотип
            TerminalOrg terminalOrg = null;
            try
            {
                byte[] Logo;
                 terminalOrg = connect.FindFirstObject<TerminalOrg>(x => true);
                if (terminalOrg.Logo != null)
                {
                    Logo = terminalOrg.Logo;
                    MemoryStream ms = new MemoryStream(Logo);
                    Bookmark LogoBook = richServer.Document.Bookmarks["LogoBook"];
                    var v_Image = richServer.Document.Images.Insert(LogoBook.Range.Start, DocumentImageSource.FromStream(ms));
                    v_Image.Size = new System.Drawing.Size(300, 150);

                    Bookmark LogoBook1 = richServer.Document.Bookmarks["LogoBook1"];
                    var v_Image1 = richServer.Document.Images.Insert(LogoBook1.Range.Start, DocumentImageSource.FromStream(ms));
                    v_Image1.Size = new System.Drawing.Size(300, 150);
                }
                else
                {
                    Bookmark LogoBook = richServer.Document.Bookmarks["LogoBook"];
                    richServer.Document.Replace(LogoBook.Range, "");
                    Bookmark LogoBook1 = richServer.Document.Bookmarks["LogoBook1"];
                    richServer.Document.Replace(LogoBook1.Range, "");
                }
            }
            catch { }

            // терминал
            try
            {
                string Terminal = "";
                Terminal = containerExtraditionAct.Request?.Terminal?.Name ?? "";
                Bookmark TerminalBook = richServer.Document.Bookmarks["TerminalBook"];
                richServer.Document.Replace(TerminalBook.Range, Terminal);

                Bookmark TerminalBook1 = richServer.Document.Bookmarks["TerminalBook1"];
                richServer.Document.Replace(TerminalBook1.Range, Terminal);
            }
            catch { }

            // дата акта
            try
            {
                string ActDate = "";
                ActDate = containerExtraditionAct.ActDate.ToShortDateString() ?? "";
                Bookmark ActDateBook = richServer.Document.Bookmarks["ActDateBook"];
                richServer.Document.Replace(ActDateBook.Range, ActDate);

                Bookmark ActDateBook1 = richServer.Document.Bookmarks["ActDateBook1"];
                richServer.Document.Replace(ActDateBook1.Range, ActDate);
            }
            catch { }

            // номер акта
            try
            {
                string ActNo = "";
                ActNo = containerExtraditionAct.ActNumber ?? "";
                Bookmark ActNoBook = richServer.Document.Bookmarks["ActNoBook"];
                richServer.Document.Replace(ActNoBook.Range, ActNo);

                Bookmark ActNoBook1 = richServer.Document.Bookmarks["ActNoBook1"];
                richServer.Document.Replace(ActNoBook1.Range, ActNo);
            }
            catch { }

            // примечание
            try
            {
                string SpecialNotes = "";
                SpecialNotes = containerExtraditionAct.Container?.SpecialNotes ?? "";
                Bookmark SpecialNotesBook = richServer.Document.Bookmarks["SpecialNotesBook"];
                richServer.Document.Replace(SpecialNotesBook.Range, SpecialNotes);

                Bookmark SpecialNotesBook1 = richServer.Document.Bookmarks["SpecialNotesBook1"];
                richServer.Document.Replace(SpecialNotesBook1.Range, SpecialNotes);
            }
            catch { }

            // номер контейнера
            try
            {
                string ContainerNumber = "";
                ContainerNumber = containerExtraditionAct.Container?.ContainerNumber ?? "";
                Bookmark ContainerNumberBook = richServer.Document.Bookmarks["ContainerNumberBook"];
                richServer.Document.Replace(ContainerNumberBook.Range, ContainerNumber);

                Bookmark ContainerNumberBook1 = richServer.Document.Bookmarks["ContainerNumberBook1"];
                richServer.Document.Replace(ContainerNumberBook1.Range, ContainerNumber);
            }
            catch { }

            // размер
            try
            {
                string ContainerSize = "";
                ContainerSize = containerExtraditionAct.Container?.Size?.Name ?? "";
                Bookmark ContainerSizeBook = richServer.Document.Bookmarks["ContainerSizeBook"];
                richServer.Document.Replace(ContainerSizeBook.Range, ContainerSize);

                Bookmark ContainerSizeBook1 = richServer.Document.Bookmarks["ContainerSizeBook1"];
                richServer.Document.Replace(ContainerSizeBook1.Range, ContainerSize);
            }
            catch { }

            // тип контейнера
            try
            {
                string ContainerType = "";
                ContainerType = containerExtraditionAct.Container?.ContainerType?.Name ?? "";
                Bookmark ContainerTypeBook = richServer.Document.Bookmarks["ContainerTypeBook"];
                richServer.Document.Replace(ContainerTypeBook.Range, ContainerType);

                Bookmark ContainerTypeBook1 = richServer.Document.Bookmarks["ContainerTypeBook1"];
                richServer.Document.Replace(ContainerTypeBook1.Range, ContainerType);
            }
            catch { }


            // порожний/груженый
            try
            {
                string IsLaden = "Порожний";
                if (containerExtraditionAct.Container.isLaden)
                    IsLaden = "Груженый";
                Bookmark IsLadenBook = richServer.Document.Bookmarks["IsLadenBook"];
                richServer.Document.Replace(IsLadenBook.Range, IsLaden);

                Bookmark IsLadenBook1 = richServer.Document.Bookmarks["IsLadenBook1"];
                richServer.Document.Replace(IsLadenBook1.Range, IsLaden);
            }
            catch { }

            // Номер пломбы
            try
            {
                string SealNumber = "";
                SealNumber = containerExtraditionAct.Container?.SealNumber ?? "";
                Bookmark SealNumberBook = richServer.Document.Bookmarks["SealNumberBook"];
                richServer.Document.Replace(SealNumberBook.Range, SealNumber);

                Bookmark SealNumberBook1 = richServer.Document.Bookmarks["SealNumberBook1"];
                richServer.Document.Replace(SealNumberBook1.Range, SealNumber);
            }
            catch { }

            // Отправитель/владелец
            try
            {
                string sender = "ООО Футовик";
                if (terminalOrg != null)
                    sender = terminalOrg.Name;
                Bookmark OwnerBook = richServer.Document.Bookmarks["OwnerBook"];
                richServer.Document.Replace(OwnerBook.Range, sender);

                Bookmark OwnerBook1 = richServer.Document.Bookmarks["OwnerBook1"];
                richServer.Document.Replace(OwnerBook1.Range, sender);
            }
            catch { }

            // Перевозчик
            try
            {
                string TransportCompany = "";
                TransportCompany = containerExtraditionAct.TransportCompany ?? "";
                Bookmark TransportCompanyBook = richServer.Document.Bookmarks["TransportCompanyBook"];
                richServer.Document.Replace(TransportCompanyBook.Range, TransportCompany);

                Bookmark TransportCompanyBook1 = richServer.Document.Bookmarks["TransportCompanyBook1"];
                richServer.Document.Replace(TransportCompanyBook1.Range, TransportCompany);
            }
            catch { }

            // Тип транспорта
            try
            {
                string TransportType = "";
                TransportType = containerExtraditionAct.TransportType?.Name ?? "";
                Bookmark TransportTypeBook = richServer.Document.Bookmarks["TransportTypeBook"];
                richServer.Document.Replace(TransportTypeBook.Range, TransportType);

                Bookmark TransportTypeBook1 = richServer.Document.Bookmarks["TransportTypeBook1"];
                richServer.Document.Replace(TransportTypeBook1.Range, TransportType);
            }
            catch { }

            // Марка транспорта
            try
            {
                string TransportMark = "";
                TransportMark = containerExtraditionAct.TransportMark?.Name ?? "";
                Bookmark TransportMarkBook = richServer.Document.Bookmarks["TransportMarkBook"];
                richServer.Document.Replace(TransportMarkBook.Range, TransportMark);

                Bookmark TransportMarkBook1 = richServer.Document.Bookmarks["TransportMarkBook1"];
                richServer.Document.Replace(TransportMarkBook1.Range, TransportMark);
            }
            catch { }

            // Номер транспорта
            try
            {
                string TransportNumber = "";
                TransportNumber = containerExtraditionAct.TransportNumber ?? "";
                Bookmark TransportNumberBook = richServer.Document.Bookmarks["TransportNumberBook"];
                richServer.Document.Replace(TransportNumberBook.Range, TransportNumber);

                Bookmark TransportNumberBook1 = richServer.Document.Bookmarks["TransportNumberBook1"];
                richServer.Document.Replace(TransportNumberBook1.Range, TransportNumber);
            }
            catch { }

            // Водитель
            try
            {
                string DriverName = "";
                DriverName = containerExtraditionAct.DriverName ?? "";
                Bookmark DriverNameBook = richServer.Document.Bookmarks["DriverNameBook"];
                richServer.Document.Replace(DriverNameBook.Range, DriverName);

                Bookmark DriverNameBook1 = richServer.Document.Bookmarks["DriverNameBook1"];
                richServer.Document.Replace(DriverNameBook1.Range, DriverName);
            }
            catch { }

            string contNum = "";
            try
            {
                contNum = containerExtraditionAct.Container.ContainerNumber;
            }
            catch { }
            string path = Path.GetTempPath() + @"\Акт выдачи контейнера № " + contNum + "_" +
            DateTime.Now.ToLocalTime().ToString().Replace("/", "_").Replace("\\", "_").Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.Rtf);

            containerExtraditionAct.RtfText = "";
            //View.RefreshDataSource();
            //View.Refresh();
            //os.Refresh();

            StreamReader rtfStreamReader = new StreamReader(path);
            containerExtraditionAct.RtfText = rtfStreamReader.ReadToEnd();
            


            MemoryStream pdfStream = new MemoryStream();
            richServer.ExportToPdf(pdfStream);

            byte[] docPDF = pdfStream.ToArray();

            string pathPdf = @"Акт выдачи контейнера № " + contNum + "_" +
           DateTime.Now.ToLocalTime().ToString().Replace("/", "_").Replace("\\", "_").Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".pdf";
            var fileData = connect.CreateObject<FileData>();
            pdfStream.Seek(0, System.IO.SeekOrigin.Begin);
            fileData.LoadFromStream(pathPdf, pdfStream);
            containerExtraditionAct.File = fileData;


            containerExtraditionAct.Save();
            os.CommitChanges();

            string content = System.Convert.ToBase64String(docPDF);

            string script = @"var newWindow = window.open();
newWindow.document.write('<iframe src=""data:application/pdf;base64," + content + @""" frameborder=""0"" allowfullscreen style=""width: 100%;height: 100%""></iframe>');";
            WebWindow.CurrentRequestWindow.RegisterClientScript("showPDF", script, true);

            
            os.Refresh();
            //View.RefreshDataSource();
            View.Refresh();
            RefreshFrame();

            //if (pageControl != null && pageControl.TabPages.Count > 0)
            //{
            //    //pageControl.ClientSideEvents.Init = "function(s,e){e.reloadContentOnCallback;}";
            //    pageControl.ClientSideEvents.Init = "function(s,e){s.SetActiveTabIndex(1);}";
            //}

            // открываем новое view с WebRichEditUserControl.ascx
            //ViewItem vItem = ((DashboardView)View).FindItem("RichTextDashBoardView") as ViewItem; 
        }

        protected void RefreshFrame()
        {
            RefreshController controller = Frame.GetController<RefreshController>();
            if (controller != null)
                controller.RefreshAction.DoExecute();
        }
    }
}
