﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;
using TerminalSystem2.OrgStructure;

namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreateEmailNotificationsController : ViewController
    {
        public CreateEmailNotificationsController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Отправить сообщение клиенту и на терминал о приемке и выдаче контейнера
        /// </summary>
        /// <param name="request"></param>
        /// <param name="requestStatus"></param>
        public void CreateNotifacations(ContainerHistory ContainerHistory, Connect connect)
        {
            // Отправляем письмо клиенту и на терминал
            string messbody = "";
            string titleClient = "";
            string path = "";
            string mails = "";
            string t = "";
            string s = "";
            string te = "";
            string se= "";

            string conNum = "";
            string conType = "";
            string ter = "";
            string stock = "";
            // Здравствуйте!
            // Контейнер № .... тип .... [размещен на терминале ... в сток ... / выдан с терминала ... из стока ....]. Водитель ....  

            // Дублируем на английском
            // Hello!
            // The container is placed on the terminal in stock
            
            if (ContainerHistory != null)
            {
                if (ContainerHistory.Container != null)
                {
                    try
                    {
                        conNum = ContainerHistory.Container.ContainerNumber;
                    }
                    catch { }
                    try
                    {
                        conType = ContainerHistory.Container.ContainerType.Name;
                    }
                    catch { }
                    try
                    {
                        ter = ContainerHistory.Stock.Terminal.Name;
                    }
                    catch { }
                    try
                    {
                        stock = ContainerHistory.Stock.Name;
                    }
                    catch { }
                    switch (ContainerHistory.ContainerHistoryKind)
                    {
                        case Enums.EContainerHistoryKind.Размещение:
                            t = "размещен на терминале";
                            te = "is placed on the Terminal";
                            s = "в стоке";
                            se = "in the Stock";
                            break;
                        case Enums.EContainerHistoryKind.Выдача:
                            t = "выдан с терминала";
                            te = "issued from the Terminal";
                            s = "из стока";
                            se = "from Stock";
                            break;
                    }
                    titleClient = $"Контейнер № {conNum} [{conType}] {t} {ter} / The Container #{conNum} [{conType}] {te} {ter}";
                    messbody = "Здравствуйте!" + Environment.NewLine;
                    messbody += String.Format($"Контейнер № { conNum} [{conType}] {t} {ter} {s} {stock}.") + Environment.NewLine;
                    try
                    {
                        messbody += String.Format($"Водитель: {ContainerHistory.DriverName ?? ""}");
                    }
                    catch { }
                    try
                    {
                        messbody += String.Format($", т. {ContainerHistory.DriverPhoneNumber ?? ""}");
                    }
                    catch { }
                    try
                    {
                        messbody += String.Format($", а/м {ContainerHistory.TransportType?.Name ?? ""} {ContainerHistory.TransportNumber ?? ""}.");
                    }
                    catch { }
                    messbody += Environment.NewLine + "Всего Вам доброго и хорошего настроения!";

                    // создаем английский текст
                    try
                    {
                        messbody += Environment.NewLine + Environment.NewLine + Environment.NewLine;
                        messbody += "Hello!" + Environment.NewLine;
                        messbody += $"The Container #{conNum} [{conType}] {te} {ter} {se} {stock}." + Environment.NewLine;
                        try
                        {
                            messbody += $"The Driver: {ContainerHistory.DriverName ?? ""}";
                        }
                        catch { }
                        try
                        {
                            messbody += $", phone: {ContainerHistory.DriverPhoneNumber ?? ""}";
                        }
                        catch { }
                        try
                        {
                            messbody += $", car {ContainerHistory.TransportType?.Name ?? ""} {ContainerHistory.TransportNumber ?? ""}.";
                        }
                        catch { }
                    }
                    catch { }
                    messbody += Environment.NewLine + "All the best and good mood!";
                    try
                    {
                        if (!String.IsNullOrEmpty(ContainerHistory.Container.Owner.Email))
                        {
                            
                            mails = ContainerHistory.Container.Owner.Email;
                            


                            // отсылаем на терминал
                            string bcc = "";
                            try
                            {
                                bcc += connect.FindFirstObject<TerminalOrg>(x => true).Email;
                            }
                            catch { }
                            try
                            {
                                foreach (Employee emp in ContainerHistory.Stock.Terminal.Employees)
                                {
                                    try
                                    {
                                        bcc += "," + emp.Email;
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                            // отсылаем клиенту и на терминал (скрытой копией)
                            SystemDir.MailMessageLogic.SendMailMessage(connect, titleClient, messbody, path, mails, bcc);

                            //SystemDir.MailMessageLogic.SendMailMessage(titleClient, messbody, path, mails, "donotreply@it-cis.ru");
                        }
                        else
                        {
                            // отсылаем на терминал
                            string bcc = "";
                            try
                            {
                                mails = connect.FindFirstObject<TerminalOrg>(x => true).Email;
                            }
                            catch { }
                            try
                            {
                                foreach (Employee emp in ContainerHistory.Stock.Terminal.Employees)
                                {
                                    try
                                    {
                                        bcc += "," + emp.Email;
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                            // отсылаем на терминал
                            SystemDir.MailMessageLogic.SendMailMessage(connect, titleClient, messbody, path, mails, bcc);
                        }
                    }
                    catch { }
                }
            }
        }
    }
}