﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ReportsV2;
using TerminalSystem2.Containers;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.SystemDir;
using DevExpress.XtraRichEdit;
using System.Reflection;
using TerminalSystem2.OrgStructure;
using System.IO;
using DevExpress.XtraRichEdit.API.Native;
using DevExpress.ExpressApp.Web.Layout;
using DevExpress.Web;
using DevExpress.ExpressApp.Web;

namespace TerminalSystem2.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReportContainerPlacementActController : ViewController
    {
        public ReportContainerPlacementActController()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        DetailView currentView;
        WebLayoutManager layoutManager;
        ASPxPageControl pageControl = null;

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            //((WebLayoutManager)View.LayoutManager).PageControlCreated += OnPageControlCreated;

            currentView = (DetailView)View;
            //((WebLayoutManager)currentView.LayoutManager).ItemCreated += LayoutElementViewController_ItemCreated;

            ((WebLayoutManager)currentView.LayoutManager).PageControlCreated += OnPageControlCreated;
            //layoutManager = (WebLayoutManager)currentView.LayoutManager;
            //layoutManager.ItemCreated += OnItemCreated;
        }

        //TabbedControlGroup tabbedControlGroup;
        //void OnItemCreated(object sender, ItemCreatedEventArgs e)
        //{
        //    // Check this Id in the E372.Module/Model.DesignedDiffs.xafml file
        //    try
        //    {
        //        if (e.ModelLayoutElement.Id == "Item1")
        //        {
        //            try { tabbedControlGroup = (TabbedControlGroup)e.Item; }
        //            catch { }
        //        }
        //    }
        //    catch { }

        //}
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            currentView = (DetailView)View;
            ((WebLayoutManager)currentView.LayoutManager).PageControlCreated -= OnPageControlCreated;
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        private void OnPageControlCreated(object sender, PageControlCreatedEventArgs e)
        {
            // Check this Id in the E372.Module/Model.DesignedDiffs.xafml file
            if (e.Model.Id == "Item4")
            {
                pageControl = e.PageControl;
                pageControl.Disposed += (object sender2, EventArgs e2) =>
                {
                    pageControl = null;
                };
            }
            //if (e.Model.Id == "Item4")
            //{
            //    e.PageControl.ClientSideEvents.Init = "function(s,e){s.SetActiveTabIndex(1);}";
            //    ((WebLayoutManager)currentView.LayoutManager).PageControlCreated -= OnPageControlCreated;
            //}
        }

        private void ContainerInspectionActLandScapeAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            
            IObjectSpace os = Application.CreateObjectSpace();

            ContainerPlacementAct containerPlacementAct;
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            containerPlacementAct = os.FindObject<ContainerPlacementAct>(new BinaryOperator("Oid", x_id));
            Connect connect = Connect.FromObjectSpace(os);
            containerPlacementAct.RtfText = String.Empty;
            os.CommitChanges();
            //CreateContainerInspectionPlacementActReport(containerPlacementAct);
            OnDeactivated();
            CreateContainerInspectionPlacementActRich(os, connect, containerPlacementAct);
        }

        /// <summary>
        /// Сформировать акт осмотра контейнера при размещении на терминале с помощью встроенного в xaf построителя отчетов (Report2)
        /// </summary>
        /// <param name="containerPlacementAct"></param>
        //private void CreateContainerInspectionPlacementActReport(ContainerPlacementAct containerPlacementAct)
        //{
        //    // запускаем формирование самого шаблона
        //    // находим наш шаблон справки
        //    IReportDataV2 reportData = (IReportDataV2)ObjectSpace.FindObject(Application.Modules.FindModule<ReportsModuleV2>().ReportDataType, new BinaryOperator("DisplayName", "Акт осмотра контейнера"));
        //    // открываем окно с превью документа для нашего объекта (в критерии передаем его ИД)
        //    string reportContainerHandle = ReportDataProvider.ReportsStorage.GetReportContainerHandle(reportData);
        //    Frame.GetController<ReportServiceController>().ShowPreview(reportContainerHandle, new BinaryOperator("Oid", containerPlacementAct.Oid.ToString()));
        //}

        /// <summary>
        /// Сформировать акт осмотра контейнера при размещении на терминале с помощью RichEditDocumentServer
        /// </summary>
        /// <param name="os"></param>
        /// <param name="connect"></param>
        /// <param name="containerPlacementAct"></param>
        private void CreateContainerInspectionPlacementActRich(IObjectSpace os, Connect connect, ContainerPlacementAct containerPlacementAct)
        {
            OnActivated();
            RichEditDocumentServer richServer = new RichEditDocumentServer();
            string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                LocalPath.Replace("TerminalSystem2.Module.DLL", @"\DocTemplates\ActOfTechStatusContainer.rtf");

            richServer.LoadDocumentTemplate(templatePath);

            // ищем терминальную организацию и вставляем, если есть - логотип
            try
            {
                byte[] Logo;
                TerminalOrg terminalOrg = connect.FindFirstObject<TerminalOrg>(x => true);
                if (terminalOrg.Logo != null)
                {
                    Logo = terminalOrg.Logo;
                    MemoryStream ms = new MemoryStream(Logo);
                    Bookmark LogoBook = richServer.Document.Bookmarks["LogoBook"];
                    var v_Image = richServer.Document.Images.Insert(LogoBook.Range.Start, DocumentImageSource.FromStream(ms));
                    v_Image.Size = new System.Drawing.Size(300, 150);

                    Bookmark LogoBook1 = richServer.Document.Bookmarks["LogoBook1"];
                    var v_Image1 = richServer.Document.Images.Insert(LogoBook1.Range.Start, DocumentImageSource.FromStream(ms));
                    v_Image1.Size = new System.Drawing.Size(300, 150);
                }
                else
                {
                    Bookmark LogoBook = richServer.Document.Bookmarks["LogoBook"];
                    richServer.Document.Replace(LogoBook.Range, "");
                    Bookmark LogoBook1 = richServer.Document.Bookmarks["LogoBook1"];
                    richServer.Document.Replace(LogoBook1.Range, "");
                }
            }
            catch { }

            // терминал
            try
            {
                string Terminal = "";
                Terminal = containerPlacementAct.Stock?.Terminal?.Name?? "";
                Bookmark TerminalBook = richServer.Document.Bookmarks["TerminalBook"];
                richServer.Document.Replace(TerminalBook.Range, Terminal);

                Bookmark TerminalBook1 = richServer.Document.Bookmarks["TerminalBook1"];
                richServer.Document.Replace(TerminalBook1.Range, Terminal);
            }
            catch { }

            // дата акта
            try
            {
                string ActDate = "";
                ActDate = containerPlacementAct.ActDate.ToShortDateString()?? "";
                Bookmark ActDateBook = richServer.Document.Bookmarks["ActDateBook"];
                richServer.Document.Replace(ActDateBook.Range, ActDate);

                Bookmark ActDateBook1 = richServer.Document.Bookmarks["ActDateBook1"];
                richServer.Document.Replace(ActDateBook1.Range, ActDate);
            }
            catch { }

            // номер акта
            try
            {
                string ActNo = "";
                ActNo = containerPlacementAct.ActNumber?? "";
                Bookmark ActNoBook = richServer.Document.Bookmarks["ActNoBook"];
                richServer.Document.Replace(ActNoBook.Range, ActNo);

                Bookmark ActNoBook1 = richServer.Document.Bookmarks["ActNoBook1"];
                richServer.Document.Replace(ActNoBook1.Range, ActNo);
            }
            catch { }

            // примечание
            try
            {
                string SpecialNotes = "";
                SpecialNotes = containerPlacementAct.Container?.SpecialNotes?? "";
                Bookmark SpecialNotesBook = richServer.Document.Bookmarks["SpecialNotesBook"];
                richServer.Document.Replace(SpecialNotesBook.Range, SpecialNotes);

                Bookmark SpecialNotesBook1 = richServer.Document.Bookmarks["SpecialNotesBook1"];
                richServer.Document.Replace(SpecialNotesBook1.Range, SpecialNotes);
            }
            catch { }

            // номер контейнера
            try
            {
                string ContainerNumber = "";
                ContainerNumber = containerPlacementAct.Container?.ContainerNumber ?? "";
                Bookmark ContainerNumberBook = richServer.Document.Bookmarks["ContainerNumberBook"];
                richServer.Document.Replace(ContainerNumberBook.Range, ContainerNumber);

                Bookmark ContainerNumberBook1 = richServer.Document.Bookmarks["ContainerNumberBook1"];
                richServer.Document.Replace(ContainerNumberBook1.Range, ContainerNumber);
            }
            catch { }

            // размер
            try
            {
                string ContainerSize = "";
                ContainerSize = containerPlacementAct.Container?.Size?.Name ?? "";
                Bookmark ContainerSizeBook = richServer.Document.Bookmarks["ContainerSizeBook"];
                richServer.Document.Replace(ContainerSizeBook.Range, ContainerSize);

                Bookmark ContainerSizeBook1 = richServer.Document.Bookmarks["ContainerSizeBook1"];
                richServer.Document.Replace(ContainerSizeBook1.Range, ContainerSize);
            }
            catch { }

            // тип контейнера
            try
            {
                string ContainerType = "";
                ContainerType = containerPlacementAct.Container?.ContainerType?.Name ?? "";
                Bookmark ContainerTypeBook = richServer.Document.Bookmarks["ContainerTypeBook"];
                richServer.Document.Replace(ContainerTypeBook.Range, ContainerType);

                Bookmark ContainerTypeBook1 = richServer.Document.Bookmarks["ContainerTypeBook1"];
                richServer.Document.Replace(ContainerTypeBook1.Range, ContainerType);
            }
            catch { }


            // порожний/груженый
            try
            {
                string IsLaden = "Порожний";
                if(containerPlacementAct.Container.isLaden)
                    IsLaden = "Груженый";
                Bookmark IsLadenBook = richServer.Document.Bookmarks["IsLadenBook"];
                richServer.Document.Replace(IsLadenBook.Range, IsLaden);

                Bookmark IsLadenBook1 = richServer.Document.Bookmarks["IsLadenBook1"];
                richServer.Document.Replace(IsLadenBook1.Range, IsLaden);
            }
            catch { }

            // Номер пломбы
            try
            {
                string SealNumber = "";
                SealNumber = containerPlacementAct.Container?.SealNumber ?? "";
                Bookmark SealNumberBook = richServer.Document.Bookmarks["SealNumberBook"];
                richServer.Document.Replace(SealNumberBook.Range, SealNumber);

                Bookmark SealNumberBook1 = richServer.Document.Bookmarks["SealNumberBook1"];
                richServer.Document.Replace(SealNumberBook1.Range, SealNumber);
            }
            catch { }

            // Отправитель/владелец
            try
            {
                string Owner = "";
                Owner = containerPlacementAct.Container?.Owner?.Name ?? "";
                Bookmark OwnerBook = richServer.Document.Bookmarks["OwnerBook"];
                richServer.Document.Replace(OwnerBook.Range, Owner);

                Bookmark OwnerBook1 = richServer.Document.Bookmarks["OwnerBook1"];
                richServer.Document.Replace(OwnerBook1.Range, Owner);
            }
            catch { }

            // Перевозчик
            try
            {
                string TransportCompany = "";
                TransportCompany = containerPlacementAct.TransportCompany ?? "";
                Bookmark TransportCompanyBook = richServer.Document.Bookmarks["TransportCompanyBook"];
                richServer.Document.Replace(TransportCompanyBook.Range, TransportCompany);

                Bookmark TransportCompanyBook1 = richServer.Document.Bookmarks["TransportCompanyBook1"];
                richServer.Document.Replace(TransportCompanyBook1.Range, TransportCompany);
            }
            catch { }

            // Тип транспорта
            try
            {
                string TransportType = "";
                TransportType = containerPlacementAct.TransportType?.Name ?? "";
                Bookmark TransportTypeBook = richServer.Document.Bookmarks["TransportTypeBook"];
                richServer.Document.Replace(TransportTypeBook.Range, TransportType);

                Bookmark TransportTypeBook1 = richServer.Document.Bookmarks["TransportTypeBook1"];
                richServer.Document.Replace(TransportTypeBook1.Range, TransportType);
            }
            catch { }

            // Марка транспорта
            try
            {
                string TransportMark = "";
                TransportMark = containerPlacementAct.TransportMark?.Name ?? "";
                Bookmark TransportMarkBook = richServer.Document.Bookmarks["TransportMarkBook"];
                richServer.Document.Replace(TransportMarkBook.Range, TransportMark);

                Bookmark TransportMarkBook1 = richServer.Document.Bookmarks["TransportMarkBook1"];
                richServer.Document.Replace(TransportMarkBook1.Range, TransportMark);
            }
            catch { }

            // Номер транспорта
            try
            {
                string TransportNumber = "";
                TransportNumber = containerPlacementAct.TransportNumber ?? "";
                Bookmark TransportNumberBook = richServer.Document.Bookmarks["TransportNumberBook"];
                richServer.Document.Replace(TransportNumberBook.Range, TransportNumber);

                Bookmark TransportNumberBook1 = richServer.Document.Bookmarks["TransportNumberBook1"];
                richServer.Document.Replace(TransportNumberBook1.Range, TransportNumber);
            }
            catch { }

            // Водитель
            try
            {
                string DriverName = "";
                DriverName = containerPlacementAct.DriverName ?? "";
                Bookmark DriverNameBook = richServer.Document.Bookmarks["DriverNameBook"];
                richServer.Document.Replace(DriverNameBook.Range, DriverName);

                Bookmark DriverNameBook1 = richServer.Document.Bookmarks["DriverNameBook1"];
                richServer.Document.Replace(DriverNameBook1.Range, DriverName);
            }
            catch { }

            string contNum = "";
            try {
                contNum = containerPlacementAct.Container.ContainerNumber;
            }
            catch { }
            string path = Path.GetTempPath() + @"\Акт приемки контейнера № " + contNum + "_" +
            DateTime.Now.ToLocalTime().ToString().Replace("/", "_").Replace("\\", "_").Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".rtf";

            richServer.SaveDocument(path, DocumentFormat.Rtf);

            containerPlacementAct.RtfText = "";
            //View.RefreshDataSource();
            //View.Refresh();
            //os.Refresh();
           

            StreamReader rtfStreamReader = new StreamReader(path);
            containerPlacementAct.RtfText = rtfStreamReader.ReadToEnd();

            MemoryStream pdfStream = new MemoryStream();
            richServer.ExportToPdf(pdfStream);

            byte[] docPDF = pdfStream.ToArray();

            string pathPdf = @"Акт приемки контейнера № " + contNum + "_" +
           DateTime.Now.ToLocalTime().ToString().Replace("/", "_").Replace("\\", "_").Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".pdf";
            var fileData = connect.CreateObject<FileData>();
            pdfStream.Seek(0, System.IO.SeekOrigin.Begin);
            fileData.LoadFromStream(pathPdf, pdfStream);
            containerPlacementAct.File = fileData;
            

            containerPlacementAct.Save();
            os.CommitChanges();

            string content = System.Convert.ToBase64String(docPDF);

            string script = @"var newWindow = window.open();
newWindow.document.write('<iframe src=""data:application/pdf;base64," + content + @""" frameborder=""0"" allowfullscreen style=""width: 100%;height: 100%""></iframe>');";
            WebWindow.CurrentRequestWindow.RegisterClientScript("showPDF", script, true);

            os.Refresh();
            View.RefreshDataSource();
            View.Refresh();
            RefreshFrame();

            //if (pageControl != null && pageControl.TabPages.Count > 0)
            //{
            //    //pageControl.ClientSideEvents.Init = "function(s,e){e.reloadContentOnCallback;}";
            //    pageControl.ClientSideEvents.Init = "function(s,e){s.SetActiveTabIndex(1);}";
            //}

            // открываем новое view с WebRichEditUserControl.ascx
            //ViewItem vItem = ((DashboardView)View).FindItem("RichTextDashBoardView") as ViewItem; 
        }

        protected void RefreshFrame()
        {
            RefreshController controller = Frame.GetController<RefreshController>();
            if (controller != null)
                controller.RefreshAction.DoExecute();
        }
    }
}
