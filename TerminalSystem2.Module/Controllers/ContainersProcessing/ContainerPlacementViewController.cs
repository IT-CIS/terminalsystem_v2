﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Clients;
using TerminalSystem2.Terminals;
using TerminalSystem2.Tarification;
using TerminalSystem2.OrgStructure;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.ExpressApp.Web;
using TerminalSystem2.Module.Controllers.ContainersProcessing;

namespace TerminalSystem2.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ContainerPlacementViewController : ViewController
    {
        public ContainerPlacementViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            Employee empl = null;
            ClientEmployee clientEmpl = null;
            if (SecuritySystem.CurrentUser != null)
            {
                PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;

                IObjectSpace os = Application.CreateObjectSpace();
                empl = os.FindObject<Employee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                clientEmpl = os.FindObject<ClientEmployee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                if (clientEmpl != null)
                {
                    Frame.GetController<ContainerPlacementViewController>().ContainerPlacementAction.Active.SetItemValue("myReason", false);
                    Frame.GetController<ContainerPlacementViewController>().ContainerFinishPlacementAction.Active.SetItemValue("myReason", false);
                    Frame.GetController<ContainerPlacementViewController>().ContainerPlacementPopupAction.Active.SetItemValue("myReason", false);
                }
            }
            if (View is DetailView)
            {
                if (empl != null)
                {
                    if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersPlacement")
                    {
                        RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;
                        // to do - изменение видимости в зависисмости от типа сотрудника (сотрудник клиента или терминала)

                        if (request.RequestStatus == Enums.ERequestStatus.Подана || request.RequestStatus == Enums.ERequestStatus.Обработка ||
                            request.RequestStatus == Enums.ERequestStatus.Отредактирована || request.RequestStatus == Enums.ERequestStatus.ОбработанаЧастично)
                        {
                            Frame.GetController<ContainerPlacementViewController>().ContainerPlacementAction.Active.SetItemValue("myReason", true);
                        }
                        else
                            Frame.GetController<ContainerPlacementViewController>().ContainerPlacementAction.Active.SetItemValue("myReason", false);
                    }
                    if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.Containers.ContainerPlacementAct")
                    {
                        ContainerPlacementAct containerPlacementAct = View.CurrentObject as ContainerPlacementAct;
                        if (containerPlacementAct.Container.onTerminal == false)
                        {
                            Frame.GetController<ContainerPlacementViewController>().ContainerFinishPlacementAction.Active.SetItemValue("myReason", true);
                        }
                        else
                            Frame.GetController<ContainerPlacementViewController>().ContainerFinishPlacementAction.Active.SetItemValue("myReason", false);
                    }
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Приемка контейнера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerPlacementAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;
            //RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            RequestContainersPlacement request;
            request = os.FindObject<RequestContainersPlacement>(new BinaryOperator("Oid", x_id));
            //UnitOfWork unitOfWork = (UnitOfWork)request.Session;
            //Connect connect = Connect.FromUnitOfWork(unitOfWork);
            Connect connect = Connect.FromObjectSpace(os);
            RequestContainerInfo requestContainerInfo = null;
            RequestDriverInfo requestDriverInfo = null;
            //request.RequestContainerInfos
            DetailView masterDV = (DetailView)View;
            foreach (ListPropertyEditor editor in masterDV.GetItems<ListPropertyEditor>())
            {
                if (editor.ListView.Id == "RequestContainersPlacement_RequestContainerInfos_ListView")
                {
                    if (request.RequestContainerInfos.Count == 1)
                        requestContainerInfo = request.RequestContainerInfos[0];
                    else
                    {
                        if (editor.ListView.SelectedObjects.Count == 0)
                            throw new Exception("Необходимо выбрать один контейнер!");
                        if (editor.ListView.SelectedObjects.Count > 1)
                            throw new Exception("Необходимо выбрать только один контейнер!");
                        requestContainerInfo = editor.ListView.SelectedObjects[0] as RequestContainerInfo;
                    }
                }
                if (editor.ListView.Id == "RequestBaseDriversInfo_RequestDriverInfos_ListView")
                {
                    if (request.RequestDriverInfos.Count == 1)
                        requestDriverInfo = request.RequestDriverInfos[0];
                    else
                    {
                        try
                        {
                            requestDriverInfo = editor.ListView.SelectedObjects[0] as RequestDriverInfo;
                        }
                        catch { }
                    }
                }
            }
            if (requestContainerInfo != null)
            {
                
                ContainerPlacementAct containerPlacementAct = null;
                // Проверяем, что такой акт еще не создавали
                containerPlacementAct = connect.FindFirstObject<ContainerPlacementAct>(mc => mc.RequestContainerInfo.Oid == requestContainerInfo.Oid);
                //if (requestContainerInfo.RequestStatus == Enums.ERequestStatus.Отработана ||
                //    requestContainerInfo.RequestStatus == Enums.ERequestStatus.Обработка ||
                //    requestContainerInfo.RequestStatus == Enums.ERequestStatus.Подана)
                if (containerPlacementAct == null)
                //{
                //    containerPlacementAct = connect.FindFirstObject<ContainerPlacementAct>(mc => mc.RequestContainerInfo.Oid == requestContainerInfo.Oid);
                //}
                //else
                {
                    // создаем акт о приемке
                    containerPlacementAct = connect.CreateObject<ContainerPlacementAct>();

                    try
                    {
                        containerPlacementAct.ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(request.Session.DataLayer,
                  "Акт приемки контейнеров" + request.Client.Name + DateTime.Now.Year, string.Empty)); ;
                    }
                    catch { }
                //}
                    try { containerPlacementAct.Stock = connect.FindFirstObject<Stock>(mc => mc.Oid == requestContainerInfo.Stock.Oid); }
                    catch { }
                    // сначала проверям, указан ли водитель у контейнера напрямую, если нет, пробуем посмотреть, может его выбрали галкой
                    if (requestContainerInfo.RequestDriverInfo != null)
                    {
                        try { containerPlacementAct.DriverName = requestContainerInfo.RequestDriverInfo.DriverName?? ""; }
                        catch { }
                        try { containerPlacementAct.DriverPhoneNumber = requestContainerInfo.RequestDriverInfo.DriverPhoneNumber ?? ""; }
                        catch { }
                        try { containerPlacementAct.TransportNumber = requestContainerInfo.RequestDriverInfo.TransportNumber ?? ""; }
                        catch { }
                        try
                        {
                            if (requestContainerInfo.RequestDriverInfo.TransportType != null)
                                containerPlacementAct.TransportType = connect.FindFirstObject<dTransportType>(mc =>
                      mc.Oid == requestContainerInfo.RequestDriverInfo.TransportType.Oid);
                        }
                        catch { }
                        try
                        {
                            if (requestContainerInfo.RequestDriverInfo.TransportMark != null)
                                containerPlacementAct.TransportMark = connect.FindFirstObject<dTransportMark>(mc =>
                      mc.Oid == requestContainerInfo.RequestDriverInfo.TransportMark.Oid);
                        }
                        catch { }
                        try { containerPlacementAct.TransportCompany = requestContainerInfo.RequestDriverInfo.TransportCompany ?? ""; }
                        catch { }
                        try
                        {
                            containerPlacementAct.RequestDriverInfo = connect.FindFirstObject<RequestDriverInfo>(mc =>
                            mc.Oid == requestContainerInfo.RequestDriverInfo.Oid);
                        }
                        catch { }
                    }
                    else if(requestDriverInfo != null)
                    {
                        try { containerPlacementAct.DriverName = requestDriverInfo.DriverName ?? ""; }
                        catch { }
                        try { containerPlacementAct.DriverPhoneNumber = requestDriverInfo.DriverPhoneNumber ?? ""; }
                        catch { }
                        try { containerPlacementAct.TransportNumber = requestDriverInfo.TransportNumber ?? ""; }
                        catch { }
                        try {
                            if(requestDriverInfo.TransportType != null)
                                containerPlacementAct.TransportType = connect.FindFirstObject<dTransportType>(mc => mc.Oid == requestDriverInfo.TransportType.Oid); }
                        catch { }
                        try {
                            if (requestDriverInfo.TransportMark != null)
                                containerPlacementAct.TransportMark = connect.FindFirstObject<dTransportMark>(mc => mc.Oid == requestDriverInfo.TransportMark.Oid); }
                        catch { }
                        try { containerPlacementAct.TransportCompany = requestDriverInfo.TransportCompany ?? ""; }
                        catch { }
                        try
                        {
                            containerPlacementAct.RequestDriverInfo = connect.FindFirstObject<RequestDriverInfo>(mc => mc.Oid == requestDriverInfo.Oid);
                        }
                        catch { }
                    }
                    // заполняем данные по контейнеру
                    // проверяем, есть ли такой уже в системе, если нет - создаем, заполняя данными
                    Container container = connect.FindFirstObject<Container>(mc => mc.ContainerNumber == requestContainerInfo.Number);
                    if (container == null)
                    {
                        container = connect.CreateObject<Container>();
                        try { container.ContainerNumber = requestContainerInfo.Number; }
                        catch { }
                        try
                        {
                            if(requestContainerInfo.ContainerType != null)
                            //container.ContainerType = connect.FindFirstObject<dContainerType>(mc => mc.Name == requestContainerInfo.ContainerType.Name);
                                container.ContainerType = connect.FindFirstObject<dContainerType>(mc => mc.Oid == requestContainerInfo.ContainerType.Oid);
                        }
                        catch { }

                        container.Save();
                    }
                    container.EditFlag = true;
                    try { container.Owner = connect.FindFirstObject<Client>(mc => mc.Oid == request.Client.Oid); }
                    catch { }
                    try { container.Stock = connect.FindFirstObject<Stock>(mc => mc.Oid == requestContainerInfo.Stock.Oid); }
                    catch { }
                    try { container.Terminal = connect.FindFirstObject<Terminal>(mc => mc.Oid == request.Terminal.Oid); }
                    catch { }
                    try { container.isLaden = requestContainerInfo.isLaden; }
                    catch { }
                    try { container.LadenWeight = requestContainerInfo.LadenWeight; }
                    catch { }
                    try { container.CargoType = requestContainerInfo.CargoType?? ""; }
                    catch { }
                    try { container.SealNumber = requestContainerInfo.SealNumber ?? ""; }
                    catch { }
                    container.Save();
                    containerPlacementAct.Container = container;

                    containerPlacementAct.Request = request;
                    //requestContainerInfo.RequestStatus = Enums.ERequestStatus.Обработка;
                    containerPlacementAct.RequestContainerInfo = connect.FindFirstObject<RequestContainerInfo>(mc => mc.Oid == requestContainerInfo.Oid);

                    // ищем тарифы на лифт и хранение и подставлем их сразу в акт
                    // 1. Находим действующий контракт с клиентом
                    // 2. Находим действующую тарифную сетку
                    // 3. Ищем нужный тариф (сначала по типу операции, потом по типу контейнера)
                    // 1 //
                    //Contract contract = connect.FindFirstObject<Contract>(mc => mc.Client == request.Client && mc.ActiveFlag == true);
                    //if (contract != null)
                    //{
                    //    // 2 //
                    //    TariffScale tariffScale = connect.FindFirstObject<TariffScale>(mc => mc.Contract == contract && mc.ActiveFlag == true);
                    //    if (tariffScale != null)
                    //    {
                    //        //3-4//
                    //        IQueryable tariffs = connect.FindObjects<Tariff>(mc => mc.TariffBase == tariffScale);
                    //        foreach (Tariff tariff in tariffs)
                    //        {
                    //            try
                    //            {
                    //                if (tariff?.TariffService?.ServiceType?.Name == "Крановая операция")
                    //                {
                    //                    foreach (dContainerSubType containerSubType in tariff.TariffService.ContainerSubTypes)
                    //                    {
                    //                        if (container.ContainerType.ContainerSubType == containerSubType)
                    //                        {
                    //                            containerPlacementAct.TariffLift = tariff;
                    //                            containerPlacementAct.Save();
                    //                            break;
                    //                        }
                    //                    }
                    //                }
                    //                else
                    //                    if (tariff?.TariffService?.ServiceType?.Name == "Хранение контейнера")
                    //                {
                    //                    foreach (dContainerSubType containerSubType in tariff.TariffService.ContainerSubTypes)
                    //                    {
                    //                        if (container.ContainerType.ContainerSubType == containerSubType)
                    //                        {
                    //                            containerPlacementAct.TariffStorage = tariff;
                    //                            containerPlacementAct.Save();
                    //                            break;
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //            catch { }
                    //        }
                    //    }
                    //}
                    containerPlacementAct.Save();
                    os.CommitChanges();
            }
            requestContainerInfo.RequestStatus = Enums.ERequestStatus.Обработка;
                // открываем окно акта 
                DetailView dv = Application.CreateDetailView(os, containerPlacementAct);//Specify the IsRoot parameter if necessary.
                dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
                e.ShowViewParameters.CreatedView = dv;
                View.Close();
            }
        }

        /// <summary>
        /// Окончательно размещаем контейнер:
        /// - Создаем историю по размещению
        /// - Создаем оказанные услуги на терминале (крановая операция и начало хранения)
        /// - меняем статусы (инфо по контейнеру, и если нужно - заявки)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerFinishPlacementAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            ContainerPlacementAct containerPlacementAct = null;
            containerPlacementAct = os.FindObject<ContainerPlacementAct>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)containerPlacementAct.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            // История движения контейнера (размещения на терминале)
            ContainerHistory containerHistory = null;
            Container container;
            container = containerPlacementAct.Container;
            container.onTerminal = true;
            //container.EditFlag = false;
            container.Save();
            // проверяем, создали ли уже по этому размещению историю
            containerHistory = connect.FindFirstObject<ContainerHistory>(mc => mc.ContainerPlacementAct.Oid == containerPlacementAct.Oid);
            if (containerHistory == null)
            {
                containerHistory = connect.CreateObject<ContainerHistory>();
                containerHistory.ContainerHistoryKind = Enums.EContainerHistoryKind.Размещение;
                containerHistory.ContainerPlacementAct = containerPlacementAct;
                containerHistory.Container = container;
                containerHistory.Stock = container.Stock;
                containerHistory.Date = containerPlacementAct.ActDate;//DateTime.Now.Date;
                containerHistory.Time = DateTime.Now.ToShortTimeString();
                try
                {
                    containerHistory.DriverName = containerPlacementAct.DriverName;
                }
                catch { }
                try
                {
                    containerHistory.DriverPhoneNumber = containerPlacementAct.DriverPhoneNumber;
                }
                catch { }
                try
                {
                    containerHistory.TransportNumber = containerPlacementAct.TransportNumber;
                }
                catch { }
                try
                {
                    containerHistory.Notes = container.SpecialNotes;
                }
                catch { }
                try
                {
                    containerHistory.isLaden = container.isLaden;
                }
                catch { }
                try
                {
                    containerHistory.LadenWeight = container.LadenWeight;
                }
                catch { }
                try
                {
                    containerHistory.CargoType = container.CargoType;
                }
                catch { }
                try
                {
                    containerHistory.SealNumber = container.SealNumber;
                }
                catch { }

                containerHistory.Save();

                
                try
                {
                    CreateEmailNotificationsController CreateEmailNotificationsController = new CreateEmailNotificationsController();
                    CreateEmailNotificationsController.CreateNotifacations(containerHistory, connect);
                }
                catch { }
            }
            os.CommitChanges();
            if (containerPlacementAct.CalcType == Enums.ECalcType.byDaily)
            {
                //if (containerPlacementAct.TariffLift == null)
                //{
                //    throw new Exception("У акта приемки не указан тариф на крановую операцию. Пожалуйста, укажите его для продожения!");
                //}
                //if (containerPlacementAct.TariffStorage == null)
                //{
                //    throw new Exception("У акта приемки не указан тариф на хранение контейнера. Пожалуйста, укажите его для продожения!");
                //}



                // создаем оказанные услуги по контейнеру:
                // a. По крановой операции
                // b. Начало хранения

                /// a ///
                ServiceDone serviceLiftDone = null;
                bool tariffLiftFound = false;
                if (containerPlacementAct.LiftPrice != 0)
                {
                    serviceLiftDone = connect.CreateObject<ServiceDone>();
                    serviceLiftDone.Container = container;
                    serviceLiftDone.Client = container.Owner;
                    serviceLiftDone.StartDate = containerPlacementAct.ActDate;//DateTime.Now.Date;
                                                                              //serviceLiftDone.FinishDate = DateTime.Now.Date;
                    try
                    {
                        serviceLiftDone.Tariff = containerPlacementAct?.TariffLift;
                    }
                    catch { }
                    try
                    {
                        serviceLiftDone.UnitKind = containerPlacementAct?.TariffLift?.UnitKind;
                    }
                    catch { }
                    try
                    {
                        serviceLiftDone.Currency = containerPlacementAct.Currency;
                    }
                    catch { }
                    serviceLiftDone.ServiceCount = 1;
                    tariffLiftFound = true;
                }
                /// b ///
                ServiceDone serviceStorageDone = null;
                bool tariffStorageFound = false;
                bool isNowMonth = true;
                bool isLastMonth = true;
                // первый день текущего месяца
                var firstDayOfMonth = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 1);
                var firstDayOfLastMonth = new DateTime(DateTime.Now.Date.AddMonths(-1).Year, DateTime.Now.Date.AddMonths(-1).Month, 1);
                // услугу прошлых месяцев до прошлого месяца (единую, без разбивки по месяцам) и услугу прошлого месяца для отчета
                ServiceDone serviceStorageDoneLast = null;
                ServiceDone serviceStorageDoneLastMonth = null;
                if (containerPlacementAct.StoragePrice != 0)
                {
                    serviceStorageDone = connect.CreateObject<ServiceDone>();
                    serviceStorageDone.Container = container;
                    serviceStorageDone.Client = container.Owner;
                    serviceStorageDone.StartDate = containerPlacementAct.ActDate;//DateTime.Now.Date;
                    try
                    {
                        serviceStorageDone.Tariff = containerPlacementAct?.TariffStorage;
                    }
                    catch { }
                    try
                    {
                        serviceStorageDone.UnitKind = containerPlacementAct?.TariffStorage?.UnitKind;
                    }
                    catch { }
                    try
                    {
                        serviceStorageDone.Currency = containerPlacementAct.Currency;
                    }
                    catch { }


                    // считаем количество услуг (если контейнер размещается не по заявке а изначально) от начала действия услуги (даты акта)
                    // если контейнер был размещен в этом месяце
                    if (containerPlacementAct.ActDate.Year == DateTime.Now.Date.Year && containerPlacementAct.ActDate.Month == DateTime.Now.Date.Month)
                    {
                        TimeSpan diffDays = DateTime.Now.Date - containerPlacementAct.ActDate;
                        serviceStorageDone.ServiceCount = diffDays.Days + 1;
                        isLastMonth = false;
                    }
                    // если контейнер был размещен раньше начала месяца
                    else
                    {
                        isNowMonth = false;
                        // количество дней с начала месяца
                        TimeSpan diffDays = DateTime.Now.Date - firstDayOfMonth;
                        serviceStorageDone.StartDate = firstDayOfMonth;
                        serviceStorageDone.ServiceCount = diffDays.Days + 1;

                        // создаем услугу прошлых месяцев (за прошлый месяц для отчета и предыдущие)
                        serviceStorageDoneLastMonth = connect.CreateObject<ServiceDone>();
                        serviceStorageDoneLastMonth.Container = container;
                        serviceStorageDoneLastMonth.Client = container.Owner;
                        try
                        {
                            serviceStorageDoneLastMonth.Tariff = containerPlacementAct?.TariffStorage;
                        }
                        catch { }
                        try
                        {
                            serviceStorageDoneLastMonth.UnitKind = containerPlacementAct?.TariffStorage?.UnitKind;
                        }
                        catch { }
                        try
                        {
                            serviceStorageDoneLastMonth.Currency = containerPlacementAct.Currency;
                        }
                        catch { }
                        // если услуга начата раньше прошлого месяца
                        if (firstDayOfLastMonth > containerPlacementAct.ActDate)
                        {
                            serviceStorageDoneLastMonth.StartDate = firstDayOfLastMonth;
                            serviceStorageDoneLastMonth.ServiceCount = (firstDayOfMonth - firstDayOfLastMonth).Days;
                            isLastMonth = false;
                            // услуга предыдущих месяцев до прошлого
                            serviceStorageDoneLast = connect.CreateObject<ServiceDone>();
                            serviceStorageDoneLast.Container = container;
                            serviceStorageDoneLast.Client = container.Owner;
                            serviceStorageDoneLast.StartDate = containerPlacementAct.ActDate;
                            try
                            {
                                serviceStorageDoneLast.Tariff = containerPlacementAct?.TariffStorage;
                            }
                            catch { }
                            try
                            {
                                serviceStorageDoneLast.UnitKind = containerPlacementAct?.TariffStorage?.UnitKind;
                            }
                            catch { }
                            try
                            {
                                serviceStorageDoneLast.Currency = containerPlacementAct.Currency;
                            }
                            catch { }
                            serviceStorageDoneLast.ServiceCount = (firstDayOfLastMonth - containerPlacementAct.ActDate).Days;
                            serviceStorageDoneLast.Save();
                        }
                        else
                        {
                            serviceStorageDoneLastMonth.StartDate = containerPlacementAct.ActDate;
                            serviceStorageDoneLastMonth.ServiceCount = (firstDayOfMonth - containerPlacementAct.ActDate).Days;
                        }
                        serviceStorageDoneLastMonth.Save();
                    }

                    tariffStorageFound = true;

                }
                // Определям, есть ли скидка и ее тип. Если есть - проставляем ее значение и действие для обеих услуг
                //либо на все операции какие то проценты до определенной даты, 
                //либо на на хранение - первые несколько дней  
                if (containerPlacementAct.AddDiscount)
                {
                    if (containerPlacementAct.DiscountType == Enums.EDiscountType.byTime)
                    {
                        // смотрим, не прошла ли еще скидка
                        if (containerPlacementAct.DiscountActiveThroughDate >= containerPlacementAct.ActDate)
                        {
                            try
                            {
                                serviceLiftDone.Discount = containerPlacementAct.Discount;
                                serviceLiftDone.DiscountActiveThroughDate = containerPlacementAct.DiscountActiveThroughDate;
                            }
                            catch { }
                            if (isNowMonth)
                            {
                                try
                                {
                                    serviceStorageDone.Discount = containerPlacementAct.Discount;
                                    serviceStorageDone.DiscountActiveThroughDate = containerPlacementAct.DiscountActiveThroughDate;
                                }
                                catch { }
                            }
                            else
                            {
                                // если скидка действовала на начало текущего месяца - то ее включаем в этом месяце (соответственно, она действовала и в предыдущих месяцах)
                                if (containerPlacementAct.DiscountActiveThroughDate >= firstDayOfMonth)
                                {
                                    try
                                    {
                                        serviceStorageDone.Discount = containerPlacementAct.Discount;
                                        serviceStorageDone.DiscountActiveThroughDate = containerPlacementAct.DiscountActiveThroughDate;
                                    }
                                    catch { }
                                    try
                                    {
                                        serviceStorageDoneLastMonth.Discount = containerPlacementAct.Discount;
                                        serviceStorageDoneLastMonth.DiscountActiveThroughDate = firstDayOfMonth.AddDays(-1);
                                    }
                                    catch { }
                                    if (!isLastMonth)
                                    {
                                        try
                                        {
                                            serviceStorageDoneLast.Discount = containerPlacementAct.Discount;
                                            serviceStorageDoneLast.DiscountActiveThroughDate = firstDayOfLastMonth.AddDays(-1);
                                        }
                                        catch { }
                                    }

                                }
                                else
                                {
                                    if (isLastMonth)
                                    {
                                        try
                                        {
                                            serviceStorageDoneLastMonth.Discount = containerPlacementAct.Discount;
                                            serviceStorageDoneLastMonth.DiscountActiveThroughDate = containerPlacementAct.DiscountActiveThroughDate;
                                        }
                                        catch { }
                                    }
                                    else
                                    {
                                        // если скидка не закончила действовать на начало прошлого месяца
                                        if (containerPlacementAct.DiscountActiveThroughDate >= firstDayOfLastMonth)
                                        {
                                            try
                                            {
                                                serviceStorageDoneLastMonth.Discount = containerPlacementAct.Discount;
                                                serviceStorageDoneLastMonth.DiscountActiveThroughDate = containerPlacementAct.DiscountActiveThroughDate;
                                            }
                                            catch { }
                                            try
                                            {
                                                serviceStorageDoneLast.Discount = containerPlacementAct.Discount;
                                                serviceStorageDoneLast.DiscountActiveThroughDate = firstDayOfLastMonth.AddDays(-1);
                                            }
                                            catch { }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                serviceStorageDoneLast.Discount = containerPlacementAct.Discount;
                                                serviceStorageDoneLast.DiscountActiveThroughDate = containerPlacementAct.DiscountActiveThroughDate;
                                            }
                                            catch { }
                                        }
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        if (isNowMonth)
                        {
                            try
                            {
                                serviceStorageDone.Discount = containerPlacementAct.Discount;
                                serviceStorageDone.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(containerPlacementAct.DiscountActiveDays - 1);
                            }
                            catch { }
                        }
                        else
                        {
                            if (containerPlacementAct.ActDate.AddDays(containerPlacementAct.DiscountActiveDays - 1) >= firstDayOfMonth)
                            {
                                try
                                {
                                    serviceStorageDone.Discount = containerPlacementAct.Discount;
                                    serviceStorageDone.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(containerPlacementAct.DiscountActiveDays - 1);
                                }
                                catch { }
                                try
                                {
                                    serviceStorageDoneLastMonth.Discount = containerPlacementAct.Discount;
                                    serviceStorageDoneLastMonth.DiscountActiveThroughDate = firstDayOfMonth.AddDays(-1);
                                }
                                catch { }
                                if (!isLastMonth)
                                {
                                    try
                                    {
                                        serviceStorageDoneLast.Discount = containerPlacementAct.Discount;
                                        serviceStorageDoneLast.DiscountActiveThroughDate = firstDayOfLastMonth.AddDays(-1);
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                if (isLastMonth)
                                {
                                    try
                                    {
                                        serviceStorageDoneLastMonth.Discount = containerPlacementAct.Discount;
                                        serviceStorageDoneLastMonth.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(containerPlacementAct.DiscountActiveDays - 1);
                                    }
                                    catch { }
                                }
                                else
                                {
                                    // если скидка не закончила действовать на начало прошлого месяца
                                    if (containerPlacementAct.ActDate.AddDays(containerPlacementAct.DiscountActiveDays - 1) >= firstDayOfLastMonth)
                                    {
                                        try
                                        {
                                            serviceStorageDoneLastMonth.Discount = containerPlacementAct.Discount;
                                            serviceStorageDoneLastMonth.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(containerPlacementAct.DiscountActiveDays - 1);
                                        }
                                        catch { }
                                        try
                                        {
                                            serviceStorageDoneLast.Discount = containerPlacementAct.Discount;
                                            serviceStorageDoneLast.DiscountActiveThroughDate = firstDayOfLastMonth.AddDays(-1);
                                        }
                                        catch { }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            serviceStorageDoneLast.Discount = containerPlacementAct.Discount;
                                            serviceStorageDoneLast.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(containerPlacementAct.DiscountActiveDays - 1);
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                    }
                }
                // назначаем стоимости единицы услуги
                try
                {
                    serviceLiftDone.Value = containerPlacementAct.LiftPrice;
                }
                catch { }
                try
                {
                    serviceStorageDone.Value = containerPlacementAct.StoragePrice;
                }
                catch { }
                try {
                    if(serviceStorageDoneLastMonth != null) 
                        serviceStorageDoneLastMonth.Value = containerPlacementAct.StoragePrice; }
                catch { }
                try {
                    if (serviceStorageDoneLast != null)
                        serviceStorageDoneLast.Value = containerPlacementAct.StoragePrice; }
                catch { }

                #region Переделано значения тарифа и скидок проставляется в самом акте
                //    TariffScale tariffScale = (TariffScale)containerPlacementAct?.TariffStorage?.TariffBase;
                //if (tariffScale != null)
                //{
                //    serviceLiftDone.Currency = tariffScale.Currency;

                //    // Определям, есть ли скидка и ее тип. Если есть - проставляем ее значение и действие для обеих услуг
                //    //либо на все операции какие то проценты до определенной даты, 
                //    //либо на на хранение - первые несколько дней  
                //    if (tariffScale.AddDiscount)
                //    {
                //        if (tariffScale.DiscountType == Enums.EDiscountType.byTime)
                //        {
                //            // смотрим, не прошла ли еще скидка
                //            if (tariffScale.DiscountActiveThroughDate >= containerPlacementAct.ActDate)
                //            {
                //                serviceLiftDone.Discount = tariffScale.Discount;
                //                serviceLiftDone.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;
                //                if (isNowMonth)
                //                {
                //                    serviceStorageDone.Discount = tariffScale.Discount;
                //                    serviceStorageDone.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;
                //                }
                //                else
                //                {
                //                    // если скидка действовала на начало текущего месяца - то ее включаем в этом месяце (соответственно, она действовала и в предыдущих месяцах)
                //                    if (tariffScale.DiscountActiveThroughDate >= firstDayOfMonth)
                //                    {
                //                        serviceStorageDone.Discount = tariffScale.Discount;
                //                        serviceStorageDone.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;

                //                        serviceStorageDoneLastMonth.Discount = tariffScale.Discount;
                //                        serviceStorageDoneLastMonth.DiscountActiveThroughDate = firstDayOfMonth.AddDays(-1);

                //                        if (!isLastMonth)
                //                        {
                //                            serviceStorageDoneLast.Discount = tariffScale.Discount;
                //                            serviceStorageDoneLast.DiscountActiveThroughDate = firstDayOfLastMonth.AddDays(-1);
                //                        }

                //                    }
                //                    else
                //                    {
                //                        if (isLastMonth)
                //                        {
                //                            serviceStorageDoneLastMonth.Discount = tariffScale.Discount;
                //                            serviceStorageDoneLastMonth.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;
                //                        }
                //                        else
                //                        {
                //                            // если скидка не закончила действовать на начало прошлого месяца
                //                            if (tariffScale.DiscountActiveThroughDate >= firstDayOfLastMonth)
                //                            {
                //                                serviceStorageDoneLastMonth.Discount = tariffScale.Discount;
                //                                serviceStorageDoneLastMonth.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;
                //                                serviceStorageDoneLast.Discount = tariffScale.Discount;
                //                                serviceStorageDoneLast.DiscountActiveThroughDate = firstDayOfLastMonth.AddDays(-1);
                //                            }
                //                            else
                //                            {
                //                                serviceStorageDoneLast.Discount = tariffScale.Discount;
                //                                serviceStorageDoneLast.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;
                //                            }
                //                        }
                //                    }
                //                }

                //            }
                //        }
                //        else
                //        {
                //            if (isNowMonth)
                //            {
                //                serviceStorageDone.Discount = tariffScale.Discount;
                //                serviceStorageDone.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(tariffScale.DiscountActiveDays - 1);
                //            }
                //            else
                //            {
                //                if (containerPlacementAct.ActDate.AddDays(tariffScale.DiscountActiveDays - 1) >= firstDayOfMonth)
                //                {
                //                    serviceStorageDone.Discount = tariffScale.Discount;
                //                    serviceStorageDone.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(tariffScale.DiscountActiveDays - 1);

                //                    serviceStorageDoneLastMonth.Discount = tariffScale.Discount;
                //                    serviceStorageDoneLastMonth.DiscountActiveThroughDate = firstDayOfMonth.AddDays(-1);

                //                    if (!isLastMonth)
                //                    {
                //                        serviceStorageDoneLast.Discount = tariffScale.Discount;
                //                        serviceStorageDoneLast.DiscountActiveThroughDate = firstDayOfLastMonth.AddDays(-1);
                //                    }
                //                }
                //                else
                //                {
                //                    if (isLastMonth)
                //                    {
                //                        serviceStorageDoneLastMonth.Discount = tariffScale.Discount;
                //                        serviceStorageDoneLastMonth.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(tariffScale.DiscountActiveDays - 1);
                //                    }
                //                    else
                //                    {
                //                        // если скидка не закончила действовать на начало прошлого месяца
                //                        if (containerPlacementAct.ActDate.AddDays(tariffScale.DiscountActiveDays - 1) >= firstDayOfLastMonth)
                //                        {
                //                            serviceStorageDoneLastMonth.Discount = tariffScale.Discount;
                //                            serviceStorageDoneLastMonth.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(tariffScale.DiscountActiveDays - 1);
                //                            serviceStorageDoneLast.Discount = tariffScale.Discount;
                //                            serviceStorageDoneLast.DiscountActiveThroughDate = firstDayOfLastMonth.AddDays(-1);
                //                        }
                //                        else
                //                        {
                //                            serviceStorageDoneLast.Discount = tariffScale.Discount;
                //                            serviceStorageDoneLast.DiscountActiveThroughDate = containerPlacementAct.ActDate.AddDays(tariffScale.DiscountActiveDays - 1);
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //    // назначаем стоимости единицы услуги
                //if (container.ContainerType.ContainerTypeSize.ContainerCategorySize == Enums.EContainerCategorySize.twenty)
                //    {
                //        if (container.isLaden)
                //        {
                //            serviceLiftDone.Value = containerPlacementAct.TariffLift.Value20Laden;
                //            serviceStorageDone.Value = containerPlacementAct.TariffStorage.Value20Laden;
                //            try { serviceStorageDoneLastMonth.Value = containerPlacementAct.TariffStorage.Value20Laden; }
                //            catch { }
                //            try { serviceStorageDoneLast.Value = containerPlacementAct.TariffStorage.Value20Laden; }
                //            catch { }
                //        }
                //        else
                //        {
                //            serviceLiftDone.Value = containerPlacementAct.TariffLift.Value20Empty;
                //            serviceStorageDone.Value = containerPlacementAct.TariffStorage.Value20Empty;
                //            try { serviceStorageDoneLastMonth.Value = containerPlacementAct.TariffStorage.Value20Empty; }
                //            catch { }
                //            try { serviceStorageDoneLast.Value = containerPlacementAct.TariffStorage.Value20Empty; }
                //            catch { }
                //        }
                //    }
                //    else
                //    {
                //        if (container.isLaden)
                //        {
                //            serviceLiftDone.Value = containerPlacementAct.TariffLift.Value40Laden;
                //            serviceStorageDone.Value = containerPlacementAct.TariffStorage.Value40Laden;
                //            try { serviceStorageDoneLastMonth.Value = containerPlacementAct.TariffStorage.Value40Laden; }
                //            catch { }
                //            try { serviceStorageDoneLast.Value = containerPlacementAct.TariffStorage.Value40Laden; }
                //            catch { }
                //        }
                //        else
                //        {
                //            serviceLiftDone.Value = containerPlacementAct.TariffLift.Value40Empty;
                //            serviceStorageDone.Value = containerPlacementAct.TariffStorage.Value40Empty;
                //            try { serviceStorageDoneLastMonth.Value = containerPlacementAct.TariffStorage.Value40Empty; }
                //            catch { }
                //            try { serviceStorageDoneLast.Value = containerPlacementAct.TariffStorage.Value40Empty; }
                //            catch { }
                //        }
                //    }
                //}
                #endregion

                #region переделано по другому, ссылки на тарифы идут из акта - от них считаем
                /*
                /// 1. Ищем активный договор у клиента
                /// 2. Ищем в договоре актиную тарифную сетку
                /// 3. Ищем тарифы по виду операции 
                /// 4. Ищем нужный тариф по типу контейнера
                /// 4.1 Проверям на действие скидки
                /// 5. Создаем у контейнера 

                // 1 //
                Contract contract = connect.FindFirstObject<Contract>(mc=> mc.Client == container.Owner && mc.ActiveFlag == true);
                if(contract!= null)
                {
                    // 2 //
                    TariffScale tariffScale = connect.FindFirstObject<TariffScale>(mc=> mc.Contract == contract && mc.ActiveFlag == true);
                    if (tariffScale != null)
                    {
                        serviceLiftDone.Currency = tariffScale.Currency;

                        // Определям, есть ли скидка и ее тип. Если есть - проставляем ее значение и действие для обеих услуг
                        //либо на все операции какие то проценты до определенной даты, 
                        //либо на на хранение - первые несколько дней  
                        if (tariffScale.AddDiscount)
                        {
                            if (tariffScale.DiscountType == Enums.EDiscountType.byTime)
                            {
                                // смотрим, не прошла ли еще скидка
                                if (tariffScale.DiscountActiveThroughDate >= DateTime.Now.Date)
                                {
                                    serviceLiftDone.Discount = tariffScale.Discount;
                                    serviceLiftDone.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;

                                    serviceStorageDone.Discount = tariffScale.Discount;
                                    serviceStorageDone.DiscountActiveThroughDate = tariffScale.DiscountActiveThroughDate;

                                }
                            }
                            else
                            {
                                serviceStorageDone.Discount = tariffScale.Discount;
                                serviceStorageDone.DiscountActiveThroughDate = DateTime.Now.AddDays(tariffScale.DiscountActiveDays-1);
                            }
                        }
                        // a //
                        //3-4//
                        IQueryable tariffs = connect.FindObjects<Tariff>(mc => mc.TariffBase == tariffScale && mc.TariffService.ServiceType.Name == "Крановая операция");
                        foreach (Tariff tariff in tariffs)
                        {
                            foreach (dContainerSubType containerSubType in tariff.TariffService.ContainerSubTypes)
                            {
                                //5//
                                if (container.ContainerType.ContainerSubType == containerSubType)
                                {
                                    serviceLiftDone.Tariff = tariff;
                                    serviceLiftDone.UnitKind = tariff.UnitKind;
                                    if (container.ContainerType.ContainerTypeSize.ContainerCategorySize == Enums.EContainerCategorySize.twenty)
                                    {
                                        if (container.isLaden)
                                            serviceLiftDone.Value = tariff.Value20Laden;
                                        else
                                            serviceLiftDone.Value = tariff.Value20Empty;
                                    }
                                    else
                                    {
                                        if (container.isLaden)
                                            serviceLiftDone.Value = tariff.Value40Laden;
                                        else
                                            serviceLiftDone.Value = tariff.Value40Empty;
                                    }
                                    tariffLiftFound = true;

                                    break;
                                }
                            }
                        }
                        // b //
                        //3-4//
                        serviceStorageDone.Currency = tariffScale.Currency;
                        IQueryable storageTariffs = connect.FindObjects<Tariff>(mc => mc.TariffBase == tariffScale 
                        && mc.TariffService.ServiceType.Name == "Хранение контейнера");
                        foreach (Tariff tariff in storageTariffs)
                        {
                            foreach (dContainerSubType containerSubType in tariff.TariffService.ContainerSubTypes)
                            {
                                //5//
                                if (container.ContainerType.ContainerSubType == containerSubType)
                                {
                                    serviceStorageDone.Tariff = tariff;
                                    serviceStorageDone.UnitKind = tariff.UnitKind;
                                    if (container.ContainerType.ContainerTypeSize.ContainerCategorySize == Enums.EContainerCategorySize.twenty)
                                    {
                                        if (container.isLaden)
                                            serviceStorageDone.Value = tariff.Value20Laden;
                                        else
                                            serviceStorageDone.Value = tariff.Value20Empty;
                                    }
                                    else
                                    {
                                        if (container.isLaden)
                                            serviceStorageDone.Value = tariff.Value40Laden;
                                        else
                                            serviceStorageDone.Value = tariff.Value40Empty;
                                    }
                                    tariffStorageFound = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                // если нужного тарифа не нашли - открываем лист тарифов клиента, чтобы выбрать нужный или создать новый
                if(tariffLiftFound == false)
                {
                    // TO DO
                }
                if (tariffStorageFound == false)
                {
                    // TO DO
                }
                */
                #endregion

                // считаем расходы по крановым операциям
                try
                {
                    if (tariffLiftFound)
                    {
                        if (serviceLiftDone.DiscountActiveThroughDate != DateTime.MinValue)
                        {
                            if (serviceLiftDone.DiscountActiveThroughDate >= containerPlacementAct.ActDate)
                            {
                                serviceLiftDone.ValueTotal += serviceLiftDone.ServiceCount * serviceLiftDone.Value * (100 - serviceLiftDone.Discount) / 100;
                                serviceLiftDone.ValueTotalDate = containerPlacementAct.ActDate;
                            }
                            else
                            {
                                serviceLiftDone.ValueTotal += serviceLiftDone.Value * serviceLiftDone.ServiceCount;
                                serviceLiftDone.ValueTotalDate = containerPlacementAct.ActDate;
                            }
                        }
                        else
                        {
                            serviceLiftDone.ValueTotal += serviceLiftDone.Value * serviceLiftDone.ServiceCount;
                            serviceLiftDone.ValueTotalDate = containerPlacementAct.ActDate;
                        }
                        serviceLiftDone.isDone = true;
                        serviceLiftDone.FinishDate = containerPlacementAct.ActDate;//DateTime.Now.Date;
                        serviceLiftDone.Save();
                    }
                }
                catch { }
                // считаем расходы по хранению
                try
                {
                    if (tariffStorageFound)
                    {
                        if (serviceStorageDone.DiscountActiveThroughDate != DateTime.MinValue || serviceStorageDoneLastMonth?.DiscountActiveThroughDate != DateTime.MinValue
                            || serviceStorageDoneLast?.DiscountActiveThroughDate != DateTime.MinValue)
                        {
                            // если скидка действовала на момент акта
                            //if (serviceStorageDone.DiscountActiveThroughDate >= containerPlacementAct.ActDate)
                            //{
                            // если скидка действует до сих пор
                            if (serviceStorageDone.DiscountActiveThroughDate >= DateTime.Now.Date)
                            {
                                serviceStorageDone.ValueTotal += serviceStorageDone.ServiceCount * serviceStorageDone.Value * (100 - serviceStorageDone.Discount) / 100;
                                serviceStorageDone.ValueTotalDate = DateTime.Now.Date;
                                if (!isNowMonth)
                                {
                                    serviceStorageDoneLastMonth.ValueTotal += serviceStorageDoneLastMonth.Value * serviceStorageDoneLastMonth.ServiceCount *
                                        (100 - serviceStorageDoneLastMonth.Discount) / 100;
                                    serviceStorageDoneLastMonth.ValueTotalDate = firstDayOfMonth.AddDays(-1);
                                    if (!isLastMonth)
                                    {
                                        serviceStorageDoneLast.ValueTotal += serviceStorageDoneLast.Value * serviceStorageDoneLast.ServiceCount *
                                        (100 - serviceStorageDoneLast.Discount) / 100;
                                        serviceStorageDoneLast.ValueTotalDate = firstDayOfLastMonth.AddDays(-1);
                                    }
                                }
                            }
                            // если скидка закончилась между размещением контейнера и внесением информации о нем (сегодня)
                            else
                            {
                                if (isNowMonth)
                                {
                                    int serviceDiscountDays = 0;
                                    if (serviceStorageDone.DiscountActiveThroughDate != DateTime.MinValue)
                                        serviceDiscountDays = (serviceStorageDone.DiscountActiveThroughDate - containerPlacementAct.ActDate).Days + 1;

                                    serviceStorageDone.ValueTotal += serviceDiscountDays * serviceStorageDone.Value * (100 - serviceStorageDone.Discount) / 100 +
                                        serviceStorageDone.Value * (serviceStorageDone.ServiceCount - serviceDiscountDays);
                                    serviceStorageDone.ValueTotalDate = DateTime.Now.Date;
                                }
                                else
                                {
                                    int serviceDiscountDays = 0;
                                    if (serviceStorageDone.DiscountActiveThroughDate != DateTime.MinValue)
                                        serviceDiscountDays = (serviceStorageDone.DiscountActiveThroughDate - firstDayOfMonth).Days + 1;

                                    serviceStorageDone.ValueTotal += serviceDiscountDays * serviceStorageDone.Value * (100 - serviceStorageDone.Discount) / 100 +
                                        serviceStorageDone.Value * (serviceStorageDone.ServiceCount - serviceDiscountDays);
                                    serviceStorageDone.ValueTotalDate = DateTime.Now.Date;

                                    if (isLastMonth)
                                    {
                                        int serviceDiscountDaysLastMonth = 0;
                                        if (serviceStorageDoneLastMonth.DiscountActiveThroughDate != DateTime.MinValue)
                                            serviceDiscountDaysLastMonth = (serviceStorageDoneLastMonth.DiscountActiveThroughDate - containerPlacementAct.ActDate).Days + 1;
                                        serviceStorageDoneLastMonth.ValueTotal += serviceDiscountDaysLastMonth * serviceStorageDoneLastMonth.Value *
                                            (100 - serviceStorageDoneLastMonth.Discount) / 100 +
                                            serviceStorageDoneLastMonth.Value * (serviceStorageDoneLastMonth.ServiceCount - serviceDiscountDaysLastMonth);
                                        serviceStorageDoneLastMonth.ValueTotalDate = firstDayOfMonth.AddDays(-1);
                                    }
                                    else
                                    {
                                        int serviceDiscountDaysLastMonth = 0;
                                        if (serviceStorageDoneLastMonth.DiscountActiveThroughDate != DateTime.MinValue)
                                            serviceDiscountDaysLastMonth = (serviceStorageDoneLastMonth.DiscountActiveThroughDate - firstDayOfLastMonth).Days + 1;
                                        serviceStorageDoneLastMonth.ValueTotal += serviceDiscountDaysLastMonth * serviceStorageDoneLastMonth.Value *
                                            (100 - serviceStorageDoneLastMonth.Discount) / 100 +
                                            serviceStorageDoneLastMonth.Value * (serviceStorageDoneLastMonth.ServiceCount - serviceDiscountDaysLastMonth);
                                        serviceStorageDoneLastMonth.ValueTotalDate = firstDayOfMonth.AddDays(-1);

                                        int serviceDiscountDaysLast = 0;
                                        serviceDiscountDaysLast = (serviceStorageDoneLast.DiscountActiveThroughDate - containerPlacementAct.ActDate).Days + 1;
                                        serviceStorageDoneLast.ValueTotal += serviceDiscountDaysLast * serviceStorageDoneLast.Value * (100 - serviceStorageDoneLast.Discount) / 100 +
                                            serviceStorageDoneLast.Value * (serviceStorageDoneLast.ServiceCount - serviceDiscountDaysLast);
                                        serviceStorageDoneLast.ValueTotalDate = firstDayOfLastMonth.AddDays(-1);
                                    }
                                }
                            }
                            //}
                            //else
                            //{
                            //    serviceStorageDone.ValueTotal += serviceStorageDone.Value * serviceStorageDone.ServiceCount;
                            //    serviceStorageDone.ValueTotalDate = DateTime.Now.Date;
                            //}
                        }
                        else
                        {
                            serviceStorageDone.ValueTotal += serviceStorageDone.Value * serviceStorageDone.ServiceCount;
                            serviceStorageDone.ValueTotalDate = DateTime.Now.Date;
                            if (!isNowMonth)
                            {
                                serviceStorageDoneLastMonth.ValueTotal += serviceStorageDoneLastMonth.Value * serviceStorageDoneLastMonth.ServiceCount;
                                serviceStorageDoneLastMonth.ValueTotalDate = firstDayOfMonth.AddDays(-1);
                                if (!isLastMonth)
                                {
                                    serviceStorageDoneLast.ValueTotal += serviceStorageDoneLast.Value * serviceStorageDoneLast.ServiceCount;
                                    serviceStorageDoneLast.ValueTotalDate = firstDayOfLastMonth.AddDays(-1);
                                }
                            }
                        }
                    }
                    if (!isNowMonth)
                    {
                        // закрываем услугу хранения прошлых месяцев
                        serviceStorageDoneLastMonth.FinishDate = firstDayOfMonth.AddDays(-1);
                        serviceStorageDoneLastMonth.isDone = true;
                        serviceStorageDoneLastMonth.Save();
                        if (!isLastMonth)
                        {
                            serviceStorageDoneLast.FinishDate = firstDayOfLastMonth.AddDays(-1);
                            serviceStorageDoneLast.isDone = true;
                            serviceStorageDoneLast.Save();
                        }
                    }
                    serviceStorageDone.Save();
                }
                catch { }
                
                
            }
            
            // меняем статус инфо по контейнеру
            try
            {
                RequestContainerInfo requestContainerInfo = containerPlacementAct?.RequestContainerInfo;
                RequestContainersPlacement request = containerPlacementAct?.Request;
                requestContainerInfo.RequestStatus = Enums.ERequestStatus.Отработана;

                requestContainerInfo.Save();
                os.CommitChanges();
                bool isClosed = true;
                foreach (RequestContainerInfo rContainerInfo in containerPlacementAct.Request.RequestContainerInfos)
                {
                    if (rContainerInfo.RequestStatus != Enums.ERequestStatus.Отработана)
                    {
                        isClosed = false;
                        break;
                    }
                }
                if (SecuritySystem.CurrentUser != null)
                {
                    PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;
                    request.Employee = os.FindObject<Employee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                }
                request.RequestStatus = Enums.ERequestStatus.ОбработанаЧастично;
                if (isClosed)
                {
                    request.RequestStatus = Enums.ERequestStatus.Отработана;
                    request.RequestStopDate = DateTime.Now.Date;
                    request.IsFinished = true;
                }
                request.Save();
            }
            catch { }
            os.CommitChanges();
            View.Close();
        }


        /// <summary>
        /// Размещаем контейнер на терминале из списка контейнеров. При нажатии на кнопку открывается Popup окно ContainerPlacementInfo с указанием номера контейнера, 
        /// типа, стока, и терминала (терминал пробуем определить автоматом).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerPlacementPopupAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            ContainerPlacementInfo placementInfo = connect.CreateObject<ContainerPlacementInfo>();
            placementInfo.ContainerNumber = "";
            placementInfo.Save();
            UnitOfWork unitOfWorkNew = (UnitOfWork)placementInfo.Session;
            unitOfWorkNew.CommitChanges();

            DetailView dv = Application.CreateDetailView(os, placementInfo);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.View = dv;
        }

        /// <summary>
        /// Размещение контейнера в стоке сотрудником терминала
        /// 1. Сначала открываем окно заполнения данных по контейнеру (номер и тип), если такой контейнер есть в системе, то в акт подставлем его.
        /// 2. потом открываем окно акта о приемке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerPlacementFromStockPopupAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace(typeof(Stock));//View.ObjectSpace;
            //RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;

            var x_id = ((BaseObject)View.CurrentObject).Oid;

            Stock stock;
            stock = os.FindObject<Stock>(new BinaryOperator("Oid", x_id));
            //Stock stock = (Stock)View.CurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)stock.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            ContainerPlacementInfo placementInfo = connect.CreateObject<ContainerPlacementInfo>();
            placementInfo.ContainerNumber = "";
            try { placementInfo.Owner = connect.FindFirstObject<Client>(mc => mc.Oid == stock.Client.Oid); }
            catch { }
            //try {
                placementInfo.Stock = stock;// connect.FindFirstObject<Stock>(mc => mc.Oid == stock.Oid); }
            //catch { }
            try { placementInfo.Terminal = connect.FindFirstObject<Terminal>(mc => mc.Oid == stock.Terminal.Oid); }
            catch { }
            placementInfo.Save();
            UnitOfWork unitOfWorkNew = (UnitOfWork)placementInfo.Session;
            unitOfWorkNew.CommitChanges();
            // открываем окно информации 
            DetailView dv = Application.CreateDetailView(os, placementInfo);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.View = dv;
        }

        /// <summary>
        /// При нажатии на кнопку Ок в PopUp
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerPlacementPopupAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            ContainerPlacementInfo placementInfo;
            // получаем наш попап объект, его сессию и сохраняем его, после этого, получаем его из ObjectSpace
            placementInfo = (ContainerPlacementInfo)e.PopupWindowViewCurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)placementInfo.Session;
            placementInfo.Save();
            unitOfWork.CommitChanges();
            //Connect connect = Connect.FromUnitOfWork(unitOfWork);

            var x_id = ((BaseObject)e.PopupWindowViewCurrentObject).Oid;
            placementInfo = os.FindObject<ContainerPlacementInfo>(new BinaryOperator("Oid", x_id));
            unitOfWork = (UnitOfWork)placementInfo.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            if (placementInfo.ContainerNumber == "")
                throw new Exception("Не указан номер контейнера!");
            if (placementInfo.ContainerType == null)
                throw new Exception("Не указан тип контейнера!");

            // ищем контейнер
            Container container = connect.FindFirstObject<Container>(x => x.ContainerNumber == placementInfo.ContainerNumber);
            if (container != null)
            {
                try
                {
                    if (container.Owner.Oid != placementInfo.Owner.Oid)
                    {
                        if (container.SpecialNotes != null && container.SpecialNotes != "")
                            container.SpecialNotes += Environment.NewLine;
                        container.SpecialNotes += String.Format("Похоже, что у контейнера сменился владелец: раньше был {0}, стал {1} !", container.Owner.Name, placementInfo.Owner.Name);
                    }
                    //throw new Exception(String.Format("В системе найден контейнер с номером [{0}], но его владелец !"));
                }
                catch { }

            }
            else
            {
                container = connect.CreateObject<Container>();
                container.ContainerNumber = placementInfo.ContainerNumber;
                try { container.ContainerType = connect.FindFirstObject<dContainerType>(mc => mc.Oid == placementInfo.ContainerType.Oid); }
                catch { }
            }
            Stock stock = placementInfo.Stock;
            ContainerPlacementAct containerPlacementAct = connect.CreateObject<ContainerPlacementAct>();

            try
            {
                containerPlacementAct.ActNumber = Convert.ToString(DistributedIdGeneratorHelper.Generate(stock.Session.DataLayer,
          "Акт приемки контейнеров" + stock.Client.Name + DateTime.Now.Year, string.Empty)); ;
            }
            catch { }
            try
            {
                RequestContainersPlacement request = null;
                RequestContainerInfo requestInfo = null;
                requestInfo = connect.FindFirstObject<RequestContainerInfo>(x=> x.RequestStatus == Enums.ERequestStatus.Подана && 
                x.Number == container.ContainerNumber && x.ContainerType == container.ContainerType);
                if (requestInfo != null)
                {
                    request = requestInfo.Request;
                    containerPlacementAct.Request = request;
                    containerPlacementAct.RequestContainerInfo = requestInfo;
                }
            }
            catch { }

            containerPlacementAct.Stock = stock;
            container.EditFlag = true;
            try { container.Owner = connect.FindFirstObject<Client>(mc => mc.Oid == stock.Client.Oid); }
            catch { }
            try { container.Stock = connect.FindFirstObject<Stock>(mc => mc.Oid == stock.Oid); }
            catch { }
            try { container.Terminal = connect.FindFirstObject<Terminal>(mc => mc.Oid == stock.Terminal.Oid); }
            catch { }
            container.Save();
            containerPlacementAct.Container = container;
            containerPlacementAct.Save();
            os.CommitChanges();
            //containerPlacementAct.SetTariffs();
            // открываем окно акта 
            DetailView dv = Application.CreateDetailView(os, containerPlacementAct);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;

            //e.ShowViewParameters.CreatedView = dv;
            Application.MainWindow.SetView(dv);
            Window window = Frame as PopupWindow;
            if (window != null)
            {
                window.Close();
            }
        }

    }
}
