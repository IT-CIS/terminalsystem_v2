﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;
using DevExpress.Xpo;
using TerminalSystem2.Clients;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

namespace TerminalSystem2.Module.Controllers.ContainersProcessing
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ContainerPhotoFromContController : ViewController
    {
        public ContainerPhotoFromContController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            ClientEmployee clientEmpl = null;
            if (SecuritySystem.CurrentUser != null)
            {
                PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;

                IObjectSpace os = Application.CreateObjectSpace();
                clientEmpl = os.FindObject<ClientEmployee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                if (clientEmpl != null)
                {
                    Frame.GetController<ContainerPhotoFromContController>().ContainerPhotoFromContAction.Active.SetItemValue("myReason", false);
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// Открыть окно акта фотографирования контейнера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContainerPhotoFromContAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            Container container = null;
            container = os.FindObject<Container>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)container.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            ContainerPhotoAct containerPhotoAct = connect.CreateObject<ContainerPhotoAct>();
            containerPhotoAct.ActDate = DateTime.Now.Date;
            containerPhotoAct.Container = container;
            containerPhotoAct.ServiceCount = 1;
            containerPhotoAct.Save();

            // открываем окно акта 
            DetailView dv = Application.CreateDetailView(os, containerPhotoAct);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.ShowViewParameters.CreatedView = dv;
            View.Close();
        }


        private void SaveContPhotoActAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            var x_id = ((BaseObject)e.CurrentObject).Oid;
            ContainerPhotoAct containerPhotoAct = null;
            containerPhotoAct = os.FindObject<ContainerPhotoAct>(new BinaryOperator("Oid", x_id));

            containerPhotoAct.AddPhotoServiceDone();
            View.Close();

        }
    }
}
