﻿namespace TerminalSystem2.Module.Controllers.Containers
{
    partial class ContainerToChangeController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TakeOnEditContainerAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TakeOnEditContainerAction
            // 
            this.TakeOnEditContainerAction.Caption = "Взять на редактирование";
            this.TakeOnEditContainerAction.ConfirmationMessage = null;
            this.TakeOnEditContainerAction.Id = "TakeOnEditContainerAction";
            this.TakeOnEditContainerAction.ToolTip = null;
            this.TakeOnEditContainerAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TakeOnEditContainerAction_Execute);
            // 
            // ContainerToChangeController
            // 
            this.Actions.Add(this.TakeOnEditContainerAction);
            this.TargetObjectType = typeof(TerminalSystem2.Containers.Container);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TakeOnEditContainerAction;
    }
}
