﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Spreadsheet;
using System.IO;
using DevExpress.Xpo;
using System.Drawing;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;
using TerminalSystem2.Containers;
using TerminalSystem2.Clients;
using TerminalSystem2.BaseClasses;
using System.Threading;
using System.Windows.Forms;
using System.Reflection;
using TerminalSystem2.Tarification;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

namespace TerminalSystem2.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreateReportsFromStockController : ViewController
    {
        public CreateReportsFromStockController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            Frame.GetController<CreateReportsFromStockController>().CreateXLSReportFromStockByMonthAction.Active.SetItemValue("myReason", false);
            Frame.GetController<CreateReportsFromStockController>().CreateXLSReportFromStockByDayAction.Active.SetItemValue("myReason", false);
            ClientEmployee clientEmpl = null;
            if (SecuritySystem.CurrentUser != null)
            {
                PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;

                IObjectSpace os = Application.CreateObjectSpace();
                clientEmpl = os.FindObject<ClientEmployee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                if (clientEmpl != null)
                {
                    
                    Frame.GetController<CreateReportsFromStockController>().XLSReportByDayParamAction.Active.SetItemValue("myReason", false);
                    Frame.GetController<CreateReportsFromStockController>().CreateXLSReportFromStockByDayAction.Active.SetItemValue("myReason", false);
                    Frame.GetController<CreateReportsFromStockController>().xmlReportFromStockByMonthPopupAction.Active.SetItemValue("myReason", false);
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region Не используется
        //private void ContainerTrafficReportAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        //{
        //    View.ObjectSpace.CommitChanges();
        //    IObjectSpace os = Application.CreateObjectSpace(); // Create IObjectSpace or use the existing one, e.g. View.ObjectSpace, if it is suitable for your scenario.
        //    var x_id = ((BaseObject)e.CurrentObject).Oid;


        //    ContainerTraffic сontainerTraffic;
        //    сontainerTraffic = os.FindObject<ContainerTraffic>(new BinaryOperator("Oid", x_id));
        //    сontainerTraffic.Save();

        //    UnitOfWork unitOfWork = (UnitOfWork)сontainerTraffic.Session;
        //    CreateXlsReportByContainerTraffic(unitOfWork, сontainerTraffic);
        //}

        // Создание файла отчета по движению в стоке
        //public string CreateXlsReportByContainerHistory(UnitOfWork unitOfWork, ContainerHistory сontainerTraffic)
        //{
        //    //IObjectSpace os = Application.CreateObjectSpace(); // Create IObjectSpace 
        //    // Получаем объект - движение контейнера
        //    //ContainerTraffic сontainerTraffic;
        //    //сontainerTraffic = os.FindObject<ContainerTraffic>(new BinaryOperator("Oid", containerTraffic_id));
        //    //UnitOfWork unitOfWork = (UnitOfWork)сontainerTraffic.Session;
        //    Connect connect = Connect.FromUnitOfWork(unitOfWork);
        //    //ContainerTraffic сontainerTraffic;
        //    //сontainerTraffic = connect.FindFirstObject<ContainerTraffic>(mc=> mc.Oid == new BinaryOperator("Oid", containerTraffic_id));

        //    string res = "";



        //    if (сontainerTraffic.Stock != null && сontainerTraffic.Stock.Client != null && сontainerTraffic.Container != null)
        //    {
        //        Stock stock = сontainerTraffic.Stock;
        //        Client client = сontainerTraffic.Stock.Client;
        //        Container container = сontainerTraffic.Container;
        //        Workbook workbook = new Workbook();
        //        // Access the first worksheet in the workbook.
        //        Worksheet worksheet = workbook.Worksheets[0];

        //        // вариант отчета по прибытию/убытию контейнера
        //        if (сontainerTraffic.Stock.Client.TrafficRepotKind == TerminalSystem2.Enums.ETrafficRepotKind.ПоДвижению)
        //        {
        //            // Access the "С2" cell in the worksheet.
        //            Cell cell = worksheet.Cells["A2"];

        //            res = "Терминал";
        //            cell.Value = "Терминал";
        //            try
        //            {
        //                res = res + " \"" + container.Terminal.Name + "\"";
        //            }
        //            catch { }
        //            try
        //            {
        //                res = res + " Сток \"" + stock.Name + "\"";
        //            }
        //            catch { }
        //            cell.Value = res;
        //            worksheet.MergeCells(worksheet.Range["A2:G2"]);

        //            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
        //            cell.Font.FontStyle = SpreadsheetFontStyle.Bold;

        //            cell = worksheet.Cells["A3"];
        //            cell.Value = DateTime.Now.ToShortDateString();
        //            cell.Font.FontStyle = SpreadsheetFontStyle.Bold;

        //            cell = worksheet.Cells["A4"];
        //            cell.Value = "прибытие или отправка контейнеров  /  arrival оr send containers";
        //            cell.Font.Bold = true;
        //            worksheet.MergeCells(worksheet.Range["A4:G4"]);
        //            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;


        //            cell = worksheet.Cells["A6"];
        //            cell.Value = "число / date";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["B6"];
        //            cell.Value = "время / time";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["C6"];
        //            cell.Value = "тип / type";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["D6"];
        //            cell.Value = "№ контейнера / № container ";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["E6"];
        //            cell.Value = "водитель / driver";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["F6"];
        //            cell.Value = "№ а/м";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["G6"];
        //            cell.Value = "Примечание / Note";
        //            cell.Font.Bold = true;

        //            try
        //            {
        //                cell = worksheet.Cells["A7"];
        //                cell.Value = сontainerTraffic.Date.ToShortDateString();
        //            }
        //            catch { }
        //            try
        //            {
        //                cell = worksheet.Cells["B7"];
        //                cell.Value = сontainerTraffic.Time;
        //            }
        //            catch { }
        //            try
        //            {
        //                cell = worksheet.Cells["C7"];
        //                cell.Value = container.ContainerType.Name;
        //            }
        //            catch { }
        //            try
        //            {
        //                cell = worksheet.Cells["D7"];
        //                cell.Value = container.ContainerNumber;
        //            }
        //            catch { }
        //            try
        //            {
        //                cell = worksheet.Cells["E7"];
        //                cell.Value = сontainerTraffic.DriverName;
        //            }
        //            catch { }
        //            try
        //            {
        //                cell = worksheet.Cells["F7"];
        //                cell.Value = сontainerTraffic.TransportNumber;
        //            }
        //            catch { }
        //            try
        //            {
        //                cell = worksheet.Cells["G7"];
        //                switch (сontainerTraffic.ContainerHistoryKind)
        //                {
        //                    case Enums.EContainerHistoryKind.Размещение:
        //                        cell.Value = "Прибыл / Arrived";
        //                        break;
        //                    case Enums.EContainerHistoryKind.Выдача:
        //                        cell.Value = "Убыл / Departed";
        //                        break;
        //                    //case Enums.EContainerHistoryKind.Ремонт:
        //                    //    cell.Value = "Ремонт / Arrived";
        //                    //    break;
        //                    //case Enums.EContainerHistoryKind.Выдача:
        //                    //    cell.Value = "Убыл / Departed";
        //                    //    break;
        //                    default:
        //                        cell.Value = "Прибыл / Arrived";
        //                        break;
        //                }
        //            }
        //            catch { }

        //            // Access the range of cells to be formatted.
        //            Range range = worksheet.Range["A6:G7"];

        //            // Begin updating of the range formatting. 
        //            Formatting rangeFormatting = range.BeginUpdateFormatting();

        //            // Specify font settings (font name, color, size and style).
        //            rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
        //            rangeFormatting.Alignment.ShrinkToFit = true;
        //            // Specify text alignment in cells.
        //            rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
        //            rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
        //            range.AutoFitColumns();
        //            // End updating of the range formatting.
        //            range.EndUpdateFormatting(rangeFormatting);
        //        }
        //        else
        //        {
        //            // Access the "С2" cell in the worksheet.
        //            Cell cell = worksheet.Cells["A2"];

        //            res = "Терминал";
        //            cell.Value = "Терминал";
        //            try
        //            {
        //                res = res + " \"" + container.Terminal.Name + "\"";
        //            }
        //            catch { }
        //            try
        //            {
        //                res = res + " Сток \"" + stock.Name + "\"";
        //            }
        //            catch { }
        //            cell.Value = res;
        //            worksheet.MergeCells(worksheet.Range["A2:N2"]);
        //            cell.Font.Bold = true;
        //            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

        //            cell = worksheet.Cells["A3"];
        //            cell.Value = DateTime.Now.ToShortDateString();
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["A4"];
        //            cell.Value = "приход контейнеров";
        //            cell.Font.Bold = true;
        //            worksheet.MergeCells(worksheet.Range["A4:G4"]);
        //            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

        //            cell = worksheet.Cells["I4"];
        //            cell.Value = "уход контейнеров";
        //            cell.Font.Bold = true;
        //            worksheet.MergeCells(worksheet.Range["I4:N4"]);
        //            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

        //            cell = worksheet.Cells["A6"];
        //            cell.Value = "число / date";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["I6"];
        //            cell.Value = "число / date";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["B6"];
        //            cell.Value = "время / time";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["J6"];
        //            cell.Value = "время / time";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["C6"];
        //            cell.Value = "тип / type";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["K6"];
        //            cell.Value = "тип / type";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["D6"];
        //            cell.Value = "№ контейнера / № container ";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["L6"];
        //            cell.Value = "№ контейнера / № container ";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["E6"];
        //            cell.Value = "водитель / driver";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["M6"];
        //            cell.Value = "водитель / driver";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["F6"];
        //            cell.Value = "№ а/м";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["N6"];
        //            cell.Value = "№ а/м";
        //            cell.Font.Bold = true;

        //            //cell = worksheet.Cells["G6"];
        //            //cell.Value = "Примечание / Note";
        //            //cell.Font.Bold = true;
        //            // перебираем все движения с начала месяца и в зависимости от типа (приход/уход создаем запись в таблице)
        //            ContainerHistory contrafficByMonth;
        //            int j = 0;
        //            string ldate = "", ltime = "", ltype = "", lnumber = "", ldriver = "", ltransport = "";
        //            var contrafficsArrived = connect.FindObjects<ContainerHistory>(mc => mc.Stock == stock && mc.Date.Month == DateTime.Now.Month &&
        //                mc.ContainerHistoryKind == Enums.EContainerHistoryKind.Размещение);
        //            for (int i = 0; i < contrafficsArrived.ToList<ContainerHistory>().Count; i++)
        //            {
        //                contrafficByMonth = contrafficsArrived.ToList<ContainerHistory>()[i];
        //                ldate = "A";
        //                ltime = "B";
        //                ltype = "C";
        //                lnumber = "D";
        //                ldriver = "E";
        //                ltransport = "F";

        //                try
        //                {
        //                    ldate += (i + 7).ToString();
        //                    cell = worksheet.Cells[ldate];
        //                    cell.Value = contrafficByMonth.Date.ToShortDateString();
        //                }
        //                catch { }
        //                try
        //                {
        //                    ltime += (i + 7).ToString();
        //                    cell = worksheet.Cells[ltime];
        //                    cell.Value = contrafficByMonth.Time;
        //                }
        //                catch { }
        //                try
        //                {
        //                    ltype += (i + 7).ToString();
        //                    cell = worksheet.Cells[ltype];
        //                    cell.Value = contrafficByMonth.Container.ContainerType.Name;
        //                }
        //                catch { }
        //                try
        //                {
        //                    lnumber += (i + 7).ToString();
        //                    cell = worksheet.Cells[lnumber];
        //                    cell.Value = contrafficByMonth.Container.ContainerNumber;
        //                }
        //                catch { }
        //                try
        //                {
        //                    ldriver += (i + 7).ToString();
        //                    cell = worksheet.Cells[ldriver];
        //                    cell.Value = contrafficByMonth.DriverName;
        //                }
        //                catch { }
        //                try
        //                {
        //                    ltransport += (i + 7).ToString();
        //                    cell = worksheet.Cells[ltransport];
        //                    cell.Value = contrafficByMonth.TransportNumber;
        //                }
        //                catch { }
        //                j = i;
        //            }
        //            var contrafficsDeparted = connect.FindObjects<ContainerHistory>(mc => mc.Stock == stock && mc.Date.Month == DateTime.Now.Month &&
        //                mc.ContainerHistoryKind == Enums.EContainerHistoryKind.Выдача);
        //            for (int n = 0; n < contrafficsDeparted.ToList<ContainerHistory>().Count; n++)
        //            {
        //                contrafficByMonth = contrafficsDeparted.ToList<ContainerHistory>()[n];

        //                ldate = "I";
        //                ltime = "J";
        //                ltype = "K";
        //                lnumber = "L";
        //                ldriver = "M";
        //                ltransport = "N";
        //                try
        //                {
        //                    ldate += (n + 7).ToString();
        //                    cell = worksheet.Cells[ldate];
        //                    cell.Value = contrafficByMonth.Date.ToShortDateString();
        //                }
        //                catch { }
        //                try
        //                {
        //                    ltime += (n + 7).ToString();
        //                    cell = worksheet.Cells[ltime];
        //                    cell.Value = contrafficByMonth.Time;
        //                }
        //                catch { }
        //                try
        //                {
        //                    ltype += (n + 7).ToString();
        //                    cell = worksheet.Cells[ltype];
        //                    cell.Value = contrafficByMonth.Container.ContainerType.Name;
        //                }
        //                catch { }
        //                try
        //                {
        //                    lnumber += (n + 7).ToString();
        //                    cell = worksheet.Cells[lnumber];
        //                    cell.Value = contrafficByMonth.Container.ContainerNumber;
        //                }
        //                catch { }
        //                try
        //                {
        //                    ldriver += (n + 7).ToString();
        //                    cell = worksheet.Cells[ldriver];
        //                    cell.Value = contrafficByMonth.DriverName;
        //                }
        //                catch { }
        //                try
        //                {
        //                    ltransport += (n + 7).ToString();
        //                    cell = worksheet.Cells[ltransport];
        //                    cell.Value = contrafficByMonth.TransportNumber;
        //                }
        //                catch { }
        //                if (j < n)
        //                    j = n;
        //            }
        //            j += 7;
        //            // Форматирование ячеек по приходу/уходу
        //            // Access the range of cells to be formatted.
        //            Range range = worksheet.Range["A6:N" + j.ToString()];

        //            // Begin updating of the range formatting. 
        //            Formatting rangeFormatting = range.BeginUpdateFormatting();

        //            // Specify font settings (font name, color, size and style).
        //            rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
        //            rangeFormatting.Alignment.ShrinkToFit = true;
        //            // Specify text alignment in cells.
        //            rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
        //            rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
        //            range.AutoFitColumns();
        //            // End updating of the range formatting.
        //            range.EndUpdateFormatting(rangeFormatting);

        //            // Добавляем информацию - сколько каких видов контейнеров осталось в стоке
        //            j += 3;
        //            //var containersInStockTypes = connect.FindObjects<ContainerType>(mc=> mc);

        //            //var conteinersInStock = connect.FindObjects<Container>(mc =>  mc.Stock == stock).GroupBy<ContainerType, "Name">;

        //            //XPQuery<Container> containers = Session.DefaultSession.Query<Container>();
        //            var containers = connect.FindObjects<Container>(mc => mc.Stock == stock && mc.onTerminal == true);
        //            // Select with Group By 
        //            var list = from c in containers
        //                           //where c.Stock == stock
        //                       group c by c.ContainerType into cc
        //                       where cc.Count() >= 1
        //                       select new { Title = cc.Key, Count = cc.Count() };
        //            foreach (var item in list)
        //            {
        //                j++;
        //                cell = worksheet.Cells["E" + j.ToString()];
        //                cell.Value = item.Title.Name;
        //                cell.Font.Bold = true;
        //                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
        //                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
        //                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
        //                //cell.AutoFitColumns();

        //                cell = worksheet.Cells["F" + j.ToString()];
        //                cell.Value = item.Count.ToString();
        //                cell.Font.Bold = true;
        //                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
        //                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
        //                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
        //                //cell.AutoFitColumns();
        //            }

        //            // добавляем информацию по стоку
        //            j += 1;
        //            cell = worksheet.Cells["C" + j.ToString()];
        //            worksheet.MergeCells(worksheet.Range["C" + j.ToString() + ":E" + j.ToString()]);
        //            cell.Value = "сток на терминале";
        //            cell.Font.Bold = true;
        //            //cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
        //            cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
        //            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
        //            //cell.AutoFitColumns();

        //            j += 1;
        //            int d = j;
        //            cell = worksheet.Cells["C" + j.ToString()];
        //            cell.Value = "Тип";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["D" + j.ToString()];
        //            cell.Value = "номер";
        //            cell.Font.Bold = true;

        //            cell = worksheet.Cells["E" + j.ToString()];
        //            cell.Value = "состояние";
        //            cell.Font.Bold = true;

        //            foreach (Container cont in containers)
        //            {
        //                j++;
        //                cell = worksheet.Cells["C" + j.ToString()];
        //                cell.Value = cont.ContainerType.Name;

        //                cell = worksheet.Cells["D" + j.ToString()];
        //                cell.Value = cont.ContainerNumber;

        //                cell = worksheet.Cells["E" + j.ToString()];
        //                string state = "не указано";
        //                if (cont.State != null)
        //                    if (cont.State.Name != null && cont.State.Name != String.Empty)
        //                        state = cont.State.Name;
        //                cell.Value = state;
        //            }

        //            // Форматирование ячеек по стоку
        //            range = worksheet.Range["C" + d.ToString() + ":E" + j.ToString()];
        //            rangeFormatting = range.BeginUpdateFormatting();

        //            // Specify font settings (font name, color, size and style).
        //            rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
        //            //rangeFormatting.Alignment.ShrinkToFit = true;
        //            // Specify text alignment in cells.
        //            rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
        //            rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
        //            //range.AutoFitColumns();
        //            // End updating of the range formatting.
        //            range.EndUpdateFormatting(rangeFormatting);
        //        }
        //        Column column = worksheet.Columns["G"];
        //        column.ColumnWidth = 10;
        //        column = worksheet.Columns["H"];
        //        column.ColumnWidth = 10;

        //        string path = Path.GetTempPath() + @"\Отчет_по_стоку_" + stock.Name + "_" +
        //            DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".xlsx";

        //        workbook.SaveDocument(path, DocumentFormat.OpenXml);
        //        res = path;
        //        //try
        //        //{
        //        //    System.Diagnostics.Process.Start(path);
        //        //}
        //        //catch { }
        //    }
        //    return res;
        //}
        #endregion

        /// <summary>
        /// Создание файла отчета по движению в стоке за один день
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="stock"></param>
        /// <returns>путь к файлу отчета</returns>
        public string CreateXlsReportByStock(Connect connect, Stock stock, DateTime date)
        {
            //IObjectSpace os = Application.CreateObjectSpace(); // Create IObjectSpace 
            // Получаем объект - движение контейнера
            //ContainerTraffic сontainerTraffic;
            //сontainerTraffic = os.FindObject<ContainerTraffic>(new BinaryOperator("Oid", containerTraffic_id));
            //UnitOfWork unitOfWork = (UnitOfWork)сontainerTraffic.Session;
            //Connect connect = Connect.FromUnitOfWork(unitOfWork);

            string res = "";

            if (stock.Client != null && stock.Terminal != null)
            {
                Client client = stock.Client;
                Workbook workbook = new Workbook();
                // Access the first worksheet in the workbook.
                Worksheet worksheet = workbook.Worksheets[0];

                // Access the "С2" cell in the worksheet.
                Cell cell = worksheet.Cells["A2"];

                res = "Терминал";
                cell.Value = "Терминал";
                try
                {
                    res = res + " \"" + stock.Terminal.Name + "\"";
                }
                catch { }
                try
                {
                    res = res + " Сток \"" + stock.Name + "\"";
                }
                catch { }
                cell.Value = res;
                worksheet.MergeCells(worksheet.Range["A2:N2"]);
                cell.Font.Bold = true;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = worksheet.Cells["A3"];
                cell.Value = date.ToString("dd.MM.yyyy");
                cell.Font.Bold = true;

                cell = worksheet.Cells["A4"];
                cell.Value = "приход контейнеров";
                cell.Font.Bold = true;
                worksheet.MergeCells(worksheet.Range["A4:G4"]);
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = worksheet.Cells["I4"];
                cell.Value = "уход контейнеров";
                cell.Font.Bold = true;
                worksheet.MergeCells(worksheet.Range["I4:N4"]);
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = worksheet.Cells["A6"];
                cell.Value = "число / date";
                cell.Font.Bold = true;

                cell = worksheet.Cells["K6"];
                cell.Value = "число / date";
                cell.Font.Bold = true;

                cell = worksheet.Cells["B6"];
                cell.Value = "время / time";
                cell.Font.Bold = true;

                cell = worksheet.Cells["L6"];
                cell.Value = "время / time";
                cell.Font.Bold = true;

                cell = worksheet.Cells["C6"];
                cell.Value = "тип / type";
                cell.Font.Bold = true;

                cell = worksheet.Cells["M6"];
                cell.Value = "тип / type";
                cell.Font.Bold = true;

                cell = worksheet.Cells["D6"];
                cell.Value = "№ контейнера / № container ";
                cell.Font.Bold = true;

                cell = worksheet.Cells["N6"];
                cell.Value = "№ контейнера / № container ";
                cell.Font.Bold = true;

                cell = worksheet.Cells["E6"];
                cell.Value = "водитель / driver";
                cell.Font.Bold = true;

                cell = worksheet.Cells["O6"];
                cell.Value = "водитель / driver";
                cell.Font.Bold = true;

                cell = worksheet.Cells["F6"];
                cell.Value = "№ а/м";
                cell.Font.Bold = true;

                cell = worksheet.Cells["P6"];
                cell.Value = "№ а/м";
                cell.Font.Bold = true;

                cell = worksheet.Cells["G6"];
                cell.Value = "Номер пломбы";
                cell.Font.Bold = true;

                cell = worksheet.Cells["Q6"];
                cell.Value = "Номер пломбы";
                cell.Font.Bold = true;

                cell = worksheet.Cells["H6"];
                cell.Value = "Характер груза";
                cell.Font.Bold = true;

                cell = worksheet.Cells["R6"];
                cell.Value = "Характер груза";
                cell.Font.Bold = true;

                cell = worksheet.Cells["I6"];
                cell.Value = "Примечание / Note";
                cell.Font.Bold = true;

                cell = worksheet.Cells["S6"];
                cell.Value = "Примечание / Note";
                cell.Font.Bold = true;
                // перебираем все движения за сегодняшний день и в зависимости от типа (приход/уход создаем запись в таблице)
                ContainerHistory contrafficByMonth;
                int j = 0;
                string ldate = "", ltime = "", ltype = "", lnumber = "", ldriver = "", ltransport = "", lsealNumber = "", lcargoType = "", lnote = "";
                int co = connect.FindObjects<ContainerHistory>(x => x.Stock == stock).Count();
                int co1 = connect.FindObjects<ContainerHistory>(x => x.Stock == stock && x.Date == date).Count();
                int co2 = connect.FindObjects<ContainerHistory>(x => x.Stock == stock && x.Date == date &&
                    x.ContainerHistoryKind == TerminalSystem2.Enums.EContainerHistoryKind.Размещение).Count();

                var contrafficsArrived = connect.FindObjects<ContainerHistory>(mc => mc.Stock == stock && mc.Date == date &&
                    mc.ContainerHistoryKind == TerminalSystem2.Enums.EContainerHistoryKind.Размещение);
                for (int i = 0; i < contrafficsArrived.ToList<ContainerHistory>().Count; i++)
                {
                    contrafficByMonth = contrafficsArrived.ToList<ContainerHistory>()[i];
                    ldate = "A";
                    ltime = "B";
                    ltype = "C";
                    lnumber = "D";
                    ldriver = "E";
                    ltransport = "F";
                    lsealNumber = "G";
                    lcargoType = "H";
                    lnote = "I";
                    try
                    {
                        ldate += (i + 7).ToString();
                        cell = worksheet.Cells[ldate];
                        cell.Value = contrafficByMonth.Date.ToString("dd.MM.yyyy");
                    }
                    catch { }
                    try
                    {
                        ltime += (i + 7).ToString();
                        cell = worksheet.Cells[ltime];
                        cell.Value = contrafficByMonth.Time;
                    }
                    catch { }
                    try
                    {
                        ltype += (i + 7).ToString();
                        cell = worksheet.Cells[ltype];
                        cell.Value = contrafficByMonth.Container.ContainerType.Name;
                    }
                    catch { }
                    try
                    {
                        lnumber += (i + 7).ToString();
                        cell = worksheet.Cells[lnumber];
                        cell.Value = contrafficByMonth.Container.ContainerNumber;
                    }
                    catch { }
                    try
                    {
                        ldriver += (i + 7).ToString();
                        cell = worksheet.Cells[ldriver];
                        cell.Value = contrafficByMonth.DriverName;
                    }
                    catch { }
                    try
                    {
                        ltransport += (i + 7).ToString();
                        cell = worksheet.Cells[ltransport];
                        cell.Value = contrafficByMonth.TransportNumber;
                    }
                    catch { }
                    try
                    {
                        lsealNumber += (i + 7).ToString();
                        cell = worksheet.Cells[lsealNumber];
                        cell.Value = contrafficByMonth.SealNumber;
                    }
                    catch { }
                    try
                    {
                        lcargoType += (i + 7).ToString();
                        cell = worksheet.Cells[lcargoType];
                        cell.Value = contrafficByMonth.CargoType;
                    }
                    catch { }
                    try
                    {
                        lnote += (i + 7).ToString();
                        cell = worksheet.Cells[lnote];
                        cell.Value = contrafficByMonth.Notes;
                    }
                    catch { }
                    j = i;
                }
                var contrafficsDeparted = connect.FindObjects<ContainerHistory>(mc => mc.Stock == stock && mc.Date == date &&
                    mc.ContainerHistoryKind == TerminalSystem2.Enums.EContainerHistoryKind.Выдача);
                for (int n = 0; n < contrafficsDeparted.ToList<ContainerHistory>().Count; n++)
                {
                    contrafficByMonth = contrafficsDeparted.ToList<ContainerHistory>()[n];

                    ldate = "K";
                    ltime = "L";
                    ltype = "M";
                    lnumber = "N";
                    ldriver = "O";
                    ltransport = "P";
                    lsealNumber = "Q";
                    lcargoType = "R";
                    lnote = "S";
                    try
                    {
                        ldate += (n + 7).ToString();
                        cell = worksheet.Cells[ldate];
                        cell.Value = contrafficByMonth.Date.ToString("dd.MM.yyyy");
                    }
                    catch { }
                    try
                    {
                        ltime += (n + 7).ToString();
                        cell = worksheet.Cells[ltime];
                        cell.Value = contrafficByMonth.Time;
                    }
                    catch { }
                    try
                    {
                        ltype += (n + 7).ToString();
                        cell = worksheet.Cells[ltype];
                        cell.Value = contrafficByMonth.Container.ContainerType.Name;
                    }
                    catch { }
                    try
                    {
                        lnumber += (n + 7).ToString();
                        cell = worksheet.Cells[lnumber];
                        cell.Value = contrafficByMonth.Container.ContainerNumber;
                    }
                    catch { }
                    try
                    {
                        ldriver += (n + 7).ToString();
                        cell = worksheet.Cells[ldriver];
                        cell.Value = contrafficByMonth.DriverName;
                    }
                    catch { }
                    try
                    {
                        ltransport += (n + 7).ToString();
                        cell = worksheet.Cells[ltransport];
                        cell.Value = contrafficByMonth.TransportNumber;
                    }
                    catch { }
                    try
                    {
                        lsealNumber += (n + 7).ToString();
                        cell = worksheet.Cells[lsealNumber];
                        cell.Value = contrafficByMonth.SealNumber;
                    }
                    catch { }
                    try
                    {
                        lcargoType += (n + 7).ToString();
                        cell = worksheet.Cells[lcargoType];
                        cell.Value = contrafficByMonth.CargoType;
                    }
                    catch { }
                    try
                    {
                        lnote += (n + 7).ToString();
                        cell = worksheet.Cells[lnote];
                        cell.Value = contrafficByMonth.Notes;
                    }
                    catch { }
                    if (j < n)
                        j = n;
                }
                j += 7;
                // Форматирование ячеек по приходу/уходу
                // Access the range of cells to be formatted.
                Range range = worksheet.Range["A6:S" + j.ToString()];

                // Begin updating of the range formatting. 
                Formatting rangeFormatting = range.BeginUpdateFormatting();

                // Specify font settings (font name, color, size and style).
                rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                rangeFormatting.Alignment.ShrinkToFit = true;
                // Specify text alignment in cells.
                rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                range.AutoFitColumns();
                // End updating of the range formatting.
                range.EndUpdateFormatting(rangeFormatting);

                // Добавляем информацию - сколько каких видов контейнеров осталось в стоке
                j += 3;
                //var containersInStockTypes = connect.FindObjects<ContainerType>(mc=> mc);

                //var conteinersInStock = connect.FindObjects<Container>(mc =>  mc.Stock == stock).GroupBy<ContainerType, "Name">;

                //XPQuery<Container> containers = Session.DefaultSession.Query<Container>();
                var containers = connect.FindObjects<Container>(mc => mc.Stock == stock && mc.onTerminal == true);
                // Select with Group By 
                var list = from c in containers
                               //where c.Stock == stock
                           group c by c.ContainerType into cc
                           where cc.Count() >= 1
                           select new { Title = cc.Key, Count = cc.Count() };
                foreach (var item in list)
                {
                    j++;
                    cell = worksheet.Cells["E" + j.ToString()];
                    cell.Value = item.Title.Name;
                    cell.Font.Bold = true;
                    cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    //cell.AutoFitColumns();

                    cell = worksheet.Cells["F" + j.ToString()];
                    cell.Value = item.Count.ToString();
                    cell.Font.Bold = true;
                    cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    //cell.AutoFitColumns();
                }

                // добавляем информацию по стоку
                j += 1;
                cell = worksheet.Cells["C" + j.ToString()];
                worksheet.MergeCells(worksheet.Range["C" + j.ToString() + ":E" + j.ToString()]);
                cell.Value = "сток на терминале";
                cell.Font.Bold = true;
                //cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                //cell.AutoFitColumns();

                j += 1;
                int d = j;
                cell = worksheet.Cells["C" + j.ToString()];
                cell.Value = "Тип";
                cell.Font.Bold = true;

                cell = worksheet.Cells["D" + j.ToString()];
                cell.Value = "номер";
                cell.Font.Bold = true;

                cell = worksheet.Cells["E" + j.ToString()];
                cell.Value = "состояние";
                cell.Font.Bold = true;

                cell = worksheet.Cells["F" + j.ToString()];
                cell.Value = "номер пломбы";
                cell.Font.Bold = true;

                cell = worksheet.Cells["G" + j.ToString()];
                cell.Value = "характер груза";
                cell.Font.Bold = true;

                foreach (Container cont in containers)
                {
                    j++;

                    cell = worksheet.Cells["B" + j.ToString()];
                    //ContainerHistory h = connect.FindFirstObject<ContainerHistory>(mc=> mc.Container == cont )
                    string dd = "";
                    try
                    {
                        dd = cont.ContainerHistorys.OrderByDescending(mc => mc.Date).First<ContainerHistory>().Date.ToString("dd.MM.yyyy");
                    }
                    catch { }
                    cell.Value = dd;

                    cell = worksheet.Cells["C" + j.ToString()];
                    cell.Value = cont.ContainerType.Name;

                    cell = worksheet.Cells["D" + j.ToString()];
                    cell.Value = cont.ContainerNumber;

                    cell = worksheet.Cells["E" + j.ToString()];
                    string state = "не указано";
                    if (cont.State != null)
                        if (cont.State.Name != null && cont.State.Name != String.Empty)
                            state = cont.State.Name;
                    cell.Value = state;

                    try
                    {
                        cell = worksheet.Cells["F" + j.ToString()];
                        string sealNumber = "не указано";
                        if (cont.SealNumber != null && cont.SealNumber != "")
                            sealNumber = cont.SealNumber;
                        cell.Value = sealNumber;
                    }
                    catch { }
                    try
                    {
                        cell = worksheet.Cells["G" + j.ToString()];
                        string cargoType = "не указано";
                        if (cont.CargoType != null && cont.CargoType != "")
                            cargoType = cont.CargoType;
                        cell.Value = cargoType;
                    }
                    catch { }
                }

                // Форматирование ячеек по стоку
                range = worksheet.Range["B" + d.ToString() + ":G" + j.ToString()];
                rangeFormatting = range.BeginUpdateFormatting();

                // Specify font settings (font name, color, size and style).
                rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                //rangeFormatting.Alignment.ShrinkToFit = true;
                // Specify text alignment in cells.
                rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                rangeFormatting.Alignment.WrapText = true;
                range.AutoFitRows();
                //range.AutoFitColumns();

                // End updating of the range formatting.

                range.EndUpdateFormatting(rangeFormatting);

                //try
                //{

                //// Форматирование ячеек по всему файлу
                //range = worksheet.Range["A7:O" + j.ToString()];
                //rangeFormatting = range.BeginUpdateFormatting();

                //// Specify font settings (font name, color, size and style).
                //rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                //rangeFormatting.Alignment.ShrinkToFit = true;
                //// Specify text alignment in cells.
                //rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                //rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                //range.AutoFitColumns();
                //// End updating of the range formatting.
                //range.EndUpdateFormatting(rangeFormatting);
                //}
                //catch (Exception ex) { LogHelperClass.WriteErr(ex, "Форматирование ячеек по всему файлу"); }

                //Column column = worksheet.Columns["G"];
                //column.ColumnWidth = 10;
                Column column = worksheet.Columns["C"];
                column.ColumnWidth = 450;

                column = worksheet.Columns["E"];
                column.ColumnWidth = 500;

                column = worksheet.Columns["F"];
                column.ColumnWidth = 400;

                column = worksheet.Columns["G"];
                column.ColumnWidth = 500;

                column = worksheet.Columns["H"];
                column.ColumnWidth = 500;

                column = worksheet.Columns["I"];
                column.ColumnWidth = 500;

                column = worksheet.Columns["j"];
                column.ColumnWidth = 50;

                column = worksheet.Columns["M"];
                column.ColumnWidth = 450;

                column = worksheet.Columns["O"];
                column.ColumnWidth = 500;

                column = worksheet.Columns["P"];
                column.ColumnWidth = 400;

                column = worksheet.Columns["Q"];
                column.ColumnWidth = 500;

                column = worksheet.Columns["R"];
                column.ColumnWidth = 500;

                column = worksheet.Columns["S"];
                column.ColumnWidth = 500;

                string path = Path.GetTempPath() + //AppDomain.CurrentDomain.BaseDirectory + "/App_Data/UploadTemp/" + //Path.GetTempPath() + 
                    @"\Отчет_по_стоку_" + stock.Name.Replace("\"", "") + "_" +
                    date.ToString("dd.MM.yyyy H:mm:ss").Replace(".", "_").Replace(" ", "_").Replace(":", "_").Replace("\\", "_").Replace("/", "_") + ".xlsx";

                workbook.SaveDocument(path, DocumentFormat.OpenXml);
                res = path;




                //try
                //{
                //    System.Diagnostics.Process.Start(path);
                //}
                //catch { }
            }
            return res;
            #region //
            //if (stock.Client != null && stock.Terminal != null)
            //{
            //    Client client = stock.Client;
            //    Workbook workbook = new Workbook();
            //    // Access the first worksheet in the workbook.
            //    Worksheet worksheet = workbook.Worksheets[0];

            //    // Access the "С2" cell in the worksheet.
            //    Cell cell = worksheet.Cells["A2"];

            //    res = "Терминал";
            //    cell.Value = "Терминал";
            //    try
            //    {
            //        res = res + " \"" + stock.Terminal.Name + "\"";
            //    }
            //    catch { }
            //    try
            //    {
            //        res = res + " Сток \"" + stock.Name + "\"";
            //    }
            //    catch { }
            //    cell.Value = res;
            //    worksheet.MergeCells(worksheet.Range["A2:N2"]);
            //    cell.Font.Bold = true;
            //    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

            //    cell = worksheet.Cells["A3"];
            //    cell.Value = DateTime.Now.ToShortDateString();
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["A4"];
            //    cell.Value = "приход контейнеров";
            //    cell.Font.Bold = true;
            //    worksheet.MergeCells(worksheet.Range["A4:G4"]);
            //    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

            //    cell = worksheet.Cells["I4"];
            //    cell.Value = "уход контейнеров";
            //    cell.Font.Bold = true;
            //    worksheet.MergeCells(worksheet.Range["I4:N4"]);
            //    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

            //    cell = worksheet.Cells["A6"];
            //    cell.Value = "число / date";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["I6"];
            //    cell.Value = "число / date";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["B6"];
            //    cell.Value = "время / time";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["J6"];
            //    cell.Value = "время / time";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["C6"];
            //    cell.Value = "тип / type";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["K6"];
            //    cell.Value = "тип / type";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["D6"];
            //    cell.Value = "№ контейнера / № container ";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["L6"];
            //    cell.Value = "№ контейнера / № container ";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["E6"];
            //    cell.Value = "водитель / driver";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["M6"];
            //    cell.Value = "водитель / driver";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["F6"];
            //    cell.Value = "№ а/м";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["N6"];
            //    cell.Value = "№ а/м";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["G6"];
            //    cell.Value = "Примечание / Note";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["O6"];
            //    cell.Value = "Примечание / Note";
            //    cell.Font.Bold = true;
            //    // перебираем все движения за сегодняшний день и в зависимости от типа (приход/уход создаем запись в таблице)
            //    ContainerHistory contrafficByMonth;
            //    int j = 0;
            //    string ldate = "", ltime = "", ltype = "", lnumber = "", ldriver = "", ltransport = "", lnote = "";
            //    var contrafficsArrived = connect.FindObjects<ContainerHistory>(mc => mc.Stock == stock && mc.Date == DateTime.Now.Date &&
            //        mc.ContainerHistoryKind == Enums.EContainerHistoryKind.Размещение);
            //    for (int i = 0; i < contrafficsArrived.ToList<ContainerHistory>().Count; i++)
            //    {
            //        contrafficByMonth = contrafficsArrived.ToList<ContainerHistory>()[i];
            //        ldate = "A";
            //        ltime = "B";
            //        ltype = "C";
            //        lnumber = "D";
            //        ldriver = "E";
            //        ltransport = "F";
            //        lnote = "G";
            //        try
            //        {
            //            ldate += (i + 7).ToString();
            //            cell = worksheet.Cells[ldate];
            //            cell.Value = contrafficByMonth.Date.ToShortDateString();
            //        }
            //        catch { }
            //        try
            //        {
            //            ltime += (i + 7).ToString();
            //            cell = worksheet.Cells[ltime];
            //            cell.Value = contrafficByMonth.Time;
            //        }
            //        catch { }
            //        try
            //        {
            //            ltype += (i + 7).ToString();
            //            cell = worksheet.Cells[ltype];
            //            cell.Value = contrafficByMonth.Container.ContainerType.Name;
            //        }
            //        catch { }
            //        try
            //        {
            //            lnumber += (i + 7).ToString();
            //            cell = worksheet.Cells[lnumber];
            //            cell.Value = contrafficByMonth.Container.ContainerNumber;
            //        }
            //        catch { }
            //        try
            //        {
            //            ldriver += (i + 7).ToString();
            //            cell = worksheet.Cells[ldriver];
            //            cell.Value = contrafficByMonth.DriverName;
            //        }
            //        catch { }
            //        try
            //        {
            //            ltransport += (i + 7).ToString();
            //            cell = worksheet.Cells[ltransport];
            //            cell.Value = contrafficByMonth.TransportNumber;
            //        }
            //        catch { }
            //        try
            //        {
            //            lnote += (i + 7).ToString();
            //            cell = worksheet.Cells[lnote];
            //            cell.Value = contrafficByMonth.Notes;
            //        }
            //        catch { }
            //        j = i;
            //    }
            //    var contrafficsDeparted = connect.FindObjects<ContainerHistory>(mc => mc.Stock == stock && mc.Date == DateTime.Now.Date &&
            //        mc.ContainerHistoryKind == Enums.EContainerHistoryKind.Выдача);
            //    for (int n = 0; n < contrafficsDeparted.ToList<ContainerHistory>().Count; n++)
            //    {
            //        contrafficByMonth = contrafficsDeparted.ToList<ContainerHistory>()[n];

            //        ldate = "I";
            //        ltime = "J";
            //        ltype = "K";
            //        lnumber = "L";
            //        ldriver = "M";
            //        ltransport = "N";
            //        lnote = "O";
            //        try
            //        {
            //            ldate += (n + 7).ToString();
            //            cell = worksheet.Cells[ldate];
            //            cell.Value = contrafficByMonth.Date.ToShortDateString();
            //        }
            //        catch { }
            //        try
            //        {
            //            ltime += (n + 7).ToString();
            //            cell = worksheet.Cells[ltime];
            //            cell.Value = contrafficByMonth.Time;
            //        }
            //        catch { }
            //        try
            //        {
            //            ltype += (n + 7).ToString();
            //            cell = worksheet.Cells[ltype];
            //            cell.Value = contrafficByMonth.Container.ContainerType.Name;
            //        }
            //        catch { }
            //        try
            //        {
            //            lnumber += (n + 7).ToString();
            //            cell = worksheet.Cells[lnumber];
            //            cell.Value = contrafficByMonth.Container.ContainerNumber;
            //        }
            //        catch { }
            //        try
            //        {
            //            ldriver += (n + 7).ToString();
            //            cell = worksheet.Cells[ldriver];
            //            cell.Value = contrafficByMonth.DriverName;
            //        }
            //        catch { }
            //        try
            //        {
            //            ltransport += (n + 7).ToString();
            //            cell = worksheet.Cells[ltransport];
            //            cell.Value = contrafficByMonth.TransportNumber;
            //        }
            //        catch { }
            //        try
            //        {
            //            lnote += (n + 7).ToString();
            //            cell = worksheet.Cells[lnote];
            //            cell.Value = contrafficByMonth.Notes;
            //        }
            //        catch { }
            //        if (j < n)
            //            j = n;
            //    }
            //    j += 7;
            //    // Форматирование ячеек по приходу/уходу
            //    // Access the range of cells to be formatted.
            //    Range range = worksheet.Range["A6:O" + j.ToString()];

            //    // Begin updating of the range formatting. 
            //    Formatting rangeFormatting = range.BeginUpdateFormatting();

            //    // Specify font settings (font name, color, size and style).
            //    rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
            //    rangeFormatting.Alignment.ShrinkToFit = true;
            //    // Specify text alignment in cells.
            //    rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            //    rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            //    range.AutoFitColumns();
            //    // End updating of the range formatting.
            //    range.EndUpdateFormatting(rangeFormatting);

            //    // Добавляем информацию - сколько каких видов контейнеров осталось в стоке
            //    j += 3;
            //    //var containersInStockTypes = connect.FindObjects<ContainerType>(mc=> mc);

            //    //var conteinersInStock = connect.FindObjects<Container>(mc =>  mc.Stock == stock).GroupBy<ContainerType, "Name">;

            //    //XPQuery<Container> containers = Session.DefaultSession.Query<Container>();
            //    var containers = connect.FindObjects<Container>(mc => mc.Stock == stock && mc.onTerminal == true);
            //    // Select with Group By 
            //    var list = from c in containers
            //                   //where c.Stock == stock
            //               group c by c.ContainerType into cc
            //               where cc.Count() >= 1
            //               select new { Title = cc.Key, Count = cc.Count() };
            //    foreach (var item in list)
            //    {
            //        j++;
            //        cell = worksheet.Cells["E" + j.ToString()];
            //        cell.Value = item.Title.Name;
            //        cell.Font.Bold = true;
            //        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
            //        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            //        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            //        //cell.AutoFitColumns();

            //        cell = worksheet.Cells["F" + j.ToString()];
            //        cell.Value = item.Count.ToString();
            //        cell.Font.Bold = true;
            //        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
            //        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            //        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            //        //cell.AutoFitColumns();
            //    }

            //    // добавляем информацию по стоку
            //    j += 1;
            //    cell = worksheet.Cells["C" + j.ToString()];
            //    worksheet.MergeCells(worksheet.Range["C" + j.ToString() + ":E" + j.ToString()]);
            //    cell.Value = "сток на терминале";
            //    cell.Font.Bold = true;
            //    //cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
            //    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            //    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            //    //cell.AutoFitColumns();

            //    j += 1;
            //    int d = j;
            //    cell = worksheet.Cells["C" + j.ToString()];
            //    cell.Value = "Тип";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["D" + j.ToString()];
            //    cell.Value = "номер";
            //    cell.Font.Bold = true;

            //    cell = worksheet.Cells["E" + j.ToString()];
            //    cell.Value = "состояние";
            //    cell.Font.Bold = true;

            //    foreach (Container cont in containers)
            //    {
            //        j++;

            //        cell = worksheet.Cells["B" + j.ToString()];
            //        //ContainerHistory h = connect.FindFirstObject<ContainerHistory>(mc=> mc.Container == cont )
            //        string dd = "";
            //        try
            //        {
            //            dd = cont.ContainerHistorys.OrderByDescending(mc => mc.Date).First<ContainerHistory>().Date.ToShortDateString();
            //        }
            //        catch { }
            //        cell.Value = dd;

            //        cell = worksheet.Cells["C" + j.ToString()];
            //        cell.Value = cont.ContainerType.Name;

            //        cell = worksheet.Cells["D" + j.ToString()];
            //        cell.Value = cont.ContainerNumber;

            //        cell = worksheet.Cells["E" + j.ToString()];
            //        string state = "не указано";
            //        if (cont.State != null)
            //            if (cont.State.Name != null && cont.State.Name != String.Empty)
            //                state = cont.State.Name;
            //        cell.Value = state;
            //    }

            //    // Форматирование ячеек по стоку
            //    range = worksheet.Range["B" + d.ToString() + ":E" + j.ToString()];
            //    rangeFormatting = range.BeginUpdateFormatting();

            //    // Specify font settings (font name, color, size and style).
            //    rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
            //    //rangeFormatting.Alignment.ShrinkToFit = true;
            //    // Specify text alignment in cells.
            //    rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            //    rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            //    //range.AutoFitColumns();
            //    // End updating of the range formatting.
            //    range.EndUpdateFormatting(rangeFormatting);


            //    //Column column = worksheet.Columns["G"];
            //    //column.ColumnWidth = 10;
            //    Column column = worksheet.Columns["H"];
            //    column.ColumnWidth = 100;

            //    string path = Path.GetTempPath() + @"\Отчет_по_стоку_" + stock.Name.Replace("\"","'") + "_" +
            //        DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".xlsx";

            //    workbook.SaveDocument(path, DocumentFormat.OpenXml);
            //    res = path;
            //    //try
            //    //{
            //    //    System.Diagnostics.Process.Start(path);
            //    //}
            //    //catch { }
            //}
            //return res;
            #endregion
        }

        private void XLSReportByDayParamAction_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            DateTime reportDate = DateTime.Now.Date;
            if (e.ParameterCurrentValue != null)
            {
                try
                {
                    reportDate = Convert.ToDateTime(e.ParameterCurrentValue);
                }
                catch { }
            }
            Stock stock = View.CurrentObject as Stock;
            UnitOfWork unitOfWork = (UnitOfWork)stock.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            CreateXLSReportFromStockByDay(connect, stock, reportDate);
        }

        /// <summary>
        /// Сформировать отчет по стоку за день (через контроллер) и прикрепить его к стоку 
        /// </summary>
        /// <param name="connect"></param>
        /// <param name="stock"></param>
        /// <param name="reportDate"></param>
        private void CreateXLSReportFromStockByDay(Connect connect, Stock stock, DateTime reportDate)
        {
            string path = "";

            try
            {
                path = CreateXlsReportByStock(connect, stock, reportDate);
            }
            catch { }

            // прикрепляем его к стоку и клиенту
            if (path != "")
            {
                try
                {
                    ServiceReport serviceReport = connect.CreateObject<ServiceReport>();
                    try
                    {
                        serviceReport.Name = String.Format("Отчет по стоку {0} за {1}", stock.Name, reportDate.ToString("dd.MM.yyyy").Replace(".", "_").Replace(" ", "_").Replace(":", "_").Replace("\\", "_").Replace("/", "_"));
                    }
                    catch { }
                    serviceReport.RegDate = reportDate;


                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
                    using (FileStream fs = File.OpenRead(path))
                    {
                        fs.CopyTo(ms);
                        ms.Seek(0, System.IO.SeekOrigin.Begin);
                        FileData filedata = connect.CreateObject<FileData>();
                        filedata.LoadFromStream(path.Split('\\').Last(), ms);
                        serviceReport.File = filedata;
                        ms.Dispose();
                    }
                    stock.ServiceReports.Add(serviceReport);
                    stock.Client.ServiceReports.Add(serviceReport);
                    connect.GetUnitOfWork().CommitChanges();
                }
                catch (Exception ex) { }
                RefreshFrame();
            }
        }
        

        protected void RefreshFrame()
        {
            RefreshController controller = Frame.GetController<RefreshController>();
            if (controller != null)
                controller.RefreshAction.DoExecute();
        }

        #region dont use

        /// <summary>
        /// сформировать отчет по стоку за день 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateXLSReportFromStockByDayAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //IObjectSpace os = Application.CreateObjectSpace();
            Stock stock = View.CurrentObject as Stock;
            UnitOfWork unitOfWork = (UnitOfWork)stock.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

        }

        private void DownloadFile(string fname, bool forceDownload)
        {
            string path = fname;
            string name = Path.GetFileName(path);
            string ext = Path.GetExtension(path);
            string type = "";
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            // set known types based on file extension  
            if (ext != null)
            {
                switch (ext.ToLower())
                {
                    case ".htm":
                    case ".html":
                        type = "text/HTML";
                        break;

                    case ".txt":
                        type = "text/plain";
                        break;

                    case ".doc":
                    case ".rtf":
                        type = "Application/msword";
                        break;

                    case ".xls":
                    case ".xlsx":
                        type = "Application/x-msexcel";
                        break;
                }
            }
            if (forceDownload)
            {
                response.AppendHeader("content-disposition",
                    "attachment; filename=" + name);
            }
            if (type != "")
                response.ContentType = type;
            response.WriteFile(path);
            response.End();
        }
        private void ss(string fname)
        {
            String FileName = Path.GetFileName(fname);
            //String FilePath = fname;//@"C:/Temp";//"d:/"; //Replace this
            String FilePath = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/UploadTemp/" + FileName;
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "Application/x-msexcel";
            response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
            response.TransmitFile(FilePath);
            response.Flush();
            response.End();
        }
        protected void test()
        {
            try
            {
                Thread newThread = new Thread(new ThreadStart(ThreadMethod));
                newThread.SetApartmentState(ApartmentState.STA);
                newThread.Start();

                // try using threads as you will get a Current thread must be set to single thread apartment (STA) mode before OLE Exception .


            }
            catch (Exception ex)
            {

            }

        }

        static void ThreadMethod()
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    // Code to write the stream goes here.
                    myStream.Close();
                }
            }
        }
        #endregion

        /// <summary>
        /// открыть окно с указанием месяца для формирования отчета по нему
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateXLSReportFromStockByMonthAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            string id = ((BaseObject)e.CurrentObject).Oid.ToString();
            Stock stock = null;
            stock = connect.FindFirstObject<Stock>(x => x.Oid.ToString() == id);

            DateToFormReportByMonth dateToFormReportByMonth = connect.CreateObject<DateToFormReportByMonth>();
            dateToFormReportByMonth.DateForReport = DateTime.Now.AddMonths(-1);

            dateToFormReportByMonth.Save();
            os.CommitChanges();


            var svp = new ShowViewParameters
            {
                Context = TemplateContext.PopupWindow,
                TargetWindow = TargetWindow.NewModalWindow,
                CreatedView = this.Application.CreateDetailView(
        os,
        dateToFormReportByMonth,
        true)
            };
            DialogController contr = new DialogController();

            contr.AcceptAction.Caption = "Выбрать объект";
            //contr.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(CreateXLSReportFromStockByMonth(connect,stock, dateToFormReportByMonth.DateForReport.Month,
            //    dateToFormReportByMonth.DateForReport.Year));
            svp.Controllers.Add(contr);
            this.Application.ShowViewStrategy.ShowView(svp, new ShowViewSource(null, null));
        }

        /// <summary>
        /// Открываем попап окно с выбором даты
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xmlReportFromStockByMonthPopupAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            Connect connect = Connect.FromObjectSpace(os);
            stockId = ((BaseObject)View.CurrentObject).Oid.ToString();
            

            DateToFormReportByMonth dateToFormReportByMonth = connect.CreateObject<DateToFormReportByMonth>();
            dateToFormReportByMonth.DateForReport = DateTime.Now.AddMonths(-1);

            dateToFormReportByMonth.Save();
            os.CommitChanges();

            // открываем окно информации 
            DetailView dv = Application.CreateDetailView(os, dateToFormReportByMonth);//Specify the IsRoot parameter if necessary.
            dv.ViewEditMode = DevExpress.ExpressApp.Editors.ViewEditMode.Edit;
            e.View = dv;
        }
        string stockId = "";
        /// <summary>
        /// При нажатии на кнопку ОК в попап окне (с выбором даты)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xmlReportFromStockByMonthPopupAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();//View.ObjectSpace;

            DateToFormReportByMonth dateToFormReportByMonth;
            // получаем наш попап объект, его сессию и сохраняем его, после этого, получаем его из ObjectSpace
            dateToFormReportByMonth = (DateToFormReportByMonth)e.PopupWindowViewCurrentObject;
            UnitOfWork unitOfWork = (UnitOfWork)dateToFormReportByMonth.Session;
            dateToFormReportByMonth.Save();
            unitOfWork.CommitChanges();
            //Connect connect = Connect.FromUnitOfWork(unitOfWork);

            var x_id = ((BaseObject)e.PopupWindowViewCurrentObject).Oid;
            dateToFormReportByMonth = os.FindObject<DateToFormReportByMonth>(new BinaryOperator("Oid", x_id));
            unitOfWork = (UnitOfWork)dateToFormReportByMonth.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            Stock stock = null;
            stock = connect.FindFirstObject<Stock>(x => x.Oid.ToString() == stockId);

            string pathToReport = "";

            pathToReport = CreateXLSReportFromStockByMonth(connect, stock, dateToFormReportByMonth.DateForReport.Month, dateToFormReportByMonth.DateForReport.Year);

            try {
                // прикрепляем его к стоку и клиенту
                if (pathToReport != "")
                {
                    try
                    {
                        ServiceReport serviceReport = connect.CreateObject<ServiceReport>();
                        try
                        {
                            serviceReport.Name = String.Format("Отчет по стоку {0} за {1}-{2} от {3}", stock.Name,
                                dateToFormReportByMonth.DateForReport.Month, dateToFormReportByMonth.DateForReport.Year,
                                DateTime.Now.Date.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_").Replace("\\", "_").Replace("/", "_"));
                        }
                        catch { }
                        serviceReport.RegDate = DateTime.Now.Date;


                        System.IO.MemoryStream ms = new System.IO.MemoryStream();
                        using (FileStream fs = File.OpenRead(pathToReport))
                        {
                            fs.CopyTo(ms);
                            ms.Seek(0, System.IO.SeekOrigin.Begin);
                            FileData filedata = connect.CreateObject<FileData>();
                            filedata.LoadFromStream(pathToReport.Split('\\').Last(), ms);
                            serviceReport.File = filedata;
                            ms.Dispose();
                        }
                        stock.ServiceReports.Add(serviceReport);
                        stock.Client.ServiceReports.Add(serviceReport);
                        connect.GetUnitOfWork().CommitChanges();
                    }
                    catch (Exception ex) { }
                }
            }
            catch { }

        }

        /// <summary>
        /// Сформировать сам финансовый отчет по стоку за месяц
        /// </summary>
        public string CreateXLSReportFromStockByMonth(Connect connect, Stock stock, int month, int year)
        {
            string res = "";

            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            if (stock.Client != null && stock.Terminal != null)
            {
                // Create an instance of a workbook.

                // Load a workbook from the file.

                string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                    LocalPath.Replace("TerminalSystem2.Module.DLL", @"\DocTemplates\ReportByMonth.xlsx");

                workbook.LoadDocument(templatePath, DocumentFormat.OpenXml);

                // Access the first worksheet in the workbook.
                Worksheet worksheet = workbook.Worksheets[0];
                Cell cell;

                Client client = stock.Client;

                cell = worksheet.Cells["A3"];
                cell.Value = String.Format("Реестр № {0}/{1} от {2}", month, year, DateTime.Now.Date.ToString("dd.MM.yyyy"));

                cell = worksheet.Cells["A7"];
                string cl = "";
                try { cl = String.Format("Заказчик: «{0}» сток «{1}»", client.Name, stock.Name); }
                catch { }
                cell.Value = cl;

                int i = 0;
                int o = 0;
                bool isServicesExist = true;
                foreach (Container container in stock.Client.Containers)
                {
                    if (container.ContainerHistorys!= null && container.ContainerHistorys.Count != 0 &&
                        container.ContainerHistorys.OrderByDescending(x => x.Date).FirstOrDefault().Stock == stock)
                    {// у каждого контейнера смотрим операции в текущем месяце
                        IQueryable ServicesDone = connect.FindObjects<ServiceDone>(x => x.Container == container &&
                        (x.StartDate.Year == year && x.StartDate.Month == month));
                        if (ServicesDone != null)
                        {
                            foreach (ServiceDone serviceDone in ServicesDone)
                            {
                                i++;
                                cell = worksheet.Cells["A" + (9 + i).ToString()];
                                cell.Value = i.ToString();

                                cell = worksheet.Cells["B" + (9 + i).ToString()];
                                cell.Value = container.ContainerNumber;

                                cell = worksheet.Cells["C" + (9 + i).ToString()];
                                cell.Value = container?.ContainerType?.Name?? "";

                                string isLaden = "";
                                if (container.isLaden)
                                    isLaden = "гр.";
                                else
                                    isLaden = "не гр.";
                                cell = worksheet.Cells["D" + (9 + i).ToString()];
                                cell.Value = isLaden;

                                // ищем прибытие контейнера по его истории

                                cell = worksheet.Cells["E" + (9 + i).ToString()];
                                try
                                {
                                    string inStockDate = "";
                                    foreach (ContainerHistory h in container.ContainerHistorys.OrderByDescending(x => x.Date))
                                    {
                                        if (h.ContainerHistoryKind == Enums.EContainerHistoryKind.Размещение)
                                        {
                                            inStockDate = h.Date.ToString("dd.MM.yyyy");
                                            break;
                                        }
                                    }
                                    cell.Value = inStockDate;
                                }
                                catch { }
                                //cell.Value = serviceDone.StartDate.ToShortDateString();

                                //string startReportDate = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day).ToShortDateString();
                                //if (serviceDone.StartDate.Month == DateTime.Now.Month)
                                //    startReportDate = serviceDone.StartDate.ToShortDateString();
                                //cell = worksheet.Cells["F" + (9 + i).ToString()];
                                //cell.Value = startReportDate;

                                cell = worksheet.Cells["F" + (9 + i).ToString()];
                                if (serviceDone.StartDate != DateTime.MinValue)
                                    cell.Value = serviceDone.StartDate.ToString("dd.MM.yyyy");

                                cell = worksheet.Cells["G" + (9 + i).ToString()];
                                if(serviceDone.ValueTotalDate != DateTime.MinValue)
                                    cell.Value = serviceDone.ValueTotalDate.ToString("dd.MM.yyyy");// ToShortDateString();

                                cell = worksheet.Cells["H" + (9 + i).ToString()];
                                cell.Value = serviceDone?.ServiceType?.Name?? "";

                                cell = worksheet.Cells["I" + (9 + i).ToString()];
                                cell.Value = serviceDone.ServiceCount.ToString();

                                cell = worksheet.Cells["J" + (9 + i).ToString()];
                                cell.Value = serviceDone?.UnitKind?.Name?? "";

                                cell = worksheet.Cells["K" + (9 + i).ToString()];
                                cell.Value = serviceDone.Value;

                                cell = worksheet.Cells["L" + (9 + i).ToString()];
                                if (serviceDone.DiscountActiveThroughDate != DateTime.MinValue)
                                {
                                    cell.Value = (serviceDone.DiscountActiveThroughDate - serviceDone.StartDate).Days + 1;
                                }
                                else
                                    cell.Value = "";

                                cell = worksheet.Cells["M" + (9 + i).ToString()];
                                cell.Value = serviceDone.Discount;

                                cell = worksheet.Cells["N" + (9 + i).ToString()];
                                cell.Value = serviceDone.ValueTotal;

                                if (i > 5)
                                {
                                    worksheet.Rows.Insert(i + 9);
                                    Range range = worksheet.Range[String.Format("A{0}:N{0}", (i + 10).ToString())];
                                    Formatting rangeFormatting = range.BeginUpdateFormatting();
                                    rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
                                    rangeFormatting.Font.Name = "Tahoma";
                                    rangeFormatting.Font.Size = 8;
                                    rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                                    rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                                    range.EndUpdateFormatting(rangeFormatting);
                                    o++;
                                }
                                try
                                {
                                    cell = worksheet.Cells["O" + (20 + o).ToString()];
                                    cell.Value = serviceDone.Currency.Code ?? "";
                                }
                                catch { }
                            }
                        }
                        else
                            isServicesExist = false;
                    }
                }

                string monthstring = "";
                switch (month)
                {
                    case 1:
                        monthstring = "январь";
                        break;
                    case 2:
                        monthstring = "февраль";
                        break;
                    case 3:
                        monthstring = "март";
                        break;
                    case 4:
                        monthstring = "апрель";
                        break;
                    case 5:
                        monthstring = "май";
                        break;
                    case 6:
                        monthstring = "июнь";
                        break;
                    case 7:
                        monthstring = "июль";
                        break;
                    case 8:
                        monthstring = "август";
                        break;
                    case 9:
                        monthstring = "сентябрь";
                        break;
                    case 10:
                        monthstring = "октябрь";
                        break;
                    case 11:
                        monthstring = "ноябрь";
                        break;
                    case 12:
                        monthstring = "декабрь";
                        break;
                    default:
                        monthstring = DateTime.Now.Month.ToString();
                        break;
                }
                if (isServicesExist)
                {
                    string path = Path.GetTempPath() + @"\Отчет_по_стоку_" + stock.Name + "_за_" + monthstring + "_" + year + "_" +
                      DateTime.Now.ToString("dd.MM.yyyy H:mm:ss").Replace(".", "_").Replace(" ", "_").Replace(":", "_").Replace("\\", "_").Replace("/", "_") + ".xlsx";
                    workbook.SaveDocument(path, DocumentFormat.OpenXml);
                    res = path;
                }
            }
            return res;
        }

        
    }
}

