﻿namespace TerminalSystem2.Module.Controllers
{
    partial class CreateReportsFromStockController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreateXLSReportFromStockByDayAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CreateXLSReportFromStockByMonthAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.xmlReportFromStockByMonthPopupAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.XLSReportByDayParamAction = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            // 
            // CreateXLSReportFromStockByDayAction
            // 
            this.CreateXLSReportFromStockByDayAction.Caption = "Сформировать отчет за день";
            this.CreateXLSReportFromStockByDayAction.ConfirmationMessage = null;
            this.CreateXLSReportFromStockByDayAction.Id = "CreateXLSReportFromStockByDayAction";
            this.CreateXLSReportFromStockByDayAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.CreateXLSReportFromStockByDayAction.TargetObjectType = typeof(TerminalSystem2.Terminals.Stock);
            this.CreateXLSReportFromStockByDayAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CreateXLSReportFromStockByDayAction.ToolTip = null;
            this.CreateXLSReportFromStockByDayAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CreateXLSReportFromStockByDayAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateXLSReportFromStockByDayAction_Execute);
            // 
            // CreateXLSReportFromStockByMonthAction
            // 
            this.CreateXLSReportFromStockByMonthAction.Caption = "Сформировать фин. отчет";
            this.CreateXLSReportFromStockByMonthAction.ConfirmationMessage = null;
            this.CreateXLSReportFromStockByMonthAction.Id = "CreateXLSReportFromStockByMonthAction";
            this.CreateXLSReportFromStockByMonthAction.ToolTip = null;
            this.CreateXLSReportFromStockByMonthAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CreateXLSReportFromStockByMonthAction_Execute);
            // 
            // xmlReportFromStockByMonthPopupAction
            // 
            this.xmlReportFromStockByMonthPopupAction.AcceptButtonCaption = null;
            this.xmlReportFromStockByMonthPopupAction.CancelButtonCaption = null;
            this.xmlReportFromStockByMonthPopupAction.Caption = "Сформировать фин. отчет за месяц";
            this.xmlReportFromStockByMonthPopupAction.ConfirmationMessage = null;
            this.xmlReportFromStockByMonthPopupAction.Id = "xmlReportFromStockByMonthPopupAction";
            this.xmlReportFromStockByMonthPopupAction.ToolTip = null;
            this.xmlReportFromStockByMonthPopupAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.xmlReportFromStockByMonthPopupAction_CustomizePopupWindowParams);
            this.xmlReportFromStockByMonthPopupAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.xmlReportFromStockByMonthPopupAction_Execute);
            // 
            // XLSReportByDayParamAction
            // 
            this.XLSReportByDayParamAction.Caption = "Отчет за день";
            this.XLSReportByDayParamAction.ConfirmationMessage = null;
            this.XLSReportByDayParamAction.Id = "XLSReportByDayParamAction";
            this.XLSReportByDayParamAction.NullValuePrompt = null;
            this.XLSReportByDayParamAction.ShortCaption = null;
            this.XLSReportByDayParamAction.TargetObjectType = typeof(TerminalSystem2.Terminals.Stock);
            this.XLSReportByDayParamAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.XLSReportByDayParamAction.ToolTip = null;
            this.XLSReportByDayParamAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.XLSReportByDayParamAction.ValueType = typeof(System.DateTime);
            this.XLSReportByDayParamAction.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.XLSReportByDayParamAction_Execute);
            // 
            // CreateReportsFromStockController
            // 
            this.Actions.Add(this.CreateXLSReportFromStockByDayAction);
            this.Actions.Add(this.CreateXLSReportFromStockByMonthAction);
            this.Actions.Add(this.xmlReportFromStockByMonthPopupAction);
            this.Actions.Add(this.XLSReportByDayParamAction);
            this.TargetObjectType = typeof(TerminalSystem2.Terminals.Stock);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }


        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CreateXLSReportFromStockByDayAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CreateXLSReportFromStockByMonthAction;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction xmlReportFromStockByMonthPopupAction;
        private DevExpress.ExpressApp.Actions.ParametrizedAction XLSReportByDayParamAction;
    }
}
