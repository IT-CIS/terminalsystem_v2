﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.Clients;
using DevExpress.Xpo;
using TerminalSystem2.SystemDir;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using System.Text.RegularExpressions;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.OrgStructure;

namespace TerminalSystem2.Module.Controllers.ClientsController
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AddClientEmployeesViewController : ViewController
    {
        ModificationsController controller;
        public AddClientEmployeesViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            controller = Frame.GetController<ModificationsController>();
            if (controller != null)
                controller.SaveAction.Executed += SaveObjectAction_Executed;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        void SaveObjectAction_Executed(object sender, ActionBaseEventArgs e)
        {
            IObjectSpace os = Application.CreateObjectSpace();
            ClientEmployee clientEmployee;
            // получаем наш попап объект, его сессию и сохраняем его, после этого, получаем его из ObjectSpace
            var x_oid = ((BaseObject)View.CurrentObject).Oid;
            Connect connect = Connect.FromObjectSpace(os);
            clientEmployee = connect.FindFirstObject<ClientEmployee>(x=> x.Oid == x_oid);
            clientEmployee.Save();

            PermissionPolicyUser user = null;
            if (!string.IsNullOrEmpty(clientEmployee.SysUserString))
            {
                //Regex rgx = new Regex(@"^[a-zA-Zа-яА-Я0-9]");
                //if (!rgx.IsMatch(clientEmployee.SysUserString))
                //{
                //    throw new UserFriendlyException(String.Format($"Имя пользователя системы должно содержать только русские, латинские буквы и цифры."));
                //}
                user = connect.FindFirstObject<PermissionPolicyUser>(x => x.UserName == clientEmployee.SysUserString);
                if(user == null)
                {
                    user = connect.CreateObject<PermissionPolicyUser>();
                    user.ChangePasswordOnFirstLogon = true;
                    user.IsActive = true;
                    user.UserName = clientEmployee.SysUserString;
                    PermissionPolicyRole clientRole = connect.FindFirstObject<PermissionPolicyRole>(x => x.Name == "Сотрудник клиента");
                    if (clientRole != null)
                        user.Roles.Add(clientRole);
                    user.Save();
                    clientEmployee.SysUser = user;
                    clientEmployee.Save();
                }
                else
                {
                    ClientEmployee otherClientEmployee = connect.FindFirstObject<ClientEmployee>(x => x.SysUser.Oid == user.Oid);
                    if(otherClientEmployee != null)
                    {
                        if (otherClientEmployee.Oid != clientEmployee.Oid)
                        {
                            throw new UserFriendlyException(String.Format($"Другой пользователь с таким аккаунтом уже заведен в системе. Пожалуйста, задайте другое имя пользователя системы."));
                        }
                    }
                    else
                    {
                        Employee otherEmpl = connect.FindFirstObject<Employee>(x => x.SysUser.Oid == user.Oid);
                        if (otherEmpl != null)
                        {
                            throw new UserFriendlyException($"Другой пользователь (сотрудник терминала {otherEmpl.BriefName ?? ""}) " +
                                $"с таким аккаунтом уже заведен в системе. Пожалуйста, задайте другое имя пользователя системы.");
                        }
                        else
                        {
                            clientEmployee.SysUser = user;
                            clientEmployee.Save();
                        }
                    }
                }
                
            }
            //if (user != null)
            //{
            //    clientEmployee.SysUser = user;
            //    clientEmployee.Save();
            //}
            //else
            //    clientEmployee.Delete();
            os.CommitChanges();
            RefreshFrame();
        }

        /// <summary>
        /// Обновить фрейм (страницу)
        /// </summary>
        protected void RefreshFrame()
        {
            RefreshController controller = Frame.GetController<RefreshController>();
            if (controller != null)
                controller.RefreshAction.DoExecute();
        }
    }
}
