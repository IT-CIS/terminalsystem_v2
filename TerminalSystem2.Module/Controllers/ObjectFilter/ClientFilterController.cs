﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Clients;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Containers;

namespace TerminalSystem2.Module.Controllers.ClientFilter
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ClientFilterController : ViewController
    {
        public ClientFilterController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            bool isAdmin = false;
            // Perform various tasks depending on the target View.
            if (SecuritySystem.CurrentUser != null)
            {
                ClientEmployee clientEmpl = null;
                PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;
                foreach (PermissionPolicyRole role in currentuser.Roles)
                {
                    if (role.IsAdministrative)
                        isAdmin = true;
                }

                
                //isAdmin = false;
                if (!isAdmin)
                {
                    IObjectSpace os = Application.CreateObjectSpace();
                    clientEmpl = os.FindObject<ClientEmployee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                    if (clientEmpl != null)
                    {
                        if (View is ListView)
                        {
                            if (View.ObjectTypeInfo.Type == typeof(RequestContainersPlacement) ||
                            View.ObjectTypeInfo.Type == typeof(RequestContainersExtradition))
                            {
                                CriteriaOperator criteria = new BinaryOperator("Client", GetClient());
                                //CriteriaOperator[] operands = new CriteriaOperator[] {
                                //       new BinaryOperator("BetIsDone", true),
                                //       new BinaryOperator("BetIsDone", true),
                                //       new BinaryOperator("Player", GetPlayer())
                                //    };
                                //CriteriaOperator criteria = new FunctionOperator(FunctionOperatorType.Iif, operands);
                                ((ListView)View).CollectionSource.Criteria["Filter1"] = criteria;
                            }
                            if (View.ObjectTypeInfo.Type == typeof(Container))
                            {
                                CriteriaOperator criteria = new BinaryOperator("Owner", GetClient());
                                ((ListView)View).CollectionSource.Criteria["Filter1"] = criteria;
                            }
                        }
                    }
                }
            }
        }
        private Client GetClient()
        {
            Client client = null;
            if (SecuritySystem.CurrentUser != null)
            {
                ClientEmployee currentEmpl = ObjectSpace.FindObject<ClientEmployee>(new BinaryOperator("SysUser.Oid", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    client = currentEmpl.Client?? null;
            }
            return client;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
