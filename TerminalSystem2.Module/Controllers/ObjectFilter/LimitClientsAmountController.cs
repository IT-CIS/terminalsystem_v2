﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.Clients;

namespace TerminalSystem2.Module.Controllers.ObjectFilter
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class LimitClientsAmountController : ViewController
    {
        private NewObjectViewController controller;
        protected override void OnActivated()
        {
            base.OnActivated();
            //controller = Frame.GetController<NewObjectViewController>();
            //if (controller != null)
            //{
            //    controller.ObjectCreating += controller_ObjectCreating;
            //}
        }
        void controller_ObjectCreating(object sender, ObjectCreatingEventArgs e)
        {
            //if ((e.ObjectType == typeof(Client)) &&
            //    (e.ObjectSpace.GetObjectsCount(typeof(Client), null) >= 1))
            //{
            //    e.Cancel = true;
            //    throw new UserFriendlyException(
            //        "В тестовой версии возможно создание только одного клиента!");
            //}
            //if ((e.ObjectType == typeof(ClientEmployee)) &&
            //    (e.ObjectSpace.GetObjectsCount(typeof(ClientEmployee), null) >= 1))
            //{
            //    e.Cancel = true;
            //    throw new UserFriendlyException(
            //        "В тестовой версии возможно создание только одного сотрудника клиента!");
            //}
        }
        protected override void OnDeactivated()
        {
            //if (controller != null)
            //{
            //    controller.ObjectCreating -= controller_ObjectCreating;
            //}
            base.OnDeactivated();
        }
    }
}
