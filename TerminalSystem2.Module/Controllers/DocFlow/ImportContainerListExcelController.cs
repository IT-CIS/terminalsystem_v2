﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Spreadsheet;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Containers;
using TerminalSystem2.DocFlow;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;

namespace TerminalSystem2.Module.Controllers.DocFlow
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ImportContainerListExcelController : ViewController
    {
        public ImportContainerListExcelController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        /// <summary>
        /// импорт данных по контейнерам из файла Excel в заявки (по приемке и выдаче контейнеров)
        /// </summary>
        /// <param name="fileStream"></param>
        /// <param name="os"></param>
        /// <param name="requestOid"></param>
        /// <param name="requesttype"></param>
        public void ImportContainerListInRequestFromExcel(Stream fileStream, IObjectSpace os, string requestOid, string requesttype)
        {
            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            workbook.LoadDocument(fileStream, DocumentFormat.Xlsx);
            Worksheet worksheet = workbook.Worksheets[0];

            //if (worksheet.Columns.LastUsedIndex < 13)
            //{
            //    throw new Exception($"Не корректный файл. В нем всего {worksheet.Columns.LastUsedIndex} столбцов. Проверьте импортируемый файл!");
            //    return;
            //}

            string contNum = "";
            string contType = "";
            string contStock = "";

            Connect connect = Connect.FromObjectSpace(os);
            RequestContainersPlacement request = connect.FindFirstObject<RequestContainersPlacement>(x => x.Oid.ToString() == requestOid);
            //if (helper == null)
            //    helper = connect.CreateObject<HelperClassToImportEquipFromExcel>();

            //os.CommitChanges();
            int r = 0;
            for (int i = 2; i < worksheet.Rows.LastUsedIndex + 2; i++)
            //for (int i = 2; i < 10; i++)
            {
                contNum = "";
                // данные исполнителя
                try
                {
                    if (worksheet.Cells["A" + i.ToString()].Value != null)
                        contNum = worksheet.Cells["A" + i.ToString()].Value.ToString().Trim();
                }
                catch { }
                try
                {
                    if (worksheet.Cells["B" + i.ToString()].Value != null)
                        contType = worksheet.Cells["B" + i.ToString()].Value.ToString().Trim();
                }
                catch { }
                try
                {
                    if (worksheet.Cells["C" + i.ToString()].Value != null)
                        contStock = worksheet.Cells["C" + i.ToString()].Value.ToString().Trim();
                }
                catch { }
                if (contNum!= "")
                    if (contNum.Length < 12)
                    {
                        if (requesttype == "placement")
                            ImportContainerPlacement(os, requestOid, contNum, contType, contStock);
                    }
                if(requesttype == "extradition")
                {
                    ImportContainerExtradition(os, requestOid, contNum, contType, contStock);
                }
            }
        }

        /// <summary>
        /// импорт данных по контейнеру в заявку по приемки контейнера
        /// </summary>
        /// <param name="os"></param>
        /// <param name="requestOid"></param>
        /// <param name="contNum"></param>
        /// <param name="contType"></param>
        /// <param name="contStock"></param>
        private void ImportContainerPlacement(IObjectSpace os, string requestOid, string contNum, string contType, string contStock)
        {
            Connect connect = Connect.FromObjectSpace(os);
            RequestContainersPlacement request = connect.FindFirstObject<RequestContainersPlacement>(x => x.Oid.ToString() == requestOid);
            request.Save();
            bool isExist = false;
            try
            {
                foreach (RequestContainerInfo info in request.RequestContainerInfos)
                {
                    if (info.Number == contNum)
                    {
                        isExist = true;
                        break;
                    }
                }
            }
            catch { }
            if(!isExist)
            {
                RequestContainerInfo info = connect.CreateObject<RequestContainerInfo>();
                info.Number = contNum;
                // тип контейнера
                try
                {
                    if (contType != "")
                    {
                        dContainerType containerType = null;
                        containerType = connect.FindFirstObject<dContainerType>(x => x.Name == contType);
                        if (containerType == null)
                        {
                            if (contType.Contains(" "))
                            {
                                string contType1 = contType.Split(' ')[0];
                                string contType2 = contType.Split(' ')[1];
                                //containerType = connect.FindFirstObject<dContainerType>(x => x.Name.Contains(contType1));
                                //containerType = connect.FindFirstObject<dContainerType>(x => x.Name.Contains(contType2));
                                containerType = connect.FindFirstObject<dContainerType>(x => x.Name.Contains(contType1) && x.Name.Contains(contType2));
                            }

                        }
                        info.ContainerType = connect.FindFirstObject<dContainerType>(x => x.Oid == containerType.Oid);
                    }
                }
                catch { }
            // сток
            try
                {
                    Stock stock = null;
                    if (contStock!="")
                    {
                        stock = connect.FindFirstObject<Stock>(x => x.Client.Oid == request.Client.Oid && x.Terminal.Oid == request.Terminal.Oid && x.Name == contStock);
                    }
                    else { stock = connect.FindFirstObject<Stock>(x => x.Client.Oid == request.Client.Oid && x.Terminal.Oid == request.Terminal.Oid); }
                    info.Stock = connect.FindFirstObject<Stock>(x => x.Oid == stock.Oid);
                }
                catch (Exception)
                {
                }
                info.Request = request;
                info.Save();

            }
            os.CommitChanges();
        }

        /// <summary>
        /// импорт данных по контейнеру в заявку по выдаче контейнера
        /// </summary>
        /// <param name="os"></param>
        /// <param name="requestOid"></param>
        /// <param name="contNum"></param>
        /// <param name="contType"></param>
        /// <param name="contStock"></param>
        private void ImportContainerExtradition(IObjectSpace os, string requestOid, string contNum, string contType, string contStock)
        {
            Connect connect = Connect.FromObjectSpace(os);
            RequestContainersExtradition request = connect.FindFirstObject<RequestContainersExtradition>(x => x.Oid.ToString() == requestOid);

            bool isExist = false;
            if (contNum != "")
            {
                try
                {
                    foreach (RequestContainerExtraditionInfo info in request.RequestContainerInfos)
                    {
                        if (info.Number == contNum)
                        {
                            isExist = true;
                            break;
                        }
                    }
                }
                catch { }
            }
            if (!isExist)
            {
                RequestContainerExtraditionInfo info = connect.CreateObject<RequestContainerExtraditionInfo>();
                info.Number = contNum;
                try {
                    info.Container = connect.FindFirstObject<Container>(x => x.ContainerNumber == contNum);
                }
                catch { }
                // тип контейнера
                try
                {
                    dContainerType containerType = null;
                    if (info.Container != null)
                        containerType = connect.FindFirstObject<dContainerType>(x => x.Oid == info.Container.ContainerType.Oid);
                    else
                    {
                        if (contType != "")
                        {
                            containerType = connect.FindFirstObject<dContainerType>(x => x.Name == contType);
                            if (containerType == null)
                            {
                                if (contType.Contains(" "))
                                {
                                    string contType1 = contType.Split(' ')[0];
                                    string contType2 = contType.Split(' ')[1];
                                    containerType = connect.FindFirstObject<dContainerType>(x => x.Name.Contains(contType1) && x.Name.Contains(contType2));

                                }
                            }
                            
                        }
                    }
                    info.ContainerType = connect.FindFirstObject<dContainerType>(x => x.Oid == containerType.Oid);
                }
                catch { }
                // сток
                try
                {
                    Stock stock = null;
                    try {
                        if (info.Container != null)
                            stock = connect.FindFirstObject<Stock>(x => x.Oid == info.Container.Stock.Oid);
                    }
                    catch { }
                    if (stock == null)
                    {
                        if (contStock != "")
                        {
                            stock = connect.FindFirstObject<Stock>(x => x.Client.Oid == request.Client.Oid && x.Terminal.Oid == request.Terminal.Oid && x.Name == contStock);
                        }
                        else { stock = connect.FindFirstObject<Stock>(x => x.Client.Oid == request.Client.Oid && x.Terminal.Oid == request.Terminal.Oid); }
                    }
                    info.Stock = connect.FindFirstObject<Stock>(x => x.Oid == stock.Oid);
                }
                catch (Exception)
                {
                }
                info.Request = request;
                info.Save();

            }
            os.CommitChanges();
        }
    }
}
