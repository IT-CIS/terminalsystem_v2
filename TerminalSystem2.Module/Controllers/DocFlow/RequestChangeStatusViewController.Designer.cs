﻿namespace TerminalSystem2.Module.Controllers
{
    partial class RequestChangeStatusViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RequestSubmissionAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RequestProcessingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RequestOnEditAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.RequestFinishEditAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RequestSubmissionAction
            // 
            this.RequestSubmissionAction.Caption = "Подать заявку";
            this.RequestSubmissionAction.Category = "RequestSubmission";
            this.RequestSubmissionAction.ConfirmationMessage = null;
            this.RequestSubmissionAction.Id = "RequestSubmissionAction";
            this.RequestSubmissionAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.RequestSubmissionAction.TargetObjectType = typeof(TerminalSystem2.DocFlow.RequestBase);
            this.RequestSubmissionAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.RequestSubmissionAction.ToolTip = null;
            this.RequestSubmissionAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.RequestSubmissionAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RequestSubmissionAction_Execute);
            // 
            // RequestProcessingAction
            // 
            this.RequestProcessingAction.Caption = "В обработку";
            this.RequestProcessingAction.ConfirmationMessage = null;
            this.RequestProcessingAction.Id = "RequestProcessingAction";
            this.RequestProcessingAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.RequestProcessingAction.TargetObjectType = typeof(TerminalSystem2.DocFlow.RequestBase);
            this.RequestProcessingAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.RequestProcessingAction.ToolTip = null;
            this.RequestProcessingAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.RequestProcessingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RequestProcessingAction_Execute);
            // 
            // RequestOnEditAction
            // 
            this.RequestOnEditAction.Caption = "Взять на редактирование";
            this.RequestOnEditAction.ConfirmationMessage = null;
            this.RequestOnEditAction.Id = "RequestOnEditAction";
            this.RequestOnEditAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.RequestOnEditAction.TargetObjectType = typeof(TerminalSystem2.DocFlow.RequestBase);
            this.RequestOnEditAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.RequestOnEditAction.ToolTip = null;
            this.RequestOnEditAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.RequestOnEditAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RequestOnEditAction_Execute);
            // 
            // RequestFinishEditAction
            // 
            this.RequestFinishEditAction.Caption = "Сохранить изменения";
            this.RequestFinishEditAction.ConfirmationMessage = null;
            this.RequestFinishEditAction.Id = "RequestFinishEditAction";
            this.RequestFinishEditAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.RequestFinishEditAction.TargetObjectType = typeof(TerminalSystem2.DocFlow.RequestBase);
            this.RequestFinishEditAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.RequestFinishEditAction.ToolTip = null;
            this.RequestFinishEditAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.RequestFinishEditAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RequestFinishEditAction_Execute);
            // 
            // RequestChangeStatusViewController
            // 
            this.Actions.Add(this.RequestSubmissionAction);
            this.Actions.Add(this.RequestProcessingAction);
            this.Actions.Add(this.RequestOnEditAction);
            this.Actions.Add(this.RequestFinishEditAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RequestSubmissionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RequestProcessingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RequestOnEditAction;
        private DevExpress.ExpressApp.Actions.SimpleAction RequestFinishEditAction;
    }
}
