﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.DocFlow;
using DevExpress.Xpo;
using System.IO;
using System.Web;
using DevExpress.ExpressApp.Web;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Clients;
using TerminalSystem2.SystemDir;
using System.Threading.Tasks;

namespace TerminalSystem2.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RequestChangeStatusViewController : ViewController
    {
        public RequestChangeStatusViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        private void ChangeVisibleActions()
        {
            Employee empl = null;
            ClientEmployee clientEmpl = null;
            if (SecuritySystem.CurrentUser != null)
            {
                PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;

                IObjectSpace os = Application.CreateObjectSpace();
                empl = os.FindObject<Employee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                clientEmpl = os.FindObject<ClientEmployee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
                //empl = 
                //foreach (PermissionPolicyRole role in currentuser.Roles)
                //{
                //    if (role.IsAdministrative)
                //        isAdmin = true;
                //}
            }
            if (View is DetailView)
            {
                if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersPlacement" ||
                    View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersExtradition")
                {
                    RequestBase request = View.CurrentObject as RequestBase;

                    if (request.RequestStatus == Enums.ERequestStatus.Черновик)
                    {
                        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", true);
                        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);
                    }
                    if (request.RequestStatus == Enums.ERequestStatus.Подана || request.RequestStatus == Enums.ERequestStatus.Отредактирована)
                    {
                        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", true);
                        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", true);
                    }
                    if (request.RequestStatus == Enums.ERequestStatus.Редактирование)
                    {
                        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", true);
                        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);
                    }
                    if (request.RequestStatus == Enums.ERequestStatus.Обработка)
                    {
                        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", true);
                        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);
                    }
                    if (request.RequestStatus == Enums.ERequestStatus.Отработана || request.RequestStatus == Enums.ERequestStatus.ОбработанаЧастично
                        || request.RequestStatus == Enums.ERequestStatus.ЗакрытаПоВремени)
                    {
                        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);
                    }
                    if (clientEmpl != null)
                        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);
                    #region NOT USING
                    //if (clientEmpl != null)
                    //{
                    //    if (request.RequestStatus == Enums.ERequestStatus.Черновик)
                    //    {
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", true);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                    //    }
                    //    //else
                    //    //    Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                    //    if (request.RequestStatus == Enums.ERequestStatus.Подана || request.RequestStatus == Enums.ERequestStatus.Отредактирована)
                    //    {
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", true);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                    //    }
                    //    if (request.RequestStatus == Enums.ERequestStatus.Редактирование)
                    //    {
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", true);

                    //    }
                    //    if (request.RequestStatus == Enums.ERequestStatus.Обработка)
                    //    {
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", true);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                    //    }
                    //    if (request.RequestStatus == Enums.ERequestStatus.Отработана || request.RequestStatus == Enums.ERequestStatus.ОбработанаЧастично)
                    //    {
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                    //    }
                    //    //else
                    //    //    Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);

                    //    // Скрываем для клиента Экшены сотрудника терминала
                    //    Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);
                    //}
                    //if (empl != null)
                    //{
                    //    // Скрываем для сотрудника терминала Экшены клиента
                    //    Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                    //    Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                    //    Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                    //    if(request.RequestStatus == Enums.ERequestStatus.Обработка || request.RequestStatus == Enums.ERequestStatus.Редактирование)
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);
                    //    else
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", true);

                    //    if (request.RequestStatus == Enums.ERequestStatus.Черновик)
                    //    {
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", true);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                    //        Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                    //    }
                    //}

                    //if (request.RequestStatus == Enums.ERequestStatus.Отработана || request.RequestStatus == Enums.ERequestStatus.ЗакрытаПоВремени)
                    //{
                    //    Frame.GetController<RequestChangeStatusViewController>().RequestSubmissionAction.Active.SetItemValue("myReason", false);
                    //    Frame.GetController<RequestChangeStatusViewController>().RequestOnEditAction.Active.SetItemValue("myReason", false);
                    //    Frame.GetController<RequestChangeStatusViewController>().RequestFinishEditAction.Active.SetItemValue("myReason", false);
                    //    Frame.GetController<RequestChangeStatusViewController>().RequestProcessingAction.Active.SetItemValue("myReason", false);

                    //}
                    #endregion
                }
            }
        }
        /// <summary>
        /// Подать заявку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestSubmissionAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = View.ObjectSpace;
            Connect connect = Connect.FromObjectSpace(os);
            if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersPlacement")
            {
                RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;
                if (request.RequestStatus == Enums.ERequestStatus.Черновик)
                {
                    if (request.RequestContainerInfos.Count < 1)
                        throw new Exception("Необходимо указать хотя бы один контейнер!");
                    string messError = "Ошибка проверки данных:" + Environment.NewLine;
                    bool isError = false;
                    foreach (RequestContainerInfo containerInfo in request.RequestContainerInfos)
                    {
                        if (containerInfo.ContainerType == null)
                        {
                            messError += $"У контейнера {containerInfo.Number} не указан тип контейнера." + Environment.NewLine;
                            isError = true;
                        }
                        if (containerInfo.Stock == null)
                        {
                            messError += $"У контейнера {containerInfo.Number} не указан сток." + Environment.NewLine;
                            isError = true;
                        }
                    }
                    if(isError)
                    {
                        throw new UserFriendlyException(messError);
                    }
                    //if (request.RequestDriverInfos.Count < 1)
                    //    throw new Exception("Необходимо указать информацию по хотя бы одному водителю!");
                    request.RequestStatus = Enums.ERequestStatus.Подана;
                    foreach(RequestContainerInfo containerInfo in request.RequestContainerInfos)
                    {
                        containerInfo.RequestStatus = Enums.ERequestStatus.Подана;
                        containerInfo.Save();
                    }
                    request.Save();
                    os.CommitChanges();
                    Messanger.ShowMessage(Application, "Инфо", "Заявка подана", InformationType.Info);
                    // отправляем сообщения
                    SendNotifacationsByRequest(connect, request, request.RequestStatus);
                }
            }
            if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersExtradition")
            {
                RequestContainersExtradition request = View.CurrentObject as RequestContainersExtradition;
                if (request.RequestStatus == Enums.ERequestStatus.Черновик)
                {
                    if (request.RequestContainerInfos.Count < 1)
                        throw new Exception("Необходимо указать хотя бы один контейнер!");
                    //if (request.RequestDriverInfos.Count < 1)
                    //    throw new Exception("Необходимо указать информацию по хотя бы одному водителю!");
                    request.RequestStatus = Enums.ERequestStatus.Подана;
                    request.Save();
                    os.CommitChanges();
                    Messanger.ShowMessage(Application, "Инфо", "Заявка подана", InformationType.Info);
                    // отправляем сообщения
                    SendNotifacationsByRequestExtradition(connect, request, request.RequestStatus);
                    
                }
            }
            //if (SecuritySystem.CurrentUser != null)
            //{
            //    PermissionPolicyUser currentuser = SecuritySystem.CurrentUser as PermissionPolicyUser;

            //    ClientEmployee empl = os.FindObject<ClientEmployee>(new BinaryOperator("SysUser.Oid", currentuser.Oid));
            //    if (empl != null)
            //        View.Close();
            //}
            View.Close();
        }

        /// <summary>
        /// Отправить сообщение клиенту и на терминал о подаче и изменении заявки
        /// </summary>
        /// <param name="request"></param>
        /// <param name="requestStatus"></param>
        private void SendNotifacationsByRequest(Connect connect, RequestContainersPlacement request, Enums.ERequestStatus requestStatus)
        {
            // Отправляем письмо клиенту и на терминал
            string messbody = "";
            string titleClient = "";
            string titleTerminal = "";
            string messClient = "";
            string messTerminal = "";
            switch (requestStatus)
            {
                case Enums.ERequestStatus.Подана:
                    titleClient = $"От Вашего имени подана заявка № {request.RequestNumber} {request.RequestType.Name.ToLower()} / The Application No.{request.RequestNumber} " +
                        $"for the placement of containers has been submitted on your behalf";
  
                      titleTerminal = String.Format("На Ваш терминал {0} подана заявка № {1} {2}!", request.Terminal.Name,
                        request.RequestNumber, request.RequestType.Name.ToLower());
                    messClient = String.Format(@"Здравствуйте!<br>
От Вашего имени на терминал {0} подана заявка № {1} {2} по следующим контейнерам:",  
request.Terminal.Name, request.RequestNumber, request.RequestType.Name.ToLower());
                    messTerminal = String.Format(@"Здравствуйте!<br>
На терминал {0} от клиента {1} подана заявка № {2} {3} по следующим контейнерам:", request.Terminal.Name, request.Client.Name,
request.RequestNumber, request.RequestType.Name.ToLower());
                    break;
                case Enums.ERequestStatus.Отредактирована:
                    titleClient =$"В поданную Вами ранее заявку № {request.RequestNumber} от {request.RequestStartDate.ToShortDateString()} " +
                        $"{request.RequestType.Name.ToLower()} были внесены изменения / Your Application No. {request.RequestNumber} " +
                        $"dated {request.RequestStartDate.ToShortDateString()} for the placement of containers has been changed";

                    titleTerminal = String.Format("В поданную ранее заявку  № {0} от {1} {2} на терминал {3} были внесены изменения!", 
                        request.RequestNumber, request.RequestStartDate.ToShortDateString(), request.RequestType.Name.ToLower(), request.Terminal.Name);
                    messClient = String.Format(@"Здравствуйте!<br> 
В поданную Вами ранее заявку № {0} {1} на терминал {2} были внесены изменения.<br>Актуальная информация по заявке представлена ниже:", 
request.RequestNumber, request.RequestType.Name.ToLower(), request.Terminal.Name);
                    messTerminal = String.Format(@"Здравствуйте!<br> 
В поданную ранее заявку  № {0} от {1} {2} на терминал {3} от клиента {4} были внесены изменения!<br>
Актуальная информация по заявке представлена ниже:",
                        request.RequestNumber, request.RequestStartDate.ToShortDateString(), 
                        request.RequestType.Name.ToLower(), request.Terminal.Name, request.Client.Name);
                    break;
                case Enums.ERequestStatus.Обработка:
                    titleClient = $"Ваша заявка № {request.RequestNumber} {request.RequestType.Name.ToLower()} принята в обработку/ " +
                        $"Your Application No.{request.RequestNumber} " +
                        $"for the placement of containers has been processed";

                    //titleTerminal = String.Format("На Ваш терминал {0} подана заявка № {1} {2}!", request.Terminal.Name,
                    //  request.RequestNumber, request.RequestType.Name.ToLower());
                    messClient = String.Format(@"Здравствуйте!<br>
Ваша заявка № {1} {2} на терминал {0} принята в обработку. Контейнеры из заявки:",
request.Terminal.Name, request.RequestNumber, request.RequestType.Name.ToLower());
//                    messTerminal = String.Format(@"Здравствуйте! 
//На терминал {0} от клиента {1} подана заявка № {2} {3} по следующим контейнерам:", request.Terminal.Name, request.Client.Name,
//request.RequestNumber, request.RequestType.Name.ToLower());
                    break;
            }
            
            
            foreach (RequestContainerInfo requestContainerInfo in request.RequestContainerInfos)
            {
                messbody += "<br>" + Environment.NewLine;
                messbody += String.Format(@"Контейнер № {0}, тип: {1}",
                    requestContainerInfo.Number, requestContainerInfo.ContainerType.Name);
                if (requestContainerInfo.ArrivalPlanDateFrom != DateTime.MinValue && requestContainerInfo.ArrivalPlanDateTo != DateTime.MinValue)
                    messbody += String.Format(@", ожидаемые даты: {0} - {1}.",
                   requestContainerInfo.ArrivalPlanDateFrom.ToShortDateString(), requestContainerInfo.ArrivalPlanDateTo.ToShortDateString());
                if (requestContainerInfo.RequestDriverInfo != null)
                {
                    try
                    {
                        messbody += "<br>" + Environment.NewLine;
                        messbody += String.Format(@"Водитель: {0}, тел.: {1}, марка ТС: {2}", requestContainerInfo.RequestDriverInfo.DriverName,
                        requestContainerInfo.RequestDriverInfo.DriverPhoneNumber,
                        requestContainerInfo.RequestDriverInfo.TransportMark.Name);
                    }
                    catch { }
                }
            }
            DevExpress.ExpressApp.View view = this.View; // A required View object instance (e.g., obtained from a ViewController or manually created via the XafApplication.CreateXXXView methods).
            DevExpress.ExpressApp.Web.WebApplication webApplication = (WebApplication)Application; // A current WebApplication instance (e.g., obtained from a ViewController or via the static WebApplication.Instance property).
            DevExpress.ExpressApp.Web.IHttpRequestManager requestManager = webApplication.RequestManager;
            DevExpress.ExpressApp.ViewShortcut shortcut = view.CreateShortcut();
            string fullViewURL = string.Format("{0}#{1}", System.Web.HttpContext.Current.Request.Url.AbsoluteUri, requestManager.GetQueryString(shortcut));
            //var link = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, request.GetType().FullName + "_" + request.Oid.ToString());
            messbody += Environment.NewLine;
            messbody += $"<br><a href={fullViewURL}>Ссылка на заявку</a>";
            //messbody += "Ссылка на заявку: " + fullViewURL;
            messbody += Environment.NewLine;
            messbody += "<br><br>Всего Вам доброго и хорошего настроения!<br><br>";
            string path = "";


            // создаем английский текст
            string messClienteng = "";
            messClienteng = Environment.NewLine + Environment.NewLine + Environment.NewLine;
            try
            {

                messClienteng += "Hello!<br>" + Environment.NewLine;
                string requestType = "for container placement";
                if (request.RequestType.Name.ToLower() == "на выдачу контейнеров")
                    requestType = "for issuing containers";
                switch (requestStatus)
                {
                    case Enums.ERequestStatus.Подана:
                        messClienteng += $"On your behalf, application No. {request.RequestNumber} {requestType} " +
                        $"was submitted to terminal {request.Terminal.Name} for the following containers:";
                        break;
                    case Enums.ERequestStatus.Отредактирована:
                        messClienteng += $"Changes have been made to your earlier application No. {request.RequestNumber} {requestType} " +
                            $"at terminal {request.Terminal.Name}.<br>Actual information on the application is presented below:";
                        break;
                    case Enums.ERequestStatus.Обработка:
                        messClienteng +=
                            $"Your Application No.{request.RequestNumber} {requestType}" +
                        $" at terminal {request.Terminal.Name} has been processed.<br>" +
                        "Actual information on the application is presented below:";
                        break;
                }
                foreach (RequestContainerInfo requestContainerInfo in request.RequestContainerInfos)
                {
                    messClienteng += "<br>" + Environment.NewLine;
                    messClienteng += String.Format(@"The Container No {0}, type: {1}",
                        requestContainerInfo.Number, requestContainerInfo.ContainerType.Name);
                    if (requestContainerInfo.ArrivalPlanDateFrom != DateTime.MinValue && requestContainerInfo.ArrivalPlanDateTo != DateTime.MinValue)
                        messClienteng += String.Format(@", expected dates: {0} - {1}.",
                       requestContainerInfo.ArrivalPlanDateFrom.ToShortDateString(), requestContainerInfo.ArrivalPlanDateTo.ToShortDateString());
                    if (requestContainerInfo.RequestDriverInfo != null)
                    {
                        try
                        {
                            messClienteng += "<br>" + Environment.NewLine;
                            messClienteng += String.Format(@"The Driver: {0}, phone.: {1}, car: {2}", requestContainerInfo.RequestDriverInfo.DriverName,
                            requestContainerInfo?.RequestDriverInfo?.DriverPhoneNumber?? "",
                            requestContainerInfo?.RequestDriverInfo?.TransportType?.Name?? "" + " " + requestContainerInfo?.RequestDriverInfo?.TransportNumber ?? "");
                        }
                        catch { }
                    }
                }
            }
            catch { }
            messClienteng += Environment.NewLine;
            messClienteng += $"<br><a href={fullViewURL}>The link</a>";
            //messClienteng += "The link: " + fullViewURL;
            messClienteng += Environment.NewLine;
            messClienteng += Environment.NewLine + "<br><br>All the best and good mood!";

            // отсылаем клиенту
            SystemDir.MailMessageLogic.SendMailMessage(connect, titleClient, messClient + messbody + messClienteng, path, request.Client.Email);

            // отсылаем на терминал
            string mails = "";
            mails = request?.Terminal?.TerminalOrg?.Email ?? "";
            try
            {
                foreach (Employee emp in request?.Terminal?.Employees)
                {
                    try
                    {
                        if(!mails.Contains(emp.Email))
                            mails += "," + emp.Email ?? "";
                    }
                    catch { }
                }
            }
            catch { }
            if (requestStatus != Enums.ERequestStatus.Обработка)
                SystemDir.MailMessageLogic.SendMailMessage(connect, titleTerminal, messTerminal + messbody, path, mails);

            //// отсылаем на терминал
            //SystemDir.MailMessageLogic.SendMailMessage(titleTerminal, messTerminal + messbody, path, request.Terminal.TerminalOrg.Email, "donotreply@it-cis.ru");

        }

        /// <summary>
        /// Отправить сообщение клиенту и на терминал о подаче и изменении заявки
        /// </summary>
        /// <param name="request"></param>
        /// <param name="requestStatus"></param>
        private void SendNotifacationsByRequestExtradition(Connect connect, RequestContainersExtradition request, Enums.ERequestStatus requestStatus)
        {
            // Отправляем письмо клиенту и на терминал
            string messbody = "";
            string titleClient = "";
            string titleTerminal = "";
            string messClient = "";
            string messTerminal = "";
            switch (requestStatus)
            {
                case Enums.ERequestStatus.Подана:
                    titleClient = $"От Вашего имени подана заявка № {request.RequestNumber} {request.RequestType.Name.ToLower()} / The Application No.{request.RequestNumber} " +
                        $"for the issue of containers has been submitted on your behalf";

                    titleTerminal = String.Format("На Ваш терминал {0} подана заявка № {1} {2}!", request.Terminal.Name,
                        request.RequestNumber, request.RequestType.Name.ToLower());
                    messClient = String.Format(@"Здравствуйте!<br> 
От Вашего имени на терминал {0} подана заявка № {1} {2} по следующим контейнерам:",
request.Terminal.Name, request.RequestNumber, request.RequestType.Name.ToLower());
                    messTerminal = String.Format(@"Здравствуйте!<br>
На терминал {0} от клиента {1} подана заявка № {2} {3} по следующим контейнерам:", 
request.Terminal.Name, request.Client.Name, request.RequestNumber, request.RequestType.Name.ToLower());
                    break;
                case Enums.ERequestStatus.Отредактирована:
                    titleClient = $"В поданную Вами ранее заявку № {request.RequestNumber} от {request.RequestStartDate.ToShortDateString()} " +
                        $"{request.RequestType.Name.ToLower()} были внесены изменения / Your Application No. {request.RequestNumber} " +
                        $"dated {request.RequestStartDate.ToShortDateString()} for the issue of containers has been changed";

                    titleTerminal = String.Format("В поданную ранее заявку  № {0} от {1} {2} с терминала {3} были внесены изменения!",
                        request.RequestNumber, request.RequestStartDate.ToShortDateString(), request.RequestType.Name.ToLower(), request.Terminal.Name);
                    messClient = String.Format(@"Здравствуйте!<br> 
В подаданную Вами ранее заявку № {0} {1} с терминала {2} были внесены изменения.<br>Актуальная информация по заявке представлена ниже:",
request.RequestNumber, request.RequestType.Name.ToLower(), request.Terminal.Name);
                    messTerminal = String.Format(@"Здравствуйте!<br> 
В поданную ранее заявку  № {0} от {1} {2} с терминала {3} от клиента {4} были внесены изменения!<br>
Актуальная информация по заявке представлена ниже:",
                        request.RequestNumber, request.RequestStartDate.ToShortDateString(),
                        request.RequestType.Name.ToLower(), request.Terminal.Name, request.Client.Name);
                    break;
                case Enums.ERequestStatus.Обработка:
                    titleClient = $"Ваша заявка № {request.RequestNumber} {request.RequestType.Name.ToLower()} принята в обработку/ " +
                        $"Your Application No.{request.RequestNumber} " +
                        $"for the placement of containers has been processed";
                    messClient = String.Format(@"Здравствуйте!<br> 
Ваша заявка № {1} {2} на терминал {0} принята в обработку. Контейнеры из заявки:",
request.Terminal.Name, request.RequestNumber, request.RequestType.Name.ToLower());
                    break;
            }


            foreach (RequestContainerExtraditionInfo requestContainerInfo in request.RequestContainerInfos)
            {
                messbody += "<br>" + Environment.NewLine;
                if(requestContainerInfo.Container!= null)
                    messbody += String.Format(@"Контейнер № {0}, тип: {1}",
                    requestContainerInfo.Container?.ContainerNumber ?? "", requestContainerInfo.Container?.ContainerType?.Name ?? "");
                else
                    messbody += String.Format(@"Контейнер № {0}, тип: {1}",
                    requestContainerInfo.Number ?? "", requestContainerInfo.ContainerType?.Name ?? "");
                //messbody += String.Format(@"Контейнер тип: {0}",
                //        requestContainerInfo.ContainerType?.Name ?? "");
                if (requestContainerInfo.ArrivalPlanDateFrom != DateTime.MinValue && requestContainerInfo.ArrivalPlanDateTo != DateTime.MinValue)
                    messbody += String.Format(@", ожидаемые даты: {0} - {1}.",
                   requestContainerInfo.ArrivalPlanDateFrom.ToShortDateString(), requestContainerInfo.ArrivalPlanDateTo.ToShortDateString());
                if (requestContainerInfo.RequestDriverInfo != null)
                {
                    try
                    {
                        messbody += "<br>" + Environment.NewLine;
                        messbody += String.Format(@"Водитель: {0}, тел.: {1}, марка ТС: {2}", requestContainerInfo.RequestDriverInfo.DriverName,
                        requestContainerInfo.RequestDriverInfo.DriverPhoneNumber,
                        requestContainerInfo.RequestDriverInfo.TransportMark.Name);
                    }
                    catch { }
                }
            }
            DevExpress.ExpressApp.View view = this.View; // A required View object instance (e.g., obtained from a ViewController or manually created via the XafApplication.CreateXXXView methods).
            DevExpress.ExpressApp.Web.WebApplication webApplication = (WebApplication)Application; // A current WebApplication instance (e.g., obtained from a ViewController or via the static WebApplication.Instance property).
            DevExpress.ExpressApp.Web.IHttpRequestManager requestManager = webApplication.RequestManager;
            DevExpress.ExpressApp.ViewShortcut shortcut = view.CreateShortcut();
            string fullViewURL = string.Format("{0}#{1}", System.Web.HttpContext.Current.Request.Url.AbsoluteUri, requestManager.GetQueryString(shortcut));
            //var link = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, request.GetType().FullName + "_" + request.Oid.ToString());
            messbody += "<br>" + Environment.NewLine;
            messbody += $"<br><a href={fullViewURL}>Ссылка на заявку</a>";
            //messbody += "Ссылка на заявку: " + fullViewURL;
            messbody += Environment.NewLine;
            messbody += "<br><br>Всего Вам доброго и хорошего настроения!<br><br>";
            string path = "";


            // создаем английский текст
            string messClienteng = "";
            messClienteng = Environment.NewLine + Environment.NewLine + Environment.NewLine;
            try
            {

                messClienteng += "Hello!<br>" + Environment.NewLine;
                string requestType = "for container placement";
                if (request.RequestType.Name.ToLower() == "на выдачу контейнеров")
                    requestType = "for issuing containers";
                switch (requestStatus)
                {
                    case Enums.ERequestStatus.Подана:
                        messClienteng += $"On your behalf, application No. {request.RequestNumber} {requestType} " +
                        $"was submitted to terminal {request.Terminal.Name} for the following containers:";
                        break;
                    case Enums.ERequestStatus.Отредактирована:
                        messClienteng += $"Changes have been made to your earlier application No. {request.RequestNumber} {requestType} " +
                            $"at terminal {request.Terminal.Name}. Actual information on the application is presented below:";
                        break;
                    case Enums.ERequestStatus.Обработка:
                        messClienteng +=
                            $"Your Application No.{request.RequestNumber} {requestType}" +
                        $" at terminal {request.Terminal.Name} has been processed." +
                        "Actual information on the application is presented below:";
                        break;
                }
                foreach (RequestContainerExtraditionInfo requestContainerInfo in request.RequestContainerInfos)
                {
                    messClienteng += "<br>" + Environment.NewLine;

                    if (requestContainerInfo.Container != null)
                        messClienteng += String.Format(@"The Container No {0}, type: {1}",
                        requestContainerInfo.Container?.ContainerNumber ?? "", requestContainerInfo.Container?.ContainerType?.Name ?? "");
                    else
                        messClienteng += String.Format(@"The Container No {0}, type: {1}",
                        requestContainerInfo.Number ?? "", requestContainerInfo.ContainerType?.Name ?? "");


                    if (requestContainerInfo.ArrivalPlanDateFrom != DateTime.MinValue && requestContainerInfo.ArrivalPlanDateTo != DateTime.MinValue)
                        messClienteng += String.Format(@", expected dates: {0} - {1}.",
                       requestContainerInfo.ArrivalPlanDateFrom.ToShortDateString(), requestContainerInfo.ArrivalPlanDateTo.ToShortDateString());
                    if (requestContainerInfo.RequestDriverInfo != null)
                    {
                        try
                        {
                            messClienteng += "<br>" + Environment.NewLine;
                            messClienteng += String.Format(@"The Driver: {0}, phone.: {1}, car: {2}", requestContainerInfo.RequestDriverInfo.DriverName,
                            requestContainerInfo?.RequestDriverInfo?.DriverPhoneNumber ?? "",
                            requestContainerInfo?.RequestDriverInfo?.TransportType?.Name ?? "" + " " + requestContainerInfo?.RequestDriverInfo?.TransportNumber ?? "");
                        }
                        catch { }
                    }
                }
            }
            catch { }
            messClienteng += "<br>" + Environment.NewLine;
            messClienteng += $"<br><a href={fullViewURL}>The link</a>";
            //messClienteng += "The link: " + fullViewURL;
            messClienteng += Environment.NewLine;
            messClienteng += Environment.NewLine + "<br><br>All the best and good mood!";

            
            // отсылаем клиенту
            SystemDir.MailMessageLogic.SendMailMessage(connect, titleClient, messClient + messbody + messClienteng, path, request?.Client?.Email?? "");

            // отсылаем на терминал
            string mails = "";
            mails = request?.Terminal?.TerminalOrg?.Email?? "";
            try
            {
                foreach (Employee emp in request?.Terminal?.Employees)
                {
                    try
                    {
                        mails += "," + emp.Email?? "";
                    }
                    catch { }
                }
            }
            catch { }
            if (requestStatus != Enums.ERequestStatus.Обработка)
                SystemDir.MailMessageLogic.SendMailMessage(connect, titleTerminal, messTerminal + messbody, path, mails);
        }

        /// <summary>
        /// Изменить статус заявки на "В обработке"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestProcessingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = View.ObjectSpace;
            Connect connect = Connect.FromObjectSpace(os);
            RequestBase request = View.CurrentObject as RequestBase;
            request.RequestStatus = Enums.ERequestStatus.Обработка;
            request.Save();
            os.CommitChanges();

            Messanger.ShowMessage(Application, "", "Заявка принята в обработку", InformationType.Info);
            if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersPlacement")
            {
                // отправляем сообщения
                SendNotifacationsByRequest(connect, (RequestContainersPlacement)request, request.RequestStatus);
            }
            if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersExtradition")
            {
                // отправляем сообщения
                SendNotifacationsByRequestExtradition(connect, (RequestContainersExtradition)request, request.RequestStatus);
            }
            
        }

        /// <summary>
        /// Изменить статус на "На редактирование"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestOnEditAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = View.ObjectSpace;
            RequestBase request = View.CurrentObject as RequestBase;
            request.RequestStatus = Enums.ERequestStatus.Редактирование;
            request.Save();
            os.CommitChanges();
        }

        /// <summary>
        /// Сохранение данных по заявке после редактирования
        /// изменяется статус заявки, что она была отредактирована
        /// отправляются повторные сообщения клиенту и на терминал, что заявка была изменена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestFinishEditAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // сохраняем данные
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = View.ObjectSpace;
            Connect connect = Connect.FromObjectSpace(os);
            if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersPlacement")
            {
                RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;
                if (request.RequestStatus == Enums.ERequestStatus.Редактирование)
                {
                    if (request.RequestContainerInfos.Count < 1)
                        throw new Exception("Необходимо указать хотя бы один контейнер!");
                    //if (request.RequestDriverInfos.Count < 1)
                    //    throw new Exception("Необходимо указать информацию по хотя бы одному водителю!");
                    request.RequestStatus = Enums.ERequestStatus.Отредактирована;
                    request.Save();
                    os.CommitChanges();
                    Messanger.ShowMessage(Application, "", "Заявка отредактирована", InformationType.Info);
                    // отправляем сообщения
                    SendNotifacationsByRequest(connect, request, request.RequestStatus);
                    
                }
            }
            if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersExtradition")
            {
                RequestContainersExtradition request = View.CurrentObject as RequestContainersExtradition;
                if (request.RequestStatus == Enums.ERequestStatus.Редактирование)
                {
                    if (request.RequestContainerInfos.Count < 1)
                        throw new Exception("Необходимо указать хотя бы один контейнер!");
                    //if (request.RequestDriverInfos.Count < 1)
                    //    throw new Exception("Необходимо указать информацию по хотя бы одному водителю!");
                    request.RequestStatus = Enums.ERequestStatus.Отредактирована;
                    request.Save();
                    os.CommitChanges();
                    Messanger.ShowMessage(Application, "", "Заявка отредактирована", InformationType.Info);
                    // отправляем сообщения
                    SendNotifacationsByRequestExtradition(connect, request, request.RequestStatus);
                    
                }
            }
            os.CommitChanges();
            //View.Close();
        }
    }
}
