﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using TerminalSystem2.Classifiers;
using TerminalSystem2.Terminals;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.Clients;
using TerminalSystem2.SystemDir;
using System.Collections.Generic;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Tarification;
using TerminalSystem2.Containers;
using Organization = TerminalSystem2.OrgStructure.Organization;

namespace TerminalSystem2.Module.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
    public class Updater : ModuleUpdater {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }
        PermissionPolicyUser TerminalUser1;
        PermissionPolicyUser ClientUser1;
        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();

            PermissionPolicyUser userAdmin = ObjectSpace.FindObject<PermissionPolicyUser>(new BinaryOperator("UserName", "DevAdmin"));
            if (userAdmin == null)
            {
                userAdmin = ObjectSpace.CreateObject<PermissionPolicyUser>();
                userAdmin.UserName = "DevAdmin";
                // Set a password if the standard authentication type is used
                userAdmin.SetPassword("123");
            }

            // If a role with the Administrators name doesn't exist in the database, create this role
            PermissionPolicyRole adminRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Administrators"));
            if(adminRole == null) {
                adminRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                adminRole.Name = "Administrators";
            }
            adminRole.IsAdministrative = true;
            userAdmin.Roles.Add(adminRole);


            //SetTestInitialData(userAdmin);
            CreateInitialData();
            CreateFirstTerminalOrg();
            CreateFirstTariffBase();

            CreateTerminalEmployeeRole();
            CreateClientRole();
            ObjectSpace.CommitChanges(); //This line persists created object(s).
        }
        #region Заполнение начальных данных
        private void CreateFirstTerminalOrg()
        {
            Connect connect = Connect.FromObjectSpace(ObjectSpace);
            var terminalOrgList = connect.FindObjects<TerminalOrg>(x => true);
            if(terminalOrgList.Count<TerminalOrg>() == 0)
            {
                TerminalOrg terminalOrg = connect.CreateObject<TerminalOrg>();
                terminalOrg.Save();
            }
            ObjectSpace.CommitChanges();
            //TerminalOrg terminalOrg1 = connect.FindFirstObject<TerminalOrg>(x=>true);
            //var employeeList = connect.FindObjects<Employee>(x => true);
            //if (employeeList.Count<Employee>() == 0)
            //{
            //    Employee employee = connect.CreateObject<Employee>();
            //    employee.isDirector = true;
            //    employee.SysUser = TerminalUser1;
            //    terminalOrg1.Employees.Add(employee);
            //    employee.Save();
            //}
            //ObjectSpace.CommitChanges();
        }
        private void CreateFirstTariffBase()
        {
            Connect connect = Connect.FromObjectSpace(ObjectSpace);
            var tariffBaseList = connect.FindObjects<TariffBaseTerminal>(x => true);
            if (tariffBaseList.Count<TariffBaseTerminal>() == 0)
            {
                TariffBaseTerminal tariffBase = connect.CreateObject<TariffBaseTerminal>();
                tariffBase.Save();
            }

            ObjectSpace.CommitChanges();
        }
        private void CreateInitialData()
        {
            // Виды заявок
            string[] requestTypes = { "На размещение контейнеров", "На выдачу контейнеров" };
            //, "На ремонт контейнеров", "На перевод в другой сток", "На перестановку контейнеров", "На осмотр контейнеров",
            //                         "На предоставление информации по контейнерам"};
            foreach (string rt in requestTypes)
                SetRequestType(rt);

            // Габариты контейнеров
            string[] containerSizes = { "20, 20", "30, 20", "40, 40" };
            foreach (string cts in containerSizes)
                SetContainerTypeSize(cts);

            //// подвиды контейнеров
            //string[] containerSubTypesString = { "Standart",
            //                       "High Cube (HC)", "Double Door (DD)", "Side Door (SD)", "Ref", "Деревянный", "Duocon",
            //                        "Hard Top", "Open Side", "Open Top", "Flat Rack"
            //                        };
            //foreach (string cst in containerSubTypesString)
            //    SetContainerSubTypes(cst);

            // валюта
            string[] currencys = { "RU;Рубль", "USD;Доллар США", "EUR;Евро" };
            foreach (string c in currencys)
                SetCurrencys(c);

            // единицы измерения
            string[] unitKinds = { "операция", "сутки" };
            //string[] unitKinds = { "операция", "сутки", "единица", "тонна", "час", "к-т", "шт.", "пал(тн)" };
            foreach (string uk in unitKinds)
                SetUnitKinds(uk);

            // Виды услуг
            string[] serviceTypes = { "Крановая операция", "Хранение контейнера", "Фотографирование контейнера", "Осмотр контейнера клиентом" };
            foreach (string st in serviceTypes)
                SetServiceTypes(st);

            //setContainerTypes();
        }
        private void SetRequestType(string requestTypeString)
        {
            dRequestType RequestType = ObjectSpace.FindObject<dRequestType>(
                new BinaryOperator("Name", requestTypeString));
            if (RequestType == null)
            {
                RequestType = ObjectSpace.CreateObject<dRequestType>();
                RequestType.Name = requestTypeString;
            }
            RequestType.Save();
            ObjectSpace.CommitChanges();
            RequestType = null;
        }

        private void SetContainerSubTypes(string containerSubTypeString)
        {
            dContainerSubType containerSubType = ObjectSpace.FindObject<dContainerSubType>(
                new BinaryOperator("Name", containerSubTypeString));
            if (containerSubType == null)
            {
                containerSubType = ObjectSpace.CreateObject<dContainerSubType>();
                containerSubType.Name = containerSubTypeString;
            }
            containerSubType.Save();
            ObjectSpace.CommitChanges();
            containerSubType = null;
        }
        private void SetContainerTypeSize(string containerTypeSizeString)
        {
            int containerTypeSize = Convert.ToInt16(containerTypeSizeString.Split(',')[0]);
            int containerCategorySize = Convert.ToInt16(containerTypeSizeString.Split(',')[1]);
            dContainerTypeSize containerSize = ObjectSpace.FindObject<dContainerTypeSize>(
                new BinaryOperator("ContainerTypeSize", containerTypeSize));
            if (containerSize == null)
            {
                containerSize = ObjectSpace.CreateObject<dContainerTypeSize>();
                containerSize.ContainerTypeSize = containerTypeSize;
                if (containerCategorySize == 20)
                    containerSize.ContainerCategorySize = Enums.EContainerCategorySize.twenty;
                else
                    containerSize.ContainerCategorySize = Enums.EContainerCategorySize.forty;
            }
            containerSize.Save();
            ObjectSpace.CommitChanges();
            containerSize = null;
        }

        private void setContainerTypes()
        {
            IList<dContainerTypeSize> containerSizes = ObjectSpace.GetObjects<dContainerTypeSize>();
            IList<dContainerSubType> containerSubTypes = ObjectSpace.GetObjects<dContainerSubType>();
            Connect connect = Connect.FromObjectSpace(ObjectSpace);
            foreach (dContainerTypeSize containerTypeSize in containerSizes)
            {
                foreach (dContainerSubType containerSubType in containerSubTypes)
                {
                    dContainerType containerType = connect.FindFirstObject<dContainerType>(mc => mc.ContainerTypeSize == containerTypeSize &&
                mc.ContainerSubType == containerSubType);
                    if (containerType == null)
                    {
                        containerType = ObjectSpace.CreateObject<dContainerType>();
                        containerType.ContainerTypeSize = containerTypeSize;
                        containerType.ContainerSubType = containerSubType;
                        containerType.Save();
                        ObjectSpace.CommitChanges();
                    }
                }
            }
        }
        private void SetCurrencys(string currencyString)
        {
            string currencyStringCode = currencyString.Split(';')[0];
            string currencyStringName = currencyString.Split(';')[1];
            dCurrency currency =
                ObjectSpace.FindObject<dCurrency>(
                new BinaryOperator("Code", currencyStringCode));
            if (currency == null)
            {
                currency = ObjectSpace.CreateObject<dCurrency>();
                currency.Code = currencyStringCode;
                currency.Name = currencyStringName;
            }
            currency.Save();
            ObjectSpace.CommitChanges();
            currency = null;
        }
        private void SetUnitKinds(string typeString)
        {
            dUnitKind type = ObjectSpace.FindObject<dUnitKind>(
                new BinaryOperator("Name", typeString));
            if (type == null)
            {
                type = ObjectSpace.CreateObject<dUnitKind>();
                type.Name = typeString;
            }
            type.Save();
            ObjectSpace.CommitChanges();
            type = null;
        }
        private void SetServiceTypes(string serviceTypeString)
        {
            dServiceType ServiceType = ObjectSpace.FindObject<dServiceType>(
                new BinaryOperator("Name", serviceTypeString));
            if (ServiceType == null)
            {
                ServiceType = ObjectSpace.CreateObject<dServiceType>();
                ServiceType.Name = serviceTypeString;
            }
            ServiceType.Save();
            ObjectSpace.CommitChanges();
            ServiceType = null;
        }
        #endregion

        /// <summary>
        /// создаем начальные данные для теста:
        /// Терминал, терминальную организацию, пользователя
        /// клиента, сотрудника клиента
        /// связываем все между собой
        /// </summary>
        private void SetTestInitialData(PermissionPolicyUser userAdmin)
        {
            Terminal terminal = ObjectSpace.FindObject<Terminal>(new BinaryOperator("Name", "Перепечино"));
            if (terminal == null)
            {
                terminal = ObjectSpace.CreateObject<Terminal>();
                terminal.Name = "Перепечино";
                terminal.SimpleStructureFlag = true;
                terminal.Save();
            }
            TerminalOrg terminalOrg = ObjectSpace.FindObject<TerminalOrg>(new BinaryOperator("Name", "ООО Футовик"));
            if (terminalOrg == null)
            {
                terminalOrg = ObjectSpace.CreateObject<TerminalOrg>();
                terminalOrg.Name = "ООО Футовик";
                terminalOrg.Email = "info@it-cis.ru";
                terminalOrg.Terminals.Add(terminal);
                terminalOrg.Save();
            }
            Employee terminalEmployee = ObjectSpace.FindObject<Employee>(new BinaryOperator("BriefName", "Иванов"));
            if (terminalOrg == null)
            {
                terminalEmployee = ObjectSpace.CreateObject<Employee>();
                terminalEmployee.BriefName = "Алексей";
                terminalEmployee.SysUser = userAdmin;
                terminalEmployee.Terminals.Add(terminal);
                terminalEmployee.TerminalOrg = terminalOrg;
                terminalOrg.Save();
            }
            Client client = ObjectSpace.FindObject<Client>(new BinaryOperator("Name", "ООО ЦИС"));
            if (client == null)
            {
                client = ObjectSpace.CreateObject<Client>();
                client.Name = "ООО ЦИС";
                client.Email = "info@it-cis.ru";
                //client.Terminals.Add(terminal);
                
                client.Save();
            }
            ClientEmployee clientEmployee = ObjectSpace.FindObject<ClientEmployee>(new BinaryOperator("BriefName", "Дмитрий"));
            if (clientEmployee == null)
            {
                clientEmployee = ObjectSpace.CreateObject<ClientEmployee>();
                clientEmployee.BriefName = "Дмитрий";
                //clientEmployee.SysUser = ClientUser1;
                clientEmployee.Email = "dmitry@it-cis.ru";
                clientEmployee.Client = client;
                clientEmployee.Save();
            }
            Stock stock = ObjectSpace.FindObject<Stock>(new BinaryOperator("Name", "Сток ЦИС"));
            if (stock == null)
            {
                stock = ObjectSpace.CreateObject<Stock>();
                stock.Name = "Сток ЦИС";
                stock.Client = client;
                stock.Terminal = terminal;
                stock.Save();
            }
            client = null;
            clientEmployee = null;
            stock = null;

            client = ObjectSpace.FindObject<Client>(new BinaryOperator("Name", "ООО Логичное решение"));
            if (client == null)
            {
                client = ObjectSpace.CreateObject<Client>();
                client.Name = "ООО Логичное решение";
                client.Email = "info@it-cis.ru";
                //client.Terminals.Add(terminal);

                client.Save();
            }
            clientEmployee = ObjectSpace.FindObject<ClientEmployee>(new BinaryOperator("BriefName", "Стас"));
            if (clientEmployee == null)
            {
                clientEmployee = ObjectSpace.CreateObject<ClientEmployee>();
                clientEmployee.BriefName = "Стас";
                //clientEmployee.SysUser = ClientUser2;
                clientEmployee.Email = "dmitry@it-cis.ru";
                clientEmployee.Client = client;
                clientEmployee.Save();
            }
            stock = ObjectSpace.FindObject<Stock>(new BinaryOperator("Name", "Сток ЛР"));
            if (stock == null)
            {
                stock = ObjectSpace.CreateObject<Stock>();
                stock.Name = "Сток ЛР";
                stock.Client = client;
                stock.Terminal = terminal;
                stock.Save();
            }

            ObjectSpace.CommitChanges();
        }

        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }
        private PermissionPolicyRole CreateDefaultRole() {
            PermissionPolicyRole defaultRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Default"));
            if(defaultRole == null) {
                defaultRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                defaultRole.Name = "Default";

				defaultRole.AddObjectPermission<PermissionPolicyUser>(SecurityOperations.ReadOnlyAccess, "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
				defaultRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
				defaultRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "StoredPassword", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<PermissionPolicyRole>(SecurityOperations.Read, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
				defaultRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.Create, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.Create, SecurityPermissionState.Allow);                
            }
            return defaultRole;
        }
        private PermissionPolicyRole CreateClientRole()
        {
            PermissionPolicyRole clientRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Сотрудник клиента"));
            //if (clientRole != null)
            //    clientRole.Delete();
            //ObjectSpace.CommitChanges();
            if (clientRole == null)
            {
                clientRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                //}
                clientRole.Name = "Сотрудник клиента";
                clientRole.CanEditModel = false;
                clientRole.IsAdministrative = false;
                clientRole.PermissionPolicy = SecurityPermissionPolicy.DenyAllByDefault;
                clientRole.AddObjectPermission<PermissionPolicyUser>(SecurityOperations.Read, "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                clientRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                clientRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "StoredPassword", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                //clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
                clientRole.AddTypePermissionsRecursively<PermissionPolicyRole>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
                clientRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);

                clientRole.AddTypePermission<TerminalOrg>(SecurityOperations.Read, SecurityPermissionState.Allow);

                clientRole.AddTypePermission<RequestContainersPlacement>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<RequestContainersPlacement>(SecurityOperations.Delete, SecurityPermissionState.Deny);
                clientRole.AddTypePermission<RequestContainerInfo>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<RequestContainerInfo>(SecurityOperations.Delete, SecurityPermissionState.Deny);
                
                clientRole.AddTypePermission<RequestContainersExtradition>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<RequestContainersExtradition>(SecurityOperations.Delete, SecurityPermissionState.Deny);
                clientRole.AddTypePermission<RequestContainerExtraditionInfo>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<RequestContainerExtraditionInfo>(SecurityOperations.Delete, SecurityPermissionState.Deny);

                clientRole.AddTypePermission<RequestDriverInfo>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<RequestDriverInfo>(SecurityOperations.Delete, SecurityPermissionState.Deny);

                clientRole.AddTypePermission<dContainerType>(SecurityOperations.ReadOnlyAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<dRequestType>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<dContainerTypeSize>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<dContainerSubType>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<dTransportMark>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<dTransportMark>(SecurityOperations.Delete, SecurityPermissionState.Deny);
                clientRole.AddTypePermission<dTransportType>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<dTransportType>(SecurityOperations.Delete, SecurityPermissionState.Deny);

                clientRole.AddTypePermission<Container>(SecurityOperations.ReadOnlyAccess, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<Client>(SecurityOperations.Read, SecurityPermissionState.Allow);

                clientRole.AddTypePermission<ContainerHistory>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<Stock>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<Terminal>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<TerminalRow>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<TerminalSection>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<TerminalZone>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<TerminalTier>(SecurityOperations.Read, SecurityPermissionState.Allow);
                
                clientRole.AddTypePermission<dState>(SecurityOperations.Read, SecurityPermissionState.Allow);

                clientRole.AddTypePermission<Employee>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<ServiceDone>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<ClientEmployee>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<Organization>(SecurityOperations.Read, SecurityPermissionState.Allow);
                clientRole.AddTypePermission<ContainerPhoto>(SecurityOperations.ReadOnlyAccess, SecurityPermissionState.Allow);

            }
            return clientRole;
        }

        private PermissionPolicyRole CreateTerminalRole()
        {
            PermissionPolicyRole clientRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Владелец терминала"));
            //if (clientRole != null)
            //    clientRole.Delete();
            //ObjectSpace.CommitChanges();
            if (clientRole == null)
                //{
                clientRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
            clientRole.Name = "Владелец терминала";
            clientRole.CanEditModel = false;
            clientRole.IsAdministrative = false;
            clientRole.PermissionPolicy = SecurityPermissionPolicy.AllowAllByDefault;
            //clientRole.AddObjectPermission<PermissionPolicyUser>(SecurityOperations.ReadWriteAccess, "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
            //clientRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
            //clientRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "StoredPassword", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
            clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.Create, SecurityPermissionState.Allow);
            clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            clientRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            clientRole.AddTypePermission<ModelDifference>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            //clientRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            //clientRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.Create, SecurityPermissionState.Allow);
            //clientRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.Create, SecurityPermissionState.Allow);
            clientRole.AddTypePermission<TerminalOrg>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<TerminalOrg>(SecurityOperations.Create, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<TariffBaseTerminal>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<TariffBaseTerminal>(SecurityOperations.Create, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<Employee>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<Employee>(SecurityOperations.Create, SecurityPermissionState.Deny);


            clientRole.AddTypePermission<Container>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<Container>(SecurityOperations.Create, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<ClientEmployee>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<ClientEmployee>(SecurityOperations.Create, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<RequestContainersPlacement>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<RequestContainersPlacement>(SecurityOperations.Delete, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<RequestContainerInfo>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<RequestContainerInfo>(SecurityOperations.Delete, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<RequestContainersExtradition>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<RequestContainersExtradition>(SecurityOperations.Delete, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<RequestContainerExtraditionInfo>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<RequestContainerExtraditionInfo>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            





            //clientRole.AddTypePermission<RequestDriverInfo>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<RequestDriverInfo>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<dContainerType>(SecurityOperations.ReadOnlyAccess, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<dRequestType>(SecurityOperations.Read, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<dContainerTypeSize>(SecurityOperations.Read, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<dContainerSubType>(SecurityOperations.Read, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<dTransportMark>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<dTransportMark>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<dTransportType>(SecurityOperations.FullAccess, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<dTransportType>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //}
            return clientRole;
        }

        private PermissionPolicyRole CreateTerminalEmployeeRole()
        {
            PermissionPolicyRole clientRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Сотрудник терминала"));
            //if (clientRole != null)
            //    clientRole.Delete();
            ObjectSpace.CommitChanges();
            if (clientRole == null)
                //{
                clientRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
            clientRole.Name = "Сотрудник терминала";
            clientRole.CanEditModel = false;
            clientRole.IsAdministrative = false;
            clientRole.PermissionPolicy = SecurityPermissionPolicy.AllowAllByDefault;
            clientRole.AddObjectPermission<PermissionPolicyUser>(SecurityOperations.Read, "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
            clientRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
            clientRole.AddMemberPermission<PermissionPolicyUser>(SecurityOperations.Write, "StoredPassword", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
            clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.ReadOnlyAccess, SecurityPermissionState.Allow);
            clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.Create, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            //clientRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.Write, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<PermissionPolicyRole>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            clientRole.AddTypePermission<ModelDifference>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);

            clientRole.AddNavigationPermission(@"Application/NavigationItems/Items/Default", SecurityPermissionState.Deny);

            //clientRole.AddTypePermission<PermissionPolicyUser>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            //clientRole.AddTypePermissionsRecursively<PermissionPolicyRole>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            //clientRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            //clientRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            //clientRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.Create, SecurityPermissionState.Allow);
            //clientRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.Create, SecurityPermissionState.Allow);
            clientRole.AddTypePermission<TerminalOrg>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<TerminalOrg>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<TerminalOrg>(SecurityOperations.Write, SecurityPermissionState.Deny);

            //clientRole.AddTypePermission<TariffBaseTerminal>(SecurityOperations.FullAccess, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<TariffBaseTerminal>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<TariffBaseTerminal>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<TariffBaseTerminal>(SecurityOperations.Write, SecurityPermissionState.Deny);

            //clientRole.AddTypePermission<RequestContainersPlacement>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<RequestContainersPlacement>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<RequestContainerInfo>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<RequestContainerInfo>(SecurityOperations.Delete, SecurityPermissionState.Deny);

            //clientRole.AddTypePermission<RequestContainersExtradition>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<RequestContainersExtradition>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<RequestContainerExtraditionInfo>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<RequestContainerExtraditionInfo>(SecurityOperations.Delete, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<Client>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<Container>(SecurityOperations.Delete, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<Employee>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<Employee>(SecurityOperations.Create, SecurityPermissionState.Deny);

            clientRole.AddTypePermission<ClientEmployee>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            //clientRole.AddTypePermission<ClientEmployee>(SecurityOperations.Create, SecurityPermissionState.Deny);


            clientRole.AddTypePermission<Terminal>(SecurityOperations.Create, SecurityPermissionState.Deny);
            clientRole.AddTypePermission<Terminal>(SecurityOperations.Delete, SecurityPermissionState.Deny);
            
            //}
            return clientRole;
        }
    }
}
