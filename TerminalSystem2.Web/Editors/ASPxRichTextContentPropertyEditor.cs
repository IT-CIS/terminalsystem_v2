﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Editors;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web.ASPxRichEdit;
using DevExpress.Web.Office;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace TerminalSystem2.Web.Editors
{
    [PropertyEditor(typeof(string), "RTF", false)]
    public class ASPxRichTextContentPropertyEditor : ASPxPropertyEditor //WebPropertyEditor
    {
        public ASPxRichTextContentPropertyEditor(Type objectType, IModelMemberViewItem model)
            : base(objectType, model)
        {
        }

        private ASPxRichEdit richEditUserControlCore = null;

        protected override WebControl CreateEditModeControlCore()
        {
            ASPxRichEdit result = new ASPxRichEdit();
            result.ID = "aspxRichEdit";
            RenderHelper.SetupASPxWebControl(result);
            return result;
        }

        protected override WebControl CreateViewModeControlCore()
        {
            //base.CreateViewModeControlCore();
            ASPxRichEdit result = (ASPxRichEdit)CreateEditModeControlCore();
            result.Enabled = false;
            return result;
        }

        public new ASPxRichEdit Editor
        {
            get { return (ASPxRichEdit)base.Editor; }
        }

        //public ASPxRichEdit RichEditUserControl
        //{
        //    get
        //    {
        //        return richEditUserControlCore;
        //    }
        //}

        //protected override WebControl CreateViewModeControlCore()
        //{
        //    return CreateRichEditControl();
        //}


        //private WebControl CreateRichEditControl()
        //{
        //    richEditUserControlCore = new ASPxRichEdit();
        //    return richEditUserControlCore;
        //}

        //protected override void ReadViewModeValueCore()
        //{
        //    //ReadPdfValue();
        //}

        //protected override object GetControlValueCore()
        //{
        //    object controlValue = ((ASPxRichEdit)this.Editor);
        //    return controlValue;
        //}

        //protected override void ReadEditModeValueCore()
        //{
        //    //(ASPxRichEdit)this.Editor = this.PropertyValue;
        //}

        //protected override void ReadEditModeValueCore()
        //{
        //    object controlValue = ((ASPxRichEdit)this.Editor).Value;
        //    return controlValue;

        //    //ReadPdfValue();
        //}

        //private void ReadPdfValue()
        //{
        //    //    using (MemoryStream ms = new MemoryStream())
        //    //        Editor.SaveCopy(oMemoryStream, DevExpress.XtraRichEdit.DocumentFormat.Rtf)
        //    //    Using sr = New StreamReader(oMemoryStream)
        //    //        oMemoryStream.Seek(0, SeekOrigin.Begin)
        //    //        Return sr.ReadToEnd()
        //    //    End Using
        //    //End Using
        //    try
        //    {
        //        byte[] value = (byte[])PropertyValue;
        //        if (value == null || value.Length == 0)
        //        {
        //            DocumentManager.CloseDocument();
        //        }
        //        else
        //        {
        //            using (var ms = new MemoryStream(value))
        //            {
        //                richEditUserControlCore.SaveCopy(ms, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
        //                using (StreamReader sr = new StreamReader(ms))
        //                {
        //                    ms.Seek(0, SeekOrigin.Begin);
        //                    sr.ReadToEnd();
        //                }
        //            }
        //        }
        //    }
        //    catch (ArgumentException) { }
        //    //finally
        //    //{
        //    //    BindDataView();
        //    //}
        //}


        //private string psDocumentId = "";

        //private DevExpress.ExpressApp.IObjectSpace poObjectSpace;

        //private DevExpress.ExpressApp.XafApplication poApplication;


        //public void RaiseEditValueChanged()
        //{
        //    EditValueChangedHandler(Editor, new EventArgs());
        //}

        //public void CloseDocument()
        //{
        //    //  T371750
        //    DocumentManager.CloseDocument(psDocumentId);
        //}

        //protected override System.Web.UI.WebControls.WebControl CreateEditModeControlCore()
        //{
        //    ASPxRichEdit oASPxRichEdit = new ASPxRichEdit();
        //    oASPxRichEdit.CreateDefaultRibbonTabs(true);
        //    oASPxRichEdit.ShowConfirmOnLosingChanges = true;
        //    oASPxRichEdit.Theme = "Glass";
        //    oASPxRichEdit.Width = 1000;
        //    oASPxRichEdit.Height = 800;
        //    return oASPxRichEdit;
        //}

        //protected override void ReadEditModeValueCore()
        //{
        //    if (this.PropertyValue)
        //    {
        //        IsNot;
        //        null;
        //        Using;
        //        oMemoryStream = new MemoryStream(Encoding.UTF8.GetBytes(TryCast(PropertyValue, String)));
        //        RichEditUserControl.Open(psDocumentId, DevExpress.XtraRichEdit.DocumentFormat.Rtf, Function, (Unknown, oMemoryStream);
        //    }

        //}




        //protected override object GetControlValueCore()
        //{
        //    Using;
        //    oMemoryStream = new MemoryStream();
        //    RichEditUserControl.SaveCopy(oMemoryStream, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
        //    Using;
        //    sr = new StreamReader(oMemoryStream);
        //    oMemoryStream.Seek(0, SeekOrigin.Begin);
        //    return sr.ReadToEnd();
        //}

        //protected override void ReadValueCore()
        //{
        //    base.ReadValueCore();
        //}

        //protected override void WriteValueCore()
        //{
        //    base.WriteValueCore();
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {

        //    }

        //    poObjectSpace.Committing;
        //    new System.EventHandler(this.objectSpace_Committing);
        //    base.Dispose(disposing);
        //}

        //private void objectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    if (this.Control != null)
        //    {
        //        EditValueChangedHandler(Editor, new EventArgs());
        //    }

        //}

        //private void IComplexViewItem_Setup(IObjectSpace objectSpace, XafApplication application)
        //{
        //    this.poObjectSpace = objectSpace;
        //    this.poApplication = application;
        //    objectSpace.Committing += new System.EventHandler(this.objectSpace_Committing);
        //}
    }
}