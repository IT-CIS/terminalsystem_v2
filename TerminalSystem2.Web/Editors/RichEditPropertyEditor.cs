﻿using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Web.Editors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TerminalSystem2.Web.Controllers;

namespace TerminalSystem2.Web.Editors
{
    [PropertyEditor(typeof(String), "RTF", false)]
    public class RichEditPropertyEditor : WebPropertyEditor, IInplaceEditSupport
    {
        public RichEditPropertyEditor(Type objectType, IModelMemberViewItem model)
            : base(objectType, model)
        {
            ControlBindingProperty = "RtfText";
        }
        private WebRichEditUserControl richEditUserControlCore = null;
        public WebRichEditUserControl RichEditUserControl
        {
            get
            {
                return richEditUserControlCore;
            }
        }
        protected override object CreateControlCore()
        {
            richEditUserControlCore = new WebRichEditUserControl();
            UpdateReadOnly();
            return richEditUserControlCore;
        }
        protected override void OnAllowEditChanged()
        {
            base.OnAllowEditChanged();
            UpdateReadOnly();
        }
        private void UpdateReadOnly()
        {
            if (RichEditUserControl != null && RichEditUserControl.r.RichEditControl != null)
            {
                RichEditUserControl.RichEditControl.ReadOnly = !AllowEdit;
            }
        }

        #region IInplaceEditSupport Members

        public DevExpress.XtraEditors.Repository.RepositoryItem CreateRepositoryItem()
        {
            return new RepositoryItemRtfEditEx();
        }

        #endregion
    }
}