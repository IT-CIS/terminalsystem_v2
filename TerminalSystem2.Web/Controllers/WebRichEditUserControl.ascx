﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebRichEditUserControl.ascx.cs" Inherits="TerminalSystem2.Web.Controllers.WebRichEditUserControl" %>
<%@ Register assembly="DevExpress.Web.ASPxRichEdit.v19.1, Version=19.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRichEdit" tagprefix="dx" %>

<dx:ASPxRichEdit ID="ASPxRichEditUserControl" runat="server" Height="602px" Theme="Default" Width="1196px" WorkDirectory="~\App_Data\UploadTemp">
</dx:ASPxRichEdit>

