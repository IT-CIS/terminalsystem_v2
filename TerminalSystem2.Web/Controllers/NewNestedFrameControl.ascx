﻿<%@ Control Language="C#" CodeBehind="NestedFrameControlNew.ascx.cs" ClassName="NestedFrameControlNew" Inherits="TerminalSystem2.Web.Controllers.NestedFrameControlNew" %>
<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v19.1" Namespace="DevExpress.ExpressApp.Web.Templates.ActionContainers"
    TagPrefix="xaf" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v19.1" Namespace="DevExpress.ExpressApp.Web.Templates"
    TagPrefix="xaf" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v19.1" Namespace="DevExpress.ExpressApp.Web.Controls"
    TagPrefix="xaf" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v19.1" Namespace="DevExpress.ExpressApp.Web.Templates.Controls"
    TagPrefix="xaf" %>

<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0">
<dx:ASPxGlobalEvents ID="GE" ClientSideEvents-EndCallback="window.XafPageEndCallBack" runat="server" />
<div class="NestedFrame">
    <div class="nf_Menu">
        <div class="nf_leftMenu">
            <xaf:XafUpdatePanel ID="XafUpdatePanel1" CssClass="ToolBarUpdatePanel" runat="server">
                <xaf:ActionContainerHolder runat="server" ID="ObjectsCreation" ContainerStyle="Links" CssClass="nf_leftMenu_AC" Orientation="Horizontal"
                    Menu-Width="100%" Menu-ItemAutoWidth="False">
                    <actioncontainers>
                <xaf:WebActionContainer ContainerId="ObjectsCreation" />
                <xaf:WebActionContainer ContainerId="Link" />
            </actioncontainers>
                </xaf:ActionContainerHolder>
            </xaf:XafUpdatePanel>
        </div>
        <div class="nf_rightMenu">
            <xaf:XafUpdatePanel ID="UPToolBar" CssClass="ToolBarUpdatePanel" runat="server">
                <xaf:ActionContainerHolder runat="server" ID="ToolBar" ContainerStyle="Links" CssClass="nf_rightMenu_AC" Orientation="Horizontal"
                    Menu-ItemAutoWidth="False">
                    <menu width="100%" itemautowidth="False" clientinstancename="nestedFrameMenu" font-size="14px" enableadaptivity="true" itemwrap="false">
                        <borderleft borderstyle="None" />
                        <borderright borderstyle="None" />
                    </menu>
                    <actioncontainers>
                <xaf:WebActionContainer ContainerId="Edit"/>
                <xaf:WebActionContainer ContainerId="RecordEdit"/>
                
                <xaf:WebActionContainer ContainerId="Reports" />
                <xaf:WebActionContainer ContainerId="Export" />
                <xaf:WebActionContainer ContainerId="Diagnostic" />
                <xaf:WebActionContainer ContainerId="Filters" />
             </actioncontainers>
                </xaf:ActionContainerHolder>
            </xaf:XafUpdatePanel>
        </div>
    </div>
    <b class="dx-clear"></b>
    <xaf:ViewSiteControl ID="viewSiteControl" runat="server" Control-CssClass="NestedFrameViewSite" />

</div>

