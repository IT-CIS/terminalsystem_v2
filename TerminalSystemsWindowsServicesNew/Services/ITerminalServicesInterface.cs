﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSystemsWindowsServicesNew.Services
{
    /// <summary>
    /// Интерфейсы
    /// </summary>
    [ServiceContract]
    public interface ITerminalServices
    {
        [OperationContract]
        void SetStocksReportByDayControllerStart();

        [OperationContract]
        void SetStocksReportByMonthController();

        [OperationContract]
        void CheckRequestFinishDateController();

        [OperationContract]
        void SetServiceDoneTotalValueController();
    }
}
