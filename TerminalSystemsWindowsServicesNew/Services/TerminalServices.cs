﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.SystemDir;
using TerminalSystemsWindowsServicesNew.Helpers;
using LogHelperClass = TerminalSystemsWindowsServicesNew.Helpers.LogHelperClass;

namespace TerminalSystemsWindowsServicesNew.Services
{
    /// <summary>
    /// Класс с методами сервисов
    /// </summary>
    public class TerminalServices : ITerminalServices
    {
        LogHelperClass LogHelperClass = new LogHelperClass();
        public void SetStocksReportByDayControllerStart()
        {
            Connect connect = null;
            try
            {
                Set set = new Set();
                string sqlinstance = set.instance;
                string port = set.port;
                string user = set.user;
                string pass = set.pss;
                string database = set.db;
                SettingsHelperClass.Init(sqlinstance, Convert.ToInt16(port), user, pass, database);
                UnitOfWork unitOfWork = SettingsHelperClass.unitOfWork;
                connect = Connect.FromUnitOfWork(unitOfWork);
            }
            catch(Exception ex) {
                LogHelperClass.WriteErr(ex, " !!Формирование отчетов за день - Возникли какие то проблемы с инициализацией");
            }
            try
            {
                DateTime date = DateTime.Now.Date.AddDays(-1);
                TerminalSystem2.Module.Controllers.Services.StocksReportsByDayController stocksReportsByDayController =
                    new TerminalSystem2.Module.Controllers.Services.StocksReportsByDayController();
                stocksReportsByDayController.CreateXLSReportFromStockByDayAction(date, connect);

                //foreach (Stock stock in connect.FindObjects<Stock>(mc => mc.Containers.Count > 0))
                //{
                //    CreateReportsFromStockController CreateReportsFromStockController = new CreateReportsFromStockController();
                //    string path = CreateReportsFromStockController.CreateXlsReportByStock(unitOfWork, stock);
                //}
                LogHelperClass.WriteText("!!Формирование отчетов за день - Ура, все получилось, я молодец и заслужил пивка :)!!");
            }
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, " !!Формирование отчетов за день - Бля, случилась какая то херня, но я всеравно заслужил пива, но позже (!!");
            }
            finally
            {
                SettingsHelperClass.CloseConnection();
            }
        }

        public void SetStocksReportByMonthController()
        {
            LogHelperClass.WriteText("Ура, все получилось, я молодец и заслужил пивка :)");
        }

        public void CheckRequestFinishDateController()
        {
            Connect connect = null;
            UnitOfWork unitOfWork = null;
            try { 
            Set set = new Set();
            string sqlinstance = set.instance;
            string port = set.port;
            string user = set.user;
            string pass = set.pss;
            string database = set.db;
            SettingsHelperClass.Init(sqlinstance, Convert.ToInt16(port), user, pass, database);
            unitOfWork = SettingsHelperClass.unitOfWork;
            connect = Connect.FromUnitOfWork(unitOfWork);
            }
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, " !!Закрытие заявок с окончившимся сроком - Возникли какие то проблемы с инициализацией");
            }
            try
            {
                TerminalSystem2.Module.Controllers.Services.CheckRequestFinishDateController checkRequestFinishDateController =
                    new TerminalSystem2.Module.Controllers.Services.CheckRequestFinishDateController();
                checkRequestFinishDateController.CheckRequestFinishDate(unitOfWork);

                //foreach (Stock stock in connect.FindObjects<Stock>(mc => mc.Containers.Count > 0))
                //{
                //    CreateReportsFromStockController CreateReportsFromStockController = new CreateReportsFromStockController();
                //    string path = CreateReportsFromStockController.CreateXlsReportByStock(unitOfWork, stock);
                //}
                LogHelperClass.WriteText("!!Закрытие заявок с окончившимся сроком - Ура, все получилось, я молодец и заслужил пивка :)!!");
            }
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, " !!Закрытие заявок с окончившимся сроком - Бля, случилась какая то херня, но я всеравно заслужил пива, но позже (!!");
            }
            finally
            {
                SettingsHelperClass.CloseConnection();
            }
        }

        public void SetServiceDoneTotalValueController()
        {
            UnitOfWork unitOfWork = null;
            try
            {
                Set set = new Set();
                string sqlinstance = set.instance;
                string port = set.port;
                string user = set.user;
                string pass = set.pss;
                string database = set.db;
                SettingsHelperClass.Init(sqlinstance, Convert.ToInt16(port), user, pass, database);
                unitOfWork = SettingsHelperClass.unitOfWork;
                //Connect connect = Connect.FromUnitOfWork(unitOfWork);
            }
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, " !!Закрытие заявок с окончившимся сроком - Возникли какие то проблемы с инициализацией");
            }
            try
            {
                TerminalSystem2.Module.Controllers.Services.SetServiceDoneTotalValueController setServiceDoneTotalValueController =
                    new TerminalSystem2.Module.Controllers.Services.SetServiceDoneTotalValueController();
                setServiceDoneTotalValueController.AddServiceValueByDayAction(unitOfWork);

                //foreach (Stock stock in connect.FindObjects<Stock>(mc => mc.Containers.Count > 0))
                //{
                //    CreateReportsFromStockController CreateReportsFromStockController = new CreateReportsFromStockController();
                //    string path = CreateReportsFromStockController.CreateXlsReportByStock(unitOfWork, stock);
                //}
                LogHelperClass.WriteText("!!Подсчет стоимости услуг - Ура, все получилось, я молодец и заслужил пивка :)!!");
            }
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, " !!Подсчет стоимости услуг - Бля, случилась какая то херня, но я всеравно заслужил пива, но позже (!!");
            }
            finally
            {
                SettingsHelperClass.CloseConnection();
            }
        }
    }
}
