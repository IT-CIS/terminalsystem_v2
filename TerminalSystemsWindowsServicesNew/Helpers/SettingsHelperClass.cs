﻿using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSystemsWindowsServicesNew.Helpers
{
    public static class SettingsHelperClass
    {
        #region Properties
        // Состояние подключения
        public static bool isConnected = false;
        // Адрес сервера с приложением XAF
        private static string sqlinstance;
        // порт подключения
        private static int port;
        // пользователь скл сервера
        private static string sqluser;
        // Пароль скл сервера
        private static string sqlpass;
        // База данных приложения XAF
        private static string db;

        // Интерфейс для работы с приложением XAF

        public static Session session;
        //{
        //    get { return }
        //};
        //public Session Session
        //{
        //    set { this.session = value; }
        //    get { return this.session; }
        //}

        public static UnitOfWork unitOfWork;
        //public UnitOfWork UnitOfWork
        //{
        //    set { this.unitOfWork = value; }
        //    get { return this.unitOfWork; }
        //}
        //LogHelperClass LogHelperClass = new LogHelperClass();

        public static IDataLayer dl;
        #endregion

        #region Constructors
        public static void Init(string aSqlInstance, int aPort, string aSqlUser, string aSqlPass, string aDB)
        {
            sqlinstance = aSqlInstance;
            port = aPort;
            sqluser = aSqlUser;
            sqlpass = aSqlPass;
            db = aDB;
            SetXafAppConnection();
        }
        #endregion

        #region Methods
        public static void SetXafAppConnection()
        {
            LogHelperClass LogHelperClass = new LogHelperClass();

            //this.sqlinstance = aSqlInstance;
            //this.port = aPort;
            //this.sqluser = aSqlUser;
            //this.sqlpass = aSqlPass;
            //this.db = aDB;
            PostgreSqlConnectionProvider.GlobalUseLegacyGuidSupport = true;
            string conn =  MySqlConnectionProvider.GetConnectionString(sqlinstance, sqluser, sqlpass.ToString(), db);
            //string conn = DevExpress.Xpo.DB.PostgreSqlConnectionProvider.GetConnectionString(sqlinstance, port, sqluser, sqlpass, db);

            //LogHelperClass.WriteText("Получили conn = " + conn);
            try {
                XpoDefault.DataLayer = XpoDefault.GetDataLayer(conn, DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema);
                //LogHelperClass.WriteText("Получили DataLayer");
                unitOfWork = new UnitOfWork(XpoDefault.DataLayer);
                unitOfWork.Connect();
            }
            //try
            //{
            //    unitOfWork = new UnitOfWork(GetDataLayer(conn));
            //}
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, " ошибка в unitOfWork = new UnitOfWork(GetDataLayer(conn))");
            }
            LogHelperClass.WriteText("Получили unitOfWork");
            session = (Session)unitOfWork;
            //LogHelperClass.WriteText("Получили this.session из SetXafAppConnection");
        }

        private static IDataLayer GetDataLayer(string conn)
        {
            XpoDefault.Session = null;
            XPDictionary dict = new ReflectionDictionary();
            IDataStore store = XpoDefault.GetConnectionProvider(conn, AutoCreateOption.DatabaseAndSchema);
            
            dict.GetDataStoreSchema(System.Reflection.Assembly.GetExecutingAssembly());
            dl = new ThreadSafeDataLayer(dict, store);
            //dl.Connection.Close
            return dl;
        }


        public static void CloseConnection()
        {
            LogHelperClass LogHelperClass = new LogHelperClass();

            //try
            //{
            //    unitOfWork.Disconnect();
            //}
            //catch (Exception ex)
            //{
            //    LogHelperClass.WriteErr(ex, " ошибка в unitOfWork.Disconnect()");
            //}
            //try
            //{
            //    unitOfWork.Dispose();
            //}
            //catch (Exception ex)
            //{
            //    LogHelperClass.WriteErr(ex, " ошибка в unitOfWork.Dispose()");
            //}
            try
            {
                unitOfWork.Connection.Close();
            }
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, " ошибка в unitOfWork.Connection.Close()");
            }
        }
        #endregion
    }
}
