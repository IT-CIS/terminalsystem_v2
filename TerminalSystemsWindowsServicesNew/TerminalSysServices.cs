﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TerminalSystem2.Module.Controllers;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;
using TerminalSystemsWindowsServicesNew.Helpers;
using TerminalSystemsWindowsServicesNew.Services;
using LogHelperClass = TerminalSystemsWindowsServicesNew.Helpers.LogHelperClass;

namespace TerminalSystemsWindowsServicesNew
{
    public partial class TerminalSysServices : ServiceBase
    {
        public TerminalSysServices()
        {
            InitializeComponent();
        }

        
        LogHelperClass LogHelperClass = new LogHelperClass();

        protected override void OnStart(string[] args)
        {
            Uri baseAddress = new Uri("http://localhost:54144/TerminalSystem/TerminalSystemServices.svc");
            ServiceHost host = new ServiceHost(typeof(TerminalServices), baseAddress);
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.OpenTimeout = new TimeSpan(0, 4, 0);
            binding.CloseTimeout = new TimeSpan(0, 4, 0);
            binding.SendTimeout = new TimeSpan(0, 4, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 4, 0);
            host.AddServiceEndpoint(typeof(ITerminalServices), binding, "");
            host.OpenTimeout = new TimeSpan(0, 2, 0);
            //AddLog("host.Open();");
            try
            {
                host.Open();
            }
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, "Ошибка открытия хоста");
            }
            LogHelperClass.WriteText("Start: " + DateTime.Now.ToString());
            //this.StartScheduledStockReportsByDayService();
            //this.StartScheduledCheckRequestFinishDateService();
            //this.StartScheduledSetServiceDoneTotalValueService();

            ////Создаем таймер и выставляем его параметры
            //this.timer1 = new System.Timers.Timer();
            //this.timer1.Enabled = true;
            ////Интервал 10000мс - 1с.
            //this.timer1.Interval = 30000;
            //AddLog(this.timer1.Interval.ToString());
            //this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(timer1_Elapsed);
            //this.timer1.AutoReset = false;
        }
        
        
        
        private Timer stockReportsByDayTimer;
        public void StartScheduledStockReportsByDayService()
        {
            stockReportsByDayTimer = new Timer(new TimerCallback(StockReportsByDayTimerCallback));

            //string mode = ConfigurationManager.AppSettings["Mode"].ToUpper();
            //this.WriteToFile("Simple Service Mode: " + mode + " {0}");

            //Set the Default Time.
            DateTime stockReportsByDayScheduledTime = DateTime.MinValue;

            //if (mode == "DAILY")
            //{
            //    //Get the Scheduled Time from AppSettings.
            //    stockReportsByDayScheduledTime = DateTime.Parse(ConfigurationManager.AppSettings["StockReportsByDayScheduledTime"]);
            //    if (DateTime.Now > stockReportsByDayScheduledTime)
            //    {
            //        //If Scheduled Time is passed set Schedule for the next day.
            //        stockReportsByDayScheduledTime = stockReportsByDayScheduledTime.AddDays(1);
            //    }
            //}
            try
            {
                stockReportsByDayScheduledTime = DateTime.Parse(ConfigurationManager.AppSettings["StockReportsByDayScheduledTime"]);
                if (DateTime.Now > stockReportsByDayScheduledTime)
                {
                    //If Scheduled Time is passed set Schedule for the next day.
                    //stockReportsByDayScheduledTime = DateTime.Now.AddMinutes(0.5);//.AddDays(1);
                    stockReportsByDayScheduledTime = stockReportsByDayScheduledTime.AddDays(1);
                }

                TimeSpan stockReportsByDayTimeSpan = stockReportsByDayScheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", stockReportsByDayTimeSpan.Days, stockReportsByDayTimeSpan.Hours, 
                    stockReportsByDayTimeSpan.Minutes, stockReportsByDayTimeSpan.Seconds);

                LogHelperClass.WriteText($"Следующее выполнение  StockReportsByDay через: {schedule}");
                LogHelperClass.WriteText($"Следующее выполнение  StockReportsByDay в {stockReportsByDayScheduledTime}");

                //Get the difference in Minutes between the Scheduled and Current Time.
                int stockReportsByDayDueTime = Convert.ToInt32(stockReportsByDayTimeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                stockReportsByDayTimer.Change(stockReportsByDayDueTime, Timeout.Infinite);
            }
            catch { }
        }
        private void StockReportsByDayTimerCallback(object e)
        {
            LogHelperClass.WriteText(String.Format($"Начало StockReportsByDayTimerCallback"));
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.OpenTimeout = new TimeSpan(0, 4, 0);
            binding.CloseTimeout = new TimeSpan(0, 4, 0);
            binding.SendTimeout = new TimeSpan(0, 4, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 4, 0);
            ChannelFactory<ITerminalServices> cf = new ChannelFactory<ITerminalServices>(binding,
                new EndpointAddress("http://localhost:54144/TerminalSystem/TerminalSystemServices.svc"));
            ITerminalServices serv = cf.CreateChannel();
            //AddLog("SetIngeoOperator = ");
            serv.SetStocksReportByDayControllerStart();
            //AddLog("serv.close()");

            cf.Close();
            this.StartScheduledStockReportsByDayService();
        }


        private Timer setServiceDoneTotalValueTimer;
        public void StartScheduledSetServiceDoneTotalValueService()
        {
            setServiceDoneTotalValueTimer = new Timer(new TimerCallback(SetServiceDoneTotalValueTimerCallback));

            //Set the Default Time.
            DateTime setServiceDoneTotalValueScheduledTime = DateTime.MinValue;


            try
            {
                setServiceDoneTotalValueScheduledTime = DateTime.Parse(ConfigurationManager.AppSettings["SetServiceDoneTotalValueScheduledTime"]);
                if (DateTime.Now > setServiceDoneTotalValueScheduledTime)
                {
                    setServiceDoneTotalValueScheduledTime = setServiceDoneTotalValueScheduledTime.AddDays(1);
                }

                TimeSpan setServiceDoneTotalValueTimeSpan = setServiceDoneTotalValueScheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", setServiceDoneTotalValueTimeSpan.Days, setServiceDoneTotalValueTimeSpan.Hours,
                    setServiceDoneTotalValueTimeSpan.Minutes, setServiceDoneTotalValueTimeSpan.Seconds);

                LogHelperClass.WriteText($"Следующее выполнение  SetServiceDoneTotalValue через: {schedule}");
                LogHelperClass.WriteText($"Следующее выполнение  SetServiceDoneTotalValue в {setServiceDoneTotalValueScheduledTime}");

                int setServiceDoneTotalValueDueTime = Convert.ToInt32(setServiceDoneTotalValueTimeSpan.TotalMilliseconds);
                setServiceDoneTotalValueTimer.Change(setServiceDoneTotalValueDueTime, Timeout.Infinite);
            }
            catch { }

        }
        private void SetServiceDoneTotalValueTimerCallback(object e)
        {
            LogHelperClass.WriteText(String.Format($"Начало SetServiceDoneTotalValueTimerCallback"));
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.OpenTimeout = new TimeSpan(0, 4, 0);
            binding.CloseTimeout = new TimeSpan(0, 4, 0);
            binding.SendTimeout = new TimeSpan(0, 4, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 4, 0);
            ChannelFactory<ITerminalServices> cf = new ChannelFactory<ITerminalServices>(binding,
                new EndpointAddress("http://localhost:54144/TerminalSystem/TerminalSystemServices.svc"));
            ITerminalServices serv = cf.CreateChannel();
            //AddLog("SetIngeoOperator = ");
            serv.SetServiceDoneTotalValueController();
            //AddLog("serv.close()");

            cf.Close();
            this.StartScheduledSetServiceDoneTotalValueService();
        }

        private Timer checkRequestFinishDateTimer;
        public void StartScheduledCheckRequestFinishDateService()
        {
            checkRequestFinishDateTimer = new Timer(new TimerCallback(CheckRequestFinishDateTimerCallback));
            //Set the Default Time.
            DateTime checkRequestFinishDateScheduledTime = DateTime.MinValue;
            try
            {
                checkRequestFinishDateScheduledTime = DateTime.Parse(ConfigurationManager.AppSettings["CheckRequestFinishDateScheduledTime"]);
                if (DateTime.Now > checkRequestFinishDateScheduledTime)
                {
                    checkRequestFinishDateScheduledTime = checkRequestFinishDateScheduledTime.AddDays(1);
                }

                TimeSpan checkRequestFinishDateTimeSpan = checkRequestFinishDateScheduledTime.Subtract(DateTime.Now);

                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", checkRequestFinishDateTimeSpan.Days, checkRequestFinishDateTimeSpan.Hours,
                    checkRequestFinishDateTimeSpan.Minutes, checkRequestFinishDateTimeSpan.Seconds);

                LogHelperClass.WriteText($"Следующее выполнение  СheckRequestFinishDate через: {schedule}");
                LogHelperClass.WriteText($"Следующее выполнение  СheckRequestFinishDate в {checkRequestFinishDateScheduledTime}");

                int checkRequestFinishDateDueTime = Convert.ToInt32(checkRequestFinishDateTimeSpan.TotalMilliseconds);
                checkRequestFinishDateTimer.Change(checkRequestFinishDateDueTime, Timeout.Infinite);
            }
            catch { }
        }

        private void CheckRequestFinishDateTimerCallback(object e)
        {
            LogHelperClass.WriteText(String.Format($"Начало CheckRequestFinishDateTimerCallback"));
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.OpenTimeout = new TimeSpan(0, 4, 0);
            binding.CloseTimeout = new TimeSpan(0, 4, 0);
            binding.SendTimeout = new TimeSpan(0, 4, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 4, 0);
            ChannelFactory<ITerminalServices> cf = new ChannelFactory<ITerminalServices>(binding,
                new EndpointAddress("http://localhost:54144/TerminalSystem/TerminalSystemServices.svc"));
            ITerminalServices serv = cf.CreateChannel();
            //AddLog("SetIngeoOperator = ");
            serv.CheckRequestFinishDateController();
            //AddLog("serv.close()");

            cf.Close();
            this.StartScheduledCheckRequestFinishDateService();
        }

        protected override void OnStop()
        {
            this.stockReportsByDayTimer.Dispose();
        }
    }
}
