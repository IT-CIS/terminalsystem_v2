﻿using NCron.Service;
using NCron.Fluent.Crontab;
using NCron.Fluent.Generics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using TerminalServices;
using TerminalSystemWindowsService.Helpers;
//using ServicesHelperClassLibrary;

namespace TerminalSystemWindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            Bootstrap.Init(args, ServiceSetup);
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[]
            //{
            //    new TerminalSystemService()
            //};
            //ServiceBase.Run(ServicesToRun);
        }
        static void ServiceSetup(SchedulingService service)
        {
            Set set = new Set();
            string sqlinstance = set.instance;
            string port = set.port;
            string user = set.user;
            string pass = set.pss;
            string database = set.db;
            SettingsHelperClass.Init(sqlinstance, Convert.ToInt16(port), user, pass, database);
            LogHelperClass LogHelperClass = new LogHelperClass();
            //service.Hourly().Run<ReportsFromStocksByDayController>();
            Task task1 = new Task(() =>
            {
                try
                {
                    LogHelperClass.WriteText("----------- ReportsFromStocksByDay task1 начало старта--------------------");
                    service.At(set.ReportsByDayStartSett).Run<ReportsFromStocksByDayController>();
                    LogHelperClass.WriteText("----------- ReportsFromStocksByDay task1 стартанул--------------------");
                }
                catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в ReportsFromStocksByDay task1"); }
            });
            task1.Start();
            task1.Wait();
            Task task2 = new Task(() =>
            {
                try
                {
                    LogHelperClass.WriteText("----------- <SetServiceDoneTotalValue task2 начало старта--------------------");
                    service.At(set.SetServiceDoneTotalValueStartSett).Run<SetServiceDoneTotalValueController>();
                    LogHelperClass.WriteText("----------- <SetServiceDoneTotalValue task2 стартанул--------------------");
                }
                catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в <SetServiceDoneTotalValue task2"); }
            });
            task2.Start();
            task2.Wait();

            Task task3 = new Task(() =>
            {
                try
                {
                    LogHelperClass.WriteText("----------- StockReportsByMonth task3 начало старта--------------------");
                    service.At(set.ReportsByMonthStartSett).Run<StockReportsByMonthController>();
                    LogHelperClass.WriteText("----------- StockReportsByMonth task3 стартанул--------------------");
                }
                catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в StockReportsByMonth task3"); }
            });
            task3.Start();
            task3.Wait();

            Task task4 = new Task(() =>
            {
                try
                {
                    LogHelperClass.WriteText("----------- <CheckRequestFinishDate task4 начало старта--------------------");
                    service.At(set.CheckRequestFinishDateStartSett).Run<CheckRequestFinishDateController>();
                    LogHelperClass.WriteText("----------- <CheckRequestFinishDate task4 стартанул--------------------");
                }
                catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в <CheckRequestFinishDate task4"); }
            });
            task4.Start();
            task4.Wait();

            //service.At(set.ReportsByDayStartSett).Run<ReportsFromStocksByDayController>();
            //service.At(set.SetServiceDoneTotalValueStartSett).Run<SetServiceDoneTotalValueController>();
            //service.At(set.ReportsByMonthStartSett).Run<StockReportsByMonthController>();
            //service.At(set.CheckRequestFinishDateStartSett).Run<CheckRequestFinishDateController>();

            //service.
        }
    }
}
