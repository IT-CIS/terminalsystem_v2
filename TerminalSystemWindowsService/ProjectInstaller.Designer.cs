﻿namespace TerminalSystemWindowsService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TerminalSystemWindowsServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.TerminalSystemWindowsServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // TerminalSystemWindowsServiceProcessInstaller
            // 
            this.TerminalSystemWindowsServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.NetworkService;
            this.TerminalSystemWindowsServiceProcessInstaller.Password = null;
            this.TerminalSystemWindowsServiceProcessInstaller.Username = null;
            // 
            // TerminalSystemWindowsServiceInstaller
            // 
            this.TerminalSystemWindowsServiceInstaller.DisplayName = "TerminalSystemWindowsService";
            this.TerminalSystemWindowsServiceInstaller.ServiceName = "TerminalSystemWindowsService";
            this.TerminalSystemWindowsServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.TerminalSystemWindowsServiceProcessInstaller,
            this.TerminalSystemWindowsServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller TerminalSystemWindowsServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller TerminalSystemWindowsServiceInstaller;
    }
}