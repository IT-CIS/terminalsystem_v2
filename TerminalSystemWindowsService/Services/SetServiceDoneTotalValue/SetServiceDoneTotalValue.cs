﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystemWindowsService;
using TerminalSystemWindowsService.Helpers;

namespace TerminalServices
{
    public class SetServiceDoneTotalValueController : NCron.CronJob
    {
        private static Assembly assembly = null;
        LogHelperClass LogHelperClass = new LogHelperClass();

        public override void Execute()
        {
            //GetAssemblyHelperClass GetAssemblyHelperClass = new GetAssemblyHelperClass();
            //assembly = GetAssemblyHelperClass.GetAssembly();
            LogHelperClass.WriteText("Подсчет стоимости услуг за день_execute");
            //Set s = new Set();
            //string sqlinstance = s.instance;
            //string port = s.port;
            //string user = s.user;
            //string pass = s.pss;
            //string database = s.db;
            //SettingsHelperClass xafApp = new SettingsHelperClass(sqlinstance, Convert.ToInt16(port), user, pass, database);
            //xafApp.SetXafAppConnection(sqlinstance, Convert.ToInt16(port),
            //    user, pass, database);
            //UnitOfWork unitOfWork = xafApp.UnitOfWork;
            UnitOfWork unitOfWork = SettingsHelperClass.unitOfWork;
            AddServiceValueByDayAction(unitOfWork);
        }

        /// <summary>
        /// добавить стоимость незакрытой услуги за день 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AddServiceValueByDayAction(UnitOfWork unitOfWork)
        {
            // перебираем все незакрытые услуги и добавляем стоимость за новый день
            LogHelperClass.WriteText("Подсчет стоимости услуг за день_AddServiceValueByDayAction");
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            try
            {
                foreach (ServiceDone serviceDone in connect.FindObjects<ServiceDone>(mc => mc.isDone == false))
                {
                    try
                    {

                        // смотрим, если новый месяц не начался
                        if (DateTime.Now.Month == DateTime.Now.AddDays(-1).Month)
                        {
                            ServiceCalculationValue(serviceDone, unitOfWork, DateTime.Now.Date, false);
                            //// если сервис не работал несколько дней, то надо расчитать - сколько (сравниваем дату последнего расчета с сегодня)
                            //TimeSpan diffDays = DateTime.Now.Date - serviceDone.ValueTotalDate;
                            //int servcount = diffDays.Days;

                            //if (serviceDone.DiscountActiveThroughDate != DateTime.MinValue)
                            //{
                            //    if (serviceDone.DiscountActiveThroughDate >= DateTime.Now.Date)
                            //    {
                            //        serviceDone.ValueTotal += servcount * serviceDone.Value * (100 - serviceDone.Discount) / 100;
                            //        serviceDone.ValueTotalDate = DateTime.Now.Date;
                            //        serviceDone.ServiceCount += servcount;
                            //    }
                            //    else
                            //    {
                            //        serviceDone.ValueTotal += serviceDone.Value * servcount;
                            //        serviceDone.ValueTotalDate = DateTime.Now.Date;
                            //        serviceDone.ServiceCount += servcount;
                            //    }
                            //}
                            //else
                            //{

                            //    serviceDone.ValueTotal += serviceDone.Value * servcount;
                            //    serviceDone.ValueTotalDate = DateTime.Now.Date;
                            //    serviceDone.ServiceCount += servcount;
                            //}
                            //serviceDone.Save();
                            //unitOfWork.CommitChanges();
                        }
                        // если начался новый месяц - закрываем услугу вчерашним днем и создаем новую такую же
                        else
                        {
                            // закрываем услугу вчерашним днем
                            serviceDone.isDone = true;
                            serviceDone.FinishDate = DateTime.Now.AddDays(-1).Date;
                            // если не все в услуге посчитано, то считаем
                            if (serviceDone.ValueTotalDate < DateTime.Now.AddDays(-1).Date)
                            {
                                ServiceCalculationValue(serviceDone, unitOfWork, DateTime.Now.AddDays(-1).Date, false);
                            }
                            serviceDone.Save();

                            // создаем новую услугу
                            ServiceDone serviceNew = connect.CreateObject<ServiceDone>();
                            serviceNew.StartDate = DateTime.Now.Date;
                            serviceNew.Client = serviceDone.Client;
                            serviceNew.Container = serviceDone.Container;
                            serviceNew.Currency = serviceDone.Currency;
                            if(serviceDone.DiscountActiveThroughDate != null)
                            {
                                if (serviceDone.DiscountActiveThroughDate >= DateTime.Now.Date)
                                {
                                    serviceNew.DiscountActiveThroughDate = serviceDone.DiscountActiveThroughDate;
                                    serviceNew.Discount = serviceDone.Discount;
                                }
                            }
                            serviceNew.isDone = false;
                            serviceNew.Tariff = serviceDone.Tariff;
                            serviceNew.UnitKind = serviceDone.UnitKind;
                            serviceNew.Value = serviceDone.Value;
                            serviceNew.ServiceType = serviceDone.ServiceType;
                            
                            serviceNew.Save();
                            unitOfWork.CommitChanges();
                            ServiceCalculationValue(serviceNew, unitOfWork, DateTime.Now.Date, true);
                        }
                        LogHelperClass.WriteText("Подсчет стоимости услуг за день_AddServiceValueByDayAction_finish");
                        //string text = String.Format("Контейнер {0}, serviceDone: StartDate {1}, ValueTotalDate {2}, servcount {3}", 
                        //    serviceDone.Container.Name, serviceDone.StartDate.ToShortDateString(),
                        //    serviceDone.ValueTotalDate.ToShortDateString(), servcount.ToString());
                        //LogHelperClass.WriteText(text);
                    }
                    catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в AddServiceValueByDayAction"); }
                }
            }
            catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в foreach (ServiceDone serviceDone"); }
        }

        /// <summary>
        /// Функция подсчета стоимости услуги на дату
        /// </summary>
        /// <param name="serviceDone">Услуга, по которой производится расчет</param>
        /// <param name="unitOfWork"></param>
        /// <param name="dateCalc">расчитываем на дату</param>
        /// <param name="isNewService">новая услуга или нет (для определения количества услуги)</param>
        private void ServiceCalculationValue(ServiceDone serviceDone, UnitOfWork unitOfWork, DateTime dateCalc, bool isNewService)
        {
            // если сервис не работал несколько дней, то надо расчитать - сколько (сравниваем дату последнего расчета с датой расчета)
            LogHelperClass.WriteText("Подсчет стоимости услуг за день_ServiceCalculationValue_start");
            int servcount = 1;
            if (!isNewService)
            {
                TimeSpan diffDays = dateCalc - serviceDone.ValueTotalDate;
                servcount = diffDays.Days;
            }

            if (serviceDone.DiscountActiveThroughDate != dateCalc)
            {
                if (serviceDone.DiscountActiveThroughDate >= dateCalc)
                {
                    serviceDone.ValueTotal += servcount * serviceDone.Value * (100 - serviceDone.Discount) / 100;
                    serviceDone.ValueTotalDate = dateCalc;
                    serviceDone.ServiceCount += servcount;
                }
                else
                {
                    serviceDone.ValueTotal += serviceDone.Value * servcount;
                    serviceDone.ValueTotalDate = dateCalc;
                    serviceDone.ServiceCount += servcount;
                }
            }
            else
            {
                serviceDone.ValueTotal += serviceDone.Value * servcount;
                serviceDone.ValueTotalDate = dateCalc;
                serviceDone.ServiceCount += servcount;
            }
            serviceDone.Save();
            unitOfWork.CommitChanges();
            LogHelperClass.WriteText("Подсчет стоимости услуг за день_ServiceCalculationValue_finish");
        }
    }
}
