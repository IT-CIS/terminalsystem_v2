﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Spreadsheet;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;
using TerminalSystemWindowsService;
using TerminalSystemWindowsService.Helpers;

namespace TerminalServices
{
    public class StockReportsByMonthController : NCron.CronJob
    {
        private static Assembly assembly = null;
        LogHelperClass LogHelperClass = new LogHelperClass();
        string reportByMonthTamplatePath = "";
        public override void Execute()
        {
            //GetAssemblyHelperClass GetAssemblyHelperClass = new GetAssemblyHelperClass();
            //assembly = GetAssemblyHelperClass.GetAssembly();
            LogHelperClass.WriteText("Месячный отчет_execute");
            //Set s = new Set();
            //string sqlinstance = s.instance;
            //string port = s.port;
            //string user = s.user;
            //string pass = s.pss;
            //string database = s.db;
            //reportByMonthTamplatePath = s.ReportByMonthTamplatePath;
            //SettingsHelperClass xafApp = new SettingsHelperClass(sqlinstance, Convert.ToInt16(port), user, pass, database);
            //xafApp.SetXafAppConnection(sqlinstance, Convert.ToInt16(port),
            //    user, pass, database);
            //UnitOfWork unitOfWork = xafApp.UnitOfWork;
            UnitOfWork unitOfWork = SettingsHelperClass.unitOfWork;
            CreateMailReportFromStockByMonthAction(unitOfWork);
        }

        /// <summary>
        /// сформировать отчет по стокам за месяц 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CreateMailReportFromStockByMonthAction(UnitOfWork unitOfWork)
        {
            LogHelperClass.WriteText("Месячный отчет_CreateMailReportFromStockByMonthAction_start");
            // перебираем все стоки, смотрим у них за прошлый день движения контейнеров, если есть - отправляем отчет клиенту по движению
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            foreach (Stock stock in connect.FindObjects<Stock>(mc => true))
            {

                if (isServicesInStockinMonth(stock, DateTime.Now.AddMonths(-1)))
                {
                    try
                    {
                        // отсылаем клиенту
                        string titleClient = "Отчет по стоку";
                        string messbody = "";
                        string email = "";
                        string month = "";
                        switch (DateTime.Now.AddMonths(-1).Month)
                        {
                            case 1:
                                month = "январь";
                                break;
                            case 2:
                                month = "февраль";
                                break;
                            case 3:
                                month = "март";
                                break;
                            case 4:
                                month = "апрель";
                                break;
                            case 5:
                                month = "май";
                                break;
                            case 6:
                                month = "июнь";
                                break;
                            case 7:
                                month = "июль";
                                break;
                            case 8:
                                month = "август";
                                break;
                            case 9:
                                month = "сентябрь";
                                break;
                            case 10:
                                month = "октябрь";
                                break;
                            case 11:
                                month = "ноябрь";
                                break;
                            case 12:
                                month = "декабрь";
                                break;
                            default:
                                month = DateTime.Now.Month.ToString();
                                break;
                        }
                        try
                        {
                            titleClient = String.Format("Отчет по стоку {0} за {1}", stock.Name, month);
                        }
                        catch { }
                        try { messbody = String.Format(@"Здравствуйте!
Высылаем Вам отчет по стоку {0} за {1}.", stock.Name, month); }
                        catch { }
                        try { email = stock.Client.Email; }
                        catch { }
                        // формируем Excel файл и получаем ссылку на него
                        string path = "";
                        try { path = CreateXlsReportByStock(unitOfWork, stock, month ); }
                        catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в CreateXlsReportByStock"); }

                        // отправляем отчет на почту
                        try { MailMessageLogic.SendMailMessage(titleClient, messbody, path, email, "donotreply@it-cis.ru"); }
                        catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в MailMessageLogic.SendMailMessage_за месяц"); }
                        
                        // прикрепляем его к стоку и клиенту
                        try {
                            ServiceReport serviceReport = connect.CreateObject<ServiceReport>();
                            try
                            {
                                serviceReport.Name = String.Format("Отчет по стоку {0} за {1}", stock.Name, month);
                            }
                            catch { }
                            serviceReport.RegDate = DateTime.Now.Date;


                            System.IO.MemoryStream ms = new System.IO.MemoryStream();
                            using (FileStream fs = File.OpenRead(path))
                            {
                                fs.CopyTo(ms);
                                ms.Seek(0, System.IO.SeekOrigin.Begin);
                                FileData filedata = connect.CreateObject<FileData>();
                                filedata.LoadFromStream(path.Split('\\').Last(), ms);
                                serviceReport.File = filedata;
                                ms.Dispose();
                            }
                            stock.ServiceReports.Add(serviceReport);
                            stock.Client.ServiceReports.Add(serviceReport);
                            connect.GetUnitOfWork().CommitChanges();
                        }
                        catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в MailMessageLogic.SendMailMessage_за месяц"); }
                        LogHelperClass.WriteText("Месячный отчет_CreateMailReportFromStockByMonthAction_finish");
                    }
                    catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в CreateMailReportFromStockByMonthAction"); }
            }
        }
        }
        /// <summary>
        /// Есть ли услуги по контейнерам в стоке в месяце 
        /// </summary>
        /// <param name="stock">Сток</param>
        /// <param name="date">Дата в которой смотрим месяц и год</param>
        /// <returns></returns>
        private bool isServicesInStockinMonth(Stock stock, DateTime date)
        {
            bool res = false;
            foreach (ServiceDone service in stock.Client.ServicesDone)
            {
                // переделать, так как бывают косяки с началом предоставления услуг (в новом месяце не идет пересчет - услуга продолжается)
                if (service.StartDate.Year == date.Year && service.StartDate.Month == date.Month)
                {
                    res = true;
                    break;
                }
            }
            LogHelperClass.WriteText("Месячный отчет_isServicesInStockinMonth _ " + res + " " + stock.Name);
            return res;
        }
        /// <summary>
        /// Создание файла отчета по движению в стоке за месяц
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="stock"></param>
        /// <returns>путь к файлу отчета</returns>
        public string CreateXlsReportByStock(UnitOfWork unitOfWork, Stock stock, string reportmonth)
        {
            LogHelperClass.WriteText("Месячный отчет_CreateXlsReportByStock_start");
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            string res = "";

            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            if (stock.Client != null && stock.Terminal != null)
            {
                // Create an instance of a workbook.

                // Load a workbook from the file.

                //string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                //    LocalPath.Replace("WinAisogdIngeo.Module.DLL", @"\DocTemplates\1.xlsx");

                string templatePath = reportByMonthTamplatePath;//@"D:\work\!!CIS\TerminalSystem\PublicWeb2\bin\DocTemplates\ReportByMonth.xlsx";
                workbook.LoadDocument(templatePath, DocumentFormat.OpenXml);

                // Access the first worksheet in the workbook.
                Worksheet worksheet = workbook.Worksheets[0];
                Cell cell;

                Client client = stock.Client;
                
                cell = worksheet.Cells["A3"];
                cell.Value = String.Format("Реестр № {0} от {1}", DateTime.Now.Month, DateTime.Now.Date.ToString("dd.MM.yyyy"));

                cell = worksheet.Cells["A7"];
                string cl = "";
                try { cl = String.Format("Заказчик: «{0}» сток «{1}»", client.Name, stock.Name); }
                catch { }
                cell.Value = cl;

                // перебираем контейнеры в стоке --------- переделать, так как если контейнер выбыл в отчетный период, то инфа по услуге не попадет в отчет
                int i = 0;
                int o = 0;
                foreach (Container container in stock.Client.Containers)
                {
                    if (container.ContainerHistorys.OrderByDescending(x => x.Date).FirstOrDefault().Stock == stock)
                    {// у каждого контейнера смотрим операции в текущем месяце
                        IQueryable ServicesDone = connect.FindObjects<ServiceDone>(x => x.Container == container &&
                        (x.StartDate.Year == DateTime.Now.AddMonths(-1).Year && x.StartDate.Month == DateTime.Now.AddMonths(-1).Month));
                        foreach (ServiceDone serviceDone in ServicesDone)
                        {
                            i++;
                            cell = worksheet.Cells["A" + (9 + i).ToString()];
                            cell.Value = i.ToString();

                            cell = worksheet.Cells["B" + (9 + i).ToString()];
                            cell.Value = container.ContainerNumber;

                            cell = worksheet.Cells["C" + (9 + i).ToString()];
                            cell.Value = container.ContainerType.Name;

                            string isLaden = "";
                            if (container.isLaden)
                                isLaden = "гр.";
                            else
                                isLaden = "не гр.";
                            cell = worksheet.Cells["D" + (9 + i).ToString()];
                            cell.Value = isLaden;

                            // ищем прибытие контейнера по его истории

                            cell = worksheet.Cells["E" + (9 + i).ToString()];
                            try { cell.Value = container.ContainerHistorys.OrderByDescending(x => x.Date).FirstOrDefault().Date.ToString("dd.MM.yyyy"); }
                            catch { }
                            //cell.Value = serviceDone.StartDate.ToShortDateString();

                            //string startReportDate = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day).ToShortDateString();
                            //if (serviceDone.StartDate.Month == DateTime.Now.Month)
                            //    startReportDate = serviceDone.StartDate.ToShortDateString();
                            //cell = worksheet.Cells["F" + (9 + i).ToString()];
                            //cell.Value = startReportDate;

                            cell = worksheet.Cells["F" + (9 + i).ToString()];
                            cell.Value = serviceDone.StartDate.ToString("dd.MM.yyyy");

                            cell = worksheet.Cells["G" + (9 + i).ToString()];
                            cell.Value = serviceDone.ValueTotalDate.ToString("dd.MM.yyyy");// ToShortDateString();

                            cell = worksheet.Cells["H" + (9 + i).ToString()];
                            cell.Value = serviceDone.ServiceType.Name;

                            cell = worksheet.Cells["I" + (9 + i).ToString()];
                            cell.Value = serviceDone.ServiceCount.ToString();

                            cell = worksheet.Cells["J" + (9 + i).ToString()];
                            cell.Value = serviceDone.UnitKind.Name;

                            cell = worksheet.Cells["K" + (9 + i).ToString()];
                            cell.Value = serviceDone.Value;

                            cell = worksheet.Cells["L" + (9 + i).ToString()];
                            if (serviceDone.DiscountActiveThroughDate != DateTime.MinValue)
                            {
                                cell.Value = (serviceDone.DiscountActiveThroughDate - serviceDone.StartDate).Days + 1;
                            }
                            else
                                cell.Value = "";

                            cell = worksheet.Cells["M" + (9 + i).ToString()];
                            cell.Value = serviceDone.Discount;

                            cell = worksheet.Cells["N" + (9 + i).ToString()];
                            cell.Value = serviceDone.ValueTotal;

                            if (i > 5)
                            {
                                worksheet.Rows.Insert(i + 9);
                                Range range = worksheet.Range[String.Format("A{0}:N{0}", (i + 10).ToString())];
                                Formatting rangeFormatting = range.BeginUpdateFormatting();
                                rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
                                rangeFormatting.Font.Name = "Tahoma";
                                rangeFormatting.Font.Size = 8;
                                rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                                rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                                range.EndUpdateFormatting(rangeFormatting);
                                o++;
                            }
                            try
                            {
                                cell = worksheet.Cells["O" + (20 + o).ToString()];
                                cell.Value = serviceDone.Currency.Code ?? "";
                            }
                            catch { }
                        }
                    }
                }
                string path = Path.GetTempPath() + @"\Отчет_по_стоку_" + stock.Name + "_за_" + reportmonth + "_" +
                    DateTime.Now.ToString("dd.MM.yyyy H:mm:ss").Replace(".", "_").Replace(" ", "_").Replace(":", "_").Replace("\\","_").Replace("/","_") + ".xlsx";
                workbook.SaveDocument(path, DocumentFormat.OpenXml);
                LogHelperClass.WriteText("Месячный отчет_CreateXlsReportByStock_finish _ " + res);
                res = path;
            }
            return res;
        }
    }
}
