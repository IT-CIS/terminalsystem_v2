﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.DocFlow;
using TerminalSystem2.SystemDir;
using TerminalSystemWindowsService;
using TerminalSystemWindowsService.Helpers;

namespace TerminalServices
{
    public class CheckRequestFinishDateController : NCron.CronJob
    {
        LogHelperClass LogHelperClass = new LogHelperClass();
        public override void Execute()
        {
            //GetAssemblyHelperClass GetAssemblyHelperClass = new GetAssemblyHelperClass();
            //assembly = GetAssemblyHelperClass.GetAssembly();
            LogHelperClass.WriteText("Проверка срока действия заявки_Execute");
            //Set s = new Set();
            //string sqlinstance = s.instance;
            //string port = s.port;
            //string user = s.user;
            //string pass = s.pss;
            //string database = s.db;
            //SettingsHelperClass xafApp = new SettingsHelperClass(sqlinstance, Convert.ToInt16(port), user, pass, database);
            //xafApp.SetXafAppConnection(sqlinstance, Convert.ToInt16(port),
            //    user, pass, database);
            //UnitOfWork unitOfWork = xafApp.UnitOfWork;
            UnitOfWork unitOfWork = SettingsHelperClass.unitOfWork;
            CheckRequestFinishDate(unitOfWork);
        }

        /// <summary>
        /// Функция закрытия заявок с истекшим сроком исполнения
        /// </summary>
        /// <param name="unitOfWork"></param>
        private void CheckRequestFinishDate(UnitOfWork unitOfWork)
        {
            LogHelperClass.WriteText("Проверка срока действия заявки_CheckRequestFinishDate_start");
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            try
            {
                foreach (RequestBase request in connect.FindObjects<RequestBase>(x => x.IsFinished == false))
                {
                    if (request.RequestFinishDate < DateTime.Now.Date)
                    {
                        request.IsFinished = true;
                        request.RequestStopDate = DateTime.Now.Date;
                        request.RequestStatus = TerminalSystem2.Enums.ERequestStatus.ЗакрытаПоВремени;
                        request.Save();
                        unitOfWork.CommitChanges();
                    }
                }
                LogHelperClass.WriteText("Проверка срока действия заявки_CheckRequestFinishDate_finish");
            }
            catch (Exception ex) { LogHelperClass.WriteErr(ex, "Ошибка в CheckRequestFinishDate"); }
        }
    }
}
