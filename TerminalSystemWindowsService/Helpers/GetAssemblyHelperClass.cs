﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSystemWindowsService.Helpers
{
    class GetAssemblyHelperClass
    {
        public Assembly GetAssembly()
        {
            Set s = new Set();
            LogHelperClass LogHelperClass = new LogHelperClass();
            string ModuleDllPath = "";
            ModuleDllPath = s.ModuleDllPath;
            Assembly assembly = null;
            try
            {
                assembly = Assembly.LoadFrom(ModuleDllPath);
            }
            catch (Exception ex)
            {
                LogHelperClass.WriteErr(ex, "Ошибка в получении assembly");
            }
            return assembly;
        }
    }
}
