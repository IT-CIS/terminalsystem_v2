﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;

namespace ServicesHelperClassLibrary
{
    public class SetServiceDoneTotalValueController : NCron.CronJob
    {
        public override void Execute()
        {
            string conn = DevExpress.Xpo.DB.PostgreSqlConnectionProvider.GetConnectionString("127.0.0.1", 5432, "postgres", "111", "TerminalSystem_publish");
            //WriteText(conn);
            XpoDefault.DataLayer = XpoDefault.GetDataLayer(conn, DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema);
            UnitOfWork unitOfWork = new UnitOfWork(XpoDefault.DataLayer);
            AddServiceValueByDayAction(unitOfWork);
        }

        /// <summary>
        /// добавить стоимость не закрытой услуги за день 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AddServiceValueByDayAction(UnitOfWork unitOfWork)
        {
            // перебираем все не закрытые услуги и добавляем стоимость за прошедший день (если прошедший день был не началом предоставления услуги - за первый день начисляется сразу)
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            foreach (ServiceDone serviceDone in connect.FindObjects<ServiceDone>(mc => mc.isDone == false && mc.StartDate < DateTime.Now.Date.AddDays(-1)))
            {
                // если сервис не работал несколько дней, то надо расчитать - сколько (сравниваем дату последнего расчета со вчерашней)
                TimeSpan diffDays = DateTime.Now.AddDays(-1).Date - serviceDone.ValueTotalDate;
                int servcount = diffDays.Days;

                if (serviceDone.DiscountActiveThroughDate != DateTime.MinValue)
                {
                    if (serviceDone.DiscountActiveThroughDate >= DateTime.Now.AddDays(-1).Date)
                    {
                        serviceDone.ValueTotal += servcount * serviceDone.Value * (100 - serviceDone.Discount) / 100;
                        serviceDone.ValueTotalDate = DateTime.Now.AddDays(-1).Date;
                        serviceDone.ServiceCount += servcount;
                    }
                    else
                    {
                        serviceDone.ValueTotal += serviceDone.Value * servcount;
                        serviceDone.ValueTotalDate = DateTime.Now.AddDays(-1).Date;
                        serviceDone.ServiceCount += servcount;
                    }
                }
                else
                {
                    
                    serviceDone.ValueTotal += serviceDone.Value * servcount;
                    serviceDone.ValueTotalDate = DateTime.Now.AddDays(-1).Date;
                    serviceDone.ServiceCount += servcount;
                }
                serviceDone.Save();
                unitOfWork.CommitChanges();
            }
        }
    }
}
