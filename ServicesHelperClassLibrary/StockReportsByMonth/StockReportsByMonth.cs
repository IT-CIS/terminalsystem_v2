﻿using DevExpress.Spreadsheet;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Tarification;
using TerminalSystem2.Terminals;

namespace ServicesHelperClassLibrary
{
    public class StockReportsByMonthController : NCron.CronJob
    {
        public override void Execute()
        {
            string conn = DevExpress.Xpo.DB.PostgreSqlConnectionProvider.GetConnectionString("127.0.0.1", 5432, "postgres", "111", "TerminalSystem_publish");
            //WriteText(conn);
            XpoDefault.DataLayer = XpoDefault.GetDataLayer(conn, DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema);
            UnitOfWork unitOfWork = new UnitOfWork(XpoDefault.DataLayer);
            CreateMailReportFromStockByMonthAction(unitOfWork);
        }

        /// <summary>
        /// сформировать отчет по стокам за месяц 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CreateMailReportFromStockByMonthAction(UnitOfWork unitOfWork)
        {
            // перебираем все стоки, смотрим у них за прошлый день движения контейнеров, если есть - отправляем отчет клиенту по движению
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            foreach (Stock stock in connect.FindObjects<Stock>(mc => mc.Oid != null))
            {
                if (connect.IsExist<ContainerHistory>(mc => mc.Stock == stock && mc.Date == DateTime.Now.AddDays(-1).Date))
                {
                   
                    // отсылаем клиенту
                    string titleClient = "Отчет по стоку";
                    string messbody = "";
                    string email = "";
                    string month = "";
                    switch (DateTime.Now.AddDays(-1).Month)
                    {
                        case 1:
                            month = "январь";
                            break;
                        case 2:
                            month = "февраль";
                            break;
                        case 3:
                            month = "март";
                            break;
                        case 4:
                            month = "апрель";
                            break;
                        case 5:
                            month = "май";
                            break;
                        case 6:
                            month = "июнь";
                            break;
                        case 7:
                            month = "июль";
                            break;
                        case 8:
                            month = "август";
                            break;
                        case 9:
                            month = "сентябрь";
                            break;
                        case 10:
                            month = "октябрь";
                            break;
                        case 11:
                            month = "ноябрь";
                            break;
                        case 12:
                            month = "декабрь";
                            break;
                        default:
                            month = DateTime.Now.AddDays(-1).Month.ToString();
                            break;
                    }
                    try
                    {
                        titleClient = String.Format("Отчет по стоку {0} за {1}", stock.Name, month);
                    }
                    catch { }
                    try { messbody = String.Format(@"Здравствуйте!
Высылаем Вам отчет по стоку {0} за {1}.", stock.Name, month); }
                    catch { }
                    try { email = stock.Client.Email; }
                    catch { }
                    // формируем Excel файл и получаем ссылку на него
                    string path = CreateXlsReportByStock(unitOfWork, stock, month);
                    MailMessageLogic.SendMailMessage(titleClient, messbody, path, email, "donotreply@it-cis.ru");
                }
            }
        }

        /// <summary>
        /// Создание файла отчета по движению в стоке за месяц
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="stock"></param>
        /// <returns>путь к файлу отчета</returns>
        public string CreateXlsReportByStock(UnitOfWork unitOfWork, Stock stock, string reportmonth)
        {
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            string res = "";

            Workbook workbook = new DevExpress.Spreadsheet.Workbook();
            if (stock.Client != null && stock.Terminal != null)
            {
                // Create an instance of a workbook.

                // Load a workbook from the file.

                //string templatePath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).
                //    LocalPath.Replace("WinAisogdIngeo.Module.DLL", @"\DocTemplates\1.xlsx");

                string templatePath = @"D:\work\!!CIS\TerminalSystem\PublicWeb2\bin\DocTemplates\ReportByMonth.xlsx";
                workbook.LoadDocument(templatePath, DocumentFormat.OpenXml);
                //workbook.LoadDocument("Documents\\Document.xlsx", DocumentFormat.OpenXml);

                // Access the first worksheet in the workbook.
                Worksheet worksheet = workbook.Worksheets[0];

                // Access the "E1" cell in the worksheet.
                // Застройщик
                Cell cell;// = worksheet.Cells["E1"];

                Client client = stock.Client;
                //Workbook workbook = new Workbook();
                //// Access the first worksheet in the workbook.
                //Worksheet worksheet = workbook.Worksheets[0];

                //// Access the "С2" cell in the worksheet.
                cell = worksheet.Cells["A3"];
                cell.Value = String.Format("Реестр № {0} от {1}", DateTime.Now.Month, DateTime.Now.Date.ToShortDateString());

                cell = worksheet.Cells["A7"];
                string cl = "";
                try { cl = String.Format("Заказчик: «{0}» сток «{1}»", client.Name, stock.Name); }
                catch { }
                cell.Value = cl;

                // перебираем контейнеры в стоке
                int i = 0;
                foreach (Container container in stock.Containers)
                {
                    // у каждого контейнера смотрим операции в предыдущем месяце
                    IQueryable ServicesDone = connect.FindObjects<ServiceDone>(x => x.Container == container &&
                    (x.isDone == false || x.FinishDate.Month == DateTime.Now.AddDays(-1).Month));
                    foreach (ServiceDone serviceDone in ServicesDone)
                    {
                        i++;
                        cell = worksheet.Cells["A" + (9 + i).ToString()];
                        cell.Value = i.ToString();

                        cell = worksheet.Cells["B" + (9 + i).ToString()];
                        cell.Value = container.ContainerNumber;

                        cell = worksheet.Cells["C" + (9 + i).ToString()];
                        cell.Value = container.ContainerType.Name;

                        string isLaden = "";
                        if (container.isLaden)
                            isLaden = "гр.";
                        else
                            isLaden = "не гр.";
                        cell = worksheet.Cells["D" + (9 + i).ToString()];
                        cell.Value = isLaden;

                        cell = worksheet.Cells["E" + (9 + i).ToString()];
                        cell.Value = serviceDone.StartDate.ToShortDateString();

                        string startReportDate = DateTime.Now.ToShortDateString();
                        if (serviceDone.StartDate.Month == DateTime.Now.Month)
                            startReportDate = serviceDone.StartDate.ToShortDateString();
                        cell = worksheet.Cells["F" + (9 + i).ToString()];
                        cell.Value = startReportDate;

                        cell = worksheet.Cells["G" + (9 + i).ToString()];
                        cell.Value = DateTime.Now.Date.AddDays(-1).ToShortDateString();

                        cell = worksheet.Cells["H" + (9 + i).ToString()];
                        cell.Value = serviceDone.ServiceType.Name;

                        cell = worksheet.Cells["I" + (9 + i).ToString()];
                        cell.Value = serviceDone.ServiceCount.ToString();

                        cell = worksheet.Cells["J" + (9 + i).ToString()];
                        cell.Value = serviceDone.UnitKind.Name;

                        cell = worksheet.Cells["K" + (9 + i).ToString()];
                        cell.Value = serviceDone.Value;

                        cell = worksheet.Cells["L" + (9 + i).ToString()];
                        if (serviceDone.DiscountActiveThroughDate != DateTime.MinValue)
                        {
                            cell.Value = (serviceDone.DiscountActiveThroughDate - serviceDone.StartDate).Days + 1;
                        }
                        else
                            cell.Value = "";

                        cell = worksheet.Cells["M" + (9 + i).ToString()];
                        cell.Value = serviceDone.Discount;

                        cell = worksheet.Cells["N" + (9 + i).ToString()];
                        cell.Value = serviceDone.ValueTotal;

                        if (i > 5)
                        {
                            worksheet.Rows.Insert(i + 9);
                            Range range = worksheet.Range[String.Format("A{0}:N{0}", (i + 10).ToString())];
                            Formatting rangeFormatting = range.BeginUpdateFormatting();
                            rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
                            range.EndUpdateFormatting(rangeFormatting);
                        }
                    }
                }
                string path = Path.GetTempPath() + @"\Отчет_по_стоку_" + stock.Name + "_" + "_за_" + reportmonth + "_" +
                    DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".xlsx";
                workbook.SaveDocument(path, DocumentFormat.OpenXml);
                res = path;
            }
            return res;
        }
    }
}
