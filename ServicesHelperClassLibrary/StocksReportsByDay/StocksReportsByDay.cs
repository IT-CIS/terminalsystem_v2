﻿using DevExpress.Spreadsheet;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Terminals;

namespace ServicesHelperClassLibrary
{
    public class ReportsFromStocksByDayController : NCron.CronJob
    {
        public override void Execute()
        {
            string conn = DevExpress.Xpo.DB.PostgreSqlConnectionProvider.GetConnectionString("127.0.0.1", 5432, "postgres", "111", "TerminalSystem_publish");
            //WriteText(conn);
            XpoDefault.DataLayer = XpoDefault.GetDataLayer(conn, DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema);
            UnitOfWork unitOfWork = new UnitOfWork(XpoDefault.DataLayer);
            CreateXLSReportFromStockByDayAction(unitOfWork);
        }

        /// <summary>
        /// сформировать отчет по стокам за день 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CreateXLSReportFromStockByDayAction(UnitOfWork unitOfWork)
        {
            // перебираем все стоки, смотрим у них за прошлый день движения контейнеров, если есть - отправляем отчет клиенту по движению
            Connect connect = Connect.FromUnitOfWork(unitOfWork);
            foreach (Stock stock in connect.FindObjects<Stock>(mc => mc.Oid != null))
            {
                if (connect.IsExist<ContainerHistory>(mc => mc.Stock == stock && mc.Date == DateTime.Now.AddDays(-1).Date))
                {
                    // формируем Excel файл и получаем ссылку на него
                    string path = CreateXlsReportByStock(unitOfWork, stock);
                    // отсылаем клиенту
                    string titleClient = "Отчет по стоку";
                    string messbody = "";
                    string email = "";
                    try { titleClient = String.Format("Отчет по стоку {0} за {1}", stock.Name, DateTime.Now.AddDays(-1).ToShortDateString()); }
                    catch { }
                    try { messbody = String.Format(@"Здравствуйте!
Высылаем Вам отчет по стоку {0} за {1}.", stock.Name, DateTime.Now.AddDays(-1).ToShortDateString()); }
                    catch { }
                    try { email = stock.Client.Email; }
                    catch { }
                    MailMessageLogic.SendMailMessage(titleClient, messbody, path, email, "donotreply@it-cis.ru");
                }
               
            }
            
        }
        /// <summary>
        /// Создание файла отчета по движению в стоке за один день
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="stock"></param>
        /// <returns>путь к файлу отчета</returns>
        public string CreateXlsReportByStock(UnitOfWork unitOfWork, Stock stock)
        {
            //IObjectSpace os = Application.CreateObjectSpace(); // Create IObjectSpace 
            // Получаем объект - движение контейнера
            //ContainerTraffic сontainerTraffic;
            //сontainerTraffic = os.FindObject<ContainerTraffic>(new BinaryOperator("Oid", containerTraffic_id));
            //UnitOfWork unitOfWork = (UnitOfWork)сontainerTraffic.Session;
            Connect connect = Connect.FromUnitOfWork(unitOfWork);

            string res = "";



            if (stock.Client != null && stock.Terminal != null)
            {
                Client client = stock.Client;
                Workbook workbook = new Workbook();
                // Access the first worksheet in the workbook.
                Worksheet worksheet = workbook.Worksheets[0];

                // Access the "С2" cell in the worksheet.
                Cell cell = worksheet.Cells["A2"];

                res = "Терминал";
                cell.Value = "Терминал";
                try
                {
                    res = res + " \"" + stock.Terminal.Name + "\"";
                }
                catch { }
                try
                {
                    res = res + " Сток \"" + stock.Name + "\"";
                }
                catch { }
                cell.Value = res;
                worksheet.MergeCells(worksheet.Range["A2:N2"]);
                cell.Font.Bold = true;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = worksheet.Cells["A3"];
                cell.Value = DateTime.Now.AddDays(-1).ToShortDateString();
                cell.Font.Bold = true;

                cell = worksheet.Cells["A4"];
                cell.Value = "приход контейнеров";
                cell.Font.Bold = true;
                worksheet.MergeCells(worksheet.Range["A4:G4"]);
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = worksheet.Cells["I4"];
                cell.Value = "уход контейнеров";
                cell.Font.Bold = true;
                worksheet.MergeCells(worksheet.Range["I4:N4"]);
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = worksheet.Cells["A6"];
                cell.Value = "число / date";
                cell.Font.Bold = true;

                cell = worksheet.Cells["I6"];
                cell.Value = "число / date";
                cell.Font.Bold = true;

                cell = worksheet.Cells["B6"];
                cell.Value = "время / time";
                cell.Font.Bold = true;

                cell = worksheet.Cells["J6"];
                cell.Value = "время / time";
                cell.Font.Bold = true;

                cell = worksheet.Cells["C6"];
                cell.Value = "тип / type";
                cell.Font.Bold = true;

                cell = worksheet.Cells["K6"];
                cell.Value = "тип / type";
                cell.Font.Bold = true;

                cell = worksheet.Cells["D6"];
                cell.Value = "№ контейнера / № container ";
                cell.Font.Bold = true;

                cell = worksheet.Cells["L6"];
                cell.Value = "№ контейнера / № container ";
                cell.Font.Bold = true;

                cell = worksheet.Cells["E6"];
                cell.Value = "водитель / driver";
                cell.Font.Bold = true;

                cell = worksheet.Cells["M6"];
                cell.Value = "водитель / driver";
                cell.Font.Bold = true;

                cell = worksheet.Cells["F6"];
                cell.Value = "№ а/м";
                cell.Font.Bold = true;

                cell = worksheet.Cells["N6"];
                cell.Value = "№ а/м";
                cell.Font.Bold = true;

                cell = worksheet.Cells["G6"];
                cell.Value = "Примечание / Note";
                cell.Font.Bold = true;

                cell = worksheet.Cells["O6"];
                cell.Value = "Примечание / Note";
                cell.Font.Bold = true;
                // перебираем все движения за сегодняшний день и в зависимости от типа (приход/уход создаем запись в таблице)
                ContainerHistory contrafficByMonth;
                int j = 0;
                string ldate = "", ltime = "", ltype = "", lnumber = "", ldriver = "", ltransport = "", lnote = "";
                var contrafficsArrived = connect.FindObjects<ContainerHistory>(mc => mc.Stock == stock && mc.Date == DateTime.Now.Date &&
                    mc.ContainerHistoryKind == TerminalSystem2.Enums.EContainerHistoryKind.Размещение);
                for (int i = 0; i < contrafficsArrived.ToList<ContainerHistory>().Count; i++)
                {
                    contrafficByMonth = contrafficsArrived.ToList<ContainerHistory>()[i];
                    ldate = "A";
                    ltime = "B";
                    ltype = "C";
                    lnumber = "D";
                    ldriver = "E";
                    ltransport = "F";
                    lnote = "G";
                    try
                    {
                        ldate += (i + 7).ToString();
                        cell = worksheet.Cells[ldate];
                        cell.Value = contrafficByMonth.Date.ToShortDateString();
                    }
                    catch { }
                    try
                    {
                        ltime += (i + 7).ToString();
                        cell = worksheet.Cells[ltime];
                        cell.Value = contrafficByMonth.Time;
                    }
                    catch { }
                    try
                    {
                        ltype += (i + 7).ToString();
                        cell = worksheet.Cells[ltype];
                        cell.Value = contrafficByMonth.Container.ContainerType.Name;
                    }
                    catch { }
                    try
                    {
                        lnumber += (i + 7).ToString();
                        cell = worksheet.Cells[lnumber];
                        cell.Value = contrafficByMonth.Container.ContainerNumber;
                    }
                    catch { }
                    try
                    {
                        ldriver += (i + 7).ToString();
                        cell = worksheet.Cells[ldriver];
                        cell.Value = contrafficByMonth.DriverName;
                    }
                    catch { }
                    try
                    {
                        ltransport += (i + 7).ToString();
                        cell = worksheet.Cells[ltransport];
                        cell.Value = contrafficByMonth.TransportNumber;
                    }
                    catch { }
                    try
                    {
                        lnote += (i + 7).ToString();
                        cell = worksheet.Cells[lnote];
                        cell.Value = contrafficByMonth.Notes;
                    }
                    catch { }
                    j = i;
                }
                var contrafficsDeparted = connect.FindObjects<ContainerHistory>(mc => mc.Stock == stock && mc.Date == DateTime.Now.Date &&
                    mc.ContainerHistoryKind == TerminalSystem2.Enums.EContainerHistoryKind.Выдача);
                for (int n = 0; n < contrafficsDeparted.ToList<ContainerHistory>().Count; n++)
                {
                    contrafficByMonth = contrafficsDeparted.ToList<ContainerHistory>()[n];

                    ldate = "I";
                    ltime = "J";
                    ltype = "K";
                    lnumber = "L";
                    ldriver = "M";
                    ltransport = "N";
                    lnote = "O";
                    try
                    {
                        ldate += (n + 7).ToString();
                        cell = worksheet.Cells[ldate];
                        cell.Value = contrafficByMonth.Date.ToShortDateString();
                    }
                    catch { }
                    try
                    {
                        ltime += (n + 7).ToString();
                        cell = worksheet.Cells[ltime];
                        cell.Value = contrafficByMonth.Time;
                    }
                    catch { }
                    try
                    {
                        ltype += (n + 7).ToString();
                        cell = worksheet.Cells[ltype];
                        cell.Value = contrafficByMonth.Container.ContainerType.Name;
                    }
                    catch { }
                    try
                    {
                        lnumber += (n + 7).ToString();
                        cell = worksheet.Cells[lnumber];
                        cell.Value = contrafficByMonth.Container.ContainerNumber;
                    }
                    catch { }
                    try
                    {
                        ldriver += (n + 7).ToString();
                        cell = worksheet.Cells[ldriver];
                        cell.Value = contrafficByMonth.DriverName;
                    }
                    catch { }
                    try
                    {
                        ltransport += (n + 7).ToString();
                        cell = worksheet.Cells[ltransport];
                        cell.Value = contrafficByMonth.TransportNumber;
                    }
                    catch { }
                    try
                    {
                        lnote += (n + 7).ToString();
                        cell = worksheet.Cells[lnote];
                        cell.Value = contrafficByMonth.Notes;
                    }
                    catch { }
                    if (j < n)
                        j = n;
                }
                j += 7;
                // Форматирование ячеек по приходу/уходу
                // Access the range of cells to be formatted.
                Range range = worksheet.Range["A6:O" + j.ToString()];

                // Begin updating of the range formatting. 
                Formatting rangeFormatting = range.BeginUpdateFormatting();

                // Specify font settings (font name, color, size and style).
                rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                rangeFormatting.Alignment.ShrinkToFit = true;
                // Specify text alignment in cells.
                rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                range.AutoFitColumns();
                // End updating of the range formatting.
                range.EndUpdateFormatting(rangeFormatting);

                // Добавляем информацию - сколько каких видов контейнеров осталось в стоке
                j += 3;
                //var containersInStockTypes = connect.FindObjects<ContainerType>(mc=> mc);

                //var conteinersInStock = connect.FindObjects<Container>(mc =>  mc.Stock == stock).GroupBy<ContainerType, "Name">;

                //XPQuery<Container> containers = Session.DefaultSession.Query<Container>();
                var containers = connect.FindObjects<Container>(mc => mc.Stock == stock && mc.onTerminal == true);
                // Select with Group By 
                var list = from c in containers
                               //where c.Stock == stock
                           group c by c.ContainerType into cc
                           where cc.Count() >= 1
                           select new { Title = cc.Key, Count = cc.Count() };
                foreach (var item in list)
                {
                    j++;
                    cell = worksheet.Cells["E" + j.ToString()];
                    cell.Value = item.Title.Name;
                    cell.Font.Bold = true;
                    cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    //cell.AutoFitColumns();

                    cell = worksheet.Cells["F" + j.ToString()];
                    cell.Value = item.Count.ToString();
                    cell.Font.Bold = true;
                    cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    //cell.AutoFitColumns();
                }

                // добавляем информацию по стоку
                j += 1;
                cell = worksheet.Cells["C" + j.ToString()];
                worksheet.MergeCells(worksheet.Range["C" + j.ToString() + ":E" + j.ToString()]);
                cell.Value = "сток на терминале";
                cell.Font.Bold = true;
                //cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                //cell.AutoFitColumns();

                j += 1;
                int d = j;
                cell = worksheet.Cells["C" + j.ToString()];
                cell.Value = "Тип";
                cell.Font.Bold = true;

                cell = worksheet.Cells["D" + j.ToString()];
                cell.Value = "номер";
                cell.Font.Bold = true;

                cell = worksheet.Cells["E" + j.ToString()];
                cell.Value = "состояние";
                cell.Font.Bold = true;

                foreach (Container cont in containers)
                {
                    j++;

                    cell = worksheet.Cells["B" + j.ToString()];
                    //ContainerHistory h = connect.FindFirstObject<ContainerHistory>(mc=> mc.Container == cont )
                    string dd = "";
                    try
                    {
                        dd = cont.ContainerHistorys.OrderByDescending(mc => mc.Date).First<ContainerHistory>().Date.ToShortDateString();
                    }
                    catch { }
                    cell.Value = dd;

                    cell = worksheet.Cells["C" + j.ToString()];
                    cell.Value = cont.ContainerType.Name;

                    cell = worksheet.Cells["D" + j.ToString()];
                    cell.Value = cont.ContainerNumber;

                    cell = worksheet.Cells["E" + j.ToString()];
                    string state = "не указано";
                    if (cont.State != null)
                        if (cont.State.Name != null && cont.State.Name != String.Empty)
                            state = cont.State.Name;
                    cell.Value = state;
                }

                // Форматирование ячеек по стоку
                range = worksheet.Range["B" + d.ToString() + ":E" + j.ToString()];
                rangeFormatting = range.BeginUpdateFormatting();

                // Specify font settings (font name, color, size and style).
                rangeFormatting.Borders.SetAllBorders(Color.Black, BorderLineStyle.Medium);
                //rangeFormatting.Alignment.ShrinkToFit = true;
                // Specify text alignment in cells.
                rangeFormatting.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                rangeFormatting.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                //range.AutoFitColumns();
                // End updating of the range formatting.
                range.EndUpdateFormatting(rangeFormatting);


                //Column column = worksheet.Columns["G"];
                //column.ColumnWidth = 10;
                Column column = worksheet.Columns["H"];
                column.ColumnWidth = 100;

                string path = Path.GetTempPath() + @"\Отчет_по_стоку_" + stock.Name + "_" +
                    DateTime.Now.ToString().Replace(".", "_").Replace(" ", "_").Replace(":", "_") + ".xlsx";

                workbook.SaveDocument(path, DocumentFormat.OpenXml);
                res = path;
                //try
                //{
                //    System.Diagnostics.Process.Start(path);
                //}
                //catch { }
            }
            return res;
        }

        private static object sync = new object();
        public static void WriteText(string message)
        {
            try
            {
                // Путь .\\Log
                string pathToLog = @"d:\TestSevice";//Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Message");
                if (!Directory.Exists(pathToLog))
                    Directory.CreateDirectory(pathToLog); // Создаем директорию, если нужно
                string filename = Path.Combine(pathToLog, string.Format("{0}_{1:dd.MM.yyy}.log",
                AppDomain.CurrentDomain.FriendlyName, DateTime.Now));
                string fullText = string.Format("[{0:dd.MM.yyy HH:mm:ss.fff}], объект: {1}\r\n",
                DateTime.Now, message);
                lock (sync)
                {
                    File.AppendAllText(filename, fullText, Encoding.GetEncoding("Windows-1251"));
                }
            }
            catch
            {
                // Перехватываем все и ничего не делаем
            }
        }
    }
}
