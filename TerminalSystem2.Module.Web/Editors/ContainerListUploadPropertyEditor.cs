﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.Drawing;
using DevExpress.Web;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.ExpressApp.Model;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Web;
using DevExpress.Xpo;
using TerminalSystem2.SystemDir;
using TerminalSystem2.Containers;
using TerminalSystem2.DocFlow;
using TerminalSystem2.Tarification;
using TerminalSystem2.Classifiers;
using TerminalSystem2.BaseClasses;
using TerminalSystem2.OrgStructure;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Persistent.BaseImpl;
using TerminalSystem2.Clients;
using TerminalSystem2.Module.Controllers.DocFlow;

namespace TerminalSystem2.Module.Web.Editors
{
    public interface IModelContainerListUploadPropertyEditor : IModelViewItem { }

    [ViewItem(typeof(IModelContainerListUploadPropertyEditor))]
    public partial class ContainerListUploadPropertyEditor : ViewItem
    {
        public ContainerListUploadPropertyEditor(IModelViewItem modelNode, Type objectType) : base(objectType, modelNode.Id) { }

        
        // Dennis: Creates a control for the Edit mode.
        protected override object CreateControlCore()
        {
            ASPxUploadControl result = new ASPxUploadControl();
            result.AdvancedModeSettings.EnableDragAndDrop = true;
            result.AdvancedModeSettings.EnableMultiSelect = false;
            result.AdvancedModeSettings.EnableFileList = true;

            result.ShowTextBox = true;
            result.ShowProgressPanel = true;
            result.Size = 30;
            result.AdvancedModeSettings.DropZoneText = "Добавить контейнеры списком";
            result.ShowUploadButton = true;
            result.AutoStartUpload = false;
            result.ValidationSettings.AllowedFileExtensions =
                new string[] { ".xls", ".xlsx", };

            //result.ShowAddRemoveButtons = true;
            //result..ClientSideEvents..CurrentFolderChanged = "function(s, e) { FileManagerCurrentFolderChanged(s, e); }";

            result.FileUploadComplete += new EventHandler<FileUploadCompleteEventArgs>(FilesUpload_Complete);
            result.ClientSideEvents.FilesUploadComplete = "function(s,e) { RaiseXafCallback(globalCallbackControl, '', '', '', false); }";
            return result;

            //if (View is DetailView)
            //{
            //    if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.Containers.Container")
            //    {

            //        Container container = View.CurrentObject as Container;

            //        UnitOfWork unitOfWork = (UnitOfWork)container.Session;
            //        Connect connect = Connect.FromUnitOfWork(unitOfWork);

            //        if (SecuritySystem.CurrentUser != null)
            //        {
            //            Employee currentEmpl = unitOfWork.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
            //            if (currentEmpl != null)
            //            {
                            
            //            }
            //        }
            //    }
            //}
            //return null;
        }
        void FilesUpload_Complete(object sender, FileUploadCompleteEventArgs e)
        {
            if (View is DetailView)
            {
                IObjectSpace os = View.ObjectSpace;
                os.CommitChanges();
                if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersPlacement")
                {
                    RequestContainersPlacement request = View.CurrentObject as RequestContainersPlacement;
                    request.Save();
                    os.CommitChanges();
                    ImportContainerListExcelController controller = new ImportContainerListExcelController();
                    controller.ImportContainerListInRequestFromExcel(e.UploadedFile.FileContent, os, request.Oid.ToString(), "placement");
                }
                if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.DocFlow.RequestContainersExtradition")
                {
                    RequestContainersExtradition request = View.CurrentObject as RequestContainersExtradition;
                    request.Save();
                    os.CommitChanges();
                    ImportContainerListExcelController controller = new ImportContainerListExcelController();
                    controller.ImportContainerListInRequestFromExcel(e.UploadedFile.FileContent, os, request.Oid.ToString(), "extradition");
                }
            }
        }
    }
}
