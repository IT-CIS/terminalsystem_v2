﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.Drawing;
using DevExpress.Web;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.ExpressApp.Model;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Web;

namespace TerminalSystem2.Module.Web.Editors
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    [PropertyEditor(typeof(Color), EditorAliases.ColorPropertyEditor, true)]
    public partial class CustomASPxColorPropertyEditor : ASPxPropertyEditor
    {
        public CustomASPxColorPropertyEditor(Type objectType, IModelMemberViewItem info) : base(objectType, info) { }
        // Dennis: Creates a control for the Edit mode.
        protected override WebControl CreateEditModeControlCore()
        {
            ASPxColorEdit result = new ASPxColorEdit();
            result.ID = "colorEdit";
            RenderHelper.SetupASPxWebControl(result);
            return result;
        }
        // Dennis: Creates a control for the View mode, because the default label does not meet our needs. We are almost fine with the ASPxColorEdit control, just need to make it readonly for the View mode.
        protected override WebControl CreateViewModeControlCore()
        {
            //base.CreateViewModeControlCore();
            ASPxColorEdit result = (ASPxColorEdit)CreateEditModeControlCore();
            result.DropDownButton.Visible = false;
            result.DisabledStyle.Border.BorderColor = Color.Transparent;
            result.AllowNull = true;
            ColorIndicatorStyle colorStyle = new ColorIndicatorStyle();
            //result.ClientSideEvents.ColorChanged = "function(s,e){s.GetInputElement().Value = '';}";
            //result.ColorIndicatorWidth = 100;
            result.NullText = "Укажите цвет контейнера";//.ToKnownColor();//.IsNamedColor = true;
            //result = true;
            result.Enabled = false;
            return result;
        }

        // Dennis: Since we provide a custom control for the View mode, we need to manually bind it to data.
        protected override void ReadViewModeValueCore()
        {
            base.ReadViewModeValueCore();
            if (InplaceViewModeEditor is ASPxColorEdit && PropertyValue is Color)
            {
                ((ASPxColorEdit)InplaceViewModeEditor).Color = (Color)PropertyValue;
            }
        }
        // Dennis: Sets common options for the underlying controls.
        protected override void SetupControl(WebControl control)
        {
            base.SetupControl(control);
            if (control is ASPxColorEdit)
            {
                ASPxColorEdit result = (ASPxColorEdit)control;
                result.AllowNull = true;
                result.AllowUserInput = false; //Dennis: The control can display a hex value at the moment, so it is better to prevent user input by default. Track <a href="https://www.devexpress.com/issue=S33627" data-mce-href="https://www.devexpress.com/issue=S33627">https://www.devexpress.com/issue=S33627</a&gt;.
                result.EnableCustomColors = true;
                if (ViewEditMode == DevExpress.ExpressApp.Editors.ViewEditMode.Edit)
                {
                    result.ColorChanged += new EventHandler(EditValueChangedHandler);
                }
            }
        }
        // Dennis: Unsubsribes from events and releases other resources.
        public override void BreakLinksToControl(bool unwireEventsOnly)
        {
            if (Editor != null)
            {
                ((ASPxColorEdit)Editor).ColorChanged -= new EventHandler(EditValueChangedHandler);
            }
            base.BreakLinksToControl(unwireEventsOnly);
        }
        // Dennis: This is required for the ImmediatePostData attribute operation.
        protected override void SetImmediatePostDataScript(string script)
        {
            Editor.ClientSideEvents.ColorChanged = script;
        }
        public new ASPxColorEdit Editor
        {
            get { return (ASPxColorEdit)base.Editor; }
        }
    }
}
