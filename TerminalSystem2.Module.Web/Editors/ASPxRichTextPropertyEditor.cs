﻿using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Web.Editors;
using DevExpress.Web.ASPxRichEdit;
using DevExpress.XtraRichEdit.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalSystem2.Module.Web.Editors
{
    [PropertyEditor(typeof(string), "RTF", false)]
    public class ASPxRichTextPropertyEditor : WebPropertyEditor, IComplexViewItem
    {
        private string documentId;
        private DevExpress.ExpressApp.IObjectSpace objectSpace;
        private DevExpress.ExpressApp.XafApplication application;
        public ASPxRichTextPropertyEditor(Type objectType, IModelMemberViewItem model)
                : base(objectType, model)
        {
            documentId = Guid.NewGuid().ToString();
        }

        public new ASPxRichEdit Editor
        {
            get { return (ASPxRichEdit)base.Editor; }
        }

        public void RaiseEditValueChanged()
        {
            EditValueChangedHandler(Editor, new EventArgs());
        }

        protected override System.Web.UI.WebControls.WebControl CreateEditModeControlCore()
        {
            ASPxRichEdit edit = new ASPxRichEdit();
            edit.Theme = "Default";
            edit.AutoSaveTimeout = new TimeSpan(0, 1, 0);
            edit.CreateDefaultRibbonTabs(true);
            //edit.RibbonTabs[0].Visible = false;
            edit.RibbonTabs[4].Visible = false;
            edit.ShowConfirmOnLosingChanges = true;
            edit.Height = 900;
            //edit.ID = "ASPxRichEdit";
            //edit.Enabled = true;
            edit.WorkDirectory = @"C:\Temp";//Path.GetTempPath();

            //RtfDocumentExporterOptions options = new RtfDocumentExporterOptions();
            //options.
            edit.ReadOnly = false;
            return edit;
        }

        protected override void ReadEditModeValueCore()
        {
            if (this.PropertyValue != null)
            {
                using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(PropertyValue as string)))
                {
                    Editor.Open(documentId, DevExpress.XtraRichEdit.DocumentFormat.Rtf, () => ms);
                }
            }
        }

        protected override object GetControlValueCore()
        {
            using (var ms = new MemoryStream())
            {
                if(Editor != null)
                { Editor.SaveCopy(ms, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                    using (var sr = new StreamReader(ms))
                    {
                        ms.Seek(0, SeekOrigin.Begin);
                        return sr.ReadToEnd();
                    }
                }
                else
                    return null;
            }
        }

        protected override void ReadValueCore()
        {
            base.ReadValueCore();
        }

        protected override void WriteValueCore()
        {
            base.WriteValueCore();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                objectSpace.Committing -= objectSpace_Committing;
            base.Dispose(disposing);
        }

        public void Setup(DevExpress.ExpressApp.IObjectSpace objectSpace, DevExpress.ExpressApp.XafApplication application)
        {
            this.objectSpace = objectSpace;
            this.application = application;

            objectSpace.Committing += objectSpace_Committing;
        }

        void objectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.Control != null)
            {
                EditValueChangedHandler(Editor, new EventArgs());
            }
        }

    }

}

