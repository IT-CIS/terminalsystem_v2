﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Editors;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web.ASPxRichEdit;
using DevExpress.Web.Office;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace TerminalSystem2.Module.Web.Editors
{
    [PropertyEditor(typeof(string), "RTF", false)]
    public class ASPxRichTextContentPropertyEditor : ASPxPropertyEditor //WebPropertyEditor
    {
        public ASPxRichTextContentPropertyEditor(Type objectType, IModelMemberViewItem model)
            : base(objectType, model)
        {
            documentId = Guid.NewGuid().ToString();
        }
        private string documentId;
        private ASPxRichEdit richEditUserControlCore = null;

        protected override WebControl CreateEditModeControlCore()
        {
            return CreateRichEditControl();
        }

        protected override WebControl CreateViewModeControlCore()
        {
            return CreateRichEditControl();
        }

        public ASPxRichEdit Editor
        {
            get
            {
                return richEditUserControlCore;
            }
        }

        //public new ASPxRichEdit Editor
        //{
        //    get { return (ASPxRichEdit)base.Editor; }
        //}


        private WebControl CreateRichEditControl()
        {
            ASPxRichEdit result = new ASPxRichEdit();
            result.ID = "aspxRichEdit";
            result.Enabled = true;
            //result.Width = "";
            //result.AutoSaveTimeout = new TimeSpan(0, 1, 0);
            result.ShowConfirmOnLosingChanges = true;
            
            //result.RibbonTabs[4].Visible = false;
            result.Theme = "Default";
            //RenderHelper.SetupASPxWebControl(result);
            return result;
        }

        protected override void ReadEditModeValueCore()
        {
            if (this.PropertyValue != null)
            {
                using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(PropertyValue as string)))
                {
                    Editor.Open(documentId, DevExpress.XtraRichEdit.DocumentFormat.Rtf, () => ms);
                }
            }
        }

        protected override object GetControlValueCore()
        {
            using (var ms = new MemoryStream())
            {
                Editor.SaveCopy(ms, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                using (var sr = new StreamReader(ms))
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    return sr.ReadToEnd();
                }
            }
        }
    }
}