﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Model;
using DevExpress.Web;

namespace TerminalSystem2.Module.Web.Editors
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    [PropertyEditor(typeof(DateTime), false)]
    public partial class CustomDateTimePropertyEditor : ASPxDateTimePropertyEditor
    {
        public CustomDateTimePropertyEditor(Type objectType, IModelMemberViewItem info) :
        base(objectType, info)
        { }
        protected override void SetupControl(WebControl control)
        {
            base.SetupControl(control);
            if (ViewEditMode == ViewEditMode.Edit)
            {
                
                ASPxDateEdit dateEdit = (ASPxDateEdit)control;
                //dateEdit.CalendarProperties. = true;
                dateEdit.TimeSectionProperties.Visible = true;
                dateEdit.UseMaskBehavior = true;
            }
        }
    }
}
