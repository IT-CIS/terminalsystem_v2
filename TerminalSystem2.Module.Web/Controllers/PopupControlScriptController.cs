﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Web.Templates;
using DevExpress.ExpressApp.Web;
using TerminalSystem2.Containers;

namespace TerminalSystem2.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PopupControlScriptController : ViewController<DetailView>
    {
        public PopupControlScriptController() : base()
        {
            //TargetObjectType = typeof(ContainerPlacementInfo);
            TargetViewNesting = Nesting.Nested;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            ((IXafPopupWindowControlContainer)WebWindow.CurrentRequestPage).XafPopupWindowControl.CustomizePopupControl += XafPopupWindowControl_CustomizePopupControl;
        }
        private void XafPopupWindowControl_CustomizePopupControl(object sender, DevExpress.ExpressApp.Web.Controls.CustomizePopupControlEventArgs e)
        {
            string existingScript = e.PopupControl.ClientSideEvents.CloseUp;
            ICallbackManagerHolder holder = WebWindow.CurrentRequestPage as ICallbackManagerHolder;
            if (holder != null)
            {
                string redirectScript = holder.CallbackManager.GetScript();
                string closingScript = e.PopupControl.ClientSideEvents.CloseUp;
                e.PopupControl.ClientSideEvents.CloseUp = string.Format("function(s, e) {{ ({0}); {1}; }}", closingScript, redirectScript);
            }
        }
    }
}
