﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using TerminalSystem2.Clients;
using TerminalSystem2.Containers;
using TerminalSystem2.OrgStructure;
using TerminalSystem2.SystemDir;

namespace TerminalSystem2.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class HideUploadViewController : ViewController<DetailView>
    {
        public HideUploadViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        private AppearanceController appearanceController;
        protected override void OnActivated()
        {
            base.OnActivated();
            appearanceController = Frame.GetController<AppearanceController>();
            if (appearanceController != null)
            {
                appearanceController.CustomApplyAppearance += appearanceController_CustomApplyAppearance;
            }
        }
        protected override void OnDeactivated()
        {
            if (appearanceController != null)
            {
                appearanceController.CustomApplyAppearance -= appearanceController_CustomApplyAppearance;
            }
            base.OnDeactivated();
        }
        void appearanceController_CustomApplyAppearance(object sender, ApplyAppearanceEventArgs e)
        {
            if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.Containers.Container")
            {

                Container container = View.CurrentObject as Container;

                UnitOfWork unitOfWork = (UnitOfWork)container.Session;
                Connect connect = Connect.FromUnitOfWork(unitOfWork);

                if (SecuritySystem.CurrentUser != null)
                {
                    ClientEmployee currentEmpl = unitOfWork.FindObject<ClientEmployee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                    if (currentEmpl != null)
                    {
                        ViewItem propertyEditor = ((DetailView)View).FindItem("ContainerFileUploadPropertyEditor");
                        if (e.ItemName == "ContainerFileUploadPropertyEditor")
                        {
                            e.AppearanceObject.Visibility = ViewItemVisibility.Hide;
                        }
                    }
                }
            }
        }

        //private void InitNullText(ViewItem propertyEditor)
        //{
        //    //if (propertyEditor.Control.ViewEditMode == DevExpress.ExpressApp.Editors.ViewEditMode.Edit)
        //    //{
        //    //    ((ViewItem)propertyEditor..Editor).NullText = CaptionHelper.NullValueText;
        //    //}
        //}
        //private void propertyEditor_ControlCreated(object sender, EventArgs e)
        //{
        //    InitNullText((ViewItem)sender);
        //}
        //protected override void OnActivated()
        //{
        //    base.OnActivated();
        //    ViewItem propertyEditor = ((DetailView)View).FindItem("Anniversary");
        //    if (propertyEditor != null)
        //    {
        //        if (propertyEditor.Control != null)
        //        {
        //            InitNullText(propertyEditor);
        //        }
        //        else
        //        {
        //            propertyEditor.ControlCreated += new EventHandler<EventArgs>(propertyEditor_ControlCreated);
        //        }
        //    }
        //}
        //protected override void OnDeactivated()
        //{
        //    base.OnDeactivated();
        //    ViewItem propertyEditor = ((DetailView)View).FindItem("Anniversary");
        //    if (propertyEditor != null)
        //    {
        //        propertyEditor.ControlCreated -= new EventHandler<EventArgs>(propertyEditor_ControlCreated);
        //    }
        //}
    }
}
