﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using TerminalSystem2.Module.Web.Editors;

namespace TerminalSystem2.Module.Controllers.Helpers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ASPxRichTextSaveActionExecuteController : ViewController
    {
        public ASPxRichTextSaveActionExecuteController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            ModificationsController mc = this.Frame.GetController<ModificationsController>();
            if (mc != null)
            {
                mc.SaveAction.Executing += SaveAction_Executing;
                mc.SaveAndCloseAction.Executing += SaveAction_Executing;
                mc.SaveAndNewAction.Executing += SaveAction_Executing;
            }
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            ModificationsController mc = this.Frame.GetController<ModificationsController>();
            mc.SaveAction.Executing -= SaveAction_Executing;
            mc.SaveAndCloseAction.Executing -= SaveAction_Executing;
            mc.SaveAndNewAction.Executing -= SaveAction_Executing;
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        void SaveAction_Executing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ASPxRichTextPropertyEditor editor = ((DetailView)this.View).FindItem("RtfText") as ASPxRichTextPropertyEditor;
            if (editor != null)
                editor.RaiseEditValueChanged();
        }

    }
}
